//
//  CoverFlowViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "CoverFlowViewController.h"

#import "DatabaseManager.h"

#define GESTURE

@interface CoverFlowViewController ()

@end

@implementation CoverFlowViewController

@synthesize carousel = _carousel;
@synthesize itemList = _itemList;

@synthesize tapDelegate = _tapDelegate;
@synthesize gestureDelegate = _gestureDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.carousel = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)initialize
{
    self.carousel.type = iCarouselTypeCoverFlow2;

#ifdef GESTURE
    // ジェスチャーセット
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(longPressRecognizer:)];
    gr.delegate = self;
    [self.carousel addGestureRecognizer:gr];
#endif
}

- (UIImage *)getPreviewFromContent:(NSUInteger)index
{
    ContentData* contentRecord = [self getContentData:index];
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

- (ContentData *)getContentData:(NSUInteger)index
{
    return [self.itemList objectAtIndex:index];
}

#pragma mark - iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [self.itemList count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{    
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 300.0f)];
        ((UIImageView *)view).image = [self getPreviewFromContent:index];
        view.contentMode = UIViewContentModeCenter;
    }
    
    return view;
}

#pragma mark - iCarousel delegate methods

/**
 * didSelectItemAtIndex
 * アイテムタップ時
 */
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"Did select item at index %d", index);
    
    [self.tapDelegate notifyDidSelectItemAtIndex:index];
}

/**
 * carouselWillBeginDragging
 * ドラッグ開始時
 */
- (void)carouselWillBeginDragging:(iCarousel *)carousel
{
    NSLog(@"carouselWillBeginDragging");
}

/**
 * carouselDidEndDragging
 * ドラッグ終了時(アニメーション中)
 */
- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate
{
    NSLog(@"carouselDidEndDragging");
    NSLog(@"itemIndex = %d", [carousel currentItemIndex]);
}

/**
 * carouselDidEndDecelerating
 * ドラッグ終了時(アニメーション終了時)
 */
- (void)carouselDidEndDecelerating:(iCarousel *)carousel
{
    NSLog(@"carouselDidEndDecelerating");
    NSLog(@"itemIndex = %d", [carousel currentItemIndex]);
}

#pragma mark - Gesture Recogniser

#ifdef GESTURE

- (void)longPressRecognizer:(UIGestureRecognizer *)recognizer
{
    switch (recognizer.state)
    {
        // 長押開始
        case UIGestureRecognizerStateBegan:
        {
            NSLog(@"UIGestureRecognizerStateBegan");
            
            CGPoint point = [recognizer locationInView:self.carousel];
            UIView *hitView = [self.carousel hitTest:point withEvent:nil];
            
            if ([hitView isKindOfClass:[UIImageView class]]) {
                NSLog(@"index = %d", [self.carousel currentItemIndex]);
                NSInteger index = [self.carousel currentItemIndex];
                
                [self.gestureDelegate notifyGestureStateBegan:recognizer index:index];
            }
        }
            break;
            
        // ドラッグ移動
        case UIGestureRecognizerStateChanged:
        {
            NSLog(@"UIGestureRecognizerStateChanged");
            
            [self.gestureDelegate notifyGestureStateChanged:recognizer];
        }
            break;
            
        // 長押終了
        case UIGestureRecognizerStateEnded:
        {
            NSLog(@"UIGestureRecognizerStateEnded");
            
            [self.gestureDelegate notifyGestureStateEnded:recognizer];
        }
            break;
            
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateCancelled:
        {
            NSLog(@"UIGestureRecognizerStatePossible");
        }
            break;
            
        default:
        case UIGestureRecognizerStateFailed:
            NSLog(@"UIGestureRecognizerStateFailed");
            break;
    }
}

#endif

@end

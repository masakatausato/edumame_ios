//
//  ProductIndexData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductIndexData : NSObject

@property (nonatomic) NSInteger contentId;
@property (nonatomic) NSInteger productId;

- (id)init;

- (id)initWithContentId:(NSInteger)contentId productId:(NSInteger)productId;

@end

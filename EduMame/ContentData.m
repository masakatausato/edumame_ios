//
//  ContentData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ContentData.h"

@implementation ContentData

- (id)init
{
    self = [super init];
    if(self){
        self.contentsId = -1;
        self.title = @"";
        self.description = @"";
        self.keyword = @"";
        self.provider = @"";
        self.author = @"";
        self.revision = 0;
        self.level = 0;
        self.attribute = 0;
        self.targetFlag = 0;
        self.contentType = 0;
        self.printFlag = 0;
        self.useTerm = 0;
        self.validTermStart = @"";
        self.validTermEnd = @"";
        self.validTermFlag = NO;
        self.invalidFlag = NO;
        self.content = @"";
        self.preview = @"";
        self.created = @"";
        self.modified = @"";
        self.categoryId = -1;
        self.subcategoryId = -1;        
    }
    return self;
}

- (id)initWithContentsId:(NSInteger)contentsId title:(NSString*)title description:(NSString*)description keyword:(NSString*)keyword provider:(NSString*)provider author:(NSString*)author revision:(NSInteger)revision level:(NSInteger)level attribute:(NSInteger)attribute targetFlag:(NSInteger)targetFlag contentType:(NSInteger)contentType printFlag:(NSInteger)printFlag useTerm:(NSInteger)useTerm validTermStart:(NSString*)validTermStart validTermEnd:(NSString*)validTermEnd validTermFlag:(BOOL)validTermFlag invalidFlag:(BOOL)invalidFlag content:(NSString*)content preview:(NSString*)preview created:(NSString*)created modified:(NSString*)modified categoryId:(NSInteger)categoryId subcategoryId:(NSInteger)subcategoryId
{
    self = [super init];
    if(self){
        self.contentsId = contentsId;
        self.title = title;
        self.description = description;
        self.keyword = keyword;
        self.provider = provider;
        self.author = author;
        self.revision = revision;
        self.level = level;
        self.attribute = attribute;
        self.targetFlag = targetFlag;
        self.contentType = contentType;
        self.printFlag = printFlag;
        self.useTerm = useTerm;
        self.validTermStart = validTermStart;
        self.validTermEnd = validTermEnd;
        self.validTermFlag = validTermFlag;
        self.invalidFlag = invalidFlag;
        self.content = content;
        self.preview = preview;
        self.created = created;
        self.modified = modified;
        self.categoryId = categoryId;
        self.subcategoryId = subcategoryId;
    }
    return self;
}

@end

//
//  EMPlayerHelper.h
//  EMPreview2
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EMPlayerDefine.h"

@interface EMPlayerHelper : NSObject

// 浮動小数の補間
+ (Float32)lerpFloat:(Float32)f1 f2:(Float32)f2 rate:(Float32)rate;

// 矩形の補間
+ (CGRect)lerpRect:(CGRect)r1 r2:(CGRect)r2 rate:(Float32)rate;

// JSONエフェクトの変換
+ (NSInteger)convertJsonEffect:(NSString*)jsonEffect;

// JSON真偽値の変換
+ (BOOL)convertJsonBoolean:(NSString*)jsonBoolean;

// JSON時間の変換
+ (Float32)convertJsonTime:(NSString*)jsonTime;

// JSON矩形の変換
+ (CGRect)convertJsonRect:(NSDictionary*)jsonRect;

@end

//
//  ManualProductsViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/19.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ManualProductsViewController.h"

#import "CreateManager.h"

#import "ManualViewController.h"
#import "ManualSearchConditionData.h"

@interface ManualProductsViewController ()

@end

@implementation ManualProductsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initialize];
    [self initializeUIBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    // プロダクトデータ取得
    // 検索条件作成
    ManualSearchConditionData *data = [[CreateManager sharedCreateManager] searchConditionData];
    NSString* option = @"where ";
        
    NSString* categoryOption = @"genre = ";
    NSString* levelOption = @"level = ";
    NSString* termOption = @"term = ";
        
    if ([data.category length] > 0)
    {
        categoryOption = [categoryOption stringByAppendingString:data.category];
        if ([data.level length] > 0) {
            categoryOption = [categoryOption stringByAppendingString:@" and "];
            levelOption = [levelOption stringByAppendingString:data.level];
            if ([data.term length] > 0) {
                levelOption = [levelOption stringByAppendingString:@" and "];
                termOption = [termOption stringByAppendingString:data.term];
                    
                option = [option stringByAppendingString:[[categoryOption stringByAppendingString: levelOption] stringByAppendingString: termOption]];
            } else {
                option = [option stringByAppendingString:[categoryOption stringByAppendingString: levelOption]];
            }
        } else if ([data.term length] > 0) {
            categoryOption = [categoryOption stringByAppendingString:@" and "];
            termOption = [termOption stringByAppendingString:data.term];
            
            option = [option stringByAppendingString:[categoryOption stringByAppendingString: termOption]];
        } else {
            option = [option stringByAppendingString:categoryOption];
        }
    }
    else if ([data.level length] > 0)
    {
        levelOption = [levelOption stringByAppendingString:data.level];
        if ([data.term length] > 0) {
            levelOption = [levelOption stringByAppendingString:@" and "];
            termOption = [termOption stringByAppendingString:data.term];
                
            option = [option stringByAppendingString:[levelOption stringByAppendingString: termOption]];
        } else {
            option = [option stringByAppendingString:levelOption];
        }
    }
    else if ([data.term length] > 0)
    {
        termOption = [termOption stringByAppendingString:data.term];
        option = [option stringByAppendingString:termOption];
    }
    else
    {
        option = nil;
    }
    
    // TODO:オプション
    [self getProductDataFromDatabase:/*option*/nil];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.isProduct = YES;
    self.gridViewCtrl.itemList = [self.productData mutableCopy];
    self.gridViewCtrl.tapDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    [self.topContainerView addSubview:self.gridViewCtrl.view];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"プリント準備"];
}

/**
 * getProductDataFromDatabase
 * DBからプロダクトデータ取得
 * @param selectOptions
 */
- (void)getProductDataFromDatabase:(NSString *)selectOptions
{
    self.productData = [[NSMutableArray array] init];
    NSArray* productRecords = [DatabaseManager selectProductData:selectOptions];
    for (NSInteger i = 0; i < [productRecords count]; i++)
    {
        ProductData* productRecord = [productRecords objectAtIndex:i];
        [self.productData addObject:productRecord];
    }
}

/**
 * setContentDataFromDatabase
 * DBからコンテンツデータ取得
 * @param selectOptions
 */
- (void)setContentDataFromDatabase:(NSInteger)index
{
    self.contentData = [[NSMutableArray array] init];
    
    ProductData* productData = [self.productData objectAtIndex:index];
    
    NSString* option = [NSString stringWithFormat:@"where product_id = %d", productData.productsId];
    
    NSArray* productIndexRecords = [DatabaseManager selectProductIndexData:nil];
    for (NSInteger i = 0; i < [productIndexRecords count]; i++)
    {
        ProductIndexData* productIndexRecord = [productIndexRecords objectAtIndex:i];

        option = [NSString stringWithFormat:@"where contents_id = %d", productIndexRecord.contentId];
        
        NSArray* contentRecords = [DatabaseManager selectContentData:option];
        for (NSInteger i = 0; i < [contentRecords count]; i++)
        {
            ContentData* contentRecord = [contentRecords objectAtIndex:i];
            [self.contentData addObject:contentRecord];
        }
    }
}

- (NSMutableArray *)getPreviewList
{
    NSMutableArray* previewList = [NSMutableArray array];
    
    NSInteger cnt = [self.productData count];
    for (int i = 0; i < cnt; i++) {
        [previewList addObject:[CreateManager getPreviewFromProduct:[self getProductData:i] index:i]];
    }
    
    return previewList;
}

- (ProductData *)getProductData:(NSUInteger)index
{
    return [self.productData objectAtIndex:index];
}

#pragma mark - Cover Flow View & Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{    
    [self setContentDataFromDatabase:index];
    [self performSegueWithIdentifier:@"ManualSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if ([identifier isEqualToString:@"ManualSegue"])
    {
        ManualViewController* nextViewController = [segue destinationViewController];
        nextViewController.contentData = self.contentData;
    }
}

@end

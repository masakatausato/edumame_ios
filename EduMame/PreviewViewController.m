//
//  PreViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/10/29.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PreviewViewController.h"
#import "PreviewGridViewCell.h"
#import "PreviewData.h"

@interface PreviewViewController ()

@end

@implementation PreviewViewController

@synthesize gridView;
@synthesize previewListData;
@synthesize previewDetailViewCtrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(105, 100, 600, 900)];
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.gridView.autoresizesSubviews = YES;
    self.gridView.delegate = self;
    self.gridView.dataSource = self;
    [self.view addSubview:gridView];
    
    self.previewListData = [PreviewData getPreviewData];
    
    [self.gridView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (NSUInteger)numberOfItemsInGridView:(AQGridView *)aGridView
{
    return [self.previewListData count];
}

- (AQGridViewCell *)gridView:(AQGridView *)aGridView cellForItemAtIndex:(NSUInteger) index
{
    static NSString *PlainCellIdentifier = @"PlainCellIdentifier";
    PreviewGridViewCell *cell = (PreviewGridViewCell *)[aGridView dequeueReusableCellWithIdentifier:PlainCellIdentifier];
    if (cell == nil) {
        cell = [[PreviewGridViewCell alloc] initWithFrame: CGRectMake(0, 0, 158, 234)
                                        reuseIdentifier: PlainCellIdentifier];
    }
    
    PreviewData *previewData = [self.previewListData objectAtIndex:index];
    [cell.previewImageView setImage:previewData.image];
    
    return cell;
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aGridView
{
    // 画像より大きく設定する
    return (CGSizeMake(180, 255));
}

- (void)gridView:(AQGridView *)aGridView didSelectItemAtIndex:(NSUInteger) index
{
    // 選択カーソルは表示しない
    [self.gridView deselectItemAtIndex:index animated:NO];

	PreviewGridViewCell *cell = (PreviewGridViewCell *)[self.gridView cellForItemAtIndex: index];
    if (cell != nil) {
        UIImage *selectedImageView = cell.previewImageView.image;
    
        // 詳細View作成
        previewDetailViewCtrl = [[PreviewDetailViewController alloc] initWithNibName:@"PreviewDetailView" bundle:nil];
        previewDetailViewCtrl._image = selectedImageView;
        previewDetailViewCtrl._previewIndex = index;
        previewDetailViewCtrl._previewListData = self.previewListData;
    
        float w = previewDetailViewCtrl.view.frame.size.width;
        float h = previewDetailViewCtrl.view.frame.size.height;
        previewDetailViewCtrl.view.frame = CGRectMake(0, 0, w, h);
        CGPoint center = CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2);
        previewDetailViewCtrl.view.center = center;
    
        [self.view addSubview:previewDetailViewCtrl.view];
    }
}


- (IBAction)dismiss:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

@end

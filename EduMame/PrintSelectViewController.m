//
//  PrintSelectViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PrintSelectViewController.h"
#import "PrintGridViewCell.h"
#import "CreateManager.h"

@interface PrintSelectViewController ()

@end

@implementation PrintSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initialize];
    [self initializeUIBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)initialize
{
    // checkedData初期化
    // Hidden:YES チェックなし
    // Hidden:NO チェックあり
    self.checkHiddenData = [[NSMutableArray array] init];
    for (int i = 0; i < [self.contentData count]; i++) {
        [self.checkHiddenData addObject:[NSNumber numberWithBool:NO]];
    }
    
    // GridView
    self.gridViewCtrl = [[PrintGridViewController alloc] init];
    self.gridViewCtrl.itemList = [self.contentData mutableCopy];
    self.gridViewCtrl.checkHiddenData = self.checkHiddenData;
    self.gridViewCtrl.tapDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    [self.topContainerView addSubview:self.gridViewCtrl.view];

    // TODO:長いのでリファクタリング対象
    // ラベル
    NSString* tapDate = [[CreateManager sharedCreateManager] tapDate];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *formatterDate = [inputFormatter dateFromString:tapDate];
    
    // 日付を取得
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:
                                   NSYearCalendarUnit |
                                   NSMonthCalendarUnit |
                                   NSDayCalendarUnit
                                              fromDate:formatterDate];
    NSString *labelString = [NSString stringWithFormat:@"%d年%d月%d日", dateComps.year, dateComps.month, dateComps.day];
    
    // 日付のラベルを設定
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    label.text = labelString;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont fontWithName:@"AppleGothic" size:20];
    
    [self.view addSubview:label];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"プリント内容"];
}

#pragma mark - My Book Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
    NSNumber *currentHidden = [self.checkHiddenData objectAtIndex:index];
    BOOL bCurrentHidden = [currentHidden boolValue];
    [self.checkHiddenData replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:!bCurrentHidden]];
    
     // チェックデータ更新
    self.gridViewCtrl.checkHiddenData = self.checkHiddenData;
}

#pragma mark - Air Print Methods

- (IBAction)onPrint:(id)sender
{
    self.printContentData = [[NSMutableArray array] init];
    int checkCnt = [self.checkHiddenData count];
    for (int i = 0; i < checkCnt; i++) {
        NSNumber *currentHidden = [self.checkHiddenData objectAtIndex:i];
        BOOL bCurrentHidden = [currentHidden boolValue];
        
        if (bCurrentHidden == NO) {
            // チェックがはいったコンテンツデータを格納
            ContentData *contentData = [self.contentData objectAtIndex:i];
            [self.printContentData addObject:contentData];
        }
    }
    
    //TODO:印刷処理
}

@end

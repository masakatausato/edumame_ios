//
//  SearchModalPanel.h
//  EduMame
//
//  Created by gclue_mita on 12/10/31.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@interface ReminderModalPanel : UATitledModalPanel <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *reminderView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
- (void)removeForKeyboardNotifications;

@end

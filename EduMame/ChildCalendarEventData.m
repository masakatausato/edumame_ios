//
//  ChildCalendarEventData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildCalendarEventData.h"

@implementation ChildCalendarEventData

- (id)init
{
    self = [super init];
    if(self){
        self.childCalendarEventId = -1;
        self.date = @"";
        self.evaluation = 0;
        self.childId = -1;
    }
    return self;
}

- (id)initWithChildCalendarEventId:(NSInteger)childCalendarEventId date:(NSString*)date evaluation:(NSInteger)evaluation childId:(NSInteger)childId
{
    self = [super init];
    if(self){
        self.childCalendarEventId = childCalendarEventId;
        self.date = date;
        self.evaluation = evaluation;
        self.childId = childId;
    }
    return self;
}

@end

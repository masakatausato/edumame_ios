//
//  SubcategoryData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SubcategoryData.h"

@implementation SubcategoryData

- (id)init
{
    self = [super init];
    if(self){
        self.subcategoryId = -1;
        self.name = @"";
        self.categoryId = -1;
    }
    return self;
}

- (id)initWithSubcategoryId:(NSInteger)subcategoryId name:(NSString*)name categoryId:(NSInteger)categoryId
{
    self = [super init];
    if(self){
        self.subcategoryId = subcategoryId;
        self.name = name;
        self.categoryId = categoryId;
    }
    return self;
}

@end

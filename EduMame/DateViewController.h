//
//  DateViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateView.h"

@protocol DateViewDelegate <NSObject>

- (void)notifyAddSubView:(DateView *)dateView;
- (void)notifyTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface DateViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) id<DateViewDelegate> delegate;
@property (strong, nonatomic) NSDate *date;

- (void)reloadData;

@end

//
//  PrintGridViewCell.m
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PrintGridViewCell.h"

@implementation PrintGridViewCell

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];
    if (self)
    {
        UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        self.checkedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CHECKED_WIDTH, CHECKED_HEIGHT)];
        [self.checkedImageView setImage:[UIImage imageNamed:@"checked.png"]];
        
        [mainView addSubview:self.imageView];
        [mainView addSubview:self.checkedImageView];
        
        [self.contentView addSubview:mainView];
    }
    
    return self;
}

@end

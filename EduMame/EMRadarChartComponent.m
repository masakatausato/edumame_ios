//
//  EMRadarChartComponent.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/07.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "EMRadarChartComponent.h"

/**
 * レーダーチャートコンポーネント
 */
@implementation EMRadarChartComponent

/**
 * 初期化を行う
 * @param itemNumber レーダーチャートの項目数
 * @param scale レーダーチャートのスケール
 * @return id
 */
- (id)initWithItemNumber:(NSInteger)itemNumber scale:(CGFloat)scale
{
    self = [super init];
    if(self){
        _scale = scale;
        _itemArray = [NSMutableArray array];
        for(NSInteger i = 0; i < itemNumber; i++)
        {
            NSMutableDictionary* item = [NSMutableDictionary dictionary];
            [item setObject:@"" forKey:@"label"];
            [item setObject:[NSNumber numberWithFloat:0] forKey:@"value"];
            [item setObject:[NSNumber numberWithFloat:0] forKey:@"radian"];
            [item setObject:[NSNumber valueWithCGPoint:CGPointMake(0, 0)] forKey:@"point"];
            [item setObject:[NSNumber valueWithCGPoint:CGPointMake(0, 0)] forKey:@"maxPoint"];
            [_itemArray addObject:item];
        }
    }
    return self;
}

/**
 * オブジェクトの生成と初期化を行う
 * @param itemNumber レーダーチャートの項目数
 * @param scale レーダーチャートのスケール
 * @return id
 */
+ (id)radarChartWithItemNumber:(NSInteger)itemNumber scale:(CGFloat)scale
{
    return [[EMRadarChartComponent alloc] initWithItemNumber:itemNumber scale:scale];
}

/**
 * 設定をもとに角度、座標、最大座標を再計算する
 */
- (void)calculate
{
    CGFloat stride = (2.0 * M_PI) / [_itemArray count];
    for(NSInteger i = 0; i < [_itemArray count]; i++)
    {
        NSMutableDictionary* item = [_itemArray objectAtIndex:i];
        
        CGFloat radian = stride * i;
        CGFloat r1 = [self itemValueAtIndex:i] * _scale;
        CGFloat x1 = sinf(radian) * r1;
        CGFloat y1 = cosf(radian) * r1;
        CGFloat r2 = 1.0 * _scale;
        CGFloat x2 = sinf(radian) * r2;
        CGFloat y2 = cosf(radian) * r2;
        
        [item setObject:[NSNumber numberWithFloat:radian] forKey:@"radian"];
        [item setObject:[NSNumber valueWithCGPoint:CGPointMake(x1, y1)] forKey:@"point"];
        [item setObject:[NSNumber valueWithCGPoint:CGPointMake(x2, y2)] forKey:@"maxPoint"];
    }
}

/**
 * 項目のラベルを設定する
 * @param lavel ラベル
 * @param itemIndex 設定する項目のインデックス
 */
- (void)setItemLabel:(NSString*)label itemIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    [item setObject:label forKey:@"label"];
}

/**
 * 項目のラベルを設定する
 * @param labelArray 項目ごとのラベル配列
 */
- (void)setItemLabelByArray:(NSArray*)labelArray
{
    for(NSInteger i = 0; i < [_itemArray count]; i++)
    {
        NSMutableDictionary* item = [_itemArray objectAtIndex:i];
        [item setObject:[labelArray objectAtIndex:i] forKey:@"label"];
    }
}

/**
 * 項目の値を設定する
 * @param value 値（0.0 〜 1.0）
 * @param itemIndex 設定する項目のインデックス
 */
- (void)setItemValue:(CGFloat)value itemIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    [item setObject:[NSNumber numberWithFloat:value] forKey:@"value"];
}

/**
 * 項目の値を設定する
 * @param valueArray 項目ごとの値配列
 */
- (void)setItemValueByArray:(NSArray*)valueArray
{
    for(NSInteger i = 0; i < [_itemArray count]; i++)
    {
        NSMutableDictionary* item = [_itemArray objectAtIndex:i];
        [item setObject:[valueArray objectAtIndex:i] forKey:@"value"];
    }
}

/**
 * 項目のラベルを取得する
 * @param itemIndex 取得する項目のインデックス
 * @return ラベル
 */
- (NSString*)itemLabelAtIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    return [item objectForKey:@"label"];
}

/**
 * 項目の値を取得する
 * @param itemIndex 取得する項目のインデックス
 * @return 値
 */
- (CGFloat)itemValueAtIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    return [(NSNumber*)[item objectForKey:@"value"] floatValue];
}

/**
 * 項目の角度を取得する
 * @param itemIndex 取得する項目のインデックス
 * @return 角度
 */
- (CGFloat)itemRadianAtIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    return [(NSNumber*)[item objectForKey:@"radian"] floatValue];
}

/**
 * 項目の座標を取得する
 * @param itemIndex 取得する項目のインデックス
 * @return 座標
 */
- (CGPoint)itemPointAtIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    return [(NSNumber*)[item objectForKey:@"point"] CGPointValue];
}

/**
 * 項目の最大座標を取得する
 * @param itemIndex 取得する項目のインデックス
 * @return 最大座標
 */
- (CGPoint)itemMaxPointAtIndex:(NSInteger)itemIndex
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    return [(NSNumber*)[item objectForKey:@"maxPoint"] CGPointValue];
}

/**
 * 項目の指定値における座標を取得する
 * @param itemIndex 取得する項目のインデックス
 * @param forValue 値
 * @return 座標
 */
- (CGPoint)itemPointAtIndex:(NSInteger)itemIndex forValue:(CGFloat)value
{
    NSMutableDictionary* item = [_itemArray objectAtIndex:itemIndex];
    CGFloat radian = [(NSNumber*)[item objectForKey:@"radian"] floatValue];
    CGFloat r = value * _scale;
    CGFloat x = sinf(radian) * r;
    CGFloat y = cosf(radian) * r;
    return CGPointMake(x, y);
}


@end

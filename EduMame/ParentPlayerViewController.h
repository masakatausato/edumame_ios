//
//  ParentPlayerViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/04.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMPlayerResponder.h"

@interface ParentPlayerViewController : UIViewController <EMPlayerResponder, UIGestureRecognizerDelegate>

// 読み込みコンテンツ名
@property NSString* contentName;

@end

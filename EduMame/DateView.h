//
//  DateView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateView : UIView

// 日付表示ラベル
@property (strong, nonatomic) UILabel *dateLabel;

// 日付
@property (strong, nonatomic) NSDate *date;

- (void)setBackColor:(UIColor *)backgroundColor;

@end

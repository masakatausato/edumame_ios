//
//  PasswordPopOverView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PasswordViewDelegate <NSObject>

- (void)notifyLogin:(NSString*)password;
- (void)notifyCancel;

@end

@interface PasswordPopOverView : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) id<PasswordViewDelegate> delegate;

- (IBAction)selectLogin:(id)sender;
- (IBAction)selectCancel:(id)sender;

@end

//
//  PreviewGridViewCell.h
//  EduMame
//
//  Created by gclue_mita on 12/10/30.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "AQGridViewCell.h"

@interface PreviewGridViewCell : AQGridViewCell

@property (nonatomic, retain) UIImageView *previewImageView;

@end

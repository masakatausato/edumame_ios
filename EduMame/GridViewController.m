//
//  GridViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "GridViewController.h"
#import "GridViewCell.h"

#import "DatabaseManager.h"

#define GESTURE

@interface GridViewController ()

@end

@implementation GridViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isProduct = false;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.aqGridView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)initialize
{
//    self.aqGridView = [[AQGridView alloc] initWithFrame:CGRectMake(0, 10, 980, 530)];
    self.aqGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.aqGridView.autoresizesSubviews = YES;
//    self.aqGridView.delegate = self;
//    self.aqGridView.dataSource = self;
//    [self.view addSubview:self.aqGridView];
    
#ifdef GESTURE
    // ジェスチャーセット
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(longPressRecognizer:)];
    gr.delegate = self;
    [self.aqGridView addGestureRecognizer:gr];
#endif

    [self.aqGridView reloadData];
}

//TODO:リファクタリング
- (UIImage *)getPreview:(NSUInteger)index
{
    if (self.isProduct) {
        ProductData* productData = (ProductData*)[self getData:index];
        NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* productPath = [NSString stringWithFormat:@"%@/product/%@", documentDirectory, productData.product];
        NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", productPath];
        
        return [UIImage imageWithContentsOfFile:previewPath];
    } else {
        ContentData* contentData = (ContentData*)[self getData:index];
        NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentData.content];
        NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
        
        return [UIImage imageWithContentsOfFile:previewPath];
    }
}

- (id)getData:(NSUInteger)index
{
    return [self.itemList objectAtIndex:index];
}

#pragma mark - Grid View Methods

- (NSUInteger)numberOfItemsInGridView:(AQGridView *)aqGridView
{
    return self.itemList.count;
}

- (AQGridViewCell *)gridView:(AQGridView *)aqGridView cellForItemAtIndex:(NSUInteger)index
{
    static NSString *GridViewCellIdentifier = @"GridViewCellIdentifier";
    GridViewCell *cell = (GridViewCell *)[aqGridView dequeueReusableCellWithIdentifier:GridViewCellIdentifier];
    if (cell == nil) {
        cell = [[GridViewCell alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)
                                   reuseIdentifier:GridViewCellIdentifier];
    }
    
    UIImage *image = [self getPreview:index];    
    [cell.imageView setImage:image];
    
    return cell;
}

- (void)gridView:(AQGridView *)aqGridView didSelectItemAtIndex:(NSUInteger)index
{
    [self.aqGridView deselectItemAtIndex:index animated:NO];
    
    [self.tapDelegate notifyDidSelectItemAtIndex:index];
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aqGridView
{
    // 画像より大きく設定する
    return (CGSizeMake(WIDTH+10, HEIGHT+20));
}

#pragma mark - Gesture Recogniser

#ifdef GESTURE

- (void)longPressRecognizer:(UIGestureRecognizer *)recognizer
{
    switch (recognizer.state)
    {
        // 長押開始
        case UIGestureRecognizerStateBegan:
        {
            NSLog(@"UIGestureRecognizerStateBegan");
            
            NSUInteger indexForItemAtPoint = [self.aqGridView indexForItemAtPoint: [recognizer locationInView:self.aqGridView]];
            GridViewCell *cell = (GridViewCell *)[self.aqGridView cellForItemAtIndex:indexForItemAtPoint];
            NSInteger indexForCell = [self.aqGridView indexForCell:cell];
            
            [self.gestureDelegate notifyGestureStateBegan:recognizer index:indexForCell];
        }
            break;
            
        // ドラッグ移動
        case UIGestureRecognizerStateChanged:
        {
            NSLog(@"UIGestureRecognizerStateChanged");
            
            [self.gestureDelegate notifyGestureStateChanged:recognizer];
        }
            break;
            
        // 長押終了
        case UIGestureRecognizerStateEnded:
        {
            NSLog(@"UIGestureRecognizerStateEnded");
            
            [self.gestureDelegate notifyGestureStateEnded:recognizer];
        }
            break;
            
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateCancelled:
        {
            NSLog(@"UIGestureRecognizerStatePossible");
        }
            break;
            
        default:
        case UIGestureRecognizerStateFailed:
            NSLog(@"UIGestureRecognizerStateFailed");
            break;
    }
}

#endif

@end

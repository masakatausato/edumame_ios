//
//  ProfilePopOverView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SettingProfilePopOverView.h"

@interface SettingProfilePopOverView ()

@end

@implementation SettingProfilePopOverView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    // 登録情報取得
    [self getRegisterInfo];
    
    // データ表示
    [self.userId setText:_parentData.userId];
    [self.parentName setText:_parentData.username];
    [self.parentMail setText:_parentData.email];
    [self.childName setText:_childrenData.username];
    [self.childBirthday setText:_childrenData.birthday];
    self.childrenData.gender == 0 ? (self.childGender.selectedSegmentIndex = 0) : (self.childGender.selectedSegmentIndex = 1);
    
    // スクロールビューセット
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.scrollView.contentSize = CGSizeMake(300.0f, 600.0f);
}

/**
 * getRegisterInfo
 * 登録情報取得
 */
- (void)getRegisterInfo
{
    [self getParentData];
    [self getChildrenData];
}

/**
 * getParentData
 * 親情報取得
 */
- (void)getParentData
{
    // DBからユーザ情報取得
    NSArray* parentRecords = [DatabaseManager selectParentData:nil];
    int parentCnt = [parentRecords count];
    if (parentCnt <= 0) {
        return;
    } else {
        for (NSInteger i = 0; i < parentCnt; i++) {
            _parentData = [parentRecords objectAtIndex:i];
        }
    }
}

/**
 * getChildrenData
 * 子情報取得
 */
- (void)getChildrenData
{
    // DBからユーザ情報取得
    NSArray* childRecords = [DatabaseManager selectChildrenData:nil];
    int childCnt = [childRecords count];
    if (childCnt <= 0) {
        return;
    } else {
        for (NSInteger i = 0; i < childCnt; i++) {
            _childrenData = [childRecords objectAtIndex:i];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

// viewが消えた後に、データを保存
- (void)viewDidDisappear:(BOOL)animated
{
    [self updateParentData];
    [self setupChild];
}

- (void)updateParentData
{
    NSMutableArray* parentRecords = [NSMutableArray array];
    
    // 親テーブル更新
    self.parentData.userId = self.userId.text;
    self.parentData.username = self.parentName.text;
    self.parentData.email = self.parentMail.text;
    
    // レコードを格納
    [parentRecords addObject:self.parentData];
    
    // データベースに親テーブル格納
    [DatabaseManager updateParentData:parentRecords];
}

- (void)setupChild
{
    NSMutableArray* childRecords = [NSMutableArray array];
    
    // 子テーブル作成
    self.childrenData.username = self.childName.text;
    self.childrenData.birthday = self.childBirthday.text;
    self.childGender.selectedSegmentIndex == 0 ? (self.childrenData.gender = 0) : (self.childrenData.gender = 1);
    
    // レコードを格納
    [childRecords addObject:self.childrenData];
    
    // データベースに親テーブル格納
    [DatabaseManager updateChildrenData:childRecords];
}

@end

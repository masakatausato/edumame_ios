//
//  CreateManager.m
//  EduMame
//
//  Created by gclue_mita on 13/02/20.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "CreateManager.h"
#import "ContentManager.h"

/**
 * CreateManager
 * プリント作成管理シングルトンクラス
 */
@implementation CreateManager

#pragma mark - Singleton

// シングルトンインスタンス
static CreateManager* sharedInstance = nil;

+ (id)sharedCreateManager
{    
	@synchronized(self) {
		if (!sharedInstance) {
			sharedInstance = [[self alloc] init];
		}
	}
	return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
	@synchronized(self) {
		if (!sharedInstance) {
			sharedInstance = [super allocWithZone:zone];
		}
	}
	return sharedInstance;
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

#pragma mark - Database Methods

/**
 * setupWorkbookRecord
 * WorkbookDataをDB新規登録
 * @param date
 */
+ (void)setupWorkbookRecord:(NSDate *)date
{
    // WorkbookDataレコード追加
    NSInteger timestamp = [date timeIntervalSince1970];
    WorkbookData * workbookData = [[WorkbookData alloc] init];
    workbookData.plannedAt = timestamp;
    workbookData.name = [CreateManager getFormatDate:date dateFormat:@"yyyy-MM-dd"];
    workbookData.childId = 1;
    
    NSMutableArray *newWorkbookRecord = [NSMutableArray array];
    [newWorkbookRecord addObject:workbookData];
    [DatabaseManager insertWorkbookData:newWorkbookRecord lastInsertRowId:nil];
}

/**
 * setupWorkbookIndexRecord
 * WorkbookIndexDataをDB新規登録
 * @param workbookData
 * @param contentData
 */
+ (void)setupWorkbookIndexRecord:(WorkbookData *)workbookData contentData:(ContentData *)contentData
{
    // WorkbookIndexDataレコード追加
    WorkbookIndexData *workbookIndexData = [[WorkbookIndexData alloc] init];
    workbookIndexData.workbookId = workbookData.workbookId;
    workbookIndexData.contentId = contentData.contentsId;
    
    NSMutableArray *newWorkbookIndexRecord = [NSMutableArray array];
    [newWorkbookIndexRecord addObject:workbookIndexData];
    [DatabaseManager insertWorkbookIndexData:newWorkbookIndexRecord lastInsertRowId:nil];
}

/**
 * setupContentDataRecord
 */
+ (void)setupContentDataRecord:(ContentData *)contentData
{}

/**
 * updateWorkbookRecord
 */
+ (WorkbookData *)updateWorkbookRecord:(NSDate *)date
{    
    // Workbookレコード検索
    // 同じnameのレコードがなければ新規追加
    NSString* option = @"where name =";
    option = [option stringByAppendingFormat:@" '%@'", [CreateManager getFormatDate:date dateFormat:@"yyyy-MM-dd"]];
    
    NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
    
    WorkbookData *workbookData = nil;
    if ([targetWorkbookRecord count] > 0)
    {
        workbookData = [targetWorkbookRecord objectAtIndex:0];
    }
    else
    {
        [CreateManager setupWorkbookRecord:date];
        
        // 新規追加時は再帰でidを更新する
        workbookData = [self updateWorkbookRecord:date];
    }
    
    return workbookData;
}

/**
 * updateWorkbookIndexRecord
 */
+ (void)updateWorkbookIndexRecord:(WorkbookData *)workbookData contentData:(ContentData *)contentData
{    
    // WorkbookIndexレコード検索
    // 同じworkbook_idのWorkbookIndexレコードを取得
    NSString *option = [NSString stringWithFormat:@"where workbook_id = %d", workbookData.workbookId];
    
    NSArray *targetWorkbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
    NSUInteger workbookIndexNum = [targetWorkbookIndexRecord count];

    // 同じコンテンツIDが含まれていたら追加しない
    for (int i = 0; i < workbookIndexNum; i++) {
        WorkbookIndexData *workbookIndexData = [targetWorkbookIndexRecord objectAtIndex:i];
        if (workbookIndexData.contentId == contentData.contentsId) {
            return;
        }
    }

    [self setupWorkbookIndexRecord:workbookData contentData:contentData];
}

/**
 * updateWorkbookIndexRecord
 */
+ (void)updateWorkbookIndexRecord:(WorkbookData *)workbookData contentArray:(NSMutableArray *)contentArray
{
    NSUInteger contentNum = [contentArray count];
    
    for (int i = 0; i < contentNum; i++) {
        ContentData *contentData = [contentArray objectAtIndex:i];
        [CreateManager updateWorkbookIndexRecord:workbookData contentData:contentData];
    }
}

/**
 * updateContentDataRecord
 */
+ (void)updateContentDataRecord
{}

#pragma mark - Date Methods

/**
 * getNextDate
 * 現在日から指定した日数の日付を取得する
 * @param day
 * @return nextDay
 */
+ (NSDate*)getNextDate:(NSInteger)day
{
    NSDate *now = [NSDate date];
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:day];
    NSDate *nextDay = [cal dateByAddingComponents:comps toDate:now options:0];
    
    return nextDay;
}

/**
 * getFormatDate
 * 指定したフォーマットに変更した日付取得
 * @param date
 * @param dateFormat (yyyy-MM-dd HH:mm Z)
 * @return dateString
 */
+ (NSString*)getFormatDate:(NSDate*)date dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

#pragma mark - Preview Methods

/**
 * getPreviewFromContent
 * コンテンツデータからプレビュー画像を取得
 * @param contentData
 * @param index
 */
+ (UIImage *)getPreviewFromContent:(ContentData *)contentData index:(NSUInteger)index
{
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentData.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

/**
 * getPreviewFromProduct
 * プロダクトデータからプレビュー画像を取得
 * @param contentData
 * @param index
 */
+ (UIImage *)getPreviewFromProduct:(ProductData *)productData index:(NSUInteger)index
{
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* productPath = [NSString stringWithFormat:@"%@/product/%@", documentDirectory, productData.product];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", productPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

/**
 * getContentArrayFromJSON
 * JSONから画像データを取得し配列に格納し返す
 */
+ (NSMutableArray *)getContentArrayFromJSON:(NSString*)contentName
{
    NSError* error = nil;
    
    // コンテンツのJSONを読み込む
    NSString* contentPath = [[ContentManager contentsPath] stringByAppendingPathComponent:contentName];
    NSString* jsonPath = [NSString stringWithFormat:@"%@/content.json", contentPath];
    NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
    if (jsonData == nil) return nil;
    
    // JSONを解析
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if(error != nil) return nil;
    
    NSMutableArray *contentArray = [NSMutableArray array];
    [CreateManager makeContentArray:contentPath contentJSON:[json objectForKey:@"intro"] answerContent:NO contentArray:contentArray];
    [CreateManager makeContentArray:contentPath contentJSON:[json objectForKey:@"answer"] answerContent:YES contentArray:contentArray];
    
    return contentArray;
}

+ (void)makeContentArray:(NSString*)path contentJSON:(NSArray*)contentJSON answerContent:(BOOL)answerContent contentArray:(NSMutableArray *)contentArray
{
    NSError* error = nil;
    
    // JSONデータを解析する
    for(NSInteger i = 0; i < [contentJSON count]; i++)
    {
        NSDictionary* content = [contentJSON objectAtIndex:i];
        
        NSString* resourceType = [content objectForKey:@"resourceType"];
        NSString* resourceFileName = [content objectForKey:@"resourceFile"];
        
        // リソースの読み込み
        //if([[self.resourceList allKeys] containsObject:resourceFileName] == NO)
        {
            NSString* contentPath = [NSString stringWithFormat:@"%@/%@", path, resourceFileName];
            NSLog(@"%@",contentPath);
            
            if([resourceType isEqualToString:@"Image"])
            {
                // 画像リソース
                NSData* imageData = [NSData dataWithContentsOfFile:contentPath];
                if(imageData == nil) continue;
                UIImage* image = [UIImage imageWithData:imageData];
                if(image == nil) continue;
                [contentArray addObject:image];
            }
#if 0 // 画像データのみ取得。
            else if([resourceType isEqualToString:@"Sound"])
            {
                // 音声リソース
                NSData* soundData = [NSData dataWithContentsOfFile:contentPath];
                if(soundData == nil) continue;
                AVAudioPlayer* audioPlayer = [[AVAudioPlayer alloc] initWithData:soundData error:&error];
                if(audioPlayer == nil) continue;
                [contentArray setObject:audioPlayer forKey:resourceFileName];
            }
            else if([resourceType isEqualToString:@"Movie"])
            {
                // 動画リソース
                NSURL* videoURL = [NSURL fileURLWithPath:contentPath];
                AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:videoURL];
                if(playerItem == nil) continue;
                [contentArray setObject:playerItem forKey:resourceFileName];
            }
#endif
        }
    }
}


@end

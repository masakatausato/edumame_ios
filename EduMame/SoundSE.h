//
//  SoundSE.h
//  EduMame
//
//  Created by GClue on 13/02/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>

@interface SoundSE : NSObject
{
    SystemSoundID sound;
}

- (id)initSE:(NSString *)name type:(NSString *)oftype;
- (void)playSE;
- (void)playStop;

@end

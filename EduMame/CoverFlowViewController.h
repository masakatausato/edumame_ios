//
//  CoverFlowViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@protocol CoverFlowViewDelegate <NSObject>

- (void)notifyDidSelectItemAtIndex:(NSInteger)index;

@end

@protocol CoverFlowViewGestureDelegate <NSObject>

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer index:(NSInteger)index;
- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer;
- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer;

@end

@interface CoverFlowViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) NSMutableArray *itemList;

@property (strong, nonatomic) id<CoverFlowViewDelegate> tapDelegate;
@property (strong, nonatomic) id<CoverFlowViewGestureDelegate> gestureDelegate;

@end

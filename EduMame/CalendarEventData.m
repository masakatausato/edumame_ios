//
//  CalendarEventData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "CalendarEventData.h"

@implementation CalendarEventData

- (id)init
{
    self = [super init];
    if(self){
        self.calendarEventId = -1;
        self.eventTypeCode = 0;
        self.plannedData = @"";
        self.summary = @"";
        self.description = @"";
        self.childId = -1;
    }
    return self;
}

- (id)initWithCalendarEventId:(NSInteger)calendarEventId eventTypeCode:(NSInteger)eventTypeCode plannedData:(NSString*)plannedData summary:(NSString*)summary description:(NSString*)description childId:(NSInteger)childId
{
    self = [super init];
    if(self){
        self.calendarEventId = calendarEventId;
        self.eventTypeCode = eventTypeCode;
        self.plannedData = plannedData;
        self.summary = summary;
        self.description = description;
        self.childId = childId;
    }
    return self;
}

@end

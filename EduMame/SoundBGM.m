//
//  SoundBGM.m
//  EduMame
//
//  Created by GClue on 13/02/04.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SoundBGM.h"

AVAudioPlayer *audio;

@implementation SoundBGM

/*
 bgm...再生する音楽のファイル名
type...再生する音楽ファイルの拡張子(mp3、wav等）
loop...ループ回数。（-1： 無限、0：１回、1：２回...)
*/
- (id) initBGM:(NSString *)bgm type:(NSString *)ofType loop:(int)loop {
        
    NSString* path = [[NSBundle mainBundle] pathForResource:bgm ofType:ofType];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    audio.numberOfLoops = loop;
    
    // オーディオプレイヤーのオーディオレベル計測のオン/オフ状態を決定する。
//    [_audio setMeteringEnabled:YES];

    return self;
}

- (void) playBGM {
    if(audio.playing) {
        return;
    }
    [audio play];
}

- (void) pauseBGM {
    if(audio == nil) return;
    if(audio.playing){
        [audio pause];
    }
}

- (void) stopBGM {
    if(audio == nil) return;
    [audio stop];
}

- (float) getVolume {
    return audio.volume;
}

// 音量は0.0〜1.0で指定する
- (void) setVolume:(double)volume {
    audio.volume = volume;
}

@end

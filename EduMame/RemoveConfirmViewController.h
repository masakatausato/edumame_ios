//
//  RemoveConfirmViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/14.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemoveConfirmViewController : UIViewController

- (IBAction)onRemove:(id)sender;
- (IBAction)onCancel:(id)sender;

@end

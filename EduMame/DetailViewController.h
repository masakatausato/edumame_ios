//
//  WorkbookDetailViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RemoveConfirmViewController;

@protocol DetailViewDelegate <NSObject>

- (void)notifyRemoveContent:(NSInteger)index;

@end

@interface DetailViewController : UIViewController

@property (nonatomic) NSUInteger index;
@property (strong, nonatomic) NSMutableArray *itemList;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) UIImage *image;

@property (weak, nonatomic) IBOutlet UIButton *print;
@property (weak, nonatomic) IBOutlet UIButton *remove;

@property BOOL isPrintHidden;
@property BOOL isRemoveHidden;

@property (strong, nonatomic) id<DetailViewDelegate> delegate;

@property (strong, nonatomic) RemoveConfirmViewController *removeConfirmView;

- (IBAction)onPrint:(id)sender;
- (IBAction)onRemove:(id)sender;

@end

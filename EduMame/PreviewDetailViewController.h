//
//  PreviewDetailViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/11/06.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewDetailViewController : UIViewController
{
    NSUInteger _previewIndex;
    NSArray *_previewListData;
    UIImage *_image;
    UIImageView *_detailImageView;
    
    UISwipeGestureRecognizer *_swipeLeftGestureRecognizer;
    UISwipeGestureRecognizer *_swipeRightGestureRecognizer;
}

@property (nonatomic, assign) NSUInteger _previewIndex;
@property (nonatomic, copy) NSArray *_previewListData;
@property (nonatomic, copy) UIImage *_image;
@property (nonatomic, strong) IBOutlet UIImageView *_detailImageView;
@property (nonatomic, readonly) UISwipeGestureRecognizer *_swipeLeftGestureRecognizer;
@property (nonatomic, readonly) UISwipeGestureRecognizer *_swipeRightGestureRecognizer;

@end

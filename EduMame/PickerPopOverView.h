//
//  YearMonthPopOverView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/25.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickerPopOverViewDelegate <NSObject>

- (void)notifyChanged:(NSString*)year month:(NSString*)month;

@end

@interface PickerPopOverView : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) id<PickerPopOverViewDelegate> delegate;

@end

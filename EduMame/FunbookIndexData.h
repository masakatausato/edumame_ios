//
//  FunbookIndexData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FunbookIndexData : NSObject

@property (nonatomic) NSInteger funbookIndexId;
@property (nonatomic) NSString* readAt;
@property (nonatomic) BOOL canceled;
@property (nonatomic) NSInteger funbookId;

- (id)init;

- (id)initWithIFunbookIndexid:(NSInteger)funbookIndexId readAt:(NSString*)readAt canceled:(BOOL)canceled funbookId:(NSInteger)funbookId;

@end

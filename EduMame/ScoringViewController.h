//
//  ScoringViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/19.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoringViewController : UIViewController

@property (nonatomic) NSInteger workbookIndexId;

@end

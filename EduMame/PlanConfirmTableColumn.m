//
//  PlanConfirmTableColumn.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PlanConfirmTableColumn.h"

@implementation PlanConfirmTableColumn

- (id)init
{
    self = [super init];
    if(self){
        self.name = @"";
        self.key = @"";
        self.width = 0;
        self.textAlignment = UITextAlignmentLeft;
        self.buttonEnable = NO;
    }
    return self;
}

- (id)initWithName:(NSString*)name key:(NSString*)key width:(CGFloat)width textAlignment:(UITextAlignment)textAlignment buttonEnable:(BOOL)buttonEnable
{
    self = [super init];
    if(self){
        self.name = name;
        self.key = key;
        self.width = width;
        self.textAlignment = textAlignment;
        self.buttonEnable = buttonEnable;
    }
    return self;    
}

+ (id)column
{
    return [[PlanConfirmTableColumn alloc] init];
}

+ (id)columnWithName:(NSString*)name key:(NSString*)key width:(CGFloat)width textAlignment:(UITextAlignment)textAlignment buttonEnable:(BOOL)buttonEnable
{
    return [[PlanConfirmTableColumn alloc] initWithName:name key:key width:width textAlignment:textAlignment buttonEnable:buttonEnable];
}

@end

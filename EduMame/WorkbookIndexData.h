//
//  WorkbookIndexData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkbookIndexData : NSObject

@property (nonatomic) NSInteger workbookIndexId;
@property (nonatomic) NSString* answerRawData;
@property (nonatomic) NSInteger answerStatusCode;
@property (nonatomic) NSInteger answerStartedAt;
@property (nonatomic) NSInteger answerFinishedAt;
@property (nonatomic) NSInteger evaluation;
@property (nonatomic) NSInteger evaluationAt;
@property (nonatomic) NSString* memo;
@property (nonatomic) NSInteger againFlag;
@property (nonatomic) NSString* markRawData;
@property (nonatomic) NSInteger workbookId;
@property (nonatomic) NSInteger contentId;

- (id)init;

- (id)initWithWorkbookIndexId:(NSInteger)workbookIndexId answerRawData:(NSString*)answerRawData answerStatusCode:(NSInteger)answerStatusCode answerStartedAt:(NSInteger)answerStartedAt answerFinishedAt:(NSInteger)answerFinishedAt evaluation:(NSInteger)evaluation evaluationAt:(NSInteger)evaluationAt memo:(NSString*)memo againFlag:(NSInteger)againFlag markRawData:(NSString*)markRawData workbookId:(NSInteger)workbookId contentId:(NSInteger)contentId;

@end

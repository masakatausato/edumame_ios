//
//  PasswordPopOverView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PasswordPopOverView.h"

@interface PasswordPopOverView ()

@end

@implementation PasswordPopOverView

@synthesize delegate = _delegate;
@synthesize passwordTextField = _passwordTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPasswordTextField:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (IBAction)selectLogin:(id)sender
{
    [self.delegate notifyLogin:self.passwordTextField.text];
}

- (IBAction)selectCancel:(id)sender
{
    [self.delegate notifyCancel];
}

@end

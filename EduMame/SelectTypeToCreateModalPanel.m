//
//  SelectWorkbookCreateModalPanel.m
//  EduMame
//
//  Created by gclue_mita on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SelectTypeToCreateModalPanel.h"

@implementation SelectTypeToCreateModalPanel

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

- (id)initWithFrame:(CGRect)frame title:(NSString *)title isCreate:(BOOL)isCreate;
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
        
        if (isCreate) {
            [[NSBundle mainBundle] loadNibNamed:@"SelectTypeToCreate" owner:self options:nil];
            self.mainView = self.selectCreateTypeFromXib;
            
            // UIEdgeInsetsMake(top, left, bottom, right);
            self.margin = UIEdgeInsetsMake(180.0, 296.0, 180.0, 296.0);
            self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        } else {
            [[NSBundle mainBundle] loadNibNamed:@"ManualSearchDetail" owner:self options:nil];
            self.mainView = self.searchWorkbooksViewFromXib;
            
            // UIEdgeInsetsMake(top, left, bottom, right);
            self.margin = UIEdgeInsetsMake(180.0, 296.0, 230.0, 296.0);
            self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        }
                    
		[self.contentView addSubview:self.mainView];
    }
    return self;
}

- (IBAction)onManual:(id)sender
{
    [self.mainView removeFromSuperview];
    
    [[NSBundle mainBundle] loadNibNamed:@"ManualSearchDetail" owner:self options:nil];
    self.mainView = self.searchWorkbooksViewFromXib;

    [self.contentView addSubview:self.mainView];
}

- (IBAction)onAutoDay:(id)sender
{
    [self.selectTypeDelegate notifyAutoDay:sender];
}

- (IBAction)onAutoWeek:(id)sender
{
    [self.selectTypeDelegate notifyAutoWeek:sender];
}

- (IBAction)onSearch:(id)sender
{
    NSInteger target = self.target.selectedSegmentIndex;
    NSString *category = self.category.text;
    NSString *level = self.level.text;
    NSString *term = self.term.text;
    
    // どれか1つ以上の項目が入力されていないと検索しない
/*    if ([category length] <= 0 && [level length] <= 0 && [term length] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"検索条件エラー"
                                                        message:@"検索条件が入力されていません。\n1つ以上入力して下さい。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
    } else*/ {
        [self.selectTypeDelegate notifyManualSearch:sender target:target category:category level:level term:term];
    }
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    
    CGRect frameRect = [self roundedRectFrame];
    CGRect closeRect = [self closeButtonFrame];
    [self.closeButton setFrame:CGRectMake(frameRect.size.width + closeRect.origin.x, closeRect.origin.y, 44, 44)];
    
	[self.mainView setFrame:self.contentView.bounds];
}

@end

//
//  SoundSE.m
//  EduMame
//
//  Created by GClue on 13/02/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SoundSE.h"

@implementation SoundSE

/*
 初期化処理
  name...ファイル名
 oftpe...ファイルの拡張子
*/
- (id)initSE:(NSString *)name type:(NSString *)oftype {
#define BufferSize 1000
    
    UniChar *myBuffer = malloc(BufferSize * sizeof(UniChar));

    // ファイル名の指定
    CFStringRef cfFileName = CFStringCreateWithCString(kCFAllocatorDefault, [name UTF8String], kCFStringEncodingUTF8);
        
    // ファイルタイプの指定
    CFStringRef cfTypeName = CFStringCreateWithCString(kCFAllocatorDefault, [oftype UTF8String], kCFStringEncodingUTF8);
    
    // 効果音の登録
    CFBundleRef mainBundle = CFBundleGetMainBundle ();
    CFURLRef soundURL = CFBundleCopyResourceURL(mainBundle, cfFileName, cfTypeName, NULL);
    AudioServicesCreateSystemSoundID (soundURL, &sound);
    
    CFRelease(cfFileName);
    CFRelease(cfTypeName);
    free(myBuffer);
    
    return self;
}

/*
 効果音の再生
*/
- (void)playSE {
    AudioServicesPlaySystemSound (sound);
}

/*
 強制停止
*/
- (void)playStop {
    CFRunLoopStop(CFRunLoopGetCurrent());
}

@end

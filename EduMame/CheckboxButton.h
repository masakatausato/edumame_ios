//
//  CheckboxButton.h
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxButton : UIButton

@property BOOL checkboxSelected;

@end

//
//  ParentData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ParentData.h"

@implementation ParentData

- (id)init
{
    self = [super init];
    if(self){
        self.parentsId = -1;
        self.userId = @"";
        self.username = @"";
        self.email = @"";
        self.familyName = @"";
        self.firstName = @"";
        self.familyNameKana = @"";
        self.firstNameKana = @"";
        self.zipCode1 = @"";
        self.zipCode2 = @"";
        self.prefectureCode = @"";
        self.city = @"";
        self.address = @"";
        self.building = @"";
        self.tel = @"";
        self.birthday = @"";
        self.password = @"";
        self.secretQuestion = @"";
        self.secretQuestionAnswer = @"";
    }
    return self;
}

- (id)initWithParentsId:(NSInteger)parentsId userId:(NSString*)userId username:(NSString*)username email:(NSString*)email familiName:(NSString*)familyName firstName:(NSString*)firstName familyNameKana:(NSString*)familyNameKana firstNameKana:(NSString*)firstNameKana zipCode1:(NSString*)zipCode1 zipCode2:(NSString*)zipCode2 prefectureCode:(NSString*)prefectureCode city:(NSString*)city address:(NSString*)address building:(NSString*)building tel:(NSString*)tel birthday:(NSString*)birthday password:(NSString*)password secretQuestion:(NSString*)secretQuestion secretQuestionAnswer:(NSString*)secretQuestionAnswer
{
    self = [super init];
    if(self){
        self.parentsId = parentsId;
        self.userId = userId;
        self.username = username;
        self.email = email;
        self.familyName = familyName;
        self.firstName = firstName;
        self.familyNameKana = familyNameKana;
        self.firstNameKana = firstNameKana;
        self.zipCode1 = zipCode1;
        self.zipCode2 = zipCode2;
        self.prefectureCode = prefectureCode;
        self.city = city;
        self.address = address;
        self.building = building;
        self.tel = tel;
        self.birthday = birthday;
        self.password = password;
        self.secretQuestion = secretQuestion;
        self.secretQuestionAnswer = secretQuestionAnswer;
    }
    return self;
}

@end

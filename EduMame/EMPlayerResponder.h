//
//  EMPlayerResponder.h
//  EMPreview2
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol EMPlayerResponder <NSObject>

// コンテンツ開始
- (void)playerStartContent;

// コンテンツ終了
- (void)playerEndContent;

// 解答開始
- (void)playerStartAnswer;

// 解答終了
- (void)playerEndAnswer;

// タップ待ち開始
- (void)playerStartTap;

// タップ待ち終了
- (void)playerEndTap;

// 画像のセット
- (void)playerSetImage:(UIImage*)image;

// ムービーの再生
- (void)playerPlayMovie:(AVPlayer*)player playerItem:(AVPlayerItem*)playerItem;

// ムービーの停止
- (void)playerStopMovie;

@end

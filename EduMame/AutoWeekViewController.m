//
//  AutoWeekViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AutoWeekViewController.h"

#import "CreateManager.h"

#import "AutoPreviewViewController.h"
#import "GridViewCell.h"
#import "DragAndDropView.h"
#import "ContentDateViewController.h"

@interface AutoWeekViewController ()
{
    NSMutableArray *firstContentData;
}
@end

@implementation AutoWeekViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 初期化
    [self getGeneratedData];
    [self initialize];
}

- (void)viewDidUnload
{
    self.topContainerView = nil;
    self.bottomContainerView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Auto Generated Data Methods

- (void)getGeneratedData
{
    [self generatedAutoWeek:nil];
}

- (void)generatedAutoWeek:(NSString *)selectOptions
{
    // TODO:設定から読込
    int dayNum = 7;
    int workbookNum = 5;
    
    self.autoGeneratedData = [NSMutableArray array];
    self.selectGeneratedData = [NSMutableArray array];
    
    NSArray* contentRecords = [DatabaseManager selectContentData:selectOptions];
    for (int i = 0; i < dayNum; i++)
    {
        for (int i = 0; i < [contentRecords count]/*workbookNum*/; i++)
        {
            ContentData* contentRecord = [contentRecords objectAtIndex:i];
            [self.autoGeneratedData addObject:contentRecord];
        }
        
        [self.selectGeneratedData addObject:self.autoGeneratedData];
    }    
}

#pragma mark - Private Methods

- (void)initialize
{
    NSString* tapDate = [[CreateManager sharedCreateManager] tapDate];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *formatterDate = [inputFormatter dateFromString:tapDate];
    
    // 先頭画像保持配列初期化
    firstContentData = [NSMutableArray array];
    
    // 日付格納配列初期化
    self.dropDateTargets = [NSMutableArray array];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.itemList = [self getGeneratedData:0];
    self.gridViewCtrl.tapDelegate = self;
    self.gridViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    // DateView
    self.dateViewCtrl = [[DateViewController alloc] init];
    self.dateViewCtrl.delegate = self;
    self.dateViewCtrl.date = formatterDate;
    
    [self addChildViewController:self.dateViewCtrl];
    [self.dateViewCtrl didMoveToParentViewController:self];
    
    [self.topContainerView addSubview:self.gridViewCtrl.view];
    [self.bottomContainerView addSubview:self.dateViewCtrl.view];
}

- (NSMutableArray *)getGeneratedData:(NSInteger)index
{
    NSMutableArray *data = [NSMutableArray array];
    NSUInteger sNum = [self.selectGeneratedData count];
    
    for (int i = 0; i < sNum; i++) {
        NSMutableArray *gData = [self.selectGeneratedData objectAtIndex:i];        

        // 自動生成された各先頭データを取得
        ContentData* contentRecord = [gData objectAtIndex:0];
        [data addObject:contentRecord];
        [firstContentData addObject:contentRecord];
    }

    return data;
}

- (ContentData *)getContentData:(NSInteger)index
{
    NSMutableArray *data = [self.selectGeneratedData objectAtIndex:index];
    ContentData* contentRecord = [data objectAtIndex:index];
    return contentRecord;
}

- (NSMutableArray *)getContentArray:(NSUInteger)index
{
    NSMutableArray *data = [self.selectGeneratedData objectAtIndex:index];
    return data;
}

- (UIImage *)getPreviewFromContent:(NSUInteger)index
{
    ContentData* contentRecord = [self getContentData:index];
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

- (UIImage *)getPreviewFromFirstContentData:(NSUInteger)index
{
    ContentData* contentRecord = [firstContentData objectAtIndex:index];
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

#pragma mark -
#pragma mark - Delegate Methods

#pragma mark - Cover Flow View & Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
    AutoPreviewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AutoPreviewIdentifier"];
    viewController.autoGeneratedData = self.autoGeneratedData;
    viewController.selectGeneratedData = [self.selectGeneratedData objectAtIndex:index];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Gesture Recogniser Delegate

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer
                          index:(NSInteger)index
{
    [self setDragAndDropImage:recognizer index:index];
    [self setScaleAnimToDragAndDropImage:recognizer];
}

- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer
{
    if (self.dragAndDropView == nil)
        return;
    
    CGPoint touchPoint = [recognizer locationInView:self.view];
    [self.dragAndDropView setCenter:touchPoint];
}

- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.view];
    [self.dragAndDropView setCenter:touchPoint];
    
    DateView *dropTarget = [self dropTargetHitByPoint:touchPoint];
    
    if (dropTarget == nil && self.lastDropDateTargets != nil) {
        [self setHighlightView:self.lastDropDateTargets highlighted:NO animated:YES];
        self.lastDropDateTargets = nil;
    }
    
    if (dropTarget != nil) {
        [self setHighlightView:dropTarget highlighted:YES animated:YES];
        self.lastDropDateTargets = dropTarget;
        
        // DB登録
        [self makeWorkbookArray:self.dragAndDropView dateView:dropTarget];
        
        // ドラッグ画像削除
        [self removeDragAndDropView];
        
        // リロード
        [_dateViewCtrl reloadData];
    }
    
    [self collapseDragView];
}

#pragma mark - Date View Delegate

- (void)notifyAddSubView:(DateView *)dateView
{
    [self.dropDateTargets addObject:dateView];
}

- (void)notifyTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [[touches anyObject] locationInView:self.view];
    dateTarget = [self dropTargetHitByPoint:touchPoint];
    
    // 日付タップ時
    if (dateTarget)
    {
        // DBから日付に該当するコンテンツデータを取得しContentDateViewControllerに渡す
        
        self.dropContentData = [[NSMutableArray array] init];
        
        // 日付取得
        NSDate *date = dateTarget.date;
        
        // WorkbookData検索
        NSString* option = [NSString stringWithFormat:@"where name = %@", [CreateManager getFormatDate:date dateFormat:@"yyyy-MM-dd"]];
        
        NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
        
        WorkbookData *workbookData = nil;
        if ([targetWorkbookRecord count] > 0) {
                        
            workbookData = [targetWorkbookRecord objectAtIndex:0];
            
            // WorkbookIndexData検索
            option = [NSString stringWithFormat:@"where workbook_id = %d", workbookData.workbookId];
            
            NSArray *targetWorkbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
            
            for (int i = 0; i < [targetWorkbookIndexRecord count]; i++) {
                WorkbookIndexData *workbookIndexData = [targetWorkbookIndexRecord objectAtIndex:i];
                
                // ContentData検索
                option = [NSString stringWithFormat:@"where contents_id = %d", workbookIndexData.contentId];
                
                NSArray *targetContentRecord = [DatabaseManager selectContentData:option];
                
                for (int j = 0; j < [targetContentRecord count]; j++) {
                    ContentData *contentData = [targetContentRecord objectAtIndex:j];
                    [self.dropContentData addObject:contentData];
                }
            }
        }
        
        [self performSegueWithIdentifier:@"ContentDateSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    
    if ([identifier isEqualToString:@"ContentDateSegue"])
    {
        ContentDateViewController *nextViewController = [segue destinationViewController];
        nextViewController.contentData = self.dropContentData;
        nextViewController.date = dateTarget.date;
    }
}

#pragma mark - Drag Methods

/**
 * setDragAndDropImage
 * ドラッグ画像セット
 * @param recognizer
 * @param index
 */
- (void)setDragAndDropImage:(UIGestureRecognizer *)recognizer
                      index:(NSInteger)index
{
    UIImage *img = [self getPreviewFromFirstContentData:index];//[self getPreviewFromContent:index];
    
    UIImage *resultImage;
    CGSize size = CGSizeMake(200, 150);
    UIGraphicsBeginImageContext(size);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:resultImage];
    
    self.dragAndDropView = [[DragAndDropView alloc] initWithFrame:iv.frame];
    [self.dragAndDropView.view setImage:img];
    [self.dragAndDropView setContentData:nil];
    [self.dragAndDropView setContentArray:[self getContentArray:index]];
    [self.dragAndDropView addSubview:iv];
    [self.dragAndDropView setCenter:[recognizer locationInView:self.view]];
    
    [self.view addSubview:self.dragAndDropView];
}

/**
 * setScaleAnimToDragAndDropImage
 * ドラッグ画像にスケールアニメーションセット
 * @param recognizer
 */
- (void)setScaleAnimToDragAndDropImage:(UIGestureRecognizer *)recognizer
{
    [UIView beginAnimations: @"" context: NULL];
    [UIView setAnimationDuration: 0.2f];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
    
    // transformation-- larger, slightly transparent
    self.dragAndDropView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    // also make it center on the touch point
    self.dragAndDropView.center = [recognizer locationInView:self.view];
    
    [UIView commitAnimations];
}

/**
 * ドロップ時アニメーション
 */
- (void)collapseDragView
{
	[UIView beginAnimations:@"DropSuck" context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:.3];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(cancelAnimationDidStop:finished:context:)];
	
	self.dragAndDropView.transform = CGAffineTransformMakeScale(0.001f, 0.001f);
	self.dragAndDropView.alpha = 0.0f;
	self.dragAndDropView.center = CGPointMake(self.dragAndDropView.center.x, self.dragAndDropView.center.y);
	
	[UIView commitAnimations];
}

/**
 * ドラッグビューと日付ビュー当たり判定
 */
- (DateView *)dropTargetHitByPoint:(CGPoint)point
{
	for (DateView *dropTarget in self.dropDateTargets) {
		CGRect frameInWindow = [[dropTarget superview] convertRect:dropTarget.frame toView:self.view];
		if (CGRectContainsPoint(frameInWindow, point)) {
            return dropTarget;
		}
	}
	
	return nil;
}

/**
 * ドラッグした日付ビューアニメーション
 */
- (void)setHighlightView:(UIView *)view highlighted:(BOOL)highlighted animated:(BOOL)animated
{
	CALayer *dropLayer = view.layer;
	
	if (animated) {
		[UIView beginAnimations: @"HighlightView" context: NULL];
		[UIView setAnimationCurve: UIViewAnimationCurveLinear];
		[UIView setAnimationBeginsFromCurrentState: YES];
	}
    
	if ([dropLayer respondsToSelector: @selector(setShadowPath:)] &&
        [dropLayer respondsToSelector: @selector(shadowPath)])
    {
		if (highlighted) {
			CGMutablePathRef path = CGPathCreateMutable();
			CGPathAddRect(path, NULL, dropLayer.bounds);
			dropLayer.shadowPath = path;
			CGPathRelease(path);
			
			dropLayer.shadowOffset = CGSizeZero;
			dropLayer.shadowColor = [[UIColor blueColor] CGColor];
			dropLayer.shadowRadius = 12.0f;
			dropLayer.shadowOpacity = 1.0f;
		} else {
			dropLayer.shadowOpacity = 0.0f;
		}
	}
	
	if (animated) {
        [UIView commitAnimations];
    }
}

/**
 * removeDragAndDropView
 * ドラッグ画像を削除
 */
- (void)removeDragAndDropView
{
    [self.dragAndDropView removeFromSuperview];
    self.dragAndDropView = nil;
}

#pragma mark - Database Methods

/**
 * makeWorkbookArray
 * コンテンツ配列をDBに格納
 */
- (void)makeWorkbookArray:(DragAndDropView *)dragAndDropView dateView:(DateView *)dropTarget
{
    WorkbookData *workbookData = [CreateManager updateWorkbookRecord:dropTarget.date];
    [CreateManager updateWorkbookIndexRecord:workbookData contentArray:dragAndDropView.contentArray];
}

@end

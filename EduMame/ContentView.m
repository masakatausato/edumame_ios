//
//  ContentView.m
//  EduMame
//
//  Created by gclue_mita on 13/02/11.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ContentView.h"
#import "ContentData.h"

@implementation ContentView

@synthesize contentData = _contentData;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

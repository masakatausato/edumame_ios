//
//  CalendarView.h
//  EduMame
//
//  Created by gclue_mita on 12/10/26.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CalendarView;

@protocol CalendarViewDelegate <NSObject>

- (void)calendarView:(CalendarView *)cView didChangeState:(BOOL)opened;

@end

@interface CalendarView : UIView {
    
    CGPoint closedCenter;
    CGPoint openedCenter;
    
    UIView *handleView;
    UIPanGestureRecognizer *dragRecognizer;
    UITapGestureRecognizer *tapRecognizer;
    
    CGPoint startPos;
    CGPoint minPos;
    CGPoint maxPos;
    
    BOOL opened;
    BOOL verticalAxis;
    
    BOOL toggleOnTap;
    
    BOOL animate;
    float animationDuration;
    
    id<CalendarViewDelegate> delegate;
}

@property (nonatomic,readonly) UIView *handleView;
@property (readwrite,assign) CGPoint closedCenter;
@property (readwrite,assign) CGPoint openedCenter;
@property (nonatomic,readonly) UIPanGestureRecognizer *dragRecognizer;
@property (nonatomic,readonly) UITapGestureRecognizer *tapRecognizer;
@property (readwrite,assign) BOOL toggleOnTap;
@property (readwrite,assign) BOOL animate;
@property (readwrite,assign) float animationDuration;
@property (readwrite, strong) id<CalendarViewDelegate> delegate;

- (void)setOpened:(BOOL)op animated:(BOOL)anim;

@end

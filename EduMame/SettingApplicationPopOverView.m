//
//  ApplicationPopOverView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SettingApplicationPopOverView.h"

#define EDMAME_SETTINGS @"EdumameSettings"
#define EDMAME_SETTINGS_PLIST @"EdumameSettings.plist"

@interface SettingApplicationPopOverView () 

@end

@implementation SettingApplicationPopOverView

@synthesize delegate = _delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:EDMAME_SETTINGS ofType:@"plist"];
    //Cacheディレクトリ
    cachePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:EDMAME_SETTINGS_PLIST];
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if (![filemanager fileExistsAtPath:cachePath]) {
        [filemanager copyItemAtPath:path toPath:cachePath error:nil];
    }
                           
    switches = [NSMutableArray arrayWithContentsOfFile:cachePath];
        
    // 初期状態の指定 YES=1、NO=0
    if ([[switches objectAtIndex:0] intValue] == 0) {
        parentPasswordSwitch.on = NO;
    }
    if ([[switches objectAtIndex:1] intValue] == 0) {
        parentSoundEffect.on = NO;
    }
    if ([[switches objectAtIndex:2] intValue] == 0) {
        parentBgm.on = NO;
    }
    if ([[switches objectAtIndex:3] intValue] == 0) {
        childSoundEffect.on = NO;
    }
    if ([[switches objectAtIndex:4] intValue] == 0) {
        childBgm.on = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSettingAutoCreate:(id)sender
{    
    [self.delegate notifySettingAutoCreate];
}

- (IBAction)parentPassword:(UISwitch *)sender {
    if(sender.on) {
        [switches replaceObjectAtIndex:0 withObject:@"1"];
    } else {
        [switches replaceObjectAtIndex:0 withObject:@"0"];
    }
    [switches writeToFile:cachePath atomically:NO];
}

- (IBAction)parentSoundEffect:(UISwitch *)sender {
    if(sender.on) {
        [switches replaceObjectAtIndex:1 withObject:@"1"];
    } else {
        [switches replaceObjectAtIndex:1 withObject:@"0"];
    }
    [switches writeToFile:cachePath atomically:NO];
}

- (IBAction)parentBgm:(UISwitch *)sender {
    if(sender.on) {
        [switches replaceObjectAtIndex:2 withObject:@"1"];
    } else {
        [switches replaceObjectAtIndex:2 withObject:@"0"];
    }
    [switches writeToFile:cachePath atomically:NO];
}

- (IBAction)childSoundEffect:(UISwitch *)sender {
    if(sender.on) {
        [switches replaceObjectAtIndex:3 withObject:@"1"];
    } else {
        [switches replaceObjectAtIndex:3 withObject:@"0"];
    }
    [switches writeToFile:cachePath atomically:NO];
}

- (IBAction)childBgm:(UISwitch *)sender {
    if(sender.on) {
        [switches replaceObjectAtIndex:4 withObject:@"1"];
    } else {
        [switches replaceObjectAtIndex:4 withObject:@"0"];
    }
    [switches writeToFile:cachePath atomically:NO];
}

- (void)viewDidUnload {
    parentPasswordSwitch = nil;
    parentSoundEffect = nil;
    parentBgm = nil;
    childSoundEffect = nil;
    childBgm = nil;
    [super viewDidUnload];
}
@end

//
//  PlanConfirmTableViewCell.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PlanConfirmTableViewCell.h"
#import "PlanConfirmTableDefine.h"
#import "PlanConfirmTableColumn.h"
#import <QuartzCore/QuartzCore.h>

enum
{
    PlanConfirmTableViewTagDate = 100,
    PlanConfirmTableViewTagCategory,
    PlanConfirmTableViewTagSubcategory,
    PlanConfirmTableViewTagContentTitle,
    PlanConfirmTableViewTagProductTitle,
    PlanConfirmTableViewTagLevel,
    PlanConfirmTableViewTagEvaluation,
    PlanConfirmTableViewTagMark,
    PlanConfirmTableViewTagComment,
    PlanConfirmTableViewTagHistory,
};

static NSString* PlanConfirmTableEvaluation[] = { @"-", @"A", @"B", @"C" };
static NSString* PlanConfirmTableMark[] = { @"-", @"○", @"×", @"△" };

@implementation PlanConfirmTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier columns:(NSArray*)columns
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat xoffset = 0;
        NSInteger viewTag = PlanConfirmTableViewTagDate;
        for(NSInteger columnIndex = 0; columnIndex < [columns count]; columnIndex++)
        {
            PlanConfirmTableColumn* column = [columns objectAtIndex:columnIndex];
            
            UIView* indexView = [[UIView alloc] initWithFrame:CGRectMake(xoffset, 0, column.width, PlanConfirmTableCellHeight)];
            indexView.tag = viewTag;
            [self addSubview:indexView];
            
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(PlanConfirmTableCellMargin, 0, column.width-(PlanConfirmTableCellMargin*2), PlanConfirmTableCellHeight)];
            [label setText:column.name];
            [label setTextAlignment:column.textAlignment];
            [label setFont:[UIFont systemFontOfSize:PlanConfirmTableFontSize]];
            [label setBackgroundColor:[UIColor clearColor]];
            [indexView addSubview:label];

            if(column.buttonEnable)
            {
                UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setTag:columnIndex];
                [button setFrame:CGRectMake(0, 0, column.width, PlanConfirmTableCellHeight)];
                [button setBackgroundColor:[UIColor clearColor]];
                [button addTarget:self action:@selector(touchUpInsideCellButton:) forControlEvents:UIControlEventTouchUpInside];
                [indexView addSubview:button];
            }
            
#if 0
            UIView* borderView = [[UIView alloc] initWithFrame:CGRectMake(column.width - 1, 0, 1, PlanConfirmTableCellHeight)];
            [borderView setBackgroundColor:[UIColor lightGrayColor]];
            [indexView addSubview:borderView];
#endif
            
            xoffset += column.width;
            viewTag++;
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)reloadCellForItem:(PlanConfirmTableItem*)item;
{
    UIView* dateView = [self viewWithTag:PlanConfirmTableViewTagDate];
    UILabel* dateLabel = [dateView.subviews objectAtIndex:0];
    [dateLabel setText:item.date];

    UIView* categoryView = [self viewWithTag:PlanConfirmTableViewTagCategory];
    UILabel* categoryLabel = [categoryView.subviews objectAtIndex:0];
    [categoryLabel setText:item.category];

    UIView* subcategoryView = [self viewWithTag:PlanConfirmTableViewTagSubcategory];
    UILabel* subcategoryLabel = [subcategoryView.subviews objectAtIndex:0];
    [subcategoryLabel setText:item.subcategory];

    UIView* contentTitleView = [self viewWithTag:PlanConfirmTableViewTagContentTitle];
    UILabel* contentTitleLabel = [contentTitleView.subviews objectAtIndex:0];
    [contentTitleLabel setText:item.contentTitle];

    UIView* productTitleView = [self viewWithTag:PlanConfirmTableViewTagProductTitle];
    UILabel* productTitleLabel = [productTitleView.subviews objectAtIndex:0];
    [productTitleLabel setText:item.productTitle];

    UIView* levelView = [self viewWithTag:PlanConfirmTableViewTagLevel];
    UILabel* levelLabel = [levelView.subviews objectAtIndex:0];
    [levelLabel setText:[NSString stringWithFormat:@"%d", item.level]];

    UIView* evaluationView = [self viewWithTag:PlanConfirmTableViewTagEvaluation];
    UILabel* evaluationLabel = [evaluationView.subviews objectAtIndex:0];
    [evaluationLabel setText:PlanConfirmTableEvaluation[item.evaluation]];

    UIView* markView = [self viewWithTag:PlanConfirmTableViewTagMark];
    UILabel* markLabel = [markView.subviews objectAtIndex:0];
    [markLabel setText:PlanConfirmTableMark[item.mark]];

    UIView* commentView = [self viewWithTag:PlanConfirmTableViewTagComment];
    UILabel* commentLabel = [commentView.subviews objectAtIndex:0];
    [commentLabel setText:item.comment];

    UIView* historyView = [self viewWithTag:PlanConfirmTableViewTagHistory];
    UILabel* historyLabel = [historyView.subviews objectAtIndex:0];
    if(item.history > 0)
    {
        [historyLabel setText:[NSString stringWithFormat:@"%d", item.history]];
    }
    else
    {
        [historyLabel setText:@"-"];
    }

}

- (void)willDisplayCellForItem:(PlanConfirmTableItem*)item
{
    UIColor* backgroundColor = nil;
    switch(item.status)
    {
        case PlanConfirmTableItemStatusCurrent:
            backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
            break;
        case PlanConfirmTableItemStatusFinished:
            backgroundColor = [UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.0f];
            break;
        case PlanConfirmTableItemStatusFuture:
            backgroundColor = [UIColor colorWithRed:0.8f green:0.8f blue:1.0f alpha:1.0f];
            break;
    }
    [self setBackgroundColor:backgroundColor];
}

- (IBAction)touchUpInsideCellButton:(id)sender
{
    NSInteger columnIndex = [sender tag];
    if ([[_delegate class] instancesRespondToSelector:@selector(pushCell:columnIndex:)])
    {
        [_delegate pushCell:self columnIndex:columnIndex];
    }
}

@end

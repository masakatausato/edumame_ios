//
//  WorkBookCalendarView.h
//  EduMame
//
//  Created by GClue on 13/02/13.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMCalendarComponent.h"
#import "EMCalendarResponder.h"

@interface WorkBookCalendarView : UIView

@property id<EMCalendarResponder> delegate;

@property (readonly) EMCalendarComponent* calendarComponent;

- (void)previousMonth;

- (void)nextMonth;

- (void)setStampImage:(UIImage*)image forTag:(NSInteger)tag;

- (void)reloadCalendar;

@end
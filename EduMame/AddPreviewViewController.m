//
//  AddPreviewViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/14.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AddPreviewViewController.h"

@interface AddPreviewViewController ()

@end

@implementation AddPreviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // 初期化
    [self initialize];
    [self initializeUIBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    self.dropDateTargets = [NSMutableArray array];
    
    // CoverFlowView
    self.coverflowViewCtrl = [[CoverFlowViewController alloc] init];
    self.coverflowViewCtrl.itemList = [self.contentData mutableCopy];
    self.coverflowViewCtrl.tapDelegate = self;
    self.coverflowViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.coverflowViewCtrl];
    [self.coverflowViewCtrl didMoveToParentViewController:self];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.itemList = [self.contentData mutableCopy];
    self.gridViewCtrl.tapDelegate = self;
    self.gridViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    // ContentView
    self.contentViewCtrl = [[ContentViewController alloc] init];
    self.contentViewCtrl.itemList = [self.searchedContentData mutableCopy];
    self.contentViewCtrl.tapDelegate = self;
    self.contentViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.contentViewCtrl];
    [self.contentViewCtrl didMoveToParentViewController:self];
    
    [self.topContainerView addSubview:self.coverflowViewCtrl.view];
    [self.bottomContainerView addSubview:self.contentViewCtrl.view];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"プリント準備"];
    
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"カバーフロー"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedView:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:item, nil];
}

- (void)onChangedView:(id)sender
{
    UIViewController *fromViewCtrl;
    UIViewController *toViewCtrl;
    
    if (self.coverflowViewCtrl.isViewLoaded && self.coverflowViewCtrl.view.superview) {
        fromViewCtrl = self.coverflowViewCtrl;
        toViewCtrl = self.gridViewCtrl;
        
        [self.navigationItem.rightBarButtonItem setTitle:@"カバーフロー"];
    } else {
        fromViewCtrl = self.gridViewCtrl;
        toViewCtrl = self.coverflowViewCtrl;
        
        [self.navigationItem.rightBarButtonItem setTitle:@"グリッド"];
    }
    
    [fromViewCtrl.view.superview addSubview:toViewCtrl.view];
    [fromViewCtrl.view removeFromSuperview];
    
    [self.view setNeedsLayout];
}

#pragma mark - Cover Flow View & Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
}

#pragma mark - Gesture Recogniser Delegate

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer
                          index:(NSInteger)index
{
}

- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer
{
}

- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer
{
}

#pragma mark - Content View Delegate

- (void)notifyTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

@end

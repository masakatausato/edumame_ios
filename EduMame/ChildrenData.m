//
//  ChildrenData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildrenData.h"

@implementation ChildrenData

- (id)init
{
    self = [super init];
    if(self){
        self.childId = -1;
        self.username = @"";
        self.passwordType = 0;
        self.password = @"";
        self.familyName = @"";
        self.firstName = @"";
        self.familyNameKana = @"";
        self.firstNameKana = @"";
        self.birthday = @"";
        self.gender = 0;
        self.parentsId = -1;
    }
    return self;
}

- (id)initWithChildId:(NSInteger)childId username:(NSString*)username passwordType:(NSInteger)passwordType password:(NSString*)password familyName:(NSString*)familyName firstName:(NSString*)firstName familyNameKana:(NSString*)familyNameKana firstNameKana:(NSString*)firstNameKana birthday:(NSString*)birthday gender:(NSInteger)gender parentsId:(NSInteger)parentsId
{
    self = [super init];
    if(self){
        self.childId = childId;
        self.username = username;
        self.passwordType = passwordType;
        self.password = password;
        self.familyName = familyName;
        self.firstName = firstName;
        self.familyNameKana = familyNameKana;
        self.firstNameKana = firstNameKana;
        self.birthday = birthday;
        self.gender = gender;
        self.parentsId = parentsId;
    }
    return self;    
}

@end

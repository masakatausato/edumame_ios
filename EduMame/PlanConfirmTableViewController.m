//
//  PlanConfirmTableViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PlanConfirmTableViewController.h"
#import "PlanConfirmTableDefine.h"
#import "PlanConfirmTableViewCell.h"
#import "PlanConfirmTableColumn.h"
#import "PlanConfirmTableItem.h"
#import "ScoringViewController.h"
#import "DatabaseManager.h"
#import <QuartzCore/QuartzCore.h>

@interface PlanConfirmTableViewController ()
{
    IBOutlet UITableView* _tableView;
    IBOutlet UIView* _tableIndexView;
    NSArray* _tableColumns;
    NSArray* _tableItems;
    NSArray* _workbookIndexRecords;
    NSInteger _selectedRowIndex;
    NSInteger _sortColumnIndex;
    BOOL _sortAscending;
}
@end

@implementation PlanConfirmTableViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // テーブルカラムの生成
    NSMutableArray* tableColumns = [NSMutableArray array];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"予定日" key:@"date" width:100 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"カテゴリ" key:@"category" width:100 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"サブカテゴリ" key:@"subcategory" width:100 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"問題" key:@"contentTitle" width:150 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"教材名" key:@"productTitle" width:200 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"難易度" key:@"level" width:70 textAlignment:UITextAlignmentCenter buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"評価" key:@"evaluation" width:50 textAlignment:UITextAlignmentCenter buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"正誤" key:@"mark" width:50 textAlignment:UITextAlignmentCenter buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"コメント" key:@"comment" width:150 textAlignment:UITextAlignmentLeft buttonEnable:NO]];
    [tableColumns addObject:[PlanConfirmTableColumn columnWithName:@"履歴" key:@"history" width:50 textAlignment:UITextAlignmentCenter buttonEnable:YES]];
    _tableColumns = [NSArray arrayWithArray:tableColumns];
    
    // 見出しビューの作成
    CGFloat xoffset = 0;
    for(NSInteger columnIndex = 0; columnIndex < [_tableColumns count]; columnIndex++)
    {
        PlanConfirmTableColumn* column = [_tableColumns objectAtIndex:columnIndex];
        
        UIView* indexView = [[UIView alloc] initWithFrame:CGRectMake(xoffset, 0, column.width, PlanConfirmTableCellHeight)];
        indexView.backgroundColor = [UIColor clearColor];
        [_tableIndexView addSubview:indexView];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(PlanConfirmTableCellMargin, 0, column.width-(PlanConfirmTableCellMargin*2), PlanConfirmTableCellHeight)];
        label.text = column.name;
        label.textAlignment = column.textAlignment;
        label.textColor = [UIColor blackColor];
        [label setBackgroundColor:[UIColor clearColor]];
        label.font = [UIFont systemFontOfSize:PlanConfirmTableFontSize];
        [indexView addSubview:label];

        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:columnIndex];
        [button setFrame:CGRectMake(0, 0, column.width, PlanConfirmTableCellHeight)];
        [button setBackgroundColor:[UIColor clearColor]];
        [button addTarget:self action:@selector(touchUpInsideHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [indexView addSubview:button];
        
#if 0
        UIView* borderView = [[UIView alloc] initWithFrame:CGRectMake(column.width - 1, 0, 1, PlanConfirmTableCellHeight)];
        [borderView setBackgroundColor:[UIColor blackColor]];
        [indexView addSubview:borderView];
#endif

        xoffset += column.width;
    }
    
    // 見出しビューのボーダー
    UIView* indexBorderView = [[UIView alloc] initWithFrame:CGRectMake(0, _tableIndexView.bounds.size.height - 1, _tableIndexView.bounds.size.width, 1)];
	//indexBorderView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [indexBorderView setBackgroundColor:[UIColor blackColor]];
    [_tableIndexView addSubview:indexBorderView];
    
    // セルビューの登録
    //[_tableView registerClass:[PlanConfirmTableViewCell class] forCellReuseIdentifier:PlanConfirmTableCellIdentifier];
    
    // テーブルアイテムの生成
    //[self updateTableItems];
    //[self sortTable:_sortColumnIndex reset:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    // アラートビューを表示
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"更新中";
    alertView.message = @"しばらくお待ちください。";
    alertView.delegate = self;
    [alertView show];

    // テーブルアイテムを更新
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self updateTableItems];
        [self sortTable:_sortColumnIndex reset:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_tableView reloadData];
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        });
    });
}

- (void)willPresentAlertView:(UIAlertView*)alertView
{
    // インジケータービューを生成
    UIActivityIndicatorView* activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.frame = CGRectMake(0, alertView.frame.size.height / 2, alertView.frame.size.width, alertView.frame.size.height / 2);
    [activityIndicatorView startAnimating];
    
    // インジケータービューをアラートビューに追加
    [alertView addSubview:activityIndicatorView];
}


- (IBAction)touchUpInsideHeaderButton:(id)sender
{
    NSInteger columnIndex = [sender tag];
    [self sortTable:columnIndex reset:NO];
    [_tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if([identifier isEqualToString:@"ScoringSegue"])
    {
        PlanConfirmTableItem* item = [_tableItems objectAtIndex:_selectedRowIndex];
        ScoringViewController* nextViewController = [segue destinationViewController];
        nextViewController.workbookIndexId = item.workbookIndexId;
    }
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return PlanConfirmTableCellHeight;
}

- (int)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [_tableItems count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlanConfirmTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:PlanConfirmTableCellIdentifier];
    if (cell == nil)
    {
        cell = [[PlanConfirmTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:PlanConfirmTableCellIdentifier columns:_tableColumns];
    }
    
    PlanConfirmTableItem* item = [_tableItems objectAtIndex:indexPath.row];
    [cell reloadCellForItem:item];
    cell.delegate = self;
    cell.tag = indexPath.row;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PlanConfirmTableItem* item = [_tableItems objectAtIndex:indexPath.row];
    if(item.answered)
    {
        _selectedRowIndex = indexPath.row;
        [self performSegueWithIdentifier:@"ScoringSegue" sender:self];        
    }
}

- (void)tableView:(UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    PlanConfirmTableViewCell* planConfirmTableViewCell = (PlanConfirmTableViewCell*)cell;

    PlanConfirmTableItem* item = [_tableItems objectAtIndex:indexPath.row];
    [planConfirmTableViewCell willDisplayCellForItem:item];
}

- (void)pushCell:(id)viewCell columnIndex:(NSInteger)columnIndex
{
    PlanConfirmTableViewCell* cell = viewCell;
    NSInteger rowIndex = cell.tag;
    PlanConfirmTableItem* item = [_tableItems objectAtIndex:rowIndex];
    if(item.history > 1)
    {
        PlanConfirmTableViewController* nextViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"PlanConfirmTableViewControllerIdentifier"];
        nextViewController.filter = _filter;
        nextViewController.contentIdFilter = item.contentId;
        nextViewController.contentIdFilterEnable = YES;
        nextViewController.historyEnable = NO;
        [self.navigationController pushViewController:nextViewController animated:YES];
    }
}

- (void)updateTableItems
{
    NSDate* currentDate = [DatabaseManager dateFromDate:[NSDate date]];
    
    // データベースのアクセス
    NSString* option = nil;
    switch(_filter)
    {
        case PlanConfirmTableFilterAll:
            option = [NSString stringWithFormat:@"WHERE workbook_index.workbook_index_id >= 0"];
            break;
        case PlanConfirmTableFilterMiss:
            //TODO:要検討
            option = [NSString stringWithFormat:@"WHERE workbook_index.workbook_index_id >= 0"];
            break;
        case PlanConfirmTableFilterNotAnswer:
            option = [NSString stringWithFormat:@"WHERE workbook_index.answer_finished_at = 0"];
            break;
        case PlanConfirmTableFilterNotScoring:
            option = [NSString stringWithFormat:@"WHERE NOT (workbook_index.answer_finished_at = 0) AND (workbook_index.evaluation = 0)"];
            break;
        case PlanConfirmTableFilterFinishScoring:
            option = [NSString stringWithFormat:@"WHERE NOT (workbook_index.answer_finished_at = 0) AND NOT (workbook_index.evaluation = 0)"];
            break;
    }
    if(_contentIdFilterEnable)
    {
        option = [NSString stringWithFormat:@"%@ AND workbook_index.content_id = %d", option, _contentIdFilter];
    }
    _workbookIndexRecords = [DatabaseManager selectWorkbookIndexDataRecursive:option];
    
    // テーブルアイテムの生成
    NSMutableArray* tableItems = [NSMutableArray array];
    for(NSInteger i = 0; i < [_workbookIndexRecords count]; i++)
    {
        WorkbookIndexData* workbookIndexRecord = [(NSDictionary*)[_workbookIndexRecords objectAtIndex:i] objectForKey:@"WorkbookIndex"];
        WorkbookData* workbookRecord = [(NSDictionary*)[_workbookIndexRecords objectAtIndex:i] objectForKey:@"Workbook"];
        ContentData* contentRecord = [(NSDictionary*)[_workbookIndexRecords objectAtIndex:i] objectForKey:@"Content"];
        
        ProductIndexData* productIndexRecord = [DatabaseManager first:[DatabaseManager selectProductIndexData:[NSString stringWithFormat:@"WHERE content_id = %d", contentRecord.contentsId]]];
        ProductData* productRecord = [DatabaseManager first:[DatabaseManager selectProductData:[NSString stringWithFormat:@"WHERE products_id =  %d", productIndexRecord.productId]]];
        CategoryData* categoryRecord = [DatabaseManager first:[DatabaseManager selectCategoryData:[NSString stringWithFormat:@"WHERE category_id =  %d", contentRecord.categoryId]]];
        SubcategoryData* subcategoryRecord = [DatabaseManager first:[DatabaseManager selectSubcategoryData:[NSString stringWithFormat:@"WHERE subcategory_id =  %d", contentRecord.subcategoryId]]];
        
        PlanConfirmTableItem* item = [[PlanConfirmTableItem alloc] init];
        item.workbookIndexId = workbookIndexRecord.workbookIndexId;
        item.contentId = workbookIndexRecord.contentId;
        item.answered = (workbookIndexRecord.answerFinishedAt != 0);
        
        NSDate* date = [DatabaseManager dateFromString:workbookRecord.name];
        switch([currentDate compare:date])
        {
            case NSOrderedSame:
                item.status = PlanConfirmTableItemStatusCurrent;
                break;
            case NSOrderedAscending:
                item.status = PlanConfirmTableItemStatusFuture;
                break;
            case NSOrderedDescending:
                item.status = PlanConfirmTableItemStatusFinished;
                break;
        }
        
        item.date = workbookRecord.name;
        item.category = categoryRecord.name;
        item.subcategory = subcategoryRecord.name;
        item.contentTitle = contentRecord.title;
        item.productTitle = productRecord.title;
        item.level = contentRecord.level;
        item.evaluation = workbookIndexRecord.evaluation;
        
        item.mark = 0;
        NSData* markRawData = [workbookIndexRecord.markRawData dataUsingEncoding:NSStringEncodingConversionAllowLossy];
        NSDictionary* markRawDataJSON = [NSJSONSerialization JSONObjectWithData:markRawData options:NSJSONReadingAllowFragments error:nil];
        if(markRawDataJSON)
        {
            NSArray* marks = [markRawDataJSON objectForKey:@"marks"];
            if([marks count] > 0)
            {
                item.mark = [(NSNumber*)[marks objectAtIndex:0] integerValue];
            }
        }
        
        item.comment = workbookIndexRecord.memo;
        
        if(_historyEnable)
        {
            NSArray* records = [DatabaseManager selectWorkbookIndexData:[NSString stringWithFormat:@"%@ AND content_id = %d", option, workbookIndexRecord.contentId]];
            item.history = [records count];
        }
        else
        {
            item.history = 0;
        }
        
        [tableItems addObject:item];
    }
    _tableItems = [NSArray arrayWithArray:tableItems];    
}

- (void)sortTable:(NSInteger)columnIndex reset:(BOOL)reset
{
    PlanConfirmTableColumn* column = [_tableColumns objectAtIndex:columnIndex];
    
    if(!reset && (_sortColumnIndex == columnIndex))
    {
        _sortAscending = !_sortAscending;
    }
    else
    {
        _sortColumnIndex = columnIndex;
        _sortAscending = YES;
    }
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:column.key ascending:_sortAscending];
    _tableItems = [_tableItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [self reloadTableHeader];
}

- (void)reloadTableHeader
{
    // 見出しビューの更新
    for(NSInteger columnIndex = 0; columnIndex < [_tableColumns count]; columnIndex++)
    {
        PlanConfirmTableColumn* column = [_tableColumns objectAtIndex:columnIndex];
        
        UIView* indexView = [_tableIndexView.subviews objectAtIndex:columnIndex];
        
        UILabel* label = [indexView.subviews objectAtIndex:0];
        if(columnIndex == _sortColumnIndex)
        {
            label.text = [column.name stringByAppendingString:_sortAscending ? @"▲" : @"▼"];
        }
        else
        {
            label.text = column.name;
        }
    }
}

@end

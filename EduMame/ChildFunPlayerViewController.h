//
//  ChildFunPlayerViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/23.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMPlayerResponder.h"

@interface ChildFunPlayerViewController : UIViewController<EMPlayerResponder, UIGestureRecognizerDelegate>

// 読み込みコンテンツ名
@property NSString* contentName;

@end

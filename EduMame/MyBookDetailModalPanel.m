//
//  MyBookDetailModalPanel.m
//  EduMame
//
//  Created by gclue_mita on 13/02/25.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "MyBookDetailModalPanel.h"

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

@implementation MyBookDetailModalPanel

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
        
        [[NSBundle mainBundle] loadNibNamed:@"MyBookDetail" owner:self options:nil];
        
        self.margin = UIEdgeInsetsMake(160.0, 210.0, 160.0, 210.0);
        self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        
        [self.contentView addSubview:self.detailView];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    
    CGRect frameRect = [self roundedRectFrame];
    CGRect closeRect = [self closeButtonFrame];
    [self.closeButton setFrame:CGRectMake(frameRect.size.width + closeRect.origin.x, closeRect.origin.y, 44, 44)];
    
	[self.detailView setFrame:self.contentView.bounds];
}

@end

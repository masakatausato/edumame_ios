//
//  ChildCalendarViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMCalendarResponder.h"

@interface ChildCalendarViewController : UIViewController<EMCalendarResponder>

@end

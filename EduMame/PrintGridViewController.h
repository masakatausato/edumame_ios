//
//  PrintGridViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "GridViewController.h"

@interface PrintGridViewController : GridViewController

@property (strong, nonatomic) NSMutableArray *checkHiddenData;

@end

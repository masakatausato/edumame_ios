//
//  PreviewDetailViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/11/06.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PreviewDetailViewController.h"
#import "PreviewData.h"

@interface PreviewDetailViewController ()

@end

@implementation PreviewDetailViewController

@synthesize _previewIndex;
@synthesize _previewListData;
@synthesize _image;
@synthesize _detailImageView;
@synthesize _swipeLeftGestureRecognizer;
@synthesize _swipeRightGestureRecognizer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [_detailImageView setImage:_image];
    
    // スワイプ検知(Left)
    _swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSwipeLeftGesture:)];
    _swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:_swipeLeftGestureRecognizer];
    
    // スワイプ検知(Right)
    _swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(handleSwipeRightGesture:)];
    _swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:_swipeRightGestureRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

// スワイプ(Left)
- (void)handleSwipeLeftGesture:(UISwipeGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateEnded) {
        if (0 < _previewIndex) {
            _previewIndex--;
        }
        PreviewData *previewData = [self._previewListData objectAtIndex:_previewIndex];
        [self._detailImageView setImage:previewData.image];
    }
}

// スワイプ(Right)
- (void)handleSwipeRightGesture:(UISwipeGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateEnded) {
        int maxCount = self._previewListData.count;
        if (_previewIndex < maxCount-1) {
            _previewIndex++;
        }
        PreviewData *previewData = [self._previewListData objectAtIndex:_previewIndex];
        [self._detailImageView setImage:previewData.image];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint touchLocation = [touch locationInView:self.view];
        if (!CGRectContainsPoint(self._detailImageView.frame, touchLocation)) {
            [self.view removeFromSuperview];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

@end

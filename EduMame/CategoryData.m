//
//  CategoryData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "CategoryData.h"

@implementation CategoryData

- (id)init
{
    self = [super init];
    if(self){
        self.categoryId = -1;
        self.name = @"";
    }
    return self;
}

- (id)initWithCategoryId:(NSInteger)categoryId name:(NSString*)name
{
    self = [super init];
    if(self){
        self.categoryId = categoryId;
        self.name = name;
    }
    return self;
}

@end

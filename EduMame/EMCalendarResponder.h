//
//  EMCalendarResponder.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/19.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#ifndef EduMame_EMCalendarResponder_h
#define EduMame_EMCalendarResponder_h

#import "EMCalendarComponent.h"

@protocol EMCalendarResponder <NSObject>

- (void)doConstructHeaderView:(UIView*)headerView;

- (void)doReloadHeaderView:(UIView*)headerView;

- (void)doConstructDayView:(UIView*)dayView cell:(FixedPoint)cell;

- (void)doReloadDayView:(UIView*)dayView cell:(FixedPoint)cell;

- (void)touchUpInsideDayButton:(id)sender;

- (void)touchUpInsideDayButton:(id)sender component:(EMCalendarComponent *)component;

- (void)touchUpInsideLeftArrowButton:(id)sender;

- (void)touchUpInsideRightArrowButton:(id)sender;

@end

#endif

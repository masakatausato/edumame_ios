//
//  PreviewGridViewCell.m
//  EduMame
//
//  Created by gclue_mita on 12/10/30.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PreviewGridViewCell.h"

@implementation PreviewGridViewCell

@synthesize previewImageView;

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if (self)
    {
        UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 158, 234)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        
        self.previewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 158, 234)];
        
        [mainView addSubview:previewImageView];
        
        [self.contentView addSubview:mainView];
    }
    
    return self;
}

@end

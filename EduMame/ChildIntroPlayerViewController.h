//
//  ChildIntroPlayerViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMPlayerResponder.h"

@interface ChildIntroPlayerViewController : UIViewController<EMPlayerResponder>

// 読み込みコンテンツ名
@property NSString* contentName;

// ワークブックインデックスID
@property NSInteger workbookIndexId;

@end

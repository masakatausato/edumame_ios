//
//  PrintListData.h
//  EduMame
//
//  Created by gclue_mita on 12/10/18.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrintData : NSObject

@property (nonatomic, retain) UIImage* image;

- (id)initWithImage:(UIImage*)theImage;
+ (NSMutableArray*)getPrintData;

@end

//
//  MyBookGridViewCell.h
//  EduMame
//
//  Created by gclue_mita on 13/02/21.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AQGridViewCell.h"

@interface MyBookGridViewCell : UIView

@property (strong, nonatomic) IBOutlet UILabel* title;
@property (strong, nonatomic) IBOutlet UIProgressView *progress;
@property (strong, nonatomic) IBOutlet UIImageView *coverImageView;
@property (strong, nonatomic) IBOutlet UIButton *detailButton;
@property (strong, nonatomic) IBOutlet UIButton *funButton;

@end

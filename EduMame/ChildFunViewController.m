//
//  ChildFunViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildFunViewController.h"
#import "ChildFunPlayerViewController.h"
#import "DatabaseManager.h"
#import "ContentManager.h"

@interface ChildFunViewController ()
{
    // アイテムリスト
    NSMutableArray* _itemList;
    
    // 選択したコンテンツ名
    NSString* _contentName;
    
    // ボタンシートビュー
    IBOutlet UIView* _buttonSheetView;
    
    NSInteger _currentPage;
    NSInteger _maxPage;
}
@end

@implementation ChildFunViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willPresentAlertView:(UIAlertView *)alertView
{
    // インジケータービューを生成
    UIActivityIndicatorView* activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.frame = CGRectMake(0, alertView.frame.size.height / 2, alertView.frame.size.width, alertView.frame.size.height / 2);
    [activityIndicatorView startAnimating];
    
    // インジケータービューをアラートビューに追加
    [alertView addSubview:activityIndicatorView];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if([identifier isEqualToString:@"ChildFunPlayerSegue"])
    {
        ChildFunPlayerViewController* nextViewController = [segue destinationViewController];
        nextViewController.contentName = _contentName;
    }
}

- (IBAction)touchUpInsideFunButton:(id)sender
{
    NSInteger index = [sender tag];
    NSDictionary* item = [_itemList objectAtIndex:index];
    _contentName = [item objectForKey:@"name"];
    [self performSegueWithIdentifier:@"ChildFunPlayerSegue" sender:self];
}

- (IBAction)doDismiss:(id)sender
{
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    if (index != NSNotFound && index > 0)
    {
        UIViewController* backViewController = [self.navigationController.viewControllers objectAtIndex:(index - 1)];
        [self.navigationController popToViewController:backViewController animated:YES];
    }
}

- (IBAction)onUnwindSegue:(UIStoryboardSegue *)segue
{
}

- (IBAction)onSwipeLeft:(id)sender
{
    if(_currentPage + 1 >= _maxPage) return;
    
    for(NSInteger i = 0; i < [_buttonSheetView.subviews count]; i++)
    {
        UIView* view = [_buttonSheetView.subviews objectAtIndex:i];
        [UIView animateWithDuration:0.5 animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x - view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        }];
    }
    _currentPage++;
}

- (IBAction)onSwipeRight:(id)sender
{
    if(_currentPage <= 0) return;
    
    for(NSInteger i = 0; i < [_buttonSheetView.subviews count]; i++)
    {
        UIView* view = [_buttonSheetView.subviews objectAtIndex:i];
        [UIView animateWithDuration:0.5 animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x + view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        }];
    }
    _currentPage--;
}

// 画面の更新
- (void)refresh
{
    _itemList = [NSMutableArray array];
    _currentPage = 0;
    _maxPage = 0;
 
    NSArray* contentRecords = [DatabaseManager selectContentData:[NSString stringWithFormat:@"WHERE attribute = %d OR attribute = %d", CONTENT_ATTRIBUTE_FUN, CONTENT_ATTRIBUTE_BOTH]];
    for(NSInteger i = 0; i < [contentRecords count]; i++)
    {
        ContentData* contentRecord = [contentRecords objectAtIndex:i];
        
        // アイテムリストに保存
        [_itemList addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:contentRecord.content, contentRecord.title, contentRecord.preview, nil] forKeys:[NSArray arrayWithObjects:@"name", @"title", @"preview", nil]]];
    }
    
    // ボタンシートをクリア
    while([_buttonSheetView.subviews count] > 0)
    {
        [[_buttonSheetView.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    // ボタンを生成
    NSInteger itemIndex = 0;
    while(itemIndex < [_itemList count])
    {
        UIView* sheetView = [[UIView alloc] initWithFrame:CGRectMake(_maxPage * 1024, 0, 1024, 768)];
        sheetView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
                             
        for(NSInteger j = 0; j < 6; j++)
        {
            NSDictionary* item = [_itemList objectAtIndex:itemIndex];
            NSInteger xIndex = j % 3;
            NSInteger yIndex = j / 3;
            CGFloat x = xIndex * 310;
            CGFloat y = yIndex * 310;
            NSString* contentId = [item objectForKey:@"name"];
            NSString* preview = [item objectForKey:@"preview"];
            NSString* title = [item objectForKey:@"title"];
            
            NSString* imageFileName = [[ContentManager contentsPath:contentId] stringByAppendingPathComponent:preview];
            NSData* imageData = [NSData dataWithContentsOfFile:imageFileName];
            UIImage* image = [UIImage imageWithData:imageData];
            
            UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x + 50, y + 50, 300, 300);
            button.tag = itemIndex;
            [button setImage:image forState:UIControlStateNormal];
            [button setTitle:title forState:UIControlStateNormal];
            [button addTarget:self action:@selector(touchUpInsideFunButton:) forControlEvents:UIControlEventTouchUpInside];
            [sheetView addSubview:button];
            
            itemIndex++;
            if(itemIndex >= [_itemList count]) break;
        }

        [_buttonSheetView addSubview:sheetView];
        
        _maxPage++;
    }
}

@end

//
//  MyBookshelfViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/10/17.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "MyBookshelfViewController.h"
#import "MyBookGridViewController.h"
#import "MyBookContentsViewController.h"

#import "DatabaseManager.h"
#import "ContentManager.h"

#define MAX_PRODUCT_IN_THE_PAGE 7

@interface MyBookshelfViewController ()
{
    BOOL pageControlBeingUsed;
    int selectedIndex;
    MyBookState state;
}
@end

@implementation MyBookshelfViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
    [self initializeUIBar];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    pageControlBeingUsed = NO;
    
    // DBからプロダクトデータ取得
    [self getProductDataFromDatabase:nil];
    
    NSMutableArray *bothProductList = [[NSMutableArray array] init];
    NSMutableArray *parentsProductList = [[NSMutableArray array] init];
    NSMutableArray *childProductList = [[NSMutableArray array] init];
    
    self.pageBothProductList = [[NSMutableArray array] init];
    self.pageParentsProductList = [[NSMutableArray array] init];
    self.pageChildProductList = [[NSMutableArray array] init];
    
    // 全てのプロダクトデータをターゲット毎に振り分ける
    int productCnt = [self.productData count];
    for (int i = 0; i < productCnt; i++) {
        ProductData* productData = [self.productData objectAtIndex:i];
        if (productData.targetFlag == CONTENT_TARGET_FLAG_PARENT)
        {
            [parentsProductList addObject:productData];
        }
        else if (productData.targetFlag == CONTENT_TARGET_FLAG_CHILD)
        {
            [childProductList addObject:productData];
        }
        else if (productData.attribute == CONTENT_ATTRIBUTE_LEARNING)
        {
            [bothProductList addObject:productData];
        }
    }
    
    // １ページ８個でセット
    // すべて
    int count = [bothProductList count];
    NSMutableArray *page = [[NSMutableArray array] init];
    int pageNum = 1;
    for (int i = 0; i < count; i++) {
        ProductData* productRecord = [bothProductList objectAtIndex:i];
        [page addObject:productRecord];
        
        if (i == MAX_PRODUCT_IN_THE_PAGE*pageNum) {
            [self.pageBothProductList addObject:page];
            page = [[NSMutableArray array] init];
            pageNum++;
        }
    }
    
    [self.pageBothProductList addObject:page];
    page = [[NSMutableArray array] init];
    
    // 親
    count = [parentsProductList count];
    pageNum = 1;
    for (int i = 0; i < count; i++) {
        ProductData* productRecord = [parentsProductList objectAtIndex:i];
        [page addObject:productRecord];
        
        if (i == MAX_PRODUCT_IN_THE_PAGE*pageNum) {
            [self.pageParentsProductList addObject:page];
            page = [[NSMutableArray array] init];
            pageNum++;
        }
    }

    [self.pageParentsProductList addObject:page];
    page = [[NSMutableArray array] init];
    
    // 子
    count = [childProductList count];
    pageNum = 1;
    for (int i = 0; i < count; i++) {
        ProductData* productRecord = [childProductList objectAtIndex:i];
        [page addObject:productRecord];
        
        if (i == MAX_PRODUCT_IN_THE_PAGE*pageNum) {
            [self.pageChildProductList addObject:page];
            page = [[NSMutableArray array] init];
            pageNum++;
        }
    }
    
    [self.pageChildProductList addObject:page];

    // 遷移時はすべてを表示
    [self.subtitle setText:@"すべての教材"];
    [self setupBothPage];
}

/**
 * initializeUIBar
 */
- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"My本棚"];
    
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"すべて"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedAll:)];
    
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"親"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedParents:)];
    
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"子供"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedChild:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:item3, item2, item1, nil];
}

/**
 * onChangedAll
 * @param: sender
 */
- (void)onChangedAll:(id)sender
{
    [self.subtitle setText:@"すべての教材"];
    
    [self setupBothPage];
    
    state = MYBOOK_STATE_PRODUCTS;
}

/**
 * onChangedParents
 * @param: sender
 */
- (void)onChangedParents:(id)sender
{
    [self.subtitle setText:@"親の教材"];
    
    [self setupParentsPage];
    
    state = MYBOOK_STATE_PARENTS;
}

/**
 * onChangedChild
 * @param: sender
 */
- (void)onChangedChild:(id)sender
{
    [self.subtitle setText:@"子供の教材"];
    
    [self setupChildPage];
    
    state = MYBOOK_STATE_CHILD;
}

/**
 * setupBothPage
 * すべてページ作成
 */
- (void)setupBothPage
{
    [self resetScrollView];
    
    self.currentPageViewController = [[NSMutableArray array] init];
    
    int count = [self.pageBothProductList count];
    
    for (int i = 0; i < count; i++) {
        MyBookGridViewController *myBookGridViewCtrl = [[MyBookGridViewController alloc] init];
        [self.view setBackgroundColor:[UIColor lightGrayColor]];
        myBookGridViewCtrl.productData = [self.pageBothProductList objectAtIndex:i];
        myBookGridViewCtrl.delegate = self;
        myBookGridViewCtrl.state = MYBOOK_STATE_PRODUCTS;
        
        // スクロール分フレームをずらす
        CGRect frame = myBookGridViewCtrl.view.frame;
        frame.origin.x = self.scrollView.frame.size.width * i;
        myBookGridViewCtrl.view.frame = frame;
        
        [self.currentPageViewController addObject:myBookGridViewCtrl];
        
        [self.scrollView addSubview:myBookGridViewCtrl.view];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.pageBothProductList.count, self.scrollView.frame.size.height);
    
    self.pageControl.currentPage = 0;
	self.pageControl.numberOfPages = count;
    
    [self moveCurrentViewController:self.pageControl.currentPage];
}

/**
 * setupParentsPage
 * 親ページ作成
 */
- (void)setupParentsPage
{
    [self resetScrollView];
    
    self.currentPageViewController = [[NSMutableArray array] init];
    
    int count = [self.pageParentsProductList count];
    
    for (int i = 0; i < count; i++) {
        MyBookGridViewController *myBookGridViewCtrl = [[MyBookGridViewController alloc] init];
        [self.view setBackgroundColor:[UIColor lightGrayColor]];
        myBookGridViewCtrl.productData = [self.pageParentsProductList objectAtIndex:i];
        myBookGridViewCtrl.delegate = self;
        myBookGridViewCtrl.state = MYBOOK_STATE_PARENTS;
        
        // スクロール分フレームをずらす
        CGRect frame = myBookGridViewCtrl.view.frame;
        frame.origin.x = self.scrollView.frame.size.width * i;
        myBookGridViewCtrl.view.frame = frame;
        
        [self.currentPageViewController addObject:myBookGridViewCtrl];
        
        [self.scrollView addSubview:myBookGridViewCtrl.view];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * count, self.scrollView.frame.size.height);
    
    self.pageControl.currentPage = 0;
	self.pageControl.numberOfPages = count;
    
    [self moveCurrentViewController:self.pageControl.currentPage];
}

/**
 * setupChildPage
 * 子ページ作成
 */
- (void)setupChildPage
{
    [self resetScrollView];
    
    self.currentPageViewController = [[NSMutableArray array] init];
    
    int count = [self.pageChildProductList count];
    
    for (int i = 0; i < count; i++) {
        MyBookGridViewController *myBookGridViewCtrl = [[MyBookGridViewController alloc] init];
        [self.view setBackgroundColor:[UIColor lightGrayColor]];
        myBookGridViewCtrl.productData = [self.pageChildProductList objectAtIndex:i];
        myBookGridViewCtrl.delegate = self;
        myBookGridViewCtrl.state = MYBOOK_STATE_CHILD;
        
        // スクロール分フレームをずらす
        CGRect frame = myBookGridViewCtrl.view.frame;
        frame.origin.x = self.scrollView.frame.size.width * i;
        myBookGridViewCtrl.view.frame = frame;
        
        [self.currentPageViewController addObject:myBookGridViewCtrl];
        
        [self.scrollView addSubview:myBookGridViewCtrl.view];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * count, self.scrollView.frame.size.height);
    
    self.pageControl.currentPage = 0;
	self.pageControl.numberOfPages = count;
    
    [self moveCurrentViewController:self.pageControl.currentPage];
}

- (void)moveCurrentViewController:(NSInteger)index
{
    [self addChildViewController:[self.currentPageViewController objectAtIndex:index]];
    [[self.currentPageViewController objectAtIndex:index] didMoveToParentViewController:self];
}

- (void)resetScrollView
{
    NSArray *viewsToRemove = [self.scrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
}

#pragma mark - ScrollView Methods

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
	if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	pageControlBeingUsed = NO;
}

- (IBAction)changePage
{
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
	pageControlBeingUsed = YES;
}

#pragma mark - Database Methods

/**
 * getProductDataFromDatabase
 * DBからプロダクトデータ取得
 * @param selectOptions
 */
- (void)getProductDataFromDatabase:(NSString *)selectOptions
{
    self.productData = [[NSMutableArray array] init];
    NSArray* productRecords = [DatabaseManager selectProductData:selectOptions];
    for (NSInteger i = 0; i < [productRecords count]; i++)
    {
        ProductData* productRecord = [productRecords objectAtIndex:i];
        [self.productData addObject:productRecord];
    }
}

#pragma mark - My Book Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
    selectedIndex = index;
    
    // カバーフロー画面遷移
    [self performSegueWithIdentifier:@"MyBookContentsSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    
    if ([identifier isEqualToString:@"MyBookContentsSegue"])
    {
        NSMutableArray *pageData = [self getProductData:self.pageControl.currentPage];
        ProductData *productData = [pageData objectAtIndex:selectedIndex];
        
        MyBookContentsViewController *nextViewController = [segue destinationViewController];
        nextViewController.productsId = productData.productsId;
        nextViewController.productsTitle = productData.title;
    }
}

- (NSMutableArray *)getProductData:(NSInteger)index
{
    if (state == MYBOOK_STATE_PRODUCTS) {
        return [self.pageBothProductList objectAtIndex:index];
    }
    else if (state == MYBOOK_STATE_PARENTS) {
        return [self.pageParentsProductList objectAtIndex:index];
    }
    else {
        return [self.pageChildProductList objectAtIndex:index];
    }
}

@end

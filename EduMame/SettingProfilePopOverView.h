//
//  ProfilePopOverView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"

@interface SettingProfilePopOverView : UIViewController

@property (strong, nonatomic) ParentData *parentData;
@property (strong, nonatomic) ChildrenData *childrenData;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *userId;
@property (strong, nonatomic) IBOutlet UITextField *parentName;
@property (strong, nonatomic) IBOutlet UITextField *parentMail;
@property (strong, nonatomic) IBOutlet UITextField *childName;
@property (strong, nonatomic) IBOutlet UITextField *childBirthday;
@property (strong, nonatomic) IBOutlet UISegmentedControl *childGender;

@end

//
//  ChildCalendarViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildCalendarViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "EMCalendarView.h"
#import "DatabaseManager.h"

@interface ChildCalendarViewController ()
{
    IBOutlet EMCalendarView* _calendarView;
    IBOutlet UIImageView* _activeColorImage;
    NSInteger _currentStamp;
}
@end

@implementation ChildCalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _calendarView.delegate = self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [_calendarView constructCalendar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchUpInsideStampButton:(id)sender
{
    UIButton* button = sender;
    _currentStamp = button.tag;
    
    _activeColorImage.center = button.center;
}

- (IBAction)onSwipeLeft:(id)sender
{
    [_calendarView previousMonth];
}

- (IBAction)onSwipeRight:(id)sender
{
    [_calendarView nextMonth];
}

- (IBAction)doDismiss:(id)sender
{
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    if (index != NSNotFound && index > 0)
    {
        UIViewController* backViewController = [self.navigationController.viewControllers objectAtIndex:(index - 1)];
        [self.navigationController popToViewController:backViewController animated:YES];
    }
}

- (void)doConstructHeaderView:(UIView*)headerView
{
    headerView.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:237.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
    headerView.layer.borderColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:158.0f/255.0f alpha:1.0f].CGColor;
    headerView.layer.borderWidth = 1.0f;
}

- (void)doReloadHeaderView:(UIView*)headerView
{
    
}

- (void)doConstructDayView:(UIView*)dayView cell:(FixedPoint)cell
{
    CGRect bounds = dayView.bounds;
    
    dayView.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:237.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
    dayView.layer.borderColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:158.0f/255.0f alpha:1.0f].CGColor;
    dayView.layer.borderWidth = 1.0f;

    CGFloat stampSize = (bounds.size.width > bounds.size.height) ? bounds.size.height : bounds.size.width;
    UIImageView* stampImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, stampSize, stampSize)];
    stampImage.center = CGPointMake(bounds.size.width / 2, bounds.size.height / 2);
    [dayView addSubview:stampImage];
}

- (void)doReloadDayView:(UIView*)dayView cell:(FixedPoint)cell
{
    NSDateComponents* currentComponents = [_calendarView.calendarComponent components];
    NSDateComponents* components = [_calendarView.calendarComponent componentsOfCell:cell];
    
    NSString* dayName = [NSString stringWithFormat:@"%04d-%02d-%02d", components.year, components.month, components.day];
    ChildCalendarEventData* childCalendarEventRecord = [DatabaseManager first:[DatabaseManager selectChildCalendarEventData:[NSString stringWithFormat:@"WHERE date = \"%@\"", dayName]]];

    UILabel* dayLabel = [_calendarView labelFromDayView:dayView];
    if(currentComponents.month == components.month)
    {
        if(childCalendarEventRecord)
        {
            if(cell.x == 0) dayLabel.textColor = [UIColor colorWithRed:202.0f/255.0f green:61.0f/255.0f blue:27.0f/255.0f alpha:1.0f];
            else if(cell.x == 6) dayLabel.textColor = [UIColor colorWithRed:25.0f/255.0f green:112.0f/255.0f blue:127.0f/255.0f alpha:1.0f];
            else dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:139.0f/255.0f alpha:1.0f];
        }
        else
        {
            if(cell.x == 0) dayLabel.textColor = [UIColor colorWithRed:202.0f/255.0f green:61.0f/255.0f blue:27.0f/255.0f alpha:0.25f];
            else if(cell.x == 6) dayLabel.textColor = [UIColor colorWithRed:25.0f/255.0f green:112.0f/255.0f blue:127.0f/255.0f alpha:0.25f];
            else dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:139.0f/255.0f alpha:0.25f];
        }
    }
    else
    {
        if(childCalendarEventRecord)
        {
            dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:221.0f/255.0f green:210.0f/255.0f blue:200.0f/255.0f alpha:1.0f];
        }
        else
        {
            dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:221.0f/255.0f green:210.0f/255.0f blue:200.0f/255.0f alpha:0.25f];
        }
    }
    
    UIImageView* stampImage = (UIImageView*)[_calendarView userViewFromDayView:dayView index:0];
    if(childCalendarEventRecord && (childCalendarEventRecord.evaluation > 0))
    {
        stampImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"ChildMame%02d", childCalendarEventRecord.evaluation]];
    }
    else
    {
        stampImage.image = nil;
    }
}

- (void)touchUpInsideDayButton:(id)sender
{
    NSInteger tag = [sender tag];
    UIView* dayView = [_calendarView dayViewFromTag:tag];
    if(dayView == nil) return;

    FixedPoint cell = [_calendarView cellOfTag:tag];
    NSDateComponents* components = [_calendarView.calendarComponent componentsOfCell:cell];
    NSString* dayName = [NSString stringWithFormat:@"%04d-%02d-%02d", components.year, components.month, components.day];
    ChildCalendarEventData* childCalendarEventRecord = [DatabaseManager first:[DatabaseManager selectChildCalendarEventData:[NSString stringWithFormat:@"WHERE date = \"%@\"", dayName]]];
    if(childCalendarEventRecord)
    {
        childCalendarEventRecord.evaluation = 1 + _currentStamp;
        [DatabaseManager updateChildCalendarEventData:[NSArray arrayWithObject:childCalendarEventRecord]];
    }
    else
    {
        childCalendarEventRecord = [[ChildCalendarEventData alloc] init];
        childCalendarEventRecord.date = dayName;
        childCalendarEventRecord.evaluation =  1 + _currentStamp;
        [DatabaseManager insertChildCalendarEventData:[NSArray arrayWithObject:childCalendarEventRecord] lastInsertRowId:nil];
    }

    UIImageView* stampImage = (UIImageView*)[_calendarView userViewFromDayView:dayView index:0];
    stampImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"ChildMame%02d", 1 + _currentStamp]];
}

- (void)touchUpInsideLeftArrowButton:(id)sender
{
    [_calendarView previousMonth];    
}

- (void)touchUpInsideRightArrowButton:(id)sender
{
    [_calendarView nextMonth];    
}

@end

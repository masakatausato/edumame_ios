//
//  ChildHomeSettingsModalPanel.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/22.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@interface ChildHomeSettingsModalPanel : UATitledModalPanel

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

@end

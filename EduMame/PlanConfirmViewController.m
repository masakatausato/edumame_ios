//
//  PlanConfirmViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/10/17.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PlanConfirmViewController.h"
#import "PlanConfirmTableViewController.h"

@interface PlanConfirmViewController ()
{
}
@end

@implementation PlanConfirmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;

    NSInteger filter = 0;
    if([identifier isEqualToString:@"PlanConfirmTableAllSegue"])
    {
        filter = PlanConfirmTableFilterAll;
    }
    else if([identifier isEqualToString:@"PlanConfirmTableMissSegue"])
    {
        filter = PlanConfirmTableFilterMiss;
    }
    else if([identifier isEqualToString:@"PlanConfirmTableNotAnswerSegue"])
    {
        filter = PlanConfirmTableFilterNotAnswer;
    }
    else if([identifier isEqualToString:@"PlanConfirmTableNotScoringSegue"])
    {
        filter = PlanConfirmTableFilterNotScoring;
    }
    else if([identifier isEqualToString:@"PlanConfirmTableFinishScoringSegue"])
    {
        filter = PlanConfirmTableFilterFinishScoring;
    }
    
    PlanConfirmTableViewController* nextViewController = [segue destinationViewController];
    nextViewController.filter = filter;
    nextViewController.historyEnable = YES;
}

@end

//
//  PlanConfirmTableColumn.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanConfirmTableColumn : NSObject

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* key;
@property (nonatomic) CGFloat width;
@property (nonatomic) UITextAlignment textAlignment;
@property (nonatomic) BOOL buttonEnable;

- (id)init;
- (id)initWithName:(NSString*)name key:(NSString*)key width:(CGFloat)width textAlignment:(UITextAlignment)textAlignment buttonEnable:(BOOL)buttonEnable;

+ (id)column;
+ (id)columnWithName:(NSString*)name key:(NSString*)key width:(CGFloat)width textAlignment:(UITextAlignment)textAlignment buttonEnable:(BOOL)buttonEnable;

@end

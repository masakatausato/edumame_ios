//
//  PlanConfirmTableViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanConfirmTableViewCell.h"

enum
{
    PlanConfirmTableFilterAll = 0,
    PlanConfirmTableFilterMiss,
    PlanConfirmTableFilterNotAnswer,
    PlanConfirmTableFilterNotScoring,
    PlanConfirmTableFilterFinishScoring,
};

@interface PlanConfirmTableViewController : UIViewController <PlanConfirmTableViewCellResponder>

@property (nonatomic) NSInteger filter;
@property (nonatomic) BOOL contentIdFilterEnable;
@property (nonatomic) NSInteger contentIdFilter;
@property (nonatomic) BOOL historyEnable;

@end

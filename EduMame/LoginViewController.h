//
//  LoginViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/01/23.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordPopOverView.h"
#import "UAModalPanel.h"
#import "DatabaseManager.h"

@class UILabelUnderlined;

@interface LoginViewController : UIViewController <PasswordViewDelegate, UAModalPanelDelegate>

@property (strong, nonatomic) UIPopoverController *popOverController;
@property (strong, nonatomic) PasswordPopOverView *passwordPopOverView;
@property (strong, nonatomic) IBOutlet UIButton *parentButton;
//@property (strong, nonatomic) UILabelUnderlined *reminderLabel;
@property (strong, nonatomic) IBOutlet UIButton *reminderButton;
@property (weak, nonatomic) IBOutlet UILabel *parentName;
@property (weak, nonatomic) IBOutlet UILabel *childName;

@property (strong, nonatomic) ParentData *parentData;
@property (strong, nonatomic) ChildrenData *childrenData;

- (IBAction)showPasswordPopupOverView:(id)sender;
- (IBAction)onReminder:(id)sender;

@end

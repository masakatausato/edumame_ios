//
//  ManualProductsViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/19.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridViewController.h"

@interface ManualProductsViewController : UIViewController <
GridViewDelegate>

@property (strong, nonatomic) GridViewController *gridViewCtrl;
@property (weak, nonatomic) IBOutlet UIView *topContainerView;

@property (strong, nonatomic) NSMutableArray *productData;
@property (strong, nonatomic) NSMutableArray *contentData;

@end

//
//  UIScrollView+UIScrollView_TouchEvent.m
//  EduMame
//
//  Created by gclue_mita on 13/02/06.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UIScrollView+UIScrollView_TouchEvent.h"

@implementation UIScrollView (UIScrollView_TouchEvent)

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self nextResponder] touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self nextResponder] touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

@end

//
//  AnswerdContentIndexData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnswerdContentIndexData : NSObject

@property (nonatomic) NSInteger contentId;
@property (nonatomic) NSInteger progress;
@property (nonatomic) NSInteger productId;

- (id)init;

- (id)initWithContentId:(NSInteger)contentId progress:(NSInteger)progress productId:(NSInteger)productId;

@end

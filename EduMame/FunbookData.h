//
//  FunbookData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FunbookData : NSObject

@property (nonatomic) NSInteger funbookId;
@property (nonatomic) NSInteger productId;
@property (nonatomic) NSInteger childId;

- (id)init;

- (id)initWithIFunbookId:(NSInteger)funbookId productId:(NSString*)productId childId:(NSInteger)childId;

@end

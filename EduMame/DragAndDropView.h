//
//  DragAndDropView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ContentData;

@interface DragAndDropView : UIView

// 長押選択プリント画像
@property (strong, nonatomic) UIImageView *view;

// コンテンツデータ
@property (strong, nonatomic) ContentData *contentData;

// コンテンツ配列
@property (strong, nonatomic) NSMutableArray *contentArray;

@end

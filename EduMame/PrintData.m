//
//  PrintData.m
//  EduMame
//
//  Created by gclue_mita on 12/10/18.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PrintData.h"

@implementation PrintData

@synthesize image;

- (id)initWithImage:(UIImage*)theImage
{
    self = [self init];
    if (self) {
        self.image = theImage;
    }
    return self;
}

+ (NSMutableArray*)getPrintData
{
    NSMutableArray *prints = [[NSMutableArray alloc] initWithCapacity: 8];
    PrintData *print1 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_1.png"]];
    PrintData *print2 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_2.png"]];
    PrintData *print3 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_3.png"]];
    PrintData *print4 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_4.png"]];
    PrintData *print5 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_5.png"]];
    PrintData *print6 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_6.png"]];
    PrintData *print7 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_7.png"]];
    PrintData *print8 = [[PrintData alloc] initWithImage:[UIImage imageNamed:@"print_sample_8.png"]];
    
    [prints addObject:print1.image];
    [prints addObject:print2.image];
    [prints addObject:print3.image];
    [prints addObject:print4.image];
    [prints addObject:print5.image];
    [prints addObject:print6.image];
    [prints addObject:print7.image];
    [prints addObject:print8.image];
    
    return prints;
}

@end

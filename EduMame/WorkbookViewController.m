//
//  WorkbookViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/10/17.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "WorkbookViewController.h"

#import "CreateManager.h"

#import "ManualSearchConditionData.h"

#import "PlanViewController.h"
#import "ManualViewController.h"
#import "ManualProductsViewController.h"
#import "AutoDayViewController.h"
#import "AutoWeekViewController.h"

#import "WorkBookCalendarView.h"

@interface WorkbookViewController ()
{
    CreateState state;
    NSInteger workbookId;
    ManualSearchConditionData *searchConditionData;
}
@end

@implementation WorkbookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.calendarView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 他の画面から戻った時にリロード
    [self.calendarView reloadCalendar];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Calendar Methods

- (void)touchUpInsideDayButton:(id)sender component:(EMCalendarComponent *)component
{
    NSInteger tag = [sender tag];
    
    FixedPoint cell = [self calcXY:tag];
    NSDateComponents* components = [component componentsOfCell:cell];

    // 日付取得
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDate* dt = [cal dateFromComponents:components];
    NSString *date = [CreateManager getFormatDate:dt dateFormat:@"yyyy-MM-dd"];
    
    // タップした日付をCreateManagerで保持
    [[CreateManager sharedCreateManager] setTapDate:date];
    
    // WorkbookData検索
    NSString* option = @"where name =";
    option = [option stringByAppendingFormat:@" '%@'", date];
    
    NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
    
    // プリントデータがある場合はデータ取得
    if ([targetWorkbookRecord count] > 0)
    {
        WorkbookData *workbookData = nil;
        workbookData = [targetWorkbookRecord objectAtIndex:0];
        
        // WorkbookIndexData検索
        option = @"where workbook_id =";
        option = [option stringByAppendingFormat:@" '%d'", workbookData.workbookId];
        
        NSArray *targetWorkbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
        
        for (int i = 0; i < [targetWorkbookIndexRecord count]; i++) {
            workbookId = workbookData.workbookId;
        }
        
        [self gotoPlanViewController];
    }
    else
    {
        // プリントが作成されていない場合、検索か自動生成を選択させる
        self.modalPanel = [[SelectTypeToCreateModalPanel alloc] initWithFrame:self.view.bounds title:@"作成方法選択" isCreate:YES];
        self.modalPanel.delegate = self;
        self.modalPanel.selectTypeDelegate = self;
    
        [self.view addSubview:self.modalPanel];
        [self.modalPanel show];
    }
}

- (void)touchUpInsideLeftArrowButton:(id)sender
{
    [_calendarView previousMonth];
}

- (void)touchUpInsideRightArrowButton:(id)sender
{
    [_calendarView nextMonth];
}

- (FixedPoint)calcXY:(int)tag
{
    FixedPoint cell = {((tag-100) % 7) ,  (int)((tag-100)/7)};
    return cell;
}

//- (IBAction)movePreviousMonth:(id)sender
//{
//    [self.calendarView previousMonth];
//}
//
//- (IBAction)moveNextMonth:(id)sender
//{
//    [self.calendarView nextMonth];
//}
//
//- (IBAction)onSwipeLeft:(id)sender
//{
//    [self.calendarView previousMonth];
//}
//
//- (IBAction)onSwipeRight:(id)sender
//{
//    [self.calendarView nextMonth];
//}

#pragma mark - View Methods

- (void)gotoPlanViewController
{
   [self setViewController:@"CreateIdentifier" state:STATE_PLAN];
}

- (void)notifyManualSearch:(id)sender target:(NSInteger)target category:(NSString *)category level:(NSString *)level term:(NSString *)term
{
    // 検索条件を作成
    searchConditionData = [[ManualSearchConditionData alloc] init];
    [searchConditionData setTarget:target];
    [searchConditionData setCategory:category];
    [searchConditionData setLevel:level];
    [searchConditionData setTerm:term];
    
    // 検索条件をCreateManagerで保持
    [[CreateManager sharedCreateManager] setSearchConditionData:searchConditionData];

    [self setViewController:@"CreateIdentifier" state:STATE_MANUAL];
}

- (void)notifyAutoDay:(id)sender
{
    [self setViewController:@"CreateIdentifier" state:STATE_AUTO_DAY];
}

- (void)notifyAutoWeek:(id)sender
{
    [self setViewController:@"CreateIdentifier" state:STATE_AUTO_WEEK];
}

- (void)setViewController:(NSString *)identifier state:(CreateState)aState
{
    // モーダルダイアログ非表示
    [self.modalPanel hide];
#if 0
    CreateViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    viewController.createState = state;
    viewController.workbookId = workbookId;
    viewController.searchConditionData = searchConditionData;
    [self.navigationController pushViewController:viewController animated:YES];
#endif
    
    switch (aState) {
        case STATE_PLAN:
            [self initializePlan];
            break;
            
        case STATE_MANUAL:
            [self initializeManual];
            break;
            
        case STATE_AUTO_DAY:
            [self initializeAutoDay];
            break;
            
        case STATE_AUTO_WEEK:
            [self initializeAutoWeek];
            break;
            
        default:
            break;
    }
}

/**
 * initializePlan
 */
- (void)initializePlan
{
    [self performSegueWithIdentifier:@"PlanSegue" sender:self];
}

/**
 * initializeManual
 */
- (void)initializeManual
{
    NSInteger target = [searchConditionData target];
    if (target == MANUAL_STATE_PRODUCTS) {
        [self performSegueWithIdentifier:@"ManualProductsSegue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"ManualSegue" sender:self];
    }
}

/**
 * initializeAutoDay
 */
- (void)initializeAutoDay
{
    [self performSegueWithIdentifier:@"AutoDaySegue" sender:self];
}

/**
 * initializeAutoWeek
 */
- (void)initializeAutoWeek
{
    [self performSegueWithIdentifier:@"AutoWeekSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    
    if ([identifier isEqualToString:@"PlanSegue"])
    {
        self.planViewCtrl = [segue destinationViewController];
        self.planViewCtrl.workbookId = workbookId;
    }
    
    if ([identifier isEqualToString:@"ManualProductsSegue"])
    {
        self.manualProductsViewCtrl = [segue destinationViewController];
    }
    
    if ([identifier isEqualToString:@"ManualSegue"])
    {
        self.manualViewCtrl = [segue destinationViewController];
    }
    
    if ([identifier isEqualToString:@"AutoDaySegue"])
    {
        self.autoDayViewCtrl = [segue destinationViewController];
    }
    
    if ([identifier isEqualToString:@"AutoWeekSegue"])
    {
        self.autoWeekViewCtrl = [segue destinationViewController];
    }
}

- (void)viewDidUnload
{
    [self setCalendarView:nil];
    [super viewDidUnload];
}

@end

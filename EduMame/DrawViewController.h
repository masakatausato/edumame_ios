//
//  DrawViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/12/11.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAModalPanel.h"

@interface DrawViewController : UIViewController<UAModalPanelDelegate>
{
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
    int colorPressedTag;
    BOOL isShowDrawSettings;
    BOOL isShowPalette;
    NSTimer* menuTimer;
}

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UIImageView *tempDrawImage;
@property (weak, nonatomic) IBOutlet UIImageView *activeColorImage;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationDrawItem;
@property (weak, nonatomic) IBOutlet UIView *paletteView;
@property (weak, nonatomic) IBOutlet UIView *menuView;

- (IBAction)colorPressed:(id)sender;
- (IBAction)eraserPressed:(id)sender;
- (IBAction)settingPressed:(id)sender;

- (void)save:(id)sender;
- (void)trash:(id)sender;

@property (strong) CAEmitterLayer *starEmitter;

@end

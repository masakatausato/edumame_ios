//
//  MyBookContentsViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVFoundation.h>

#import "MyBookContentsViewController.h"

#import "CreateManager.h"

#import "DetailViewController.h"
#import "DragAndDropView.h"
#import "ContentDateViewController.h"

#import "ContentManager.h"

@interface MyBookContentsViewController ()

@end

@implementation MyBookContentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initialize];
    [self initializeUIBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    // タイトルセット
    [self.productsTitleLabel setText:self.productsTitle];
    
    // 日付格納配列初期化
    self.dropDateTargets = [NSMutableArray array];
    
    // コンテンツデータ取得
    [self getContentDataFromDatabase:nil];

    // CoverFlowView
    self.coverflowViewCtrl = [[CoverFlowViewController alloc] init];
    self.coverflowViewCtrl.itemList = [self.contentData mutableCopy];
    self.coverflowViewCtrl.tapDelegate = self;
    self.coverflowViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.coverflowViewCtrl];
    [self.coverflowViewCtrl didMoveToParentViewController:self];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.itemList = [self.contentData mutableCopy];
    self.gridViewCtrl.tapDelegate = self;
    self.gridViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    // DateView
    self.dateViewCtrl = [[DateViewController alloc] init];
    self.dateViewCtrl.delegate = self;
    self.dateViewCtrl.date = [NSDate date];
    
    [self addChildViewController:self.dateViewCtrl];
    [self.dateViewCtrl didMoveToParentViewController:self];
    
    // top->CoverFlowView
    // bottom->DateView
    [self.topContainerView addSubview:self.coverflowViewCtrl.view];
    [self.bottomContainerView addSubview:self.dateViewCtrl.view];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"My本棚"];
    
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"グリッド"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedView:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:item, nil];
}

- (void)onChangedView:(id)sender
{
    UIViewController *fromViewCtrl;
    UIViewController *toViewCtrl;
    
    if (self.coverflowViewCtrl.isViewLoaded && self.coverflowViewCtrl.view.superview) {
        fromViewCtrl = self.coverflowViewCtrl;
        toViewCtrl = self.gridViewCtrl;
        
        [self.navigationItem.rightBarButtonItem setTitle:@"カバーフロー"];
    } else {
        fromViewCtrl = self.gridViewCtrl;
        toViewCtrl = self.coverflowViewCtrl;
        
        [self.navigationItem.rightBarButtonItem setTitle:@"グリッド"];
    }
    
    [fromViewCtrl.view.superview addSubview:toViewCtrl.view];
    [fromViewCtrl.view removeFromSuperview];
    
    [self.view setNeedsLayout];
}

/**
 * getContentDataFromDatabase
 * DBからコンテンツデータ取得
 * @param selectOptions
 */
- (void)getContentDataFromDatabase:(NSString *)selectOptions
{
    self.contentData = [[NSMutableArray array] init];
    
    NSString* option = @"where product_id =";
    option = [option stringByAppendingFormat:@" '%ld'", (long)self.productsId];
    
    NSArray* productIndexRecords = [DatabaseManager selectProductIndexData:option];
    for (NSInteger i = 0; i < [productIndexRecords count]; i++)
    {
        ProductIndexData* productIndexRecord = [productIndexRecords objectAtIndex:i];
        
        option = @"where contents_id =";
        option = [option stringByAppendingFormat:@" '%ld'", (long)productIndexRecord.contentId];
        
        NSArray* contentRecords = [DatabaseManager selectContentData:option];
        for (NSInteger i = 0; i < [contentRecords count]; i++)
        {
            ContentData* contentRecord = [contentRecords objectAtIndex:i];
            [self.contentData addObject:contentRecord];
        }
    }
}

- (UIImage *)getPreviewFromContent:(NSUInteger)index
{
    ContentData* contentRecord = [self getContentData:index];
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

- (ContentData *)getContentData:(NSUInteger)index
{
    return [self.contentData objectAtIndex:index];
}

#pragma mark - Cover Flow View & Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
    ContentData* contentData = [self getContentData:index];
    self.resourceList = [CreateManager getContentArrayFromJSON:contentData.content];
    
    UIImage *image = [self.resourceList objectAtIndex:0];
    
    // 詳細View作成
    self.detailViewCtrl = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    self.detailViewCtrl.index = 0;
    self.detailViewCtrl.itemList = self.resourceList;
    self.detailViewCtrl.image = image;
    
    // 印刷ボタンは非表示
    [self.detailViewCtrl setIsPrintHidden:YES];
    
    [self.view addSubview:self.detailViewCtrl.view];
}

#pragma mark - Gesture Recogniser Delegate

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer
                          index:(NSInteger)index
{
    [self setDragAndDropImage:recognizer index:index];
    [self setScaleAnimToDragAndDropImage:recognizer];
}

- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer
{
    if (self.dragAndDropView == nil)
        return;
    
    CGPoint touchPoint = [recognizer locationInView:self.view];
    [self.dragAndDropView setCenter:touchPoint];
}

- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.view];
    [self.dragAndDropView setCenter:touchPoint];
    
    DateView *dropTarget = [self dropTargetHitByPoint:touchPoint];
    
    if (dropTarget == nil && self.lastDropDateTargets != nil) {
        [self setHighlightView:self.lastDropDateTargets highlighted:NO animated:YES];
        self.lastDropDateTargets = nil;
    }
    
    if (dropTarget != nil) {
        [self setHighlightView:dropTarget highlighted:YES animated:YES];
        self.lastDropDateTargets = dropTarget;
        
        // DB登録
        [self makeWorkbookData:self.dragAndDropView dateView:dropTarget];
        
        // ドラッグ画像削除
        [self removeDragAndDropView];
        
        // リロード
        [self.dateViewCtrl reloadData];
    }
    
    [self collapseDragView];
}

#pragma mark - Date View Delegate

- (void)notifyAddSubView:(DateView *)dateView
{
    [self.dropDateTargets addObject:dateView];
}

- (void)notifyTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [[touches anyObject] locationInView:self.view];
    dateTarget = [self dropTargetHitByPoint:touchPoint];
    
    // 日付タップ時
    if (dateTarget)
    {
        // DBから日付に該当するコンテンツデータを取得しContentDateViewControllerに渡す
        
        self.dropContentData = [[NSMutableArray array] init];
        
        // 日付取得
        NSDate *date = dateTarget.date;
        
        // WorkbookData検索
        NSString* option = @"where name =";
        option = [option stringByAppendingFormat:@" '%@'", [CreateManager getFormatDate:date dateFormat:@"yyyy-MM-dd"]];
        
        NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
        
        WorkbookData *workbookData = nil;
        if ([targetWorkbookRecord count] > 0) {
            
            workbookData = [targetWorkbookRecord objectAtIndex:0];
            
            // WorkbookIndexData検索
            option = @"where workbook_id =";
            option = [option stringByAppendingFormat:@" '%d'", workbookData.workbookId];
            
            NSArray *targetWorkbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
            
            for (int i = 0; i < [targetWorkbookIndexRecord count]; i++) {
                WorkbookIndexData *workbookIndexData = [targetWorkbookIndexRecord objectAtIndex:i];
                
                // ContentData検索
                option = @"where contents_id =";
                option = [option stringByAppendingFormat:@" '%d'", workbookIndexData.contentId];
                NSArray *targetContentRecord = [DatabaseManager selectContentData:option];
                
                for (int j = 0; j < [targetContentRecord count]; j++) {
                    ContentData *contentData = [targetContentRecord objectAtIndex:j];
                    [self.dropContentData addObject:contentData];
                }
            }
        }
        
        [self performSegueWithIdentifier:@"ContentDateSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    
    if ([identifier isEqualToString:@"ContentDateSegue"])
    {
        ContentDateViewController *nextViewController = [segue destinationViewController];
        nextViewController.contentData = self.dropContentData;
        nextViewController.date = dateTarget.date;
    }
}

#pragma mark - Drag Methods

/**
 * setDragAndDropImage
 * ドラッグ画像セット
 * @param recognizer
 * @param index
 */
- (void)setDragAndDropImage:(UIGestureRecognizer *)recognizer
                      index:(NSInteger)index
{
    UIImage *img = [self getPreviewFromContent:index];
    
    UIImage *resultImage;
    CGSize size = CGSizeMake(200, 150);
    UIGraphicsBeginImageContext(size);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:resultImage];
    
    self.dragAndDropView = [[DragAndDropView alloc] initWithFrame:iv.frame];
    [self.dragAndDropView.view setImage:img];
    [self.dragAndDropView setContentData:[self getContentData:index]];
    [self.dragAndDropView addSubview:iv];
    [self.dragAndDropView setCenter:[recognizer locationInView:self.view]];
    
    [self.view addSubview:self.dragAndDropView];
}

/**
 * setScaleAnimToDragAndDropImage
 * ドラッグ画像にスケールアニメーションセット
 * @param recognizer
 */
- (void)setScaleAnimToDragAndDropImage:(UIGestureRecognizer *)recognizer
{
    [UIView beginAnimations: @"" context: NULL];
    [UIView setAnimationDuration: 0.2f];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
    
    // transformation-- larger, slightly transparent
    self.dragAndDropView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    // also make it center on the touch point
    self.dragAndDropView.center = [recognizer locationInView:self.view];
    
    [UIView commitAnimations];
}

/**
 * ドロップ時アニメーション
 */
- (void)collapseDragView
{
	[UIView beginAnimations:@"DropSuck" context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:.3];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(cancelAnimationDidStop:finished:context:)];
	
	self.dragAndDropView.transform = CGAffineTransformMakeScale(0.001f, 0.001f);
	self.dragAndDropView.alpha = 0.0f;
	self.dragAndDropView.center = CGPointMake(self.dragAndDropView.center.x, self.dragAndDropView.center.y);
	
	[UIView commitAnimations];
}

/**
 * ドラッグビューと日付ビュー当たり判定
 */
- (DateView *)dropTargetHitByPoint:(CGPoint)point
{
	for (DateView *dropTarget in self.dropDateTargets) {
		CGRect frameInWindow = [[dropTarget superview] convertRect:dropTarget.frame toView:self.view];
		if (CGRectContainsPoint(frameInWindow, point)) {
            return dropTarget;
		}
	}
	
	return nil;
}

/**
 * ドラッグした日付ビューアニメーション
 */
- (void)setHighlightView:(UIView *)view highlighted:(BOOL)highlighted animated:(BOOL)animated
{
	CALayer *dropLayer = view.layer;
	
	if (animated) {
		[UIView beginAnimations: @"HighlightView" context: NULL];
		[UIView setAnimationCurve: UIViewAnimationCurveLinear];
		[UIView setAnimationBeginsFromCurrentState: YES];
	}
    
	if ([dropLayer respondsToSelector: @selector(setShadowPath:)] &&
        [dropLayer respondsToSelector: @selector(shadowPath)])
    {
		if (highlighted) {
			CGMutablePathRef path = CGPathCreateMutable();
			CGPathAddRect(path, NULL, dropLayer.bounds);
			dropLayer.shadowPath = path;
			CGPathRelease(path);
			
			dropLayer.shadowOffset = CGSizeZero;
			dropLayer.shadowColor = [[UIColor blueColor] CGColor];
			dropLayer.shadowRadius = 12.0f;
			dropLayer.shadowOpacity = 1.0f;
		} else {
			dropLayer.shadowOpacity = 0.0f;
		}
	}
	
	if (animated) {
        [UIView commitAnimations];
    }
}

/**
 * removeDragAndDropView
 * ドラッグ画像を削除
 */
- (void)removeDragAndDropView
{
    [self.dragAndDropView removeFromSuperview];
    self.dragAndDropView = nil;
}

#pragma mark - Database Methods

/**
 * makeWorkbookData
 */
- (void)makeWorkbookData:(DragAndDropView *)dragAndDropView dateView:(DateView *)dropTarget
{
    WorkbookData *workbookData = [CreateManager updateWorkbookRecord:dropTarget.date];
    [CreateManager updateWorkbookIndexRecord:workbookData contentData:dragAndDropView.contentData];
}

@end

//
//  RemoveConfirmViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/14.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "RemoveConfirmViewController.h"

@interface RemoveConfirmViewController ()

@end

@implementation RemoveConfirmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        CGRect rect = [self.view frame];
        rect.origin.y = 550;
        [self.view setFrame:rect];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onRemove:(id)sender
{
}

- (IBAction)onCancel:(id)sender
{
    [self.view removeFromSuperview];
}

@end

//
//  ContentViewViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ContentViewController.h"

#import "CreateManager.h"

#import "ContentView.h"
#import "DatabaseManager.h"

@interface ContentViewController ()

@end

@implementation ContentViewController

@synthesize itemList = _contentsData;
@synthesize scrollView = _scrollView;
@synthesize tapDelegate = _tapDelegate;
@synthesize gestureDelegate = _gestureDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Content View Methods

- (void)initialize
{    
    int contentNum = self.itemList.count;

    self.scrollView.contentSize = CGSizeMake(150.0*(contentNum+1), 198.0f);
    [self.scrollView setScrollEnabled:YES];
    
    for (int i = 0; i < contentNum; i++) {
        ContentView *contentView = [[ContentView alloc] initWithFrame:CGRectMake(30.0f+(i*(150.0f+40.0f)), 25.0f, 150.0f, 100.0f)];
        
        [self setContentView:contentView index:i];
        
        // スクロールビューに格納
        [self.scrollView addSubview:contentView];
        
        // 日付オブジェクト格納
//        [self.dropDateTargets addObject:contentView];
    }
    
    [self.view addSubview:self.scrollView];
}

#pragma mark -

//- (UIImage *)getPreviewFromContent:(NSUInteger)index
//{
//    ContentData* contentRecord = [self getContentData:index];
//    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
//    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
////    NSLog(@"previewPath = %@", previewPath);
//    
//    return [UIImage imageWithContentsOfFile:previewPath];
//}

- (ContentData *)getContentData:(NSUInteger)index
{
    return [self.itemList objectAtIndex:index];
}

/**
 * setDragAndDropImage
 * ドラッグ画像セット
 * @param recognizer
 * @param cell
 */
- (void)setContentView:(ContentView *)contentView
                 index:(NSInteger)index
{
    UIImage *img = [CreateManager getPreviewFromContent:[self getContentData:index] index:index];
    
    UIImage *resultImage;
    CGSize size = CGSizeMake(150, 100);
    UIGraphicsBeginImageContext(size);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:resultImage];
    
    [contentView.imageView setImage:img];
    [contentView setContentData:[self getContentData:index]];
    [contentView addSubview:iv];
    
    [self.view addSubview:contentView];
}

@end

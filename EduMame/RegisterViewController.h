//
//  RegisterViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/01/22.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerPopOverView.h"

@interface RegisterViewController : UIViewController <PickerPopOverViewDelegate, UITextFieldDelegate>
{
    /** 親情報 */
    NSString *_parentID;
    NSString *_parentName;
    NSString *_parentMail;
    NSString *_parentSecretQuestion;
    NSString *_parentSecretAnswer;
    
    /** 子情報 */
    NSString *_childName;
    NSString *_childYearAndMonth;
    NSInteger _childGender;
}

@property (strong, nonatomic) IBOutlet UILabel *info;
@property (strong, nonatomic) IBOutlet UITextField *pID;
@property (strong, nonatomic) IBOutlet UITextField *pName;
@property (strong, nonatomic) IBOutlet UITextField *pMail;
@property (strong, nonatomic) IBOutlet UITextField *pSecretQuestion;
@property (strong, nonatomic) IBOutlet UITextField *pSecretAnswer;

@property (strong, nonatomic) IBOutlet UITextField *cName;
@property (strong, nonatomic) IBOutlet UITextField *cYearAndMonth;
@property (strong, nonatomic) IBOutlet UISegmentedControl *cSex;
@property (strong, nonatomic) IBOutlet UILabel *confirmSex;

@property (strong, nonatomic) IBOutlet UIButton *decideButton;
@property (strong, nonatomic) IBOutlet UIButton *reviseButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UIButton *goHomeButton;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UIPopoverController *popOverController;
@property (strong, nonatomic) PickerPopOverView *pickerPopOverView;

- (IBAction)selectDecide:(id)sender;
- (IBAction)selectRevise:(id)sender;
- (IBAction)selectRegister:(id)sender;
- (IBAction)selectHome:(id)sender;

@end

//
//  ProductData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductData : NSObject

@property (nonatomic) NSInteger productsId;
@property (nonatomic) NSString* title;
@property (nonatomic) NSString* description;
@property (nonatomic) NSString* usage;
@property (nonatomic) NSString* keyword;
@property (nonatomic) NSString* provider;
@property (nonatomic) NSString* author;
@property (nonatomic) NSInteger revision;
@property (nonatomic) NSInteger level;
@property (nonatomic) NSInteger targetFlag;
@property (nonatomic) NSInteger targetTerm;
@property (nonatomic) NSInteger monthTerm;
@property (nonatomic) NSString* validTermStart;
@property (nonatomic) NSString* validTermEnd;
@property (nonatomic) BOOL validTermFlag;
@property (nonatomic) BOOL invalidFlag;
@property (nonatomic) NSString* product;
@property (nonatomic) NSString* preview;
@property (nonatomic) NSString* created;
@property (nonatomic) NSString* modified;
@property (nonatomic) NSInteger categoryId;

- (id)init;

- (id)initWithProductsId:(NSInteger)productsId title:(NSString*)title description:(NSString*)description usage:(NSString*)usage keyword:(NSString*)keyword provider:(NSString*)provider author:(NSString*)author revision:(NSInteger)revision level:(NSInteger)level targetFlag:(NSInteger)targetFlag targetTerm:(NSInteger)targetTerm monthTerm:(NSInteger)monthTerm validTermStart:(NSString*)validTermStart validTermEnd:(NSString*)validTermEnd validTermFlag:(BOOL)validTermFlag invalidFlag:(BOOL)invalidFlag product:(NSString*)product preview:(NSString*)preview created:(NSString*)created modified:(NSString*)modified categoryId:(NSInteger)categoryId;

@end

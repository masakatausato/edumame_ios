//
//  ViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/10/04.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarView.h"
#import "CalendarViewController.h"
#import "SettingProfilePopOverView.h"
#import "SettingApplicationPopOverView.h"

@interface HomeViewController : UIViewController <
UITableViewDelegate,
UITableViewDataSource,
SettingsApplicationViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *workbookTableView;
@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) IBOutlet UIButton *calendarBtn;
@property (strong, nonatomic) IBOutlet UILabel *workbookTitleLabel;

@property (strong, nonatomic) CalendarViewController *calendarViewCtrl;
@property BOOL calendarOpened;

@property (strong, nonatomic) UIPopoverController *popOverController;
@property (strong, nonatomic) SettingProfilePopOverView *profilePopOverView;
@property (strong, nonatomic) SettingApplicationPopOverView *applicationPopOverView;

@property (weak, nonatomic) IBOutlet UIView *pointView;
@property (weak, nonatomic) IBOutlet UILabel *pointTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *pointCloseButton;

- (IBAction)onAdvice:(id)sender;
- (IBAction)onGoal:(id)sender;
- (IBAction)onDexterity:(id)sender;
- (IBAction)onExercise:(id)sender;
- (IBAction)onProduction:(id)sender;
- (IBAction)onPictureBook:(id)sender;
- (IBAction)onPointClose:(id)sender;

- (IBAction)onCalendar:(id)sender;

@end

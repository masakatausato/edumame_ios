//
//  DateView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "DateView.h"

@implementation DateView

@synthesize dateLabel = _dateLabel;
@synthesize date = _date;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _dateLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:_dateLabel];
    }
    return self;
}

- (void) setBackColor:(UIColor *)color
{
    _dateLabel.backgroundColor = color;
}

@end

//
//  ContentDateViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/13.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoverFlowViewController.h"
#import "GridViewController.h"
#import "DateViewController.h"

@interface ContentDateViewController : UIViewController

@property (strong, nonatomic) CoverFlowViewController *coverflowViewCtrl;
@property (strong, nonatomic) GridViewController *gridViewCtrl;
@property (strong, nonatomic) DateViewController *dateViewCtrl;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) NSDate *date;

@end

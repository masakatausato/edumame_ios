//
//  ApplicationPopOverView.h
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsApplicationViewDelegate <NSObject>

- (void)notifySettingAutoCreate;
- (void)notifyParentPassword:(BOOL)isOn;
- (void)notifyParentSoundEffect:(BOOL)isOn;
- (void)notifyParentBgm:(BOOL)isOn;
- (void)notifyChildSoundEffect:(BOOL)isOn;
- (void)notifyChildBgm:(BOOL)isOn;

@end

@interface SettingApplicationPopOverView : UIViewController
{
    NSString *cachePath;
    NSMutableArray  *switches;
    IBOutlet UISwitch *parentPasswordSwitch;
    IBOutlet UISwitch *parentSoundEffect;
    IBOutlet UISwitch *parentBgm;
    IBOutlet UISwitch *childSoundEffect;
    IBOutlet UISwitch *childBgm;
}

@property (weak, nonatomic) id<SettingsApplicationViewDelegate> delegate;

- (IBAction)onSettingAutoCreate:(id)sender;
- (IBAction)parentPassword:(UISwitch *)sender;
- (IBAction)parentSoundEffect:(UISwitch *)sender;
- (IBAction)parentBgm:(UISwitch *)sender;
- (IBAction)childSoundEffect:(UISwitch *)sender;
- (IBAction)childBgm:(UISwitch *)sender;

@end
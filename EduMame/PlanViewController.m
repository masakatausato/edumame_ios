//
//  PlanViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PlanViewController.h"
#import "DatabaseManager.h"
#import "DetailViewController.h"
#import "AddPreviewViewController.h"
#import "CreateManager.h"
#import "PrintSelectViewController.h"

@interface PlanViewController ()

@end

@implementation PlanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
    [self initializeUIBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    NSString* tapDate = [[CreateManager sharedCreateManager] tapDate];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *formatterDate = [inputFormatter dateFromString:tapDate];
    
    // コンテンツデータ取得
    [self getContentDataFromWorkbookId:self.workbookId];
    
    // CoverFlowView
    self.coverflowViewCtrl = [[CoverFlowViewController alloc] init];
    self.coverflowViewCtrl.itemList = [self.contentData mutableCopy];
    self.coverflowViewCtrl.tapDelegate = self;
    self.coverflowViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.coverflowViewCtrl];
    [self.coverflowViewCtrl didMoveToParentViewController:self];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.itemList = [self.contentData mutableCopy];
    self.gridViewCtrl.tapDelegate = self;
    self.gridViewCtrl.gestureDelegate = self;
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    // DateView
    self.dateViewCtrl = [[DateViewController alloc] init];
//    self.dateViewCtrl.delegate = self;
    self.dateViewCtrl.date = formatterDate;
    
    [self addChildViewController:self.dateViewCtrl];
    [self.dateViewCtrl didMoveToParentViewController:self];
    
    // top->CoverFlowView
    // bottom->DateView
    [self.topContainerView addSubview:self.gridViewCtrl.view];
    [self.bottomContainerView addSubview:self.dateViewCtrl.view];
    
    // 日付を取得
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:
                                   NSYearCalendarUnit |
                                   NSMonthCalendarUnit |
                                   NSDayCalendarUnit                                              fromDate:formatterDate];
    NSString *labelString = [NSString stringWithFormat:@"%d年%d月%d日", dateComps.year, dateComps.month, dateComps.day];
    
    // 日付のラベルを設定
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    label.text = labelString;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont fontWithName:@"AppleGothic" size:20];
    
    [self.view addSubview:label];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"プリント準備"];
    
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithTitle:@"追加"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onAddContent:)];
    UIBarButtonItem *dispalyItem = [[UIBarButtonItem alloc] initWithTitle:@"カバーフロー"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedView:)];
    UIBarButtonItem *printItem = [[UIBarButtonItem alloc] initWithTitle:@"印刷"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onPrint:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:printItem, dispalyItem, addItem, nil];
}

- (void)onAddContent:(id)sender
{
    self.modalPanel = [[SelectTypeToCreateModalPanel alloc] initWithFrame:self.view.bounds title:@"検索" isCreate:NO];
    self.modalPanel.selectTypeDelegate = self;
    //self.modalPanel.delegate = self;
    
    [self.view addSubview:self.modalPanel];
    [self.modalPanel show];
}

- (void)onChangedView:(id)sender
{
    UIViewController *fromViewCtrl;
    UIViewController *toViewCtrl;
    
    NSArray *rightBarButtonItem = self.navigationItem.rightBarButtonItems;
    UIBarButtonItem *item = [rightBarButtonItem objectAtIndex:1];
    
    if (self.coverflowViewCtrl.isViewLoaded && self.coverflowViewCtrl.view.superview) {
        fromViewCtrl = self.coverflowViewCtrl;
        toViewCtrl = self.gridViewCtrl;
        
        [item setTitle:@"カバーフロー"];
    } else {
        fromViewCtrl = self.gridViewCtrl;
        toViewCtrl = self.coverflowViewCtrl;
        
        [item setTitle:@"グリッド"];
    }
    
    [fromViewCtrl.view.superview addSubview:toViewCtrl.view];
    [fromViewCtrl.view removeFromSuperview];
    
    [self.view setNeedsLayout];
}

- (void)onPrint:(id)sender
{
    [self performSegueWithIdentifier:@"PrintSelectSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    
    if ([identifier isEqualToString:@"PrintSelectSegue"])
    {
        PrintSelectViewController *nextViewController = [segue destinationViewController];
        nextViewController.contentData = self.contentData;
    }
}

/**
 * getContentDataFromWorkbookId
 * DBから指定したWorkbookIDのコンテンツデータを取得
 * @param workbookId
 */
- (void)getContentDataFromWorkbookId:(NSInteger)workbookId
{
    self.contentData = [[NSMutableArray array] init];
    
    // WorkbookIndexData検索
    NSString *option = [NSString stringWithFormat:@"where workbook_id = %d", workbookId];
    
    NSArray *targetWorkbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
    
    for (int i = 0; i < [targetWorkbookIndexRecord count]; i++) {
        WorkbookIndexData *workbookIndexData = [targetWorkbookIndexRecord objectAtIndex:i];
        
        // ContentData検索
        option = [NSString stringWithFormat:@"where contents_id = %d", workbookIndexData.contentId];
        
        [self getContentDataFromDatabase:option];
    }
}

/**
 * getContentDataFromDatabase
 * DBからコンテンツデータ取得
 * @param selectOptions
 */
- (void)getContentDataFromDatabase:(NSString *)selectOptions
{
    NSArray* contentRecords = [DatabaseManager selectContentData:selectOptions];
    for (NSInteger i = 0; i < [contentRecords count]; i++)
    {
        ContentData* contentRecord = [contentRecords objectAtIndex:i];
        [self.contentData addObject:contentRecord];
    }
}

- (UIImage *)getPreviewFromContent:(NSUInteger)index
{
    ContentData* contentRecord = [self getContentData:index];
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, contentRecord.content];
    NSString* previewPath = [NSString stringWithFormat:@"%@/preview.jpg", contentPath];
    
    return [UIImage imageWithContentsOfFile:previewPath];
}

- (ContentData *)getContentData:(NSUInteger)index
{
    return [self.contentData objectAtIndex:index];
}

#pragma mark -
#pragma mark - Delegate Methods

#pragma mark - Cover Flow View & Grid View Delegate

- (void)notifyDidSelectItemAtIndex:(NSInteger)index
{
#if 0
    UIImage *image = [self getPreviewFromContent:index];
    
    // 詳細View作成
    self.detailViewCtrl = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    self.detailViewCtrl.index = index;
    self.detailViewCtrl.itemList = self.contentData;
    self.detailViewCtrl.image = image;
    self.detailViewCtrl.isRemoveHidden = NO;
    
    [self.view addSubview:self.detailViewCtrl.view];
#endif
    
    ContentData* contentData = [self getContentData:index];
    self.resourceList = [CreateManager getContentArrayFromJSON:contentData.content];
    
    UIImage *image = [self.resourceList objectAtIndex:0];
    
    // 詳細View作成
    self.detailViewCtrl = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    self.detailViewCtrl.index = 0;
    self.detailViewCtrl.itemList = self.resourceList;
    self.detailViewCtrl.image = image;
    self.detailViewCtrl.isRemoveHidden = NO;
    
    [self.view addSubview:self.detailViewCtrl.view];
}

#pragma mark - Modal Panel Delegate

//TODO:segue方式に変更
//TODO:AddPreviewViewControllerは必要か？ManualViewControllerに渡すだけでいいかも。その場合検索条件をCreateManagerにセットする
- (void)notifyManualSearch:(id)sender
{
    // モーダルダイアログ非表示
    [self.modalPanel hide];
    
    AddPreviewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPreviewIdentifier"];
    viewController.contentData = self.contentData;
    viewController.searchedContentData = [self getContentDataDummy:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSMutableArray *)getContentDataDummy:(NSString *)selectOptions
{
    NSMutableArray *contentData = [[NSMutableArray array] init];
    NSArray* contentRecords = [DatabaseManager selectContentData:selectOptions];
    for (NSInteger i = 0; i < [contentRecords count]; i++)
    {
        ContentData* contentRecord = [contentRecords objectAtIndex:i];
        [contentData addObject:contentRecord];
    }
    return contentData;
}

@end

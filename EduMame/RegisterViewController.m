//
//  RegisterViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/01/22.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "DatabaseManager.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

@synthesize info = _info;
@synthesize pID = _pID;
@synthesize pName = _pName;
@synthesize pMail = _pMail;
@synthesize pSecretQuestion = _pSecretQuestion;
@synthesize pSecretAnswer = _pSecretAnswer;

@synthesize cName = _cName;
@synthesize cYearAndMonth = _cYearAndMonth;
@synthesize cSex = _cSex;
@synthesize confirmSex = _confirmSex;

@synthesize decideButton = _decideButton;
@synthesize reviseButton = _reviseButton;
@synthesize registerButton = _registerButton;
@synthesize goHomeButton = _goHomeButton;

@synthesize scrollView = _scrollView;
@synthesize popOverController = _popOverController;
@synthesize pickerPopOverView = _pickerPopOverView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{   
    [super viewDidLoad];
    self.title = @"登録";
    [self.navigationItem setHidesBackButton:YES];
    
    [self registerForKeyboardNotifications];
    
    [self.cYearAndMonth setDelegate:self];
    
    [self.pID setDelegate:self];
    [self.pMail setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    // TextLabelごとに高さ調整する場合コメント解除
    BOOL isEdit = YES;//(_pMail.isEditing | _pSecretQuestion.isEditing | _pSecretAnswer.isEditing);
    if (isEdit) {
        CGPoint scrollPoint = CGPointMake(0.0f, 90.0f);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"tag = %d", textField.tag);
    if (textField.tag == 1 || textField.tag == 3) {
        return YES;
    }
    
    //テキストフィールドの編集を始めるときに、ピッカーを呼び出す。
    [self showPickerPopupView];
    
    //キーボードは表示させない
    return NO;
}

// 入力される文字のチェック(全角入力を無効化する)
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 1) {
        NSString *result = [self.pID.text stringByReplacingCharactersInRange:range withString:string];
        NSMutableCharacterSet *checkCharSet = [[NSMutableCharacterSet alloc] init];
        [checkCharSet addCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
        [checkCharSet addCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        [checkCharSet addCharactersInString:@"1234567890-;/\\?\\!:@&\\|\\=\\+\\$,\\[\\]#!'\\(\\)\\*\\.\\^\\-"];
        // 半角文字を削除して残った文字の長さが1以上なら全角が含まれる
        if([[result stringByTrimmingCharactersInSet:checkCharSet] length] > 0){
            return NO;
        }
    } else if (textField.tag == 3) {
        NSString *result = [self.pMail.text stringByReplacingCharactersInRange:range withString:string];
        NSMutableCharacterSet *checkCharSet = [[NSMutableCharacterSet alloc] init];
        [checkCharSet addCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
        [checkCharSet addCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        [checkCharSet addCharactersInString:@"1234567890-;/\\?\\!:@&\\|\\=\\+\\$,\\[\\]#!'\\(\\)\\*\\.\\^\\-"];
        
        // 半角文字を削除して残った文字の長さが1以上なら全角が含まれる
        if([[result stringByTrimmingCharactersInSet:checkCharSet] length] > 0){
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Picker Process

- (void)showPickerPopupView
{
    if (![self.popOverController isPopoverVisible])
    {
		self.pickerPopOverView = [[PickerPopOverView alloc] initWithNibName:@"PickerPopOverView" bundle:nil];
        [self.pickerPopOverView setDelegate:self];
        
		self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.pickerPopOverView];
		[self.popOverController setPopoverContentSize:CGSizeMake(300.0f, 215.0f)];
        
        // Popoverを表示する
        [self.popOverController presentPopoverFromRect:self.cYearAndMonth.bounds
                                                inView:self.cYearAndMonth
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:YES];
	}
    else
    {
		[self.popOverController dismissPopoverAnimated:YES];
	}
}

- (void)notifyChanged:(NSString*)year month:(NSString*)month
{
    NSString *birthYear = [year stringByAppendingString:@"年"];
    NSString *birthMonth = [month stringByAppendingString:@"月"];
    NSString *birthYM = [birthYear stringByAppendingString:birthMonth];

    [self.cYearAndMonth setText:birthYM];
}

#pragma mark - Button Event

/**
 * selectButton
 * 決定ボタン
 */
- (IBAction)selectDecide:(id)sender
{
    NSString *errorString = @"";
    
    // ID取得
    _parentID = self.pID.text;
    if ([[_parentID stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        errorString = [errorString stringByAppendingString:@"　ID"];
    }
    
    // 名前取得
    _parentName = self.pName.text;
    if ([[_parentName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        errorString = [errorString stringByAppendingString:@"　お母様のお名前"];
    }
    
    // メール取得
    _parentMail = self.pMail.text;
    if ([[_parentMail stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        errorString = [errorString stringByAppendingString:@"　メールアドレス"];
    }
    
    // 秘密の質問取得
    _parentSecretQuestion = self.pSecretQuestion.text;
//    if ([[_parentSecretQuestion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
//        errorString = [errorString stringByAppendingString:@"　秘密の質問"];
//    }
    
    // 秘密の質問の回答取得
    _parentSecretAnswer = self.pSecretAnswer.text;
//    if ([[_parentSecretAnswer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
//        errorString = [errorString stringByAppendingString:@"　秘密の質問の回答"];
//    }
    
    // 子名前取得
    _childName = self.cName.text;
    if ([[_childName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        errorString = [errorString stringByAppendingString:@"　お子様のお名前"];
    }
    
    // 子生年月取得
    _childYearAndMonth = self.cYearAndMonth.text;
    if ([[_childYearAndMonth stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        errorString = [errorString stringByAppendingString:@"　生年月日"];
    }
    
    // 子性別取得
    _childGender = self.cSex.selectedSegmentIndex;
    
    NSLog(@"errorString = %@", errorString);

    // 全て入力されている場合
    if ([errorString isEqualToString:@""]) {
    
        [self.info setText:@"この内容でよろしいでしょうか？"];
        [self.info setTextAlignment:UITextAlignmentCenter];
    
        [self invalidView];
        [self setButtonHide:YES isRevise:NO isRegister:NO idGoHome:YES];
    // 未入力の場合
    } else {
        
//        errorString = [errorString stringByAppendingString:@"　\nを入力して下さい"];

        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"未入力の項目があります。"
                              message:@"全ての項目を埋めて下さい。"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    
    [self logAll];
}

- (IBAction)selectRevise:(id)sender
{
    [self.info setText:@"お母様、お子様のお名前等の情報を登録してください。"];
    [self.info setTextAlignment:UITextAlignmentCenter];
    
    [self.pID setBorderStyle:UITextBorderStyleRoundedRect];
    [self.pID setEnabled:YES];
    
    [self.pName setBorderStyle:UITextBorderStyleRoundedRect];
    [self.pName setEnabled:YES];
    
    [self.pMail setBorderStyle:UITextBorderStyleRoundedRect];
    [self.pMail setEnabled:YES];
    
    [self.pSecretQuestion setBorderStyle:UITextBorderStyleRoundedRect];
    [self.pSecretQuestion setEnabled:YES];
    
    [self.pSecretAnswer setBorderStyle:UITextBorderStyleRoundedRect];
    [self.pSecretAnswer setEnabled:YES];
    
    [self.cName setBorderStyle:UITextBorderStyleRoundedRect];
    [self.cName setEnabled:YES];
    
    [self.cYearAndMonth setBorderStyle:UITextBorderStyleRoundedRect];
    [self.cYearAndMonth setEnabled:YES];
    
    [self.cSex setHidden:NO];
    [self.confirmSex setHidden:YES];
    
    [self setButtonHide:NO isRevise:YES isRegister:YES idGoHome:YES];
}

- (IBAction)selectRegister:(id)sender
{
    [self setupParent];
    [self setupChild];
    
    [self.info setText:@"ご登録が完了しました。"];
    [self.info setTextAlignment:UITextAlignmentCenter];
    
    [self invalidView];
    [self setButtonHide:YES isRevise:YES isRegister:YES idGoHome:NO];
}

- (IBAction)selectHome:(id)sender
{
//    LoginViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    [self.navigationController pushViewController:loginView animated:NO];
    
    [self performSegueWithIdentifier:@"LoginSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;

    if([identifier isEqualToString:@"LoginSegue"])
    {
    }
}

#pragma mark - Database Methods

- (void)setupParent
{
    NSMutableArray* parentRecords = [NSMutableArray array];

    // 親テーブル作成
    ParentData* parentRecord = [[ParentData alloc] init];
    parentRecord.userId = _parentID;
    parentRecord.username = _parentName;
    parentRecord.email = _parentMail;
    parentRecord.familyName = @"";
    parentRecord.firstName = @"";
    parentRecord.familyNameKana = @"";
    parentRecord.firstNameKana = @"";
    parentRecord.zipCode1 = @"";
    parentRecord.zipCode2 = @"";
    parentRecord.prefectureCode = @"";
    parentRecord.city = @"";
    parentRecord.address = @"";
    parentRecord.building = @"";
    parentRecord.tel = @"";
    parentRecord.birthday = @"";
    parentRecord.password = @"edumame";
    parentRecord.secretQuestion = _parentSecretQuestion;
    parentRecord.secretQuestionAnswer = _parentSecretAnswer;
        
    // レコードを格納
    [parentRecords addObject:parentRecord];
    
    // データベースに親テーブル格納
    [DatabaseManager deleteParentData:nil];
    [DatabaseManager insertParentData:parentRecords lastInsertRowId:nil];
}

- (void)setupChild
{
    NSMutableArray* childRecords = [NSMutableArray array];
    
    // 子テーブル作成
    ChildrenData* childRecord = [[ChildrenData alloc] init];
    childRecord.username = _childName;
    childRecord.passwordType = 0;
    childRecord.password = @"";
    childRecord.familyName = @"";
    childRecord.firstName = @"";
    childRecord.familyNameKana = @"";
    childRecord.firstNameKana = @"";
    childRecord.birthday = _childYearAndMonth;
    childRecord.gender = _childGender;
    
    // レコードを格納
    [childRecords addObject:childRecord];
    
    // データベースに親テーブル格納
    [DatabaseManager deleteChildrenData:nil];
    [DatabaseManager insertChildrenData:childRecords lastInsertRowId:nil];
}

#pragma mark - Control View

- (void)invalidView
{    
    [self.pID setBorderStyle:UITextBorderStyleNone];
    [self.pID setEnabled:NO];
    
    [self.pName setBorderStyle:UITextBorderStyleNone];
    [self.pName setEnabled:NO];
    
    [self.pMail setBorderStyle:UITextBorderStyleNone];
    [self.pMail setEnabled:NO];
    
    [self.pSecretQuestion setBorderStyle:UITextBorderStyleNone];
    [self.pSecretQuestion setEnabled:NO];
    
    [self.pSecretAnswer setBorderStyle:UITextBorderStyleNone];
    [self.pSecretAnswer setEnabled:NO];
    
    [self.cName setBorderStyle:UITextBorderStyleNone];
    [self.cName setEnabled:NO];
    
    [self.cYearAndMonth setBorderStyle:UITextBorderStyleNone];
    [self.cYearAndMonth setEnabled:NO];
    
    [self.cSex setHidden:YES];
    [self.confirmSex setHidden:NO];
    [self.confirmSex setText:_childGender == 0 ? @"男" : @"女"];
}

- (void)setButtonHide:(BOOL)isDecide isRevise:(BOOL)isRevise isRegister:(BOOL)isRegister idGoHome:(BOOL)idGoHome
{
    [self.decideButton setHidden:isDecide];
    [self.reviseButton setHidden:isRevise];
    [self.registerButton setHidden:isRegister];
    [self.goHomeButton setHidden:idGoHome];
}

#pragma mark - TEST CODE

- (void)logAll
{
    NSLog(@"pID = %@", _parentID);
    NSLog(@"pName = %@", _parentName);
    NSLog(@"pMail = %@", _parentMail);
    NSLog(@"pSecretQuestion = %@", _parentSecretQuestion);
    NSLog(@"pSecretAnswer = %@", _parentSecretAnswer);
    NSLog(@"cName = %@", _childName);
    NSLog(@"cYearAndMonth = %@", _childYearAndMonth);
    NSLog(@"cSex = %d", _childGender);
}

- (void)viewDidUnload
{
    [self setInfo:nil];
    [self setConfirmSex:nil];
    [self setGoHomeButton:nil];
    [super viewDidUnload];
}
@end

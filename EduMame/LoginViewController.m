//
//  LoginViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/01/23.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "LoginViewController.h"
#import "ChildHomeViewController.h"
#import "UILabelUnderlined.h"
#import "ReminderModalPanel.h"
#import "ContentManager.h"

#define EDMAME_SETTINGS @"EdumameSettings"
#define EDMAME_SETTINGS_PLIST @"EdumameSettings.plist"

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 * 初期化
 */
- (void)initialize
{
    // ナビゲーションタイトル変更
    self.title = @"ログイン";
    [self.navigationItem setHidesBackButton:YES];

#if 0
    // リマインダーラベル作成
    self.reminderLabel = [[UILabelUnderlined alloc] init];
    [self.reminderLabel setFrame:CGRectMake(325, 440, 200, 50)];
    [self.reminderLabel setTextColor:[UIColor blueColor]];
    [self.reminderLabel setText:@"パスワードを忘れた場合"];
    [self.reminderLabel setUserInteractionEnabled:YES];
    [self.reminderLabel setTag:100];
    
    [self.view addSubview:self.reminderLabel];
#endif
    // 登録情報取得
    [self getRegisterInfo];
    
    // 名前表示
    [self.parentName setText:self.parentData.username];    
    [self.childName setText:self.childrenData.username];
    
    //[self.parentName sizeToFit];
    //[self.childName sizeToFit];
}

#if 0
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (touch.view.tag == self.reminderLabel.tag) {
        [self selectReminderLabel:self.reminderLabel];
    }
}
#endif

- (IBAction)onReminder:(id)sender
{
    ReminderModalPanel *modalPanel = [[ReminderModalPanel alloc] initWithFrame:self.view.bounds
                                                                         title:@"秘密のパスワード入力"];
    // ダイアログが閉じた時にオブザーバーを削除する
    modalPanel.onClosePressed = ^(UAModalPanel* panel) {
        // [panel hide];
        [panel hideWithOnComplete:^(BOOL finished) {
            [(ReminderModalPanel*)panel removeForKeyboardNotifications];
            [panel removeFromSuperview];
        }];
        UADebugLog(@"onClosePressed block called from panel: %@", modalPanel);
    };

    modalPanel.delegate = self;
    [self.view addSubview:modalPanel];
    [modalPanel showFromPoint:[sender center]];    
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if([identifier isEqualToString:@"ParentHomeSegue"])
    {
    }
}

- (IBAction)showPasswordPopupOverView:(id)sender
{
    //[self performSegueWithIdentifier:@"ParentHomeSegue" sender:self];
    //return;

    
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:EDMAME_SETTINGS ofType:@"plist"];
    //Cacheディレクトリ
    NSString *cachePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:EDMAME_SETTINGS_PLIST];
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if (![filemanager fileExistsAtPath:cachePath]) {
        [filemanager copyItemAtPath:path toPath:cachePath error:nil];
    }
    
    NSMutableArray *switches = [NSMutableArray arrayWithContentsOfFile:cachePath];
    
    // passwordがOFFの場合は直接HOME画面に飛ぶ
    if ([[switches objectAtIndex:0] intValue] == 0) {
        [self performSegueWithIdentifier:@"ParentHomeSegue" sender:self];
        return;
    }
    
    if (![self.popOverController isPopoverVisible])
    {
		self.passwordPopOverView = [[PasswordPopOverView alloc] initWithNibName:@"PasswordPopOverView" bundle:nil];
        [self.passwordPopOverView setDelegate:self];
        
		self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.passwordPopOverView];
		[self.popOverController setPopoverContentSize:CGSizeMake(320.0f, 250.0f)];
        
        // Popoverを表示する
        [self.popOverController presentPopoverFromRect:self.parentButton.bounds
                                                inView:self.parentButton
                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                              animated:YES];
	}
    else
    {
		[self.popOverController dismissPopoverAnimated:YES];
	}
}

- (IBAction)touchDownResetButton:(id)sender
{
    UIButton *bt = (UIButton*)sender;
    [bt setTitle:@"OK" forState:UIControlStateDisabled];
    [bt setEnabled:NO];
    
    [ContentManager setup];
}

- (IBAction)onUnwindSegue:(UIStoryboardSegue *)segue
{
}

#pragma mark - Notify from PasswordView

/**
 */
- (void)notifyLogin:(NSString*)password
{
    [self.popOverController dismissPopoverAnimated:YES];
    
    if (password != nil && ![password isEqualToString:@""]) {
        // DBのパスワードと照合
        if ([password isEqualToString:self.parentData.password]) {
            // パスワードが一致したら親ホームに遷移
            [self performSegueWithIdentifier:@"ParentHomeSegue" sender:self];
        } else {
            //　パスワード不一致の場合、エラーダイアログ表示
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ログイン失敗"
                                                            message:@"登録情報が見つかりません。\n再度正しい情報でログインして下さい。"
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK",nil];
            [alert show];
        }
    }
}

/**
 */
- (void)notifyCancel
{
    [self.popOverController dismissPopoverAnimated:YES];
}

#pragma mark - Database Methods

/**
 * getRegisterInfo
 * 登録情報取得
 */
- (void)getRegisterInfo
{
    [self getParentData];
    [self getChildrenData];
}

/**
 * getParentData
 * 親情報取得
 */
- (void)getParentData
{
    // DBからユーザ情報取得
    NSArray* parentRecords = [DatabaseManager selectParentData:nil];
    int parentCnt = [parentRecords count];
    if (parentCnt <= 0) {
        return;
    } else {
        for (NSInteger i = 0; i < parentCnt; i++) {
            self.parentData = [parentRecords objectAtIndex:i];
        }
    }
}

/**
 * getChildrenData
 * 子情報取得
 */
- (void)getChildrenData
{
    // DBからユーザ情報取得
    NSArray* childRecords = [DatabaseManager selectChildrenData:nil];
    int childCnt = [childRecords count];
    if (childCnt <= 0) {
        return;
    } else {
        for (NSInteger i = 0; i < childCnt; i++) {
            self.childrenData = [childRecords objectAtIndex:i];
        }
    }
}

@end

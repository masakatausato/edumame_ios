//
//  EMContent.h
//  EMPreview
//
//  Created by Shinji Ochiai on 12/11/28.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EMPlayerDefine.h"

@interface EMContent : NSObject

// リソース種別
@property NSInteger resourceType;

// リソースファイル名
@property NSString* resourceFileName;

// 遷移方法
@property NSInteger changeMode;

// 表示時間
@property Float32 displayTime;

// 属性
@property NSUInteger attributes;

// EMコンテンツを生成
+(EMContent*)content:(NSInteger)resourceType resourceFileName:(NSString*)contentFileName changeMode:(NSInteger)changeMode displayTime:(Float32)displayTime;

// EMコンテンツを生成
+(EMContent*)content:(NSInteger)resourceType resourceFileName:(NSString*)contentFileName changeMode:(NSInteger)changeMode displayTime:(Float32)displayTime attributes:(NSUInteger)attributes;

@end

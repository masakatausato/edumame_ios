//
//  PrintGridViewCell.h
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AQGridViewCell.h"

#define WIDTH 200
#define HEIGHT 150

#define CHECKED_WIDTH 30
#define CHECKED_HEIGHT 30

@interface PrintGridViewCell : AQGridViewCell

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *checkedImageView;

@end

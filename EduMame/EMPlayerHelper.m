//
//  EMPlayerHelper.m
//  EMPreview2
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import "EMPlayerHelper.h"

@implementation EMPlayerHelper

// 浮動小数の補間する。
+ (Float32)lerpFloat:(Float32)f1 f2:(Float32)f2 rate:(Float32)rate
{
    return f1 + ((f2 - f1) * rate);
}

// 矩形の補間する。
+ (CGRect)lerpRect:(CGRect)r1 r2:(CGRect)r2 rate:(Float32)rate
{
    Float32 startLeft = r1.origin.x;
    Float32 startTop = r1.origin.y;
    Float32 startRight = r1.origin.x + r1.size.width;
    Float32 startBottom = r1.origin.y + r1.size.height;
    Float32 endLeft = r2.origin.x;
    Float32 endTop = r2.origin.y;
    Float32 endRight = r2.origin.x + r2.size.width;
    Float32 endBottom = r2.origin.y + r2.size.height;
    
    Float32 left = startLeft + ((endLeft - startLeft) * rate);
    Float32 top = startTop + ((endTop - startTop) * rate);
    Float32 right = startRight + ((endRight - startRight) * rate);
    Float32 bottom = startBottom + ((endBottom - startBottom) * rate);
    
    return CGRectMake(left, top, right - left, bottom - top);
}

// JSONエフェクトの変換する。
+ (NSInteger)convertJsonEffect:(NSString*)jsonEffect
{
    NSInteger effect = EMPlayerEffectNone;
    if([jsonEffect isEqualToString:@"Scroll"])
    {
        effect = EMPlayerEffectScroll;
    }
    else if([jsonEffect isEqualToString:@"Fade"])
    {
        effect = EMPlayerEffectFade;
    }
    return effect;
}

// JSON真偽値の変換する。
+ (BOOL)convertJsonBoolean:(NSString*)jsonBoolean
{
    BOOL boolean = NO;
    if([jsonBoolean isEqualToString:@"TRUE"])
    {
        boolean = YES;
    }
    return boolean;
}

// JSON時間の変換する。
+ (Float32)convertJsonTime:(NSString*)jsonTime
{
    Float32 time = 0;
    if([jsonTime isEqualToString:@"Infinity"])
    {
        time = -1;
    }
    else
    {
        time = [jsonTime floatValue];
    }
    return time;
}

// JSON矩形の変換する。
+ (CGRect)convertJsonRect:(NSDictionary*)jsonRect;
{
    NSString* jsonX = [jsonRect objectForKey:@"x"];
    NSString* jsonY = [jsonRect objectForKey:@"y"];
    NSString* jsonWidth = [jsonRect objectForKey:@"width"];
    NSString* jsonHeight = [jsonRect objectForKey:@"height"];
    
    Float32 x = (jsonX != nil) ? [jsonX floatValue] : 0;
    Float32 y = (jsonY != nil) ? [jsonY floatValue] : 0;
    Float32 width = (jsonWidth != nil) ? [jsonWidth floatValue] : 0;
    Float32 height = (jsonHeight != nil) ? [jsonHeight floatValue] : 0;
    
    return CGRectMake(x, y, width, height);
}

@end

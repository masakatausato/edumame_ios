//
//  WorkbookDetailViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "DetailViewController.h"
#import "CreateManager.h"
#import "RemoveConfirmViewController.h"

@interface DetailViewController ()
{
    UISwipeGestureRecognizer *swipeLeftGestureRecognizer;
    UISwipeGestureRecognizer *swipeRightGestureRecognizer;
}
@end

/**
 * DetailViewController
 * 詳細画面クラス
 */
@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isPrintHidden = NO;
        self.isRemoveHidden = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // タップ画像セット
    [self.imageView setImage:self.image];

    // スワイプ検知(Left)
    swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(onSwipeLeft:)];
    swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftGestureRecognizer];
    
    // スワイプ検知(Right)
    swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(onSwipeRight:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightGestureRecognizer];
    
    [self.print setHidden:self.isPrintHidden];
    [self.remove setHidden:self.isRemoveHidden];
    if (self.isRemoveHidden == NO) {
        CGAffineTransform translate = CGAffineTransformMakeTranslation(70.0, 0.0);
        [self.print setTransform:translate];
    }
}

- (void)viewDidUnload
{
    [self setPrint:nil];
    [self setRemove:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)onSwipeLeft:(UISwipeGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateEnded) {
        if (0 < self.index) {
            self.index--;
        }
        UIImage *detailImage = [self.itemList objectAtIndex:self.index];
        [self.imageView setImage:detailImage];
    }
}

- (void)onSwipeRight:(UISwipeGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateEnded) {
        int maxCount = self.itemList.count;
        if (self.index < maxCount-1) {
            self.index++;
        }
        UIImage *detailImage = [self.itemList objectAtIndex:self.index];
        [self.imageView setImage:detailImage];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint touchLocation = [touch locationInView:self.view];
        if (!CGRectContainsPoint(self.imageView.frame, touchLocation)) {
            [self.view removeFromSuperview];
        }
    }
}

- (ContentData *)getContentData:(NSUInteger)index
{
    return [self.itemList objectAtIndex:index];
}

- (IBAction)onPrint:(id)sender
{}

- (IBAction)onRemove:(id)sender
{
    self.removeConfirmView = [[RemoveConfirmViewController alloc] initWithNibName:@"RemoveConfirmViewController" bundle:nil];
    [self.view addSubview:self.removeConfirmView.view];
}

@end

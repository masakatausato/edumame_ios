//
//  SelectWorkbookCreateModalPanel.h
//  EduMame
//
//  Created by gclue_mita on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@protocol SelectTypeDelegate <NSObject>

- (void)notifyManualSearch:(id)sender
                    target:(NSInteger)target
                  category:(NSString *)category
                     level:(NSString *)level
                    term:(NSString *)term;
- (void)notifyAutoDay:(id)sender;
- (void)notifyAutoWeek:(id)sender;

@end

@interface SelectTypeToCreateModalPanel : UATitledModalPanel

@property (strong, nonatomic) UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *selectCreateTypeFromXib;
@property (strong, nonatomic) IBOutlet UIView *searchWorkbooksViewFromXib;
@property (weak, nonatomic) id<SelectTypeDelegate> selectTypeDelegate;

@property (weak, nonatomic) IBOutlet UISegmentedControl *target;
@property (weak, nonatomic) IBOutlet UITextField *category;
@property (weak, nonatomic) IBOutlet UITextField *level;
@property (weak, nonatomic) IBOutlet UITextField *term;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title isCreate:(BOOL)isCreate;

- (IBAction)onManual:(id)sender;
- (IBAction)onAutoDay:(id)sender;
- (IBAction)onAutoWeek:(id)sender;

- (IBAction)onSearch:(id)sender;

@end

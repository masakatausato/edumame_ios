//
//  ContentViewViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContentViewDelegate <NSObject>

- (void)notifyTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@protocol ContentViewGestureDelegate <NSObject>

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer index:(NSInteger)index;
- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer;
- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer;

@end

@interface ContentViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *itemList;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) id<ContentViewDelegate> tapDelegate;
@property (strong, nonatomic) id<ContentViewGestureDelegate> gestureDelegate;

@end

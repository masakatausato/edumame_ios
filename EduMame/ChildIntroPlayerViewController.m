//
//  ChildIntroPlayerViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildIntroPlayerViewController.h"
#import "ChildIntroAnswerView.h"
#import "EMPlayerHelper.h"
#import "EMPlayer.h"
#import "ContentManager.h"
#import "DatabaseManager.h"

#define EMPlayerAnswerInfinity 0
#define EMPlayerSkipEnable 1

// ステータス
enum
{
    ChildIntroPlayerStatusNone,
    ChildIntroPlayerStatusIntro,
    ChildIntroPlayerStatusAnswer,
    ChildIntroPlayerStatusTap,
    ChildIntroPlayerStatusEnd,
};

@interface ChildIntroPlayerViewController ()
{
    // 直前の更新時間
    NSDate* _previousDate;
    
    // EMプレイヤー
    EMPlayer* _player;
    
    // 現在の画像リソースを表示するビュー
    IBOutlet UIImageView* _frontImageView;
    
    // 直前の画像リソースを表示するビュー
    IBOutlet UIImageView* _backImageView;
    
    // 解答ビュー
    IBOutlet ChildIntroAnswerView* _answerView;
    
    // プレイヤービュー
    IBOutlet UIView* _playerView;

    // クロックビュー
    IBOutlet UIView* _clockView;

    // メニュービュー
    IBOutlet UIView* _menuView;

    // パレットビュー
    IBOutlet UIView* _paletteView;

    // 次へボタン
    IBOutlet UIButton* _nextButton;

    // 戻るボタン
    IBOutlet UIButton* _backButton;

    // 終わりボタン
    IBOutlet UIButton* _endButton;

    // クロックラベル
    IBOutlet UILabel* _clockLabel;

    // 選択中パレットカラーイメージ
    IBOutlet UIImageView* _activeColorImage;

    // ステータス
    NSInteger _status;
    
    // AVプレイヤーレイヤー
    AVPlayerLayer* _avplayerLayer;
    
    // 解答中フラグ
    BOOL _answerFlag;
    
    // 解答データリスト
    NSMutableArray* _answerList;
    
    // 現在選択中のパレットインデックス
    NSInteger _paletteIndex;

    // パレットビュー表示中フラグ
    BOOL _showPalette;
}
@end

@implementation ChildIntroPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // コンテンツのパス
    NSString* contentPath = [[ContentManager contentsPath] stringByAppendingPathComponent:_contentName];
    
    // EMプレイヤーをセットアップ
    _player = [[EMPlayer alloc] initWithContentPath:contentPath callbackHandler:self];    

    // 解答の時間制限をすべて無制限とする
    if(EMPlayerAnswerInfinity)
    {
        NSArray* programs = [_player introPrograms];
        for(NSInteger i = 0; i < [programs count]; i++)
        {
            EMContent* content = [programs objectAtIndex:i];
            if(content.changeMode == EMChangeModeAnswer)
            {
                content.attributes |= EMAttributeInfinity;
            }
        }
    }
    
    // トップ画面
    NSString* previewPath = [contentPath stringByAppendingPathComponent:@"preview.jpg"];
    NSData* imageData = [NSData dataWithContentsOfFile:previewPath];
    _frontImageView.image = [UIImage imageWithData:imageData];
    
    // パレットの初期化
    [self colorChange:1];
    
    // 解答データの初期化
    _answerList = [NSMutableArray array];
    _answerFlag = NO;

    // ステータスの初期化
    [self changeStatus:ChildIntroPlayerStatusNone];
    
    // 更新タイマーを起動
    _previousDate = [NSDate date];
    [NSTimer scheduledTimerWithTimeInterval:1.0f/60.0f target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];
}

- (void)viewDidLayoutSubviews
{    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ビューが閉じたイベント
- (void)viewDidDisappear:(BOOL)animated
{
    [_player cleanup];
}

// タイマーイベント
- (void)onTimer:(NSTimer*)timer
{
    // 直前のタイムアウトからの経過時間を計測
    NSDate* currentDate = [NSDate date];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_previousDate];
    _previousDate = currentDate;
    //Float32 frameRate = 1.0f / interval;
    //NSLog(@"frame rate %lf", frameRate);
    
    // ステータスごとの処理
    switch (_status)
    {
        case ChildIntroPlayerStatusNone:
            break;
            
        case ChildIntroPlayerStatusIntro:
            // EMプレイヤーを更新
            [_player update:interval];
            break;
            
        case ChildIntroPlayerStatusAnswer:
            // EMプレイヤーを更新
            [_player update:interval];
            
            // クロックを更新
            if([_player waitInfinity])
            {
                _clockLabel.text = @"∞";
            }
            else
            {
                _clockLabel.text = [NSString stringWithFormat:@"%d", (int)_player.waitTimer];
            }
            break;
            
        case ChildIntroPlayerStatusTap:
            // EMプレイヤーを更新
            [_player update:interval];
            break;

        case ChildIntroPlayerStatusEnd:
            break;
    }
}

// タッチ開始イベント
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    switch (_status)
    {
        case ChildIntroPlayerStatusIntro:
        {
            break;
        }
            
        case ChildIntroPlayerStatusAnswer:
        {
            // 解答ステータスの場合は解答を描画開始
            UITouch* touch = [touches anyObject];
            CGPoint currentPoint = [touch locationInView:self.view];
            [_answerView beginDraw:currentPoint];
            break;
        }
    }
}

// タッチムーブイベント
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ChildIntroPlayerStatusAnswer)
    {
        // 解答ステータスの場合は解答を描画継続
        UITouch* touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:self.view];
        [_answerView draw:currentPoint];
    }
    
}

// タッチ終了イベント
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ChildIntroPlayerStatusAnswer)
    {
        // 解答ステータスの場合は解答を描画継続
        [_answerView endDraw];
    }
}

// 次へボタン押下イベント
- (IBAction)touchUpInsideNextButton:(id)sender
{
    switch (_status)
    {
        case ChildIntroPlayerStatusNone:
        {
            // プレイヤーを全画面表示
            [UIView animateWithDuration:0.5 animations:^{
                [_playerView setFrame:CGRectMake(0, 0, 1024, 768)];
            }];
            
            // トップ画面を消去
            _frontImageView.image = nil;

            // 次のシーケンス
            [_player nextSequence];
            break;
        }
            
        case ChildIntroPlayerStatusIntro:
        case ChildIntroPlayerStatusAnswer:
        case ChildIntroPlayerStatusTap:
            // 次のシーケンス
            [_player nextSequence];
            break;
            
        case ChildIntroPlayerStatusEnd:
            break;
    }
}

// 戻るボタン押下イベント
- (IBAction)touchUpInsideBackButton:(id)sender
{
    // 画面を閉じる
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

// 終わりボタン押下イベント
- (IBAction)touchUpInsideEndButton:(id)sender
{
    // 答案を保存
    [self saveAnswer];

    // 画面を閉じる
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

// パレットボタンカラー押下イベント
- (IBAction)touchUpInsidePaletteColorButton:(id)sender
{
    NSInteger tag = [sender tag];

    [self colorChange:tag];
    [self closePalette:NO];
}

// パレットボタン押下イベント
- (IBAction)touchUpInsidePaletteButton:(id)sender
{
    [self togglePalette:NO];
}

// コンテンツ開始イベント
- (void)playerStartContent
{
    // ステータスの更新
    [self changeStatus:ChildIntroPlayerStatusIntro];
}

// コンテンツ終了イベント
- (void)playerEndContent
{
    // 解答ビューの記憶
    [self storeAnswerData];

    // ステータスの更新
    [self changeStatus:ChildIntroPlayerStatusEnd];
}

// 解答開始イベント
- (void)playerStartAnswer
{
    // ステータスの更新
    [self changeStatus:ChildIntroPlayerStatusAnswer];
    
    // 解答中フラグをセット
    _answerFlag = YES;
}

// 解答終了イベント
- (void)playerEndAnswer
{
    // ステータスを変更
    [self changeStatus:ChildIntroPlayerStatusIntro];
}

// タップ待ち開始
- (void)playerStartTap
{
    // ステータスの更新
    [self changeStatus:ChildIntroPlayerStatusTap];
}

// タップ待ち終了
- (void)playerEndTap
{
    // ステータスを変更
    [self changeStatus:ChildIntroPlayerStatusIntro];    
}

// 画像設定イベント
- (void)playerSetImage:(UIImage*)image
{
    // 解答ビューの記憶とクリア
    [self storeAnswerData];
    [_answerView clear];

    // 前面イメージを背面に、設定する画像を前面イメージに設定
    _backImageView.image = _frontImageView.image;
    _frontImageView.image = image;
    
    // 背面イメージ・前面イメージを左スライド
    CGRect playerFrame = _playerView.frame;
    CGFloat w = playerFrame.size.width;
    CGFloat h = playerFrame.size.height;
    _backImageView.frame = CGRectMake(0, 0, w, h);
    [UIView animateWithDuration:0.5 animations:^{
        _backImageView.frame = CGRectMake(-w, 0, w, h);
    }];
    _frontImageView.frame = CGRectMake(0, 0, w, h);
}

// 動画再生イベント
- (void)playerPlayMovie:(AVPlayer*)player playerItem:(AVPlayerItem*)playerItem
{
    _avplayerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    _avplayerLayer.frame = self.view.bounds;
    [self.view.layer addSublayer:_avplayerLayer];
}

// 動画停止イベント
- (void)playerStopMovie
{
    [_avplayerLayer removeFromSuperlayer];
}

// ステータスを変更する。
- (void)changeStatus:(NSInteger)status
{
    switch (status)
    {
        case ChildIntroPlayerStatusNone:
        {
            [self closeClock:YES];
            [self closePalette:YES];
            [self openMenu:YES];
            _backButton.hidden = NO;
            _nextButton.hidden = NO;
            _endButton.hidden = YES;
            break;
        }
        case ChildIntroPlayerStatusIntro:
        {
            [self closeClock:NO];
            [self closePalette:NO];
            [self openMenu:NO];
            _backButton.hidden = YES;
            _nextButton.hidden = NO;
            _endButton.hidden = YES;
            break;
        }
        case ChildIntroPlayerStatusAnswer:
        {
            [self openClock:NO];
            [self openPalette:NO];
            [self openMenu:NO];
            _backButton.hidden = YES;
            _nextButton.hidden = NO;
            _endButton.hidden = YES;
            
            // 解答ビューを全画面表示
            [_answerView setFrame:CGRectMake(0, 0, 1024, 768)];
            break;
        }
        case ChildIntroPlayerStatusTap:
        {
            [self closeClock:NO];
            [self closePalette:NO];
            [self openMenu:NO];
            _backButton.hidden = YES;
            _nextButton.hidden = NO;
            _endButton.hidden = YES;
            break;
        }
        case ChildIntroPlayerStatusEnd:
        {
            [self closeClock:NO];
            [self closePalette:NO];
            [self openMenu:NO];
            _backButton.hidden = YES;
            _nextButton.hidden = YES;
            _endButton.hidden = NO;

            // プレイヤーを全画面表示を解除
            [UIView animateWithDuration:0.5 animations:^{
                [_playerView setFrame:CGRectMake(1024*0.1, 768*0.1, 1024*0.8, 768*0.8)];
                [_answerView setFrame:CGRectMake(0, 0, 1024*0.8, 768*0.8)];
            }];
            break;
        }
    }
    _status = status;
}

// 解答データを記憶する。
- (void)storeAnswerData
{
    if(!_answerFlag) return;

    UIGraphicsBeginImageContext(_answerView.frame.size);
    
    [_frontImageView.image drawInRect:CGRectMake(0, 0, _frontImageView.image.size.width, _frontImageView.image.size.height)];
    [_answerView.image drawInRect:CGRectMake(0, 0, _answerView.image.size.width, _answerView.image.size.height)];
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    [_answerList addObject:[UIImage imageWithCGImage:image.CGImage]];
    _answerFlag = NO;
}

// 答案を保存する
- (void)saveAnswer
{
    // 答案保存用ディレクトリを作成
    NSString* answerPath = [ContentManager answerPath:_workbookIndexId];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if([fileManager createDirectoryAtPath:answerPath withIntermediateDirectories:YES attributes:nil error:NULL] == NO)
    {
        return;
    }
    
    // 答案を保存
    for(NSInteger i = 0; i < [_answerList count]; i++)
    {
        UIImage* image = [_answerList objectAtIndex:i];
        NSData* data = UIImagePNGRepresentation(image);
        NSString* filePath = [answerPath stringByAppendingPathComponent:[NSString stringWithFormat:ANSWER_FILE_FORMAT, i]];
        [data writeToFile:filePath atomically:YES];
    }
    
    // データベースを設定
    NSString* where = [NSString stringWithFormat:@"WHERE workbook_index_id = %d", _workbookIndexId];
    NSArray* workbookIndexRecords = [DatabaseManager selectWorkbookIndexData:where];
    if([workbookIndexRecords count] <= 0) return;
    
    WorkbookIndexData* workbookIndexRecord = [workbookIndexRecords objectAtIndex:0];
    workbookIndexRecord.answerRawData = @"{";
    workbookIndexRecord.answerRawData = [workbookIndexRecord.answerRawData stringByAppendingString:@"\"images\":["];
    for(NSInteger i = 0; i < [_answerList count]; i++)
    {
        NSString* fileName = [NSString stringWithFormat:ANSWER_FILE_FORMAT, i];
        workbookIndexRecord.answerRawData = [workbookIndexRecord.answerRawData stringByAppendingFormat:@"\"%@\",", fileName];
    }
    workbookIndexRecord.answerRawData = [workbookIndexRecord.answerRawData stringByAppendingString:@"]"];
    workbookIndexRecord.answerRawData = [workbookIndexRecord.answerRawData stringByAppendingString:@"}"];
    workbookIndexRecord.answerFinishedAt = [[NSDate date] timeIntervalSince1970];
    
    [DatabaseManager updateWorkbookIndexData:workbookIndexRecords];
}

- (void)colorChange:(NSInteger)index
{
    // アクティブマーカー
    UIView* selectView = [self.view viewWithTag:index];
    if(selectView)
    {
        _activeColorImage.center = selectView.center;
        _activeColorImage.hidden = NO;
    }
    else
    {
        _activeColorImage.hidden = YES;
    }
    
    // 色の変更
    _paletteIndex = index;
    CGFloat red = 0, green = 0, blue = 0;
    switch (_paletteIndex) {
        case 1:
            red = 231.0f/255.0f;
            green = 38.0f/255.0f;
            blue = 25.0f/255.0f;
            break;
        case 2:
            red = 135.0f/255.0f;
            green = 72.0f/255.0f;
            blue = 152.0f/255.0f;
            break;
        case 3:
            red = 50.0f/255.0f;
            green = 131.0f/255.0f;
            blue = 58.0f/255.0f;
            break;
        case 4:
            red = 60.0f/255.0f;
            green = 188.0f/255.0f;
            blue = 211.0f/255.0f;
            break;
        case 5:
            red = 90.0f/255.0f;
            green = 180.0f/255.0f;
            blue = 49.0f/255.0f;
            break;
        case 6:
            red = 175.0f/255.0f;
            green = 102.0f/255.0f;
            blue = 49.0f/255.0f;
            break;
        case 7:
            red = 12.0f/255.0f;
            green = 120.0f/255.0f;
            blue = 170.0f/255.0f;
            break;
        case 8:
            red = 245.0f/255.0f;
            green = 205.0f/255.0f;
            blue = 31.0f/255.0f;
            break;
        case 9:
            red = 234.0f/255.0f;
            green = 101.0f/255.0f;
            blue = 161.0f/255.0f;
            break;
        case 10:
            red = 240.0f/255.0f;
            green = 133.0f/255.0f;
            blue = 25.0f/255.0f;
            break;
        default:
            red = 255.0f/255.0f;
            green = 255.0f/255.0f;
            blue = 255.0f/255.0f;
            break;
    }
    [_answerView setPenColor:red colorG:green colorB:blue];
}

- (void)togglePalette:(BOOL)instant
{
    if(_showPalette)
    {
        [self closePalette:instant];
    }
    else
    {
        [self openPalette:instant];
    }
}

- (void)openPalette:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect paletteFrame = _paletteView.frame;
    CGFloat x = paletteFrame.origin.x;
    CGFloat y = viewFrame.size.height - paletteFrame.size.height;
    CGFloat w = paletteFrame.size.width;
    CGFloat h = paletteFrame.size.height;
    if(instant)
    {
        [_paletteView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_paletteView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
    _showPalette = YES;
}

- (void)closePalette:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect paletteFrame = _paletteView.frame;
    CGFloat x = paletteFrame.origin.x;
    CGFloat y = viewFrame.size.height;
    CGFloat w = paletteFrame.size.width;
    CGFloat h = paletteFrame.size.height;
    if(instant)
    {
        [_paletteView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_paletteView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
    _showPalette = NO;
}

- (void)openClock:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect clockFrame = _clockView.frame;
    CGFloat x = viewFrame.size.width - clockFrame.size.width;
    CGFloat y = clockFrame.origin.y;
    CGFloat w = clockFrame.size.width;
    CGFloat h = clockFrame.size.height;
    if(instant)
    {
        [_clockView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_clockView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)closeClock:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect clockFrame = _clockView.frame;
    CGFloat x = viewFrame.size.width;
    CGFloat y = clockFrame.origin.y;
    CGFloat w = clockFrame.size.width;
    CGFloat h = clockFrame.size.height;
    if(instant)
    {
        [_clockView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_clockView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)openMenu:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect menuFrame = _menuView.frame;
    CGFloat x = viewFrame.size.width - menuFrame.size.width;
    CGFloat y = menuFrame.origin.y;
    CGFloat w = menuFrame.size.width;
    CGFloat h = menuFrame.size.height;
    if(instant)
    {
        [_menuView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_menuView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)closeMenu:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect menuFrame = _menuView.frame;
    CGFloat x = viewFrame.size.width;
    CGFloat y = menuFrame.origin.y;
    CGFloat w = menuFrame.size.width;
    CGFloat h = menuFrame.size.height;
    if(instant)
    {
        [_menuView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_menuView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

@end

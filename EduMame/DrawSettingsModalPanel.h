//
//  DrawSettingsModalPanel.h
//  EduMame
//
//  Created by gclue_mita on 13/01/07.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@interface DrawSettingsModalPanel : UATitledModalPanel

@property (nonatomic, weak) IBOutlet UIView *drawSettingsView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UISlider *brushControl;
@property (nonatomic, weak) IBOutlet UIImageView *brushPreview;

@property CGFloat brush;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
- (IBAction)sliderChanged:(id)sender;
- (void)initBrush:(CGFloat)aBrush;

@end

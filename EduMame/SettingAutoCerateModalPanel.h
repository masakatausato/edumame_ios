//
//  PrintAutoSettingsModalPanel.h
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@interface SettingAutoCerateModalPanel : UATitledModalPanel

@property (strong, nonatomic) IBOutlet UIView *settingAutoCreateView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

@end

//
//  MyBookGridViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/21.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "MyBookGridViewController.h"
#import "MyBookDetailModalPanel.h"
#import "MyBookshelfViewController.h"

#import "CreateManager.h"

@interface MyBookGridViewController ()
{
    int selectedIndex;
}
@end

@implementation MyBookGridViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)initialize
{
    self.aqGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.aqGridView.autoresizesSubviews = YES;
    self.aqGridView.delegate = self;
    self.aqGridView.dataSource = self;

    [self.aqGridView reloadData];
}

- (ProductData *)getProductData:(NSInteger)index
{
    return [self.productData objectAtIndex:index];
}

- (void)onDetail:(UIButton *)sender event:(UIEvent *)event
{
    int index = [self indexPathForControlEvent:event];
    
    ProductData *productData = [self getProductData:index];
    
    // 詳細ダイアログ作成
    MyBookDetailModalPanel *modalPanel = [[MyBookDetailModalPanel alloc] initWithFrame:self.view.bounds
                                                                                 title:@"詳細"];
    UIImage *image = [CreateManager getPreviewFromProduct:productData index:index];
    [modalPanel.coverImageView setImage:image];
    modalPanel.title.text = productData.title;
    //    modalPanel.publisher.text = productData
    //    modalPanel.info = productData.info;
    modalPanel.term.text = [NSString stringWithFormat:@"%d月", productData.targetTerm];
    modalPanel.level.text = [NSString stringWithFormat:@"%d", productData.level];
    
    modalPanel.onClosePressed = ^(UAModalPanel* panel) {
        // モーダルダイアログ非表示
        [panel removeFromSuperview];
    };
    
    modalPanel.delegate = self;
    [self.view addSubview:modalPanel];
    [modalPanel showFromPoint:[sender center]];
}

- (void)onFun:(UIButton *)sender event:(UIEvent *)event
{
    // アラートビューを表示
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"更新中";
    alertView.message = @"しばらくお待ちください。";
    alertView.delegate = self;
    [alertView show];
    
    // テーブルアイテムを更新
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 子供に渡す
        [self passBooktoChild];
        dispatch_async(dispatch_get_main_queue(), ^{
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        });
    });
}

- (void)willPresentAlertView:(UIAlertView*)alertView
{
    // インジケータービューを生成
    UIActivityIndicatorView* activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.frame = CGRectMake(0, alertView.frame.size.height / 2, alertView.frame.size.width, alertView.frame.size.height / 2);
    [activityIndicatorView startAnimating];
    
    // インジケータービューをアラートビューに追加
    [alertView addSubview:activityIndicatorView];
}

/**
 * passBooktoChild
 */
- (void)passBooktoChild
{
}

/**
 * indexPathForControlEvent
 * UIControlEventからタッチ位置のindexPathを取得する
 */
- (int)indexPathForControlEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint p = [touch locationInView:self.aqGridView];
    int index = [self.aqGridView indexForItemAtPoint:p];
    return index;
}

/**
 * calculateProgress
 * プロダクトIDから進捗度を算出
 */
- (CGFloat)calculateProgress:(NSInteger)productsId
{
    CGFloat progress = 0.0f;
    
    //計算手順
    //1.productIdからproduct_index取得し総数を求める
    //2.product_indexからcontentIdを取得する
    //3.workbookIndexDataからcontentIdを取得し総数を求める
    //4.2と3から割合を求める
    
    //1.
    NSString* option = [NSString stringWithFormat:@"where product_id = %d", productsId];
    
    NSArray* productIndexRecords = [DatabaseManager selectProductIndexData:option];
    NSInteger productIndexTotalCount = [productIndexRecords count];
    NSInteger usedContentTotalCount = 0;
    
    for (NSInteger i = 0; i < productIndexTotalCount; i++)
    {
        //2.
        ProductIndexData* productIndexData = [productIndexRecords objectAtIndex:i];
        
        option = [NSString stringWithFormat:@"where content_id = %d", productIndexData.contentId];
        NSArray* workbookIndexRecords = [DatabaseManager selectWorkbookIndexData:option];
        
        NSInteger workbookIndexTotalCount = [workbookIndexRecords count];
        
        for (NSInteger i = 0; i < workbookIndexTotalCount; i++)
        {
            //3.
            WorkbookIndexData* workbookIndexData = [workbookIndexRecords objectAtIndex:i];
            
            // すでに解答済みなら
            if (workbookIndexData.answerFinishedAt != 0) {
                usedContentTotalCount++;
                
                // 1個でもあれば抜ける
                break;
            }
        }
    }
    
    //4.
    progress = (CGFloat)usedContentTotalCount / (CGFloat)productIndexTotalCount;
    
    return progress;
}

#pragma mark - Grid View Methods

- (NSUInteger)numberOfItemsInGridView:(AQGridView *)aqGridView
{
    return self.productData.count;
}

- (AQGridViewCell *)gridView:(AQGridView *)aqGridView cellForItemAtIndex:(NSUInteger)index
{
    static NSString *CellIdentifier = @"CellIdentifier";
    AQGridViewCell *cell = (AQGridViewCell *)[aqGridView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"MyBookGridViewCell" owner:self options:nil];
        cell = [[AQGridViewCell alloc] initWithFrame:self.aqGridViewCell.frame
									  reuseIdentifier:CellIdentifier];
		[cell.contentView addSubview:self.aqGridViewCell];
        cell.selectionStyle = AQGridViewCellSelectionStyleNone;
        
        // セル背景色は透明
        cell.contentView.backgroundColor = nil;
        cell.backgroundColor = nil;
    }
    
    MyBookGridViewCell *content = (MyBookGridViewCell *)[cell.contentView viewWithTag:1];
    
    // プロダクトデータ取得
    ProductData *productData = [self getProductData:index];

    // タイトルセット
    [content.title setText:productData.title];
    
    // プログレスバーセット
    if (self.state == MYBOOK_STATE_PARENTS || self.state == MYBOOK_STATE_CHILD) {
        [content.progress setHidden:YES];
    } else {
        [content.progress setProgress:[self calculateProgress:productData.productsId]];
    }
    
    // 表紙画像セット
    UIImage *image = [CreateManager getPreviewFromProduct:[self getProductData:index] index:index];
    [content.coverImageView setImage:image];
    
    [content.detailButton addTarget:self action:@selector(onDetail:event:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.state == MYBOOK_STATE_CHILD) {
        [content.funButton setHidden:NO];
        [content.funButton addTarget:self action:@selector(onFun:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)gridView:(AQGridView *)aqGridView didSelectItemAtIndex:(NSUInteger)index
{
    [self.aqGridView deselectItemAtIndex:index animated:NO];
    
    [self.delegate notifyDidSelectItemAtIndex:index];
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aqGridView
{
    // Xibファイル読込    
    [[NSBundle mainBundle] loadNibNamed:@"MyBookGridViewCell" owner:self options:nil];
    if (self.state == MYBOOK_STATE_CHILD) {
        return self.aqGridViewCell.frame.size;
    } else {
        self.aqGridViewCell.frame = CGRectMake(0, 0, 200, 310);
        return self.aqGridViewCell.frame.size;
    }
}

@end

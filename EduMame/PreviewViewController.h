//
//  PreViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/10/29.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "PreviewDetailViewController.h"

@interface PreviewViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource>
{
    AQGridView *gridView;
    NSArray *previewListData;
    PreviewDetailViewController *previewDetailViewCtrl;
}

@property (nonatomic, retain) IBOutlet AQGridView *gridView;
@property (nonatomic, retain) NSArray *previewListData;
@property (nonatomic, readwrite) PreviewDetailViewController *previewDetailViewCtrl;

- (IBAction)dismiss:(id)sender;

@end

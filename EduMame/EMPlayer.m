//
//  EMPlayer.m
//  EMPreview2
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import "EMPlayer.h"
#import "EMContent.h"
#import "EMPlayerHelper.h"

@implementation EMPlayer

// コンテンツパスでEMプレイヤーを初期化する。
- (id)initWithContentPath:(NSString*)contentPath callbackHandler:(id<EMPlayerResponder>)callbackHandler
{
    [self setup:contentPath callbackHandler:callbackHandler];
    return self;
}

// EMプレイヤーをセットアップしてコンテンツを再生できる状態にする。
- (BOOL)setup:(NSString*)contentPath callbackHandler:(id<EMPlayerResponder>)callbackHandler
{
    NSError* error = nil;
    
    // コンテンツのJSONを読み込む
    NSString* jsonPath = [NSString stringWithFormat:@"%@/content.json", contentPath];
    NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
    if(jsonData == nil) return NO;
    
    // JSONを解析
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if(error != nil) return NO;
    
    // メンバの初期化
    _contentPath = contentPath;
    _callbackHandler = callbackHandler;
    _resourceList = [NSMutableDictionary dictionary];
    _title = [json objectForKey:@"title"];
    _introPrograms = [self makeContentArray:contentPath contentJSON:[json objectForKey:@"intro"] answerContent:NO];
    _answerPrograms = [self makeContentArray:contentPath contentJSON:[json objectForKey:@"answer"] answerContent:YES];

    // 初期状態では問題モード
    [self changeMode:EMPlayerModeIntro];

    NSLog(@"title=%@", _title);
    return YES;
}

// EMプレイヤーをクリーンアップする。
- (void)cleanup
{
    [self stopSound];
    [self stopMovie];
    
    [_resourceList removeAllObjects];
}

// プレイヤーモードを変更する。
- (void)changeMode:(NSInteger)mode
{
    [self stopSound];
    [self stopMovie];
    
    _currentMode = mode;
    _currentProgramIndex = 0;
    _waitTimer = 0.0f;
    _waitInfinity = NO;
    _waitSound = NO;
    _waitMovie = NO;
    _currentSound = nil;
    _currentMovie = nil;
}

// 再生を更新する。
- (void)update:(Float32)progress
{
    // 無限ウェイトをチェック
    if(_waitInfinity) return;
    
    // ウェイトタイマーを処理
    if(_waitTimer > 0)
    {
        _waitTimer -= progress;
        return;
    }
    
    // 音声ウエイトをチェック
    if(_waitSound && _currentSound && _currentSound.playing)
    {
        return;
    }

    // 動画ウエイトをチェック
    if(_waitMovie && _currentMovie && CMTimeCompare(_avplayer.currentTime, _currentMovie.duration) < 0)
    {
        return;
    }

    // 現在のプログラムを取得
    EMContent* program = [self currentProgram];
    if(program == nil) return;
    
    // 遷移処理
    switch(program.changeMode)
    {
        case EMChangeModeNone:
            // 再生終了
            _waitInfinity = YES;
            break;
            
        case EMChangeModeTap:
            // タップを待つ
            _waitInfinity = YES;
            break;
            
        case EMChangeModeAuto:
            // 次のコンテンツを再生
            [self nextSequence];
            break;
            
        case EMChangeModeAnswer:
            // 解答を待つ
            [self nextSequence];
            break;
    }
}

// プログラムインデックスを進めて次のシーケンスに遷移する。
- (void)nextSequence
{
    // 最終シーケンスであるかチェック
    EMContent* program = [self currentProgram];
    if(program == nil) return;
    
    // 遷移方法ごとの処理
    switch(program.changeMode)
    {
        case EMChangeModeNone:
            // 再生終了
            return;
            
        case EMChangeModeTap:
            // タップを待つ
            [_callbackHandler playerEndTap];
            break;
            
        case EMChangeModeAuto:
            // 次のコンテンツを再生
            break;
            
        case EMChangeModeAnswer:
            // 解答を待つ
            [_callbackHandler playerEndAnswer];
            break;
    }

    // ポーズを解除
    _waitInfinity = NO;
    _waitSound = NO;
    _waitMovie = NO;
    
    // シーケンスを進める
    _currentProgramIndex++;
    
    // コンテンツの再生
    program = [self currentProgram];
    switch(program.resourceType)
    {
        case EMResourceTypeImage:
        {
            // 画像リソース
            if(!(program.attributes & EMAttributeContinueSound))
            {
                [self stopSound];
            }

            UIImage* image = [_resourceList objectForKey:program.resourceFileName];
            [_callbackHandler playerSetImage:image];
            break;
        }
            
        case EMResourceTypeSound:
        {
            // 音声リソース
            [self stopSound];
            
            if(program.attributes & EMAttributeResourceFit)
            {
                _waitSound = YES;
            }
            
            AVAudioPlayer* audioPlayer = [_resourceList objectForKey:program.resourceFileName];
            [audioPlayer play];
            _currentSound = audioPlayer;
            break;
        }
            
        case EMResourceTypeMovie:
        {
            // 動画リソース
            [self stopMovie];
            
            if(program.attributes & EMAttributeResourceFit)
            {
                _waitMovie = YES;
            }

            AVPlayerItem* playerItem = [_resourceList objectForKey:program.resourceFileName];
            if(!_avplayer || _avplayer.currentItem != playerItem)
            {
                _avplayer = [AVPlayer playerWithPlayerItem:playerItem];                
            }
            else
            {
                [playerItem seekToTime:CMTimeMake(0, 600)];
            }
            [_callbackHandler playerPlayMovie:_avplayer playerItem:playerItem];
            [_avplayer play];
            _currentMovie = playerItem;
            break;
        }
    }

    // ウェイトタイマーの設定
    if(program.attributes & EMAttributeInfinity)
    {
        // 時間無制限属性が設定されている場合は無限に待つ
        _waitInfinity = YES;
    }
    else
    {
        _waitTimer = program.displayTime;
    }

    // 再生開始を検出
    if(_currentProgramIndex == 1)
    {
        [_callbackHandler playerStartContent];
    }

    // 遷移方法ごとの処理
    switch(program.changeMode)
    {
        case EMChangeModeNone:
            // 再生終了
            [_callbackHandler playerEndContent];
            break;
            
        case EMChangeModeTap:
            // タップを待つ
            [_callbackHandler playerStartTap];
            break;
            
        case EMChangeModeAuto:
            // 次のコンテンツを再生
            break;
            
        case EMChangeModeAnswer:
            // 解答を待つ
            if(!(program.attributes & EMAttributeContinueSound))
            {
                [self stopSound];
            }
            [self stopMovie];
            [_callbackHandler playerStartAnswer];
            break;
    }
    
}

// シーケンスの移動
- (void)moveSequence:(NSInteger)programNo
{
    NSArray* programs = [self currentPrograms];
    
    // メディアを停止
    [self stopSound];
    [self stopMovie];
    
    // 指定シーケンスの直前にカレントプログラムを移動
    if(programNo <= 0)
    {
        _currentProgramIndex = 0;
    }
    else if(programNo >= [programs count] - 1)
    {
        _currentProgramIndex = [programs count] - 2;
    }
    else
    {
        _currentProgramIndex = programNo - 1;
    }
    //NSLog(@"_currentProgramIndex = %d", _currentProgramIndex);

    // 次のシーケンスへ
    [self nextSequence];
}

// 現在のプログラム配列を取得する。
- (NSArray*)currentPrograms
{
    switch(_currentMode)
    {
        case EMPlayerModeIntro:
            return _introPrograms;
            
        case EMPlayerModeAnswer:
            return _answerPrograms;
    }
    return nil;
}

// 現在のプログラムを取得する。
- (EMContent*)currentProgram
{
    NSArray* programs = [self currentPrograms];
    if([programs count] > _currentProgramIndex)
    {
        return [programs objectAtIndex:_currentProgramIndex];
    }
    return nil;
}

// 再生中の音声を停止する。
- (void)stopSound
{
    if(_currentSound == nil) return;
    [_currentSound stop];
    _currentSound.currentTime = 0;
    _currentSound = nil;
}

// 再生中の動画を停止する。
- (void)stopMovie
{
    if(_currentMovie == nil) return;
    [_avplayer pause];
    [_avplayer seekToTime:CMTimeMake(0, 600)];
    [_callbackHandler playerStopMovie];
    _currentMovie = nil;
}

// コンテンツ配列のを生成する。
-(NSArray*)makeContentArray:(NSString*)path contentJSON:(NSArray*)contentJSON answerContent:(BOOL)answerContent
{
    NSMutableArray* contentArray = [NSMutableArray array];
    NSError* error = nil;
    
    // 先頭は決め打ちのコンテンツ
    [contentArray addObject:[EMContent content:EMResourceTypeNone resourceFileName:@"" changeMode:EMChangeModeAuto displayTime:0]];
    
    // JSONデータを解析する
    for(NSInteger i = 0; i < [contentJSON count]; i++)
    {
        NSDictionary* content = [contentJSON objectAtIndex:i];
        
        NSString* resourceType = [content objectForKey:@"resourceType"];
        NSString* resourceFileName = [content objectForKey:@"resourceFile"];
        NSString* changeMode = [content objectForKey:@"changeMode"];
        NSString* displayTime = [content objectForKey:@"displayTime"];
        NSArray* attributes = [content objectForKey:@"attributes"];

        // リソースの読み込み
        if([[self.resourceList allKeys] containsObject:resourceFileName] == NO)
        {
            NSString* contentPath = [NSString stringWithFormat:@"%@/%@", path, resourceFileName];
            
            if([resourceType isEqualToString:@"Image"])
            {
                // 画像リソース
                NSData* imageData = [NSData dataWithContentsOfFile:contentPath];
                if(imageData == nil) continue;
                UIImage* image = [UIImage imageWithData:imageData];
                if(image == nil) continue;
                [_resourceList setObject:image forKey:resourceFileName];
            }
            else if([resourceType isEqualToString:@"Sound"])
            {
                // 音声リソース
                NSData* soundData = [NSData dataWithContentsOfFile:contentPath];
                if(soundData == nil) continue;
                AVAudioPlayer* audioPlayer = [[AVAudioPlayer alloc] initWithData:soundData error:&error];
                if(audioPlayer == nil) continue;
                [_resourceList setObject:audioPlayer forKey:resourceFileName];
            }
            else if([resourceType isEqualToString:@"Movie"])
            {
                // 動画リソース
                NSURL* videoURL = [NSURL fileURLWithPath:contentPath];
                AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:videoURL];
                if(playerItem == nil) continue;
                [_resourceList setObject:playerItem forKey:resourceFileName];
            }
        }
        
        // リソース種別をコンバートする
        NSInteger convertedResourceType = EMResourceTypeNone;
        if([resourceType isEqualToString:@"Image"]) convertedResourceType = EMResourceTypeImage;
        else if([resourceType isEqualToString:@"Sound"]) convertedResourceType = EMResourceTypeSound;
        else if([resourceType isEqualToString:@"Movie"]) convertedResourceType = EMResourceTypeMovie;
        
        // 遷移方法をコンバートする
        NSInteger convertedChangeMode = EMChangeModeNone;
        if([changeMode isEqualToString:@"Tap"]) convertedChangeMode = EMChangeModeTap;
        else if([changeMode isEqualToString:@"Auto"]) convertedChangeMode = EMChangeModeAuto;
        else if([changeMode isEqualToString:@"Answer"]) convertedChangeMode = EMChangeModeAnswer;
        
        // 表示時間をコンバートする
        Float32 convertedDisplayTime = [displayTime floatValue];
        
        // 属性をコンバートする
        NSUInteger convertedAttributes = 0;
        for(NSInteger j = 0; j < [attributes count]; j++)
        {
            NSString* attribute = [attributes objectAtIndex:j];
            if([attribute isEqualToString:@"ResourceFit"]) convertedAttributes |= EMAttributeResourceFit;
            else if([attribute isEqualToString:@"Infinity"]) convertedAttributes |= EMAttributeInfinity;
            else if([attribute isEqualToString:@"ContinueSound"]) convertedAttributes |= EMAttributeContinueSound;
        }
        
        // 属性によるデータの調整
        if(convertedAttributes & EMAttributeResourceFit)
        {
            // リソースにあわせて遷移方法と表示時間を調整
            convertedChangeMode = EMChangeModeAuto;
            convertedDisplayTime = 0;
        }
        
        // モードによるデータの調整
        if(answerContent)
        {
            convertedChangeMode = EMChangeModeTap;
            convertedDisplayTime = 0;
        }
       
        // コンテンツ配列に追加
        [contentArray addObject:[EMContent content:convertedResourceType resourceFileName:resourceFileName changeMode:convertedChangeMode displayTime:convertedDisplayTime attributes:convertedAttributes]];
    }
    
    // 終端は決め打ちのコンテンツ
    [contentArray addObject:[EMContent content:EMResourceTypeNone resourceFileName:@"" changeMode:EMChangeModeNone displayTime:0]];
    
    return contentArray;
}

@end

//
//  AddPreviewViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/14.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoverFlowViewController.h"
#import "GridViewController.h"
#import "ContentViewController.h"

@class DragAndDropView;

@interface AddPreviewViewController : UIViewController <
CoverFlowViewDelegate,
CoverFlowViewGestureDelegate,
GridViewDelegate,
GridViewGestureDelegate,
ContentViewDelegate,
ContentViewGestureDelegate
>

@property (strong, nonatomic) CoverFlowViewController *coverflowViewCtrl;
@property (strong, nonatomic) GridViewController *gridViewCtrl;
@property (strong, nonatomic) ContentViewController *contentViewCtrl;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;

@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) NSMutableArray *searchedContentData;

@property (strong, nonatomic) DragAndDropView *dragAndDropView;
@property (strong, nonatomic) NSMutableArray *dropDateTargets;
@property (strong, nonatomic) UIView *lastDropDateTargets;

@end

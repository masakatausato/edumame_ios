//
//  ViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/10/04.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "WorkbookTabelCell.h"
#import "SettingAutoCerateModalPanel.h"

#import "CreateManager.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.contentData = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialize];
    [self initializeUIBar];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // コンテンツデータ取得
    [self getContentDataOfToday];
    
    // テーブルビューリロード
    [self.workbookTableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.popOverController) {
        if ([self.popOverController isPopoverVisible]) {
            [self.popOverController dismissPopoverAnimated:YES];
        }
        self.popOverController = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{
    // 日付ラベルセット
    NSDate* date = [NSDate date];
    NSString *formatDate = [CreateManager getFormatDate:date dateFormat:@"yyyy年MM月dd日"];
    [self.workbookTitleLabel setText:[formatDate stringByAppendingString:@"の学習内容"]];
    
    // コンテンツデータ取得
    [self getContentDataOfToday];
    
    // カレンダービューセット
    self.calendarOpened = NO;
    self.calendarViewCtrl = [[CalendarViewController alloc] initWithNibName:@"CalendarView" bundle:nil];
    
    float w = self.calendarViewCtrl.view.frame.size.width;
    float h = self.calendarViewCtrl.view.frame.size.height;
    self.calendarViewCtrl.view.frame = CGRectMake(0, 0, w, h);
    CGPoint center = CGPointMake(-w/2, h/2+230);
    self.calendarViewCtrl.view.center = center;
    
    [self.view addSubview:self.calendarViewCtrl.view];
}

/**
 * initializeUIBar
 */
- (void)initializeUIBar
{
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *profileSettings = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                     target:self
                                                                                     action:@selector(touchedProfileSettings:)];
    UIBarButtonItem *applicationSettings = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                         target:self
                                                                                         action:@selector(touchedApplicationSettings:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:applicationSettings, profileSettings, nil];
}

/**
 * getContentDataOfToday
 */
- (void)getContentDataOfToday
{
    // 1.当日の日付からWorkbookData検索
    // 2.WorkbookIdからWorkbookIndexData検索
    // 3.ContentIdからContentData検索
    
    // 日付取得
    NSDate* date = [NSDate date];
    NSString *formatDate = [CreateManager getFormatDate:date dateFormat:@"yyyy-MM-dd"];
    
    // WorkbookData検索
    NSString* option = @"where name =";
    option = [option stringByAppendingFormat:@" '%@'", formatDate];
    
    NSArray *workbookRecord = [DatabaseManager selectWorkbookData:option];
    
    // WorkbookDataがある場合はデータ取得。なければ抜ける
    if ([workbookRecord count] < 1)
        return;

    // Workbookは日付毎に1件のみ
    WorkbookData *workbookData = [workbookRecord objectAtIndex:0];
        
    // workbookIdからWorkbookIndexDataを検索
    option = @"where workbook_id =";
    option = [option stringByAppendingFormat:@" '%d'", workbookData.workbookId];
        
    NSArray *workbookIndexRecord = [DatabaseManager selectWorkbookIndexData:option];
    
    // WorkbookIndexDataがある場合はデータ取得。なければ抜ける
    if ([workbookIndexRecord count] < 1)
        return;
    
    self.contentData = [[NSMutableArray array] init];
    
    for (int i = 0; i < [workbookIndexRecord count]; i++) {
        WorkbookIndexData *workbookIndexData = [workbookIndexRecord objectAtIndex:i];
        
        // contentIdからContentData検索
        option = @"where contents_id =";
        option = [option stringByAppendingFormat:@" '%d'", workbookIndexData.contentId];
        NSArray *targetContentRecord = [DatabaseManager selectContentData:option];
        
        for (int j = 0; j < [targetContentRecord count]; j++) {
            ContentData *contentData = [targetContentRecord objectAtIndex:j];
            [self.contentData addObject:contentData];
        }
    }
}

#pragma mark - Profile & Application Settings

- (IBAction)touchedProfileSettings:(id)sender
{
    [self showProfilePopupOverView:sender];
}

- (IBAction)touchedApplicationSettings:(id)sender
{
    [self showApplicationPopupOverView:sender];
}

- (void)showProfilePopupOverView:(id)sender
{
    if (![self.popOverController isPopoverVisible])
    {
		self.profilePopOverView = [[SettingProfilePopOverView alloc] initWithNibName:@"SettingProfilePopOverView" bundle:nil];
		self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.profilePopOverView];
		[self.popOverController setPopoverContentSize:CGSizeMake(300.0f, 300.0f)];
        
        // ナビゲーションバーにPopoverを表示する
        [self.popOverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                                       animated:YES];
	}
    else
    {
		[self.popOverController dismissPopoverAnimated:YES];
	}
}

- (void)showApplicationPopupOverView:(id)sender
{
    if (![self.popOverController isPopoverVisible])
    {
		self.applicationPopOverView = [[SettingApplicationPopOverView alloc] initWithNibName:@"SettingApplicationPopOverView" bundle:nil];
        [self.applicationPopOverView setDelegate:self];
        
		self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.applicationPopOverView];
		[self.popOverController setPopoverContentSize:CGSizeMake(300.0f, 390.0f)];
        
        // ナビゲーションバーにPopoverを表示する
        [self.popOverController presentPopoverFromBarButtonItem:sender
                                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                                       animated:YES];
	}
    else
    {
		[self.popOverController dismissPopoverAnimated:YES];
	}
}

- (void)notifySettingAutoCreate
{
    [self.popOverController dismissPopoverAnimated:YES];
    
    SettingAutoCerateModalPanel* modalPanel = [[SettingAutoCerateModalPanel alloc] initWithFrame:self.view.bounds title:@"プリント自動生成設定"];
//    modalPanel.delegate = self;
    
    [self.view addSubview:modalPanel];
    [modalPanel show];
}

#pragma mark - Calendar Methods

- (IBAction)onCalendar:(id)sender
{
    UIButton *btn = (UIButton *)sender;
	
    //アニメーションの対象となるコンテキスト
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    //アニメーションを実行する時間
    [UIView setAnimationDuration:0.5];
    //アニメーションイベントを受け取るview
    [UIView setAnimationDelegate:self];
    //アニメーション終了後に実行される
    [UIView setAnimationDidStopSelector:@selector(endAnimation)];
    
    float w = self.calendarViewCtrl.view.frame.size.width;
    float h = self.calendarViewCtrl.view.frame.size.height;
    
    if (self.calendarOpened == NO) {
        CGPoint center = CGPointMake(w/2, h/2+230);
        self.calendarViewCtrl.view.center = center;
        
        CGAffineTransform translate = CGAffineTransformMakeTranslation(w, 0.0f);
        [btn setTransform:translate];
    } else {
        CGPoint center = CGPointMake(-w/2, h/2+230);
        self.calendarViewCtrl.view.center = center;
        
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0f, 0.0f);
        [btn setTransform:translate];
    }
    
    self.calendarOpened = !self.calendarOpened;
    
    // アニメーション開始
    [UIView commitAnimations];
}

#pragma mark - Point Of The Month Methods

- (IBAction)onAdvice:(id)sender
{
    [self.pointTitleLabel setText:@"今月のアドバイス"];
    [self.pointView setHidden:NO];
}

- (IBAction)onGoal:(id)sender
{
    [self.pointTitleLabel setText:@"今月の目標"];
    [self.pointView setHidden:NO];
}

- (IBAction)onDexterity:(id)sender
{
    [self.pointTitleLabel setText:@"今月の巧緻性"];
    [self.pointView setHidden:NO];
}

- (IBAction)onExercise:(id)sender
{
    [self.pointTitleLabel setText:@"今月の運動"];
    [self.pointView setHidden:NO];
}

- (IBAction)onProduction:(id)sender
{
    [self.pointTitleLabel setText:@"今月の制作"];
    [self.pointView setHidden:NO];
}

- (IBAction)onPictureBook:(id)sender
{
    [self.pointTitleLabel setText:@"今月の絵本"];
    [self.pointView setHidden:NO];
}

- (IBAction)onPointClose:(id)sender
{
    [self.pointView setHidden:YES];
}

#pragma mark - Table View Methods

/**
 * セクション数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)learningTableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentData count];
}


static NSString* genreImageTable[] =
{
    @"ChildIntroKagami",
    @"ChildIntroKasaneZukei",
    @"ChildIntroKazu",
    @"ChildIntroNakamaSagashi",
    @"ChildIntroSeesaw",
    @"ChildIntroShiritori",
};
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WorkbookTableCell";
    
    WorkbookTabelCell *cell = (WorkbookTabelCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WorkbookTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    ContentData *contentData = [self.contentData objectAtIndex:indexPath.row];
    cell.categoryImageView.image = [UIImage imageNamed:genreImageTable[arc4random() % 5]];
    cell.titleLabel.text = contentData.title;
    cell.descriptionLabel.text = contentData.description;
    
    //TODO: WorkbookIndexから終了済みかどうかの取得
//    cell.doneImageView = ;
    
    return cell;
}

/**
 * セルの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72.0;
}

/**
 * セル選択時
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

@end

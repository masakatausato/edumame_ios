//
//  ChildHomeViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildHomeViewController.h"
#import "ChildHomeSettingsModalPanel.h"

@implementation ChildHomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)pushSettingsButton:(id)sender
{
    ChildHomeSettingsModalPanel* modalPanel = [[ChildHomeSettingsModalPanel alloc] initWithFrame:self.view.bounds title:@"せってい"];
    modalPanel.delegate = self;
    
    [self.view addSubview:modalPanel];
    [modalPanel show];
    
}

- (IBAction)doDismiss:(id)sender
{
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    if (index != NSNotFound && index > 0)
    {
        UIViewController* backViewController = [self.navigationController.viewControllers objectAtIndex:(index - 1)];
        [self.navigationController popToViewController:backViewController animated:YES];
    }
}

@end

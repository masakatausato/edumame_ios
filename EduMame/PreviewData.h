//
//  PreviewData.h
//  EduMame
//
//  Created by gclue_mita on 12/10/30.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreviewData : NSObject

@property (nonatomic, retain) UIImage* image;

- (id)initWithImage:(UIImage*)theImage;
+ (NSArray*)getPreviewData;

@end

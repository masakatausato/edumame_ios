//
//  ParentData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParentData : NSObject

@property (nonatomic) NSInteger parentsId;

@property (nonatomic) NSString* userId;

@property (nonatomic) NSString* username;

@property (nonatomic) NSString* email;

@property (nonatomic) NSString* familyName;

@property (nonatomic) NSString* firstName;

@property (nonatomic) NSString* familyNameKana;

@property (nonatomic) NSString* firstNameKana;

@property (nonatomic) NSString* zipCode1;

@property (nonatomic) NSString* zipCode2;

@property (nonatomic) NSString* prefectureCode;

@property (nonatomic) NSString* city;

@property (nonatomic) NSString* address;

@property (nonatomic) NSString* building;

@property (nonatomic) NSString* tel;

@property (nonatomic) NSString* birthday;

@property (nonatomic) NSString* password;

@property (nonatomic) NSString* secretQuestion;

@property (nonatomic) NSString* secretQuestionAnswer;

- (id)init;

- (id)initWithParentsId:(NSInteger)parentsId userId:(NSString*)userId username:(NSString*)username email:(NSString*)email familiName:(NSString*)familyName firstName:(NSString*)firstName familyNameKana:(NSString*)familyNameKana firstNameKana:(NSString*)firstNameKana zipCode1:(NSString*)zipCode1 zipCode2:(NSString*)zipCode2 prefectureCode:(NSString*)prefectureCode city:(NSString*)city address:(NSString*)address building:(NSString*)building tel:(NSString*)tel birthday:(NSString*)birthday password:(NSString*)password secretQuestion:(NSString*)secretQuestion secretQuestionAnswer:(NSString*)secretQuestionAnswer;

@end

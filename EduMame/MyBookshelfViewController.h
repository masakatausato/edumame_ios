//
//  MyBookshelfViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/10/17.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookGridViewController.h"

typedef enum MYBOOK_STATE {
    MYBOOK_STATE_NONE = -1,
    MYBOOK_STATE_PRODUCTS,
    MYBOOK_STATE_PARENTS,
    MYBOOK_STATE_CHILD
} MyBookState;

@interface MyBookshelfViewController : UIViewController <MyBookGridViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView* scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl* pageControl;

@property (weak, nonatomic) IBOutlet UILabel *subtitle;

@property (strong, nonatomic) NSMutableArray *productData;

@property (strong, nonatomic) NSMutableArray *pageBothProductList;
@property (strong, nonatomic) NSMutableArray *pageParentsProductList;
@property (strong, nonatomic) NSMutableArray *pageChildProductList;

@property (strong, nonatomic) NSMutableArray *currentPageViewController;

- (IBAction)changePage;

@end

//
//  PreviewData.m
//  EduMame
//
//  Created by gclue_mita on 12/10/30.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "PreviewData.h"

@implementation PreviewData

@synthesize image;

- (id)initWithImage:(UIImage*)theImage
{
    self = [self init];
    if (self) {
        self.image = theImage;
    }
    return self;
}

+ (NSArray*)getPreviewData
{
    PreviewData *print1 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_1.png"]];
    PreviewData *print2 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_2.png"]];
    PreviewData *print3 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_3.png"]];
    PreviewData *print4 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_4.png"]];
    PreviewData *print5 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_5.png"]];
    PreviewData *print6 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_6.png"]];
    PreviewData *print7 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_7.png"]];
    PreviewData *print8 = [[PreviewData alloc] initWithImage:[UIImage imageNamed:@"print_sample_8.png"]];
    
    return [NSArray arrayWithObjects:print1, print2, print3, print4, print5, print6, print7, print8, nil];
}

@end

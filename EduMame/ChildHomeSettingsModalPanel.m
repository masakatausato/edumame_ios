//
//  ChildHomeSettingsModalPanel.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/22.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildHomeSettingsModalPanel.h"

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

@implementation ChildHomeSettingsModalPanel

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
        
        [[NSBundle mainBundle] loadNibNamed:@"ChildHomeSettings" owner:self options:nil];
        
        self.margin = UIEdgeInsetsMake(140, 300, 140.0, 300.0);
        self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  PlanConfirmTableItem.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

enum
{
    PlanConfirmTableItemStatusCurrent = 0,
    PlanConfirmTableItemStatusFinished,
    PlanConfirmTableItemStatusFuture,
};

@interface PlanConfirmTableItem : NSObject

@property (nonatomic) NSInteger workbookIndexId;
@property (nonatomic) NSInteger contentId;
@property (nonatomic) NSInteger status;
@property (nonatomic) BOOL answered;

@property (nonatomic) NSString* date;
@property (nonatomic) NSString* category;
@property (nonatomic) NSString* subcategory;
@property (nonatomic) NSString* contentTitle;
@property (nonatomic) NSString* productTitle;
@property (nonatomic) NSInteger level;
@property (nonatomic) NSInteger evaluation;
@property (nonatomic) NSInteger mark;
@property (nonatomic) NSString* comment;
@property (nonatomic) NSInteger history;

- (id)init;

@end

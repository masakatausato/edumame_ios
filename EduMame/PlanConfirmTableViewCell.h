//
//  PlanConfirmTableViewCell.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanConfirmTableItem.h"

@protocol PlanConfirmTableViewCellResponder <NSObject>

- (void)pushCell:(id)viewCell columnIndex:(NSInteger)columnIndex;

@end



@interface PlanConfirmTableViewCell : UITableViewCell

@property id<PlanConfirmTableViewCellResponder> delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier columns:(NSArray*)columns;

- (void)reloadCellForItem:(PlanConfirmTableItem*)item;

- (void)willDisplayCellForItem:(PlanConfirmTableItem*)item;

@end

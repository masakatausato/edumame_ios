//
//  ChildHomeViewController.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAModalPanel.h"

@interface ChildHomeViewController : UIViewController<UAModalPanelDelegate>

@end

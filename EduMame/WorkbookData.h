//
//  WorkbookData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkbookData : NSObject

@property (nonatomic) NSInteger workbookId;
@property (nonatomic) NSInteger plannedAt;
@property (nonatomic) NSString* name;
@property (nonatomic) NSInteger childId;

- (id)init;

- (id)initWithWorkbookId:(NSInteger)workbookId plannedAt:(NSInteger)plannedAt name:(NSString*)name childId:(NSInteger)childId;

@end

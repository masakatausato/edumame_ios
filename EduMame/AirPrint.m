//
//  AirPrint.m
//  EduMame
//
//  Created by gclue_mita on 13/02/08.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AirPrint.h"

@interface AirPrint ()
{
    id delegate;
}
@end

@implementation AirPrint

- (void)printContent:(id)pic completionHandler:(UIPrintInteractionCompletionHandler)completionHandler
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        [pic presentFromRect:_printButton.frame inView:_printButton.superview animated:YES completionHandler:completionHandler];
    } else {
        [pic presentAnimated:YES completionHandler:completionHandler];
    }
}
 
- (IBAction)print:(id)sender fileName:(NSString *)fileName type:(NSString *)type
{
//    NSString *fileName = @"airprint_sample";
//    NSString *type = @"png";
 
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    NSData *myData = [NSData dataWithContentsOfFile:path];
 
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
 
    if (pic && [UIPrintInteractionController canPrintData:myData]) {
        pic.delegate = delegate;
 
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexNone;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
 
        pic.printingItem = myData;
 
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            //self.content = nil;
            if (!completed && error) {
                NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
 
        [self printContent:pic completionHandler:completionHandler];
    }
}

@end

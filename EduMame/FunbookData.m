//
//  FunbookData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "FunbookData.h"

@implementation FunbookData

- (id)init
{
    self = [super init];
    if(self){
        self.funbookId = -1;
        self.productId = -1;
        self.childId = -1;
    }
    return self;
}

- (id)initWithIFunbookId:(NSInteger)funbookId productId:(NSString*)productId childId:(NSInteger)childId
{
    self = [super init];
    if(self){
        self.funbookId = funbookId;
        self.productId = productId;
        self.childId = childId;
    }
    return self;
}

@end

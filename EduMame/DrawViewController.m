//
//  DrawViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/12/11.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DrawViewController.h"
#import "DrawSettingsModalPanel.h"
#import <time.h>

#define RANDOM_FLOAT_BETWEEN(x, y) (((float) rand() / RAND_MAX) * (y - x) + x)

@implementation DrawViewController

@synthesize starEmitter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    red = 0.0f/255.0f;
    green = 0.0f/255.0f;
    blue = 0.0f/255.0f;
    brush = 10.0f;
    opacity = 1.0f;
    colorPressedTag = 12;
    isShowDrawSettings = NO;
    isShowPalette = NO;
    
    [super viewDidLoad];
    
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]
                              initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                              target:self
                              action:@selector(trash:)];
    
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]
                              initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                              target:self
                              action:@selector(save:)];
    
    self.navigationDrawItem.rightBarButtonItems = [NSArray arrayWithObjects:item1, item2, nil];
	
    [self initEmittor];
    [self colorChange:1];
    [self closePalette:YES];

    _menuView.alpha = 0.0f;
    menuTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(onTimer:) userInfo:nil repeats:NO];
}

- (void)onTimer:(NSTimer*)timer
{
    [UIView animateWithDuration:0.5f animations:^{
        _menuView.alpha = 1.0f;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initEmittor
{
    CGRect viewBounds = self.view.layer.bounds;
    
	self.starEmitter = [CAEmitterLayer layer];
	self.starEmitter.emitterPosition = CGPointMake(viewBounds.size.width/2.0, viewBounds.size.height/2.0);
	self.starEmitter.emitterSize	= CGSizeMake(10, 10);
	self.starEmitter.emitterMode	= kCAEmitterLayerVolume;
	self.starEmitter.emitterShape	= kCAEmitterLayerSphere;
	self.starEmitter.renderMode		= kCAEmitterLayerAdditive;
    
    NSMutableArray* emitterCellArray = [NSMutableArray array];
    for(NSInteger i = 0; i < 6; i++)
    {
        CAEmitterCell* star = [CAEmitterCell emitterCell];
        star.velocity = 50.0;
        star.velocityRange = 100.0;
        star.emissionRange = 2.0 * M_PI;
        star.alphaSpeed = -1.0 * 2.0 / 4.0;
        star.lifetime = 0.5;
        star.scale = 0.25;
        star.scaleRange = 0.5;
        star.spin = -90.0 * ((2.0 * M_PI) / 360.0);
        star.spinRange = 180.0 * ((2.0 * M_PI) / 360.0);
        star.contents = (id)[[UIImage imageNamed:@"star"] CGImage];

        [star setName:[NSString stringWithFormat:@"star%d", i]];
        switch(i)
        {
            case 0:
                star.color = [[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.25] CGColor];
                break;
            case 1:
                star.color = [[UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.25] CGColor];
                break;
            case 2:
                star.color = [[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.25] CGColor];
                break;
            case 3:
                star.color = [[UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.25] CGColor];
                break;
            case 4:
                star.color = [[UIColor colorWithRed:1.0 green:0.0 blue:1.0 alpha:0.25] CGColor];
                break;
            case 5:
                star.color = [[UIColor colorWithRed:0.0 green:1.0 blue:1.0 alpha:0.25] CGColor];
                break;
        }
        
        [emitterCellArray addObject:star];
    }
	self.starEmitter.emitterCells = emitterCellArray;
	[self.view.layer addSublayer:self.starEmitter];
}

- (void)colorChange:(NSInteger)index
{
    // アクティブマーカー
    UIView* selectView = [self.view viewWithTag:index];
    if(selectView)
    {
        CGFloat x = selectView.frame.origin.x + (selectView.frame.size.width / 2);
        CGFloat y = selectView.frame.origin.y + (selectView.frame.size.height / 2);
        _activeColorImage.center = CGPointMake(x, y);
        _activeColorImage.hidden = NO;
    }
    else
    {
        _activeColorImage.hidden = YES;
    }
    
    // 色の変更
    colorPressedTag = index;
    switch (colorPressedTag) {
        case 1:
            red = 231.0f/255.0f;
            green = 38.0f/255.0f;
            blue = 25.0f/255.0f;
            break;
        case 2:
            red = 135.0f/255.0f;
            green = 72.0f/255.0f;
            blue = 152.0f/255.0f;
            break;
        case 3:
            red = 50.0f/255.0f;
            green = 131.0f/255.0f;
            blue = 58.0f/255.0f;
            break;
        case 4:
            red = 60.0f/255.0f;
            green = 188.0f/255.0f;
            blue = 211.0f/255.0f;
            break;
        case 5:
            red = 90.0f/255.0f;
            green = 180.0f/255.0f;
            blue = 49.0f/255.0f;
            break;
        case 6:
            red = 175.0f/255.0f;
            green = 102.0f/255.0f;
            blue = 49.0f/255.0f;
            break;
        case 7:
            red = 12.0f/255.0f;
            green = 120.0f/255.0f;
            blue = 170.0f/255.0f;
            break;
        case 8:
            red = 245.0f/255.0f;
            green = 205.0f/255.0f;
            blue = 31.0f/255.0f;
            break;
        case 9:
            red = 234.0f/255.0f;
            green = 101.0f/255.0f;
            blue = 161.0f/255.0f;
            break;
        case 10:
            red = 240.0f/255.0f;
            green = 133.0f/255.0f;
            blue = 25.0f/255.0f;
            break;
        default:
            red = 255.0f/255.0f;
            green = 255.0f/255.0f;
            blue = 255.0f/255.0f;
            break;
    }
}

- (void)togglePalette:(BOOL)instant
{
    if(isShowPalette)
    {
        [self closePalette:instant];
    }
    else
    {
        [self openPalette:instant];
    }
}

- (void)openPalette:(BOOL)instant
{
    CGRect viewFrame = self.view.frame;
    CGRect paletteFrame = _paletteView.frame;
    if(instant)
    {
        [_paletteView setFrame:CGRectMake(paletteFrame.origin.x, viewFrame.size.height - paletteFrame.size.height, paletteFrame.size.width, paletteFrame.size.height)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_paletteView setFrame:CGRectMake(paletteFrame.origin.x, viewFrame.size.height - paletteFrame.size.height, paletteFrame.size.width, paletteFrame.size.height)];
        }];
    }
    isShowPalette = YES;
}

- (void)closePalette:(BOOL)instant
{
    CGRect viewFrame = self.view.frame;
    CGRect paletteFrame = _paletteView.frame;
    if(instant)
    {
        [_paletteView setFrame:CGRectMake(paletteFrame.origin.x, viewFrame.size.height, paletteFrame.size.width, paletteFrame.size.height)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_paletteView setFrame:CGRectMake(paletteFrame.origin.x, viewFrame.size.height, paletteFrame.size.width, paletteFrame.size.height)];
        }];
    }
    isShowPalette = NO;
}

- (IBAction)colorPressed:(id)sender
{
    UIButton *colorPressed = (UIButton*)sender;
    
    [self colorChange:colorPressed.tag];
    [self closePalette:NO];
}

- (IBAction)eraserPressed:(id)sender
{
    [self colorChange:-1];
    [self closePalette:NO];
}

- (IBAction)settingPressed:(id)sender
{
    isShowDrawSettings = YES;
    
    DrawSettingsModalPanel *modalPanel = [[DrawSettingsModalPanel alloc] initWithFrame:self.view.bounds title:@"せってい"];
    modalPanel.delegate = self;
    [modalPanel initBrush:brush];
    
    [self.view addSubview:modalPanel];
    [modalPanel showFromPoint:[sender center]];
}

- (IBAction)touchUpInsidePaletteButton:(id)sender
{
    [self togglePalette:NO];
}

- (IBAction)touchUpInsideEndButton:(id)sender
{
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    if (index != NSNotFound && index > 0)
    {
        UIViewController* backViewController = [self.navigationController.viewControllers objectAtIndex:(index - 1)];
        [self.navigationController popToViewController:backViewController animated:YES];
    }    
}

- (IBAction)touchUpInsideClearButton:(id)sender
{
    [self trash:sender];
}

- (IBAction)touchUpInsideSaveButton:(id)sender
{
    [self save:sender];
}

- (void)didCloseModalPanel:(UAModalPanel *)modalPanel
{
    isShowDrawSettings = NO;
    brush = ((DrawSettingsModalPanel*)modalPanel).brush;
}

- (void)save:(id)sender
{
    if (isShowDrawSettings) {
        return;
    }
    
    UIGraphicsBeginImageContextWithOptions(self.mainImage.bounds.size, NO, 0.0);
    [self.mainImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height)];
    UIImage *saveImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(saveImage, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    // Was there an error?
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"しっぱい"
                                                        message:@"がそうのほぞんにしっぱいしました。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"とじる",nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"せいこう"
                                                        message:@"がぞうのほぞんにせいこうしました。"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"とじる", nil];
        [alert show];
    }
}

- (void)trash:(id)sender
{
    if (isShowDrawSettings) {
        return;
    }

    self.mainImage.image = nil;
}

#pragma mark - Touch Events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isShowDrawSettings) {
        return;
    }
    
    mouseSwiped = NO;

    UITouch *touch = [[event allTouches] anyObject];
	lastPoint = [touch locationInView:self.view];

    // エミッター起動
    [CATransaction begin];
	[CATransaction setDisableActions: YES];
    starEmitter.emitterPosition = lastPoint;
    for(NSInteger i = 0; i < 6; i++)
    {
        [starEmitter setValue:[NSNumber numberWithFloat:60.0f] forKeyPath:[NSString stringWithFormat:@"emitterCells.star%d.birthRate", i]];
    }
    [CATransaction commit];

    // メニュータイマー再起動
    [UIView animateWithDuration:0.5f animations:^{
        _menuView.alpha = 0.0f;
    }];
    [menuTimer invalidate];
    menuTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(onTimer:) userInfo:nil repeats:NO];

}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isShowDrawSettings) {
        return;
    }

    mouseSwiped = YES;
//    UITouch *touch = [touches anyObject];
//    CGPoint currentPoint = [touch locationInView:self.view];
    
    UITouch *touch = [[event allTouches] anyObject];
	CGPoint touchPoint = [touch locationInView:self.view];
    
//    UIGraphicsBeginImageContext(self.view.frame.size);
    UIGraphicsBeginImageContext(self.mainImage.frame.size);
    [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height)];
    
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), touchPoint.x, touchPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    CGContextStrokePath(UIGraphicsGetCurrentContext());

    self.tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    [self.tempDrawImage setAlpha:opacity];
    UIGraphicsEndImageContext();

//	[self touchAtPosition:touchPoint emitting:YES];
    
    lastPoint = touchPoint;

    // エミッターの位置を更新
    [CATransaction begin];
	[CATransaction setDisableActions: YES];
    starEmitter.emitterPosition = touchPoint;
    [CATransaction commit];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isShowDrawSettings) {
        return;
    }

    if (!mouseSwiped) {
        UIGraphicsBeginImageContext(self.mainImage.frame.size);
        [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        self.tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIGraphicsBeginImageContext(self.mainImage.frame.size);
    [self.mainImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height) blendMode:kCGBlendModeNormal alpha:opacity];
    self.mainImage.image = UIGraphicsGetImageFromCurrentImageContext();
    self.tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
    
    UITouch *touch = [[event allTouches] anyObject];
	CGPoint touchPoint = [touch locationInView:self.view];
//	[self touchAtPosition:touchPoint emitting:NO];
    
    // エミッターを停止
    [CATransaction begin];
	[CATransaction setDisableActions: YES];
    starEmitter.emitterPosition = touchPoint;
    for(NSInteger i = 0; i < 6; i++)
    {
        [starEmitter setValue:[NSNumber numberWithFloat:0.0f] forKeyPath:[NSString stringWithFormat:@"emitterCells.star%d.birthRate", i]];
    }
    [CATransaction commit];
}

- (void)viewDidUnload
{
    [self setMainImage:nil];
    [self setTempDrawImage:nil];
    [self setNavigationDrawItem:nil];
    [super viewDidUnload];
}

#if 0
// Thread
- (void) touchAtPosition:(NSTimer *)param
{
    // 値を取り出す
    CGPoint position = [[[param userInfo] objectForKey:@"position"] CGPointValue];
    Boolean isEmitting = [[[param userInfo] objectForKey:@"emitting"] boolValue];
    
	// Bling bling..
	CABasicAnimation *burst = [CABasicAnimation animationWithKeyPath:@"emitterCells.ring.birthRate"];
	burst.fromValue			= [NSNumber numberWithFloat: 30.0];	// short but intense burst
	burst.toValue			= [NSNumber numberWithFloat: 0.0];	// each birth creates 20 aditional cells!
	burst.duration			= 0.5;
	burst.timingFunction	= [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
	
    
    // 消しゴム選択時はパーティクルを表示させない
    if (!(red == 1.0 && green == 1.0 && blue == 1.0)) {
        
        [self.starEmitter setValue:[NSNumber numberWithFloat:isEmitting ? 30 : 0] forKeyPath:@"emitterCells.ring.birthRate"];
        [self.starEmitter addAnimation:burst forKey:@"burst"];
        [self.starEmitter setValue:[NSNumber numberWithFloat:RANDOM_FLOAT_BETWEEN(0.2, 0.5)] forKeyPath:@"emitterCells.ring.scale"];

        /*
         [self.starEmitter setValue:(id)[[UIColor colorWithRed:RANDOM_FLOAT_BETWEEN(0.0, 1.0)
                                                    green:RANDOM_FLOAT_BETWEEN(0.0, 1.0)
                                                     blue:RANDOM_FLOAT_BETWEEN(0.0, 1.0)
                                                    alpha:1.0] CGColor] forKeyPath:@"emitterCells.ring.color"];
         */
    
        // 色をランダムで設定
        float redColor = 0.0, blueColor = 0.0, greenColor = 0.0;
        int num = rand() % 3 + 1;
    
        // 明るめの色が選ばれやすくなる処理
        switch (num) {
            case 1:
                redColor = 1.0;
                break;
            case 2:
                greenColor = 1.0;
                break;
            case 3:
                blueColor = 1.0;
                break;
            default:
                break;
        }
    
        [self.starEmitter setValue:(id)[[UIColor colorWithRed:RANDOM_FLOAT_BETWEEN(redColor, 1.0)
                                                        green:RANDOM_FLOAT_BETWEEN(greenColor, 1.0)
                                                         blue:RANDOM_FLOAT_BETWEEN(blueColor, 1.0)
                                                        alpha:1.0] CGColor]
                                                    forKeyPath:@"emitterCells.ring.color"];
        
    }
    
	// Move to touch point
	[CATransaction begin];
	[CATransaction setDisableActions: YES];
	self.starEmitter.emitterPosition = position;
	[CATransaction commit];
}
#endif

#pragma mark - DrawSettingsModalPanelDelegate methods

- (void)closeSettings:(id)sender
{    
    brush = ((DrawSettingsModalPanel*)sender).brush;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

//
//  GridViewCell.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "GridViewCell.h"

@implementation GridViewCell

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];
    if (self)
    {
        UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        
        [mainView addSubview:self.imageView];
        
        [self.contentView addSubview:mainView];
    }
    
    return self;
}

@end

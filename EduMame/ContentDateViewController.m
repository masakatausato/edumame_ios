//
//  ContentDateViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/13.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ContentDateViewController.h"
#import "CreateManager.h"

@interface ContentDateViewController ()

@end

@implementation ContentDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initialize];
}

- (void)viewDidUnload
{
    self.topContainerView = nil;
    self.bottomContainerView = nil;
    
    [self setDateLabel:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 */
- (void)initialize
{    
    // CoverFlowView
    self.coverflowViewCtrl = [[CoverFlowViewController alloc] init];
    self.coverflowViewCtrl.itemList = [self.contentData mutableCopy];
    
    [self addChildViewController:self.coverflowViewCtrl];
    [self.coverflowViewCtrl didMoveToParentViewController:self];
    
    // GridView
    self.gridViewCtrl = [[GridViewController alloc] init];
    self.gridViewCtrl.itemList = [self.contentData mutableCopy];
    
    [self addChildViewController:self.gridViewCtrl];
    [self.gridViewCtrl didMoveToParentViewController:self];
    
    // DateView
    self.dateViewCtrl = [[DateViewController alloc] init];
    self.dateViewCtrl.date = self.date;
    
    [self addChildViewController:self.dateViewCtrl];
    [self.dateViewCtrl didMoveToParentViewController:self];

    [self.topContainerView addSubview:self.gridViewCtrl.view];
    [self.bottomContainerView addSubview:self.dateViewCtrl.view];
    
    // ラベル用の日付を取得
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:
                                   NSYearCalendarUnit |
                                   NSMonthCalendarUnit  |
                                   NSDayCalendarUnit                                              fromDate:self.date];
    NSString *labelString = [NSString stringWithFormat:@"%d年%d月%d日", dateComps.year, dateComps.month, dateComps.day];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    label.text = labelString;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = UITextAlignmentCenter;
    label.font = [UIFont fontWithName:@"AppleGothic" size:20];
    
    [self.view addSubview:label];
}

- (void)initializeUIBar
{
    // ナビゲーションタイトル
    [self setTitle:@"プリント準備"];
    
    // ナビゲーションバーにアイテム追加
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"グリッド"
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(onChangedView:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:item, nil];
}

@end

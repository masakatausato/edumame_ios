//
//  DateViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "DateViewController.h"
#import "DateView.h"
#import "DatabaseManager.h"

#define MAX_DATE_NUM 31

@interface DateViewController ()

@end

@implementation DateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initialize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

- (void)initialize
{
    int dateNum = MAX_DATE_NUM;

    self.scrollView.contentSize = CGSizeMake(100.0*(dateNum+1), 198.0f);
    [self.scrollView setScrollEnabled:YES];
    
    [self initDateView];
    
    [self.view addSubview:self.scrollView];
}

- (void)reloadData
{
    [self initDateView];
}

- (void)initDateView
{
    for (int i = 0; i < MAX_DATE_NUM; i++)
    {
        DateView *dateView = [[DateView alloc] initWithFrame:CGRectMake(30.0f+(i*(100.0f+40.0f)), 25.0f, 100.0f, 100.0f)];
        NSDate *date = [self getNextDate:self.date addingDay:i];
        
        // WorkbookData検索
        NSString* option = @"where name =";
        option = [option stringByAppendingFormat:@" '%@'", [self getFormatDate:date dateFormat:@"yyyy-MM-dd"]];
        
        NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
        
        // プリントデータがある場合は背景色変更
        if ([targetWorkbookRecord count] > 0){
            [dateView setBackColor:[UIColor cyanColor]];
        } else {
            [dateView setBackColor:[UIColor whiteColor]];
        }
        
        dateView.dateLabel.text = [self getFormatDate:date dateFormat:@"MM/dd"];
        dateView.backgroundColor = [UIColor whiteColor];
        
        // 日付格納
        dateView.date = date;
        
        // スクロールビューに格納
        [self.scrollView addSubview:dateView];
        //NSLog(@"dateView: %@", NSStringFromCGRect(dateView.frame));
        
        // 日付オブジェクト格納
        [self.delegate notifyAddSubView:dateView];
    }
}

/**
 * getNextDate
 * 指定日から指定した日数の日付を取得する
 * @param startDate
 * @param addingDay
 * @return nextDay
 */
- (NSDate*)getNextDate:(NSDate*)startDate addingDay:(NSInteger)addingDay
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:addingDay];
    NSDate *nextDay = [cal dateByAddingComponents:comps toDate:startDate options:0];
    
    return nextDay;
}

/**
 * getFormatDate
 * 指定したフォーマットに変更した日付取得
 * @param date
 * @param dateFormat (yyyy-MM-dd HH:mm Z)
 * @return dateString
 */
- (NSString*)getFormatDate:(NSDate*)date dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

#pragma mark - Touch Event Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.delegate notifyTouchesEnded:touches withEvent:event];
}

@end

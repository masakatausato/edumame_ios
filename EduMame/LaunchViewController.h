//
//  LoginViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/12/10.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RegisterViewController;

typedef enum LOGIN_STATE {
    REGISTER,
    LOGIN
} LoginState;

@interface LaunchViewController : UIViewController
{
    LoginState state;
}

@property (strong, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)onRegister:(id)sender;
- (void)createView;
- (void)createRegisterView;
- (void)moveLoginView;

@end

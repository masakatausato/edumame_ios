//
//  ChildIntroViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildIntroViewController.h"
#import "ChildIntroPlayerViewController.h"
#import "DatabaseManager.h"

@interface ChildIntroViewController ()
{
    // アイテムリスト
    NSMutableArray* _itemList;
    
    // 選択アイテムインデックス
    NSInteger _selectItemIndex;
    
    // ボタンシートビュー
    IBOutlet UIView* _buttonSheetView;

    // 現在のページ番号
    NSInteger _currentPage;
    
    // 最大ページ数
    NSInteger _maxPage;
}
@end

@implementation ChildIntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refresh];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if([identifier isEqualToString:@"ChildIntroPlayerSegue"])
    {
        NSDictionary* item = [_itemList objectAtIndex:_selectItemIndex];

        ChildIntroPlayerViewController* nextViewController = [segue destinationViewController];
        nextViewController.contentName = [item objectForKey:@"name"];
        nextViewController.workbookIndexId = [(NSNumber*)[item objectForKey:@"workbookIndexId"] intValue];
    }
}

- (IBAction)touchUpInsideIntroButton:(id)sender
{
    NSInteger index = [sender tag];
    _selectItemIndex = index;
    [self performSegueWithIdentifier:@"ChildIntroPlayerSegue" sender:self];
}

- (IBAction)onSwipeLeft:(id)sender
{
    if(_currentPage + 1 >= _maxPage) return;
    
    for(NSInteger i = 0; i < [_buttonSheetView.subviews count]; i++)
    {
        UIView* view = [_buttonSheetView.subviews objectAtIndex:i];
        [UIView animateWithDuration:0.5 animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x - view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        }];
    }
    _currentPage++;
}

- (IBAction)onSwipeRight:(id)sender
{
    if(_currentPage <= 0) return;
    
    for(NSInteger i = 0; i < [_buttonSheetView.subviews count]; i++)
    {
        UIView* view = [_buttonSheetView.subviews objectAtIndex:i];
        [UIView animateWithDuration:0.5 animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x + view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
        }];
    }
    _currentPage--;
}

- (IBAction)doDismiss:(id)sender
{
    NSUInteger index = [self.navigationController.viewControllers indexOfObject:self];
    if (index != NSNotFound && index > 0)
    {
        UIViewController* backViewController = [self.navigationController.viewControllers objectAtIndex:(index - 1)];
        [self.navigationController popToViewController:backViewController animated:YES];
    }
}

// 画面の更新
- (void)refresh
{
    _itemList = [NSMutableArray array];
    _maxPage = 0;
    
    NSString* where = @"";
    
    //TODO:現在ログイン中の子IDを判断する必要がある。
    where = [NSString stringWithFormat:@"WHERE name = \"%@\"", [DatabaseManager dateToString:[NSDate date]]];
    NSArray* workbookRecords = [DatabaseManager selectWorkbookData:where];
    for(NSInteger i = 0; i < [workbookRecords count]; i++)
    {
        WorkbookData* workbookRecord = [workbookRecords objectAtIndex:i];
        
        //where = [NSString stringWithFormat:@"WHERE (workbook_index.workbook_id = %d) AND (workbook_index.answer_finished_at = 0)", workbookRecord.workbookId];
        where = [NSString stringWithFormat:@"WHERE workbook_index.workbook_id = %d", workbookRecord.workbookId];
        NSArray* workbookIndexRecords = [DatabaseManager selectWorkbookIndexDataRecursive:where];
        for(NSInteger j = 0; j < [workbookIndexRecords count]; j++)
        {
            NSDictionary* records = [workbookIndexRecords objectAtIndex:j];
            WorkbookIndexData* workbookIndexRecord = [records objectForKey:@"WorkbookIndex"];
            ContentData* contentRecord = [records objectForKey:@"Content"];
            
            [_itemList addObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:contentRecord.content, contentRecord.title, [NSNumber numberWithInt:workbookIndexRecord.workbookIndexId], [NSNumber numberWithInt:workbookIndexRecord.answerFinishedAt != 0], nil] forKeys:[NSArray arrayWithObjects:@"name", @"title", @"workbookIndexId", @"finished", nil]]];
        }
    }
    
    // ボタンシートをクリア
    while([_buttonSheetView.subviews count] > 0)
    {
        [[_buttonSheetView.subviews objectAtIndex:0] removeFromSuperview];
    }

    // ボタンを生成
    static NSString* genreImageTable[] =
    {
        @"ChildIntroKagami",
        @"ChildIntroKasaneZukei",
        @"ChildIntroKazu",
        @"ChildIntroNakamaSagashi",
        @"ChildIntroSeesaw",
        @"ChildIntroShiritori",        
    };
    CGFloat offsetX = _currentPage * -1024;
    NSInteger itemIndex = 0;
    while(itemIndex < [_itemList count])
    {
        UIView* sheetView = [[UIView alloc] initWithFrame:CGRectMake(offsetX + (_maxPage * 1024), 0, 1024, 768)];
        sheetView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        
        for(NSInteger j = 0; j < 6; j++)
        {
            NSDictionary* item = [_itemList objectAtIndex:itemIndex];
            NSInteger xIndex = j % 3;
            NSInteger yIndex = j / 3;
            CGFloat x = xIndex * 310;
            CGFloat y = yIndex * 310;
            //NSString* contentId = [item objectForKey:@"name"];
            NSString* title = [item objectForKey:@"title"];
            NSInteger finished = [(NSNumber*)[item objectForKey:@"finished"] integerValue];
            
            UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x + 60, y + 60, 300, 300);
            button.tag = itemIndex;
            [button setImage:[UIImage imageNamed:genreImageTable[itemIndex % 6]] forState:UIControlStateNormal];
            [button setTitle:title forState:UIControlStateNormal];
            [button addTarget:self action:@selector(touchUpInsideIntroButton:) forControlEvents:UIControlEventTouchUpInside];
            [button setEnabled:finished == 0];
            [sheetView addSubview:button];
            
            itemIndex++;
            if(itemIndex >= [_itemList count]) break;
        }
        
        [_buttonSheetView addSubview:sheetView];
        
        _maxPage++;
    }

}

@end

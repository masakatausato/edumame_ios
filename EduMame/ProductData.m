//
//  ProductData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ProductData.h"

@implementation ProductData

- (id)init
{
    self = [super init];
    if(self){
        self.productsId = -1;
        self.title = @"";
        self.description = @"";
        self.usage = @"";
        self.keyword = @"";
        self.provider = @"";
        self.author = @"";
        self.revision = 0;
        self.level = 0;
        self.targetFlag = 0;
        self.targetTerm = 0;
        self.monthTerm = 0;
        self.validTermStart = @"";
        self.validTermEnd = @"";
        self.validTermFlag = NO;
        self.invalidFlag = NO;
        self.product = @"";
        self.preview = @"";
        self.created = @"";
        self.modified = @"";
        self.categoryId = -1;
    }
    return self;
}

- (id)initWithProductsId:(NSInteger)productsId title:(NSString*)title description:(NSString*)description usage:(NSString*)usage keyword:(NSString*)keyword provider:(NSString*)provider author:(NSString*)author revision:(NSInteger)revision level:(NSInteger)level targetFlag:(NSInteger)targetFlag targetTerm:(NSInteger)targetTerm monthTerm:(NSInteger)monthTerm validTermStart:(NSString*)validTermStart validTermEnd:(NSString*)validTermEnd validTermFlag:(BOOL)validTermFlag invalidFlag:(BOOL)invalidFlag product:(NSString*)product preview:(NSString*)preview created:(NSString*)created modified:(NSString*)modified categoryId:(NSInteger)categoryId
{
    self = [super init];
    if(self){
        self.productsId = productsId;
        self.title = title;
        self.description = description;
        self.usage = usage;
        self.keyword = keyword;
        self.provider = provider;
        self.author = author;
        self.revision = revision;
        self.level = level;
        self.targetFlag = targetFlag;
        self.targetTerm = targetTerm;
        self.monthTerm = monthTerm;
        self.validTermStart = validTermStart;
        self.validTermEnd = validTermEnd;
        self.validTermFlag = validTermFlag;
        self.invalidFlag = invalidFlag;
        self.product = product;
        self.preview = preview;
        self.created = created;
        self.modified = modified;
        self.categoryId = categoryId;
    }
    return self;
}

@end

//
//  PrintAutoSettingsModalPanel.m
//  EduMame
//
//  Created by gclue_mita on 13/01/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "SettingAutoCerateModalPanel.h"

@implementation SettingAutoCerateModalPanel

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

@synthesize settingAutoCreateView = _printAutoSettingsView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
        
        [[NSBundle mainBundle] loadNibNamed:@"SettingAutoCreateView" owner:self options:nil];
        
        // UIEdgeInsetsMake(top, left, bottom, right);
        self.margin = UIEdgeInsetsMake(130.0, 300.0, 130.0, 300.0);
        self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        
        [self.contentView addSubview:self.settingAutoCreateView];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    
    CGRect frameRect = [self roundedRectFrame];
    CGRect closeRect = [self closeButtonFrame];
    [self.closeButton setFrame:CGRectMake(frameRect.size.width + closeRect.origin.x, closeRect.origin.y, 44, 44)];
    
	[self.settingAutoCreateView setFrame:self.contentView.bounds];
}

@end

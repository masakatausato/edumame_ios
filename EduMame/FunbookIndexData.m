//
//  FunbookIndexData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "FunbookIndexData.h"

@implementation FunbookIndexData

- (id)init
{
    self = [super init];
    if(self){
        self.funbookIndexId = -1;
        self.readAt = @"";
        self.canceled = NO;
        self.funbookId = -1;
    }
    return self;
}

- (id)initWithIFunbookIndexid:(NSInteger)funbookIndexId readAt:(NSString*)readAt canceled:(BOOL)canceled funbookId:(NSInteger)funbookId
{
    self = [super init];
    if(self){
        self.funbookIndexId = funbookIndexId;
        self.readAt = readAt;
        self.canceled = canceled;
        self.funbookId = funbookId;
    }
    return self;
}

@end

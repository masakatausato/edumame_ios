//
//  AppDelegate.h
//  EduMame
//
//  Created by gclue_mita on 12/11/05.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  EMContent.m
//  EMPreview
//
//  Created by Shinji Ochiai on 12/11/28.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import "EMContent.h"

@implementation EMContent

// EMコンテンツを生成して返却する。
+(EMContent*)content:(NSInteger)resourceType resourceFileName:(NSString*)resourceFileName changeMode:(NSInteger)changeMode displayTime:(Float32)displayTime
{
    return [EMContent content:resourceType resourceFileName:resourceFileName changeMode:changeMode displayTime:displayTime attributes:0];
}

// EMコンテンツを生成して返却する。
+(EMContent*)content:(NSInteger)resourceType resourceFileName:(NSString*)resourceFileName changeMode:(NSInteger)changeMode displayTime:(Float32)displayTime attributes:(NSUInteger)attributes
{
    EMContent* content = [[EMContent alloc] init];
    content.resourceType = resourceType;
    content.resourceFileName = resourceFileName;
    content.changeMode = changeMode;
    content.displayTime = displayTime;
    content.attributes = attributes;
    return content;
}

@end

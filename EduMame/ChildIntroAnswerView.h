//
//  ChildIntroAnswerView.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildIntroAnswerView : UIImageView

// ペンの色を設定
- (void)setPenColor:(CGFloat)colorR colorG:(CGFloat)colorG colorB:(CGFloat)colorB;

// 描画開始
- (void)beginDraw:(CGPoint)point;

// 描画
- (void)draw:(CGPoint)point;

// 描画終了
- (void)endDraw;

// クリア
- (void)clear;

// 解答イメージデータを生成
- (UIImage*)createAnswerImage;

@end

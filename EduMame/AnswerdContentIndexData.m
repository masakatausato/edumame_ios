//
//  AnswerdContentIndexData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AnswerdContentIndexData.h"

@implementation AnswerdContentIndexData

- (id)init
{
    self = [super init];
    if(self){
        self.contentId = -1;
        self.progress = 0;
        self.productId = -1;
    }
    return self;
}

- (id)initWithContentId:(NSInteger)contentId progress:(NSInteger)progress productId:(NSInteger)productId
{
    self = [super init];
    if(self){
        self.contentId = contentId;
        self.progress = progress;
        self.productId = productId;
    }
    return self;
}

@end

//
//  ChildIntroAnswerView.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ChildIntroAnswerView.h"

@interface ChildIntroAnswerView ()
{
    // ペンの色
    CGFloat _penColorR;
    CGFloat _penColorG;
    CGFloat _penColorB;
    
    // 描画中フラグ
    BOOL _drawing;
    
    // 直前の描画座標
    CGPoint _lastPoint;
    
    // 白紙状態のイメージ
    UIImage* _backupImage;
}
@end

@implementation ChildIntroAnswerView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        _backupImage = [UIImage imageWithCGImage:[self.image CGImage]];
    }
    return self;
}

// ペンの色を設定する。
- (void)setPenColor:(CGFloat)colorR colorG:(CGFloat)colorG colorB:(CGFloat)colorB
{
    _penColorR = colorR;
    _penColorG = colorG;
    _penColorB = colorB;
}

// 描画を開始する。
- (void)beginDraw:(CGPoint)point
{
    _drawing = YES;
    _lastPoint = point;
}

// 描画する。
- (void)draw:(CGPoint)point
{
    if(!_drawing) return;
    
    UIGraphicsBeginImageContext(self.frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.image drawInRect:CGRectMake(0, 0, self.image.size.width, self.image.size.height)];
    
    CGContextMoveToPoint(context, _lastPoint.x, _lastPoint.y);
    CGContextAddLineToPoint(context, point.x, point.y);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 10.0);
    CGContextSetRGBStrokeColor(context, _penColorR, _penColorG, _penColorB, 1.0);
    CGContextSetBlendMode(context,kCGBlendModeNormal);
    CGContextStrokePath(context);
    
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    _lastPoint = point;
}

// 描画を終了する。
- (void)endDraw
{
    _drawing = NO;
}

// イメージをクリアする。
- (void)clear
{
    self.image = [UIImage imageWithCGImage:[_backupImage CGImage]];
}

// 解答イメージデータを生成する。
- (UIImage*)createAnswerImage
{
    return [UIImage imageWithCGImage:self.image.CGImage];
}


@end

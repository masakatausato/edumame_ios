//
//  LoginViewController.m
//  EduMame
//
//  Created by gclue_mita on 12/12/10.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "LaunchViewController.h"
#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "DatabaseManager.h"

@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self initialize];
    [self createView];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

/**
 * initialize
 * 初期化
 */
- (void)initialize
{
    // DBからユーザ情報取得
    NSString *userName = nil;
    NSArray* parentRecords = [DatabaseManager selectParentData:nil];
    int parentCnt = [parentRecords count];
    if (parentCnt <= 0) {
        state = REGISTER;
    } else {
        for (NSInteger i = 0; i < parentCnt; i++) {
            ParentData* parentRecord = [parentRecords objectAtIndex:i];
            userName = parentRecord.username;
        }
        
        if (userName == nil) {
            state = REGISTER;
        } else {
            state = LOGIN;
        }
    }
}

/**
 * createView
 */
- (void)createView
{
    switch (state) {
        case REGISTER:
        {
            [self createRegisterView];
        }
            break;
        case LOGIN:
        {
            [self moveLoginView];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Register Process

/**
 *
 */
- (void)createRegisterView
{
    self.navigationItem.title = @"はじめまして";
#if 0
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[registerButton setTitle:@"新規登録" forState:UIControlStateNormal];
	[registerButton sizeToFit];
    registerButton.frame = CGRectMake(0, 0, 150, 50);
    registerButton.center = CGPointMake(self.view.center.y, self.view.center.x-44);

    [registerButton addTarget:self action:@selector(registerPressed) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:registerButton];
#endif
}

/**
 * onRegister
 * 新規登録ボタン処理
 */
- (IBAction)onRegister:(id)sender
{    
//    RegisterViewController *registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
//    [self.navigationController pushViewController:registerView animated:NO];
    
    [self performSegueWithIdentifier:@"RegisterSegue" sender:self];
}

#pragma mark - Login Process

/**
 * moveLoginView
 * ログイン画面へ遷移
 */
- (void)moveLoginView
{    
//    LoginViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    [self.navigationController pushViewController:loginView animated:NO];
    
    [self performSegueWithIdentifier:@"LoginSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* identifier = segue.identifier;
    if([identifier isEqualToString:@"RegisterSegue"])
    {
    }
    if([identifier isEqualToString:@"LoginSegue"])
    {
    }
}

@end

//
//  CategoryData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryData : NSObject

@property (nonatomic) NSInteger categoryId;
@property (nonatomic) NSString* name;

- (id)init;

- (id)initWithCategoryId:(NSInteger)categoryId name:(NSString*)name;

@end

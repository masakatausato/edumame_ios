//
//  SoundBGM.h
//  EduMame
//
//  Created by GClue on 13/02/04.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface SoundBGM : NSObject

- (id) initBGM:(NSString *) bgm type:(NSString *) ofType loop:(int)loop;
- (void) playBGM;
- (void) pauseBGM;
- (void) stopBGM;
- (float) getVolume;
- (void) setVolume:(double)volume;

@end

//
//  EMPlayer.h
//  EMPreview2
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "EMPlayerDefine.h"
#import "EMPlayerResponder.h"
#import "EMContent.h"

@interface EMPlayer : NSObject

// コンテンツのパス
@property (readonly) NSString* contentPath;

// プレイヤーコールバックハンドラ
@property (readonly) id<EMPlayerResponder> callbackHandler;

// タイトル
@property (readonly) NSString* title;

// 問題プログラムの配列
@property (readonly) NSArray* introPrograms;

// 解答プログラムの配列
@property (readonly) NSArray* answerPrograms;

// 現在のモード
@property (readonly) NSInteger currentMode;

// 現在のプログラムインデックス
@property (readonly) NSInteger currentProgramIndex;

// ウェイト時間
@property (readonly) Float32 waitTimer;

// 無限ウェイトフラグ
@property (readonly) BOOL waitInfinity;

// 音声ウェイトフラグ
@property (readonly) BOOL waitSound;

// 動画ウェイトフラグ
@property (readonly) BOOL waitMovie;

// リソースリスト
@property (readonly) NSMutableDictionary* resourceList;

// AVプレイヤー
@property (readonly) AVPlayer* avplayer;

// 現在再生中の音声リソース
@property (readonly) AVAudioPlayer* currentSound;

// 現在再生中の動画リソース
@property (readonly) AVPlayerItem* currentMovie;

// コンテンツパスで初期化
- (id)initWithContentPath:(NSString*)contentPath callbackHandler:(id<EMPlayerResponder>)callbackHandler;

// コンテンツのセットアップ
- (BOOL)setup:(NSString*)contentPath callbackHandler:(id<EMPlayerResponder>)callbackHandler;

// コンテンツをクリーンアップ
- (void)cleanup;

// モードを変更
- (void)changeMode:(NSInteger)mode;

// 更新
- (void)update:(Float32)progress;

// 次のシーケンスに進行
- (void)nextSequence;

// シーケンスの移動
- (void)moveSequence:(NSInteger)programNo;

// 現在のプログラム配列を取得する。
- (NSArray*)currentPrograms;

// 現在のプログラムを取得する。
- (EMContent*)currentProgram;

@end

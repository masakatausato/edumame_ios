//
//  ChildCalendarEventData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/05.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
CREATE TABLE child_calendar_events (
                                    child_calendar_event_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    date TEXT NOT NULL DEFAULT 0,
                                    evaluation INTEGER NOT NULL DEFAULT 0
                                    );
 */

@interface ChildCalendarEventData : NSObject

@property (nonatomic) NSInteger childCalendarEventId;
@property (nonatomic) NSString* date;
@property (nonatomic) NSInteger evaluation;
@property (nonatomic) NSInteger childId;

- (id)init;

- (id)initWithChildCalendarEventId:(NSInteger)childCalendarEventId date:(NSString*)date evaluation:(NSInteger)evaluation childId:(NSInteger)childId;

@end

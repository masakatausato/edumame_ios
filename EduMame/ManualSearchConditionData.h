//
//  ManualSearchConditionData.h
//  EduMame
//
//  Created by gclue_mita on 13/02/18.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManualSearchConditionData : NSObject

@property (nonatomic) NSInteger target;
@property (copy, nonatomic) NSString *category;
@property (copy, nonatomic) NSString *level;
@property (copy, nonatomic) NSString *term;

@end

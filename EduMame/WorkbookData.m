//
//  WorkbookData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "WorkbookData.h"

@implementation WorkbookData

- (id)init
{
    self = [super init];
    if(self){
        self.workbookId = -1;
        self.plannedAt = 0;
        self.name = @"";
        self.childId = -1;
    }
    return self;
}

- (id)initWithWorkbookId:(NSInteger)workbookId plannedAt:(NSInteger)plannedAt name:(NSString*)name childId:(NSInteger)childId;
{
    self = [super init];
    if(self){
        self.workbookId = workbookId;
        self.plannedAt = plannedAt;
        self.name = name;
        self.childId = childId;
    }
    return self;
}

@end

//
//  DragAndDropView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/17.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "DragAndDropView.h"
#import "DatabaseManager.h"

@implementation DragAndDropView

@synthesize view = _view;
@synthesize contentData = _contentData;
@synthesize contentArray = _contentArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

@end

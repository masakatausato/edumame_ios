//
//  PrintGridViewController.m
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PrintGridViewController.h"
#import "PrintGridViewCell.h"

@interface PrintGridViewController ()

@end

@implementation PrintGridViewController

#pragma mark - Grid View Methods

- (NSUInteger)numberOfItemsInGridView:(AQGridView *)aqGridView
{
    return self.itemList.count;
}

- (AQGridViewCell *)gridView:(AQGridView *)aqGridView cellForItemAtIndex:(NSUInteger)index
{
    static NSString *GridViewCellIdentifier = @"GridViewCellIdentifier";
    PrintGridViewCell *cell = (PrintGridViewCell *)[aqGridView dequeueReusableCellWithIdentifier:GridViewCellIdentifier];
    if (cell == nil) {
        cell = [[PrintGridViewCell alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)
                                   reuseIdentifier:GridViewCellIdentifier];
    }
    
    UIImage *image = [self getPreview:index];
    [cell.imageView setImage:image];
    
    NSNumber *currentHidden = [self.checkHiddenData objectAtIndex:index];
    BOOL bCurrentHidden = [currentHidden boolValue];
    [cell.checkedImageView setHidden:bCurrentHidden];
    
    return cell;
}

- (void)gridView:(AQGridView *)aqGridView didSelectItemAtIndex:(NSUInteger)index
{
    [self.aqGridView deselectItemAtIndex:index animated:NO];
    
    [self.tapDelegate notifyDidSelectItemAtIndex:index];
    
    [self.aqGridView reloadData];
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aqGridView
{
    // 画像より大きく設定する
    return (CGSizeMake(WIDTH+10, HEIGHT+20));
}

@end

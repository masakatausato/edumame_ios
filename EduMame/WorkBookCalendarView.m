//
//  WorkBookCalendarView.m
//  EduMame
//
//  Created by GClue on 13/02/13.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "WorkBookCalendarView.h"
#import "DatabaseManager.h"
#import "DateViewController.h"
#import "CreateManager.h"
#import <QuartzCore/QuartzCore.h>

const NSInteger ChildCalendarDayWidth = 100;
const NSInteger ChildCalendarDayHeight = 100;

@implementation WorkBookCalendarView

- (id)init
{
    self = [super init];
    if (self) {
        _calendarComponent = [[EMCalendarComponent alloc] init];
        [self constructCalendar];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _calendarComponent = [[EMCalendarComponent alloc] init];
        [self constructCalendar];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _calendarComponent = [[EMCalendarComponent alloc] init];
        [self constructCalendar];
    }
    return self;
}

- (void)previousMonth
{
    [_calendarComponent offsetMonth:-1];
    [self reloadCalendar];
}

- (void)nextMonth
{
    [_calendarComponent offsetMonth:1];
    [self reloadCalendar];
}

- (void)setStampImage:(UIImage*)image forTag:(NSInteger)tag
{
    UIView* dayView = [self viewWithTag:tag];
    UIImageView* stampImage = [dayView.subviews objectAtIndex:0];
    stampImage.image = image;
}

- (void)constructCalendar
{
    CGFloat headerWidth = floorf(self.frame.size.width / 7) * 7;
    CGFloat headerHeight = floorf(self.frame.size.height / 10);
    CGFloat headerButtonMargin = 4.0f;
    CGFloat headerButtonWidth = headerHeight - (headerButtonMargin * 2);
    CGFloat headerButtonHeight = headerButtonWidth;
    CGFloat dayWidth = floorf(self.frame.size.width / 7);
    CGFloat dayHeight = floorf((self.frame.size.height - headerHeight) / 7);    //TODO: なぜ 6では 7なのか原因不明
    CGFloat headerFontSize = headerHeight * 0.6f;
    CGFloat dayFontSize = dayHeight * 0.6f;
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerWidth, headerHeight)];
    headerView.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:237.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
    headerView.layer.borderColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:158.0f/255.0f alpha:1.0f].CGColor;
    headerView.layer.borderWidth = 1.0f;
    [self addSubview:headerView];
    
    UILabel* headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerWidth, headerHeight)];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont systemFontOfSize:headerFontSize];
    [headerView addSubview:headerLabel];
    
    UIButton* previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousButton setFrame:CGRectMake(headerButtonMargin, headerButtonMargin, headerButtonWidth, headerButtonHeight)];
    [previousButton setImage:[UIImage imageNamed:@"ChildCalendarLeftArrow"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(touchUpInsideLeftArrowButton:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:previousButton];
    
    UIButton* nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setFrame:CGRectMake(headerWidth - headerButtonWidth - headerButtonMargin, headerButtonMargin, headerButtonWidth, headerButtonHeight)];
    [nextButton setImage:[UIImage imageNamed:@"ChildCalendarRightArrow"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(touchUpInsideRightArrowButton:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:nextButton];
    
    for(NSInteger y = 0; y < 6; y++)
    {
        for(NSInteger x = 0; x < 7; x++)
        {
            FixedPoint cell = { x, y };
            NSInteger tag = [self tagOfCell:cell];
            
            UIView* dayView = [[UIView alloc] initWithFrame:CGRectMake(x * dayWidth, (y * dayHeight) + headerHeight, dayWidth, dayHeight)];
            dayView.tag = tag;
            dayView.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:237.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
            dayView.layer.borderColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:158.0f/255.0f alpha:1.0f].CGColor;
            dayView.layer.borderWidth = 1.0f;
            [self addSubview:dayView];
            
            UIButton* dayButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, dayWidth, dayHeight)];
            dayButton.tag = tag;
            [dayButton addTarget:self action:@selector(touchUpInsideDayButton:) forControlEvents:UIControlEventTouchUpInside];
            [dayView addSubview:dayButton];
            
            UILabel* dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, dayWidth, dayHeight)];
            dayLabel.backgroundColor = [UIColor clearColor];
            dayLabel.textAlignment = NSTextAlignmentCenter;
            dayLabel.font = [UIFont systemFontOfSize:dayFontSize];
            [dayView addSubview:dayLabel];
            
            UIImageView* stampImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, dayWidth, dayHeight)];
            [dayView addSubview:stampImage];
        }
    }
    [self reloadCalendar];
}

- (void)reloadCalendar
{
    const NSString* monthTable[] =
    {
        @"January",
        @"February",
        @"March",
        @"April",
        @"May",
        @"June",
        @"July",
        @"August",
        @"September",
        @"October",
        @"November",
        @"December",
    };
    
    NSDateComponents* currentComponents = [_calendarComponent components];
    
    UIView* headerView = [self.subviews objectAtIndex:0];
    UILabel* headerLabel = [headerView.subviews objectAtIndex:0];
    headerLabel.text = [NSString stringWithFormat:@"%d年 %d月 %@", currentComponents.year, currentComponents.month, monthTable[currentComponents.month - 1]];
    
    for(NSInteger y = 0; y < 6; y++)
    {
        for(NSInteger x = 0; x < 7; x++)
        {
            FixedPoint cell = { x, y };
            NSDateComponents* components = [_calendarComponent componentsOfCell:cell];
            NSInteger tag = [self tagOfCell:cell];
            
            UIView* dayView = [self viewWithTag:tag];
            
            //UIButton* dayButton = [dayView.subviews objectAtIndex:0];
            
            UILabel* dayLabel = [dayView.subviews objectAtIndex:1];
            dayLabel.text = [NSString stringWithFormat:@"%d", components.day];
            
#if 1 // セル色変更追加
            
            // 日付取得
            NSCalendar* cal = [NSCalendar currentCalendar];
            NSDate* dt = [cal dateFromComponents:components];
            
            // WorkbookData検索
            NSString* option = @"where name =";
            option = [option stringByAppendingFormat:@" '%@'", [CreateManager getFormatDate:dt dateFormat:@"yyyy-MM-dd"]];
            
            NSArray *targetWorkbookRecord = [DatabaseManager selectWorkbookData:option];
            
            // 現在の日付と比較し、古い日付をグレーアウト
            NSDate *now = [NSDate date];
            unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSTimeZoneCalendarUnit;
            NSDateComponents *comps = [cal components:unitFlags fromDate:now];
            NSDate *nowDate = [cal dateFromComponents:comps];
            
            NSComparisonResult result = [nowDate compare:dt];
            
            // プリントデータがある場合は背景色変更
            if ([targetWorkbookRecord count] > 0)
            {
                dayView.backgroundColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.8 alpha:1.0];
            } else if(result == NSOrderedDescending) {
                // 過去の日付をグレーアウト
                dayView.backgroundColor = [UIColor lightGrayColor];
            } else{
                dayView.backgroundColor = [UIColor colorWithRed:252.0f/255.0f green:237.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
            }
#endif
            if (currentComponents.month == components.month)
            {
                if(x == 0) dayLabel.textColor = [UIColor colorWithRed:202.0f/255.0f green:61.0f/255.0f blue:27.0f/255.0f alpha:1.0f];
                else if(x == 6) dayLabel.textColor = [UIColor colorWithRed:25.0f/255.0f green:112.0f/255.0f blue:127.0f/255.0f alpha:1.0f];
                else dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:139.0f/255.0f alpha:1.0f];
            }
            else
            {
                dayLabel.textColor = dayLabel.textColor = [UIColor colorWithRed:221.0f/255.0f green:210.0f/255.0f blue:200.0f/255.0f alpha:1.0f];
            }
            
            UIImageView* stampImage = [dayView.subviews objectAtIndex:2];
            stampImage.image = nil;
        }
    }
    
    /*
     NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSLog(@"%@", [dateFormatter stringFromDate:_calendarComponent.currentDate]);
     */
}

- (IBAction)touchUpInsideDayButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideDayButton:sender component:(EMCalendarComponent *)_calendarComponent];
    }
}

- (IBAction)touchUpInsideLeftArrowButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideLeftArrowButton:sender];
    }
}

- (IBAction)touchUpInsideRightArrowButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideRightArrowButton:sender];
    }
}

- (NSInteger)tagOfCell:(FixedPoint)cell
{
    return 100 + ((cell.y * 7) + cell.x);
}

@end

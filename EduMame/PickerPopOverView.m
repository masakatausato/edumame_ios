//
//  YearMonthPopOverView.m
//  EduMame
//
//  Created by gclue_mita on 13/01/25.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PickerPopOverView.h"

@interface PickerPopOverView ()
{
    NSMutableArray *years;
    NSMutableArray *months;
    
    NSString *selectedYear;
    NSString *selectedMonth;
}
@end

@implementation PickerPopOverView

@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // ピッカーを表示
    UIPickerView *piv = [[UIPickerView alloc] init];
    piv.delegate = self;    // デリゲートを自分自身に設定
    piv.dataSource = self;  // データソースを自分自身に設定
    piv.showsSelectionIndicator = YES;  // 選択中の行に目印を付ける
    [piv setFrame:CGRectMake(0, 0, 300, 220)];
    [self.view addSubview:piv];
    
    
    //Get Current Year into i2
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    //Create Years Array from 2000 to This year
    years = [[NSMutableArray alloc] init];
    for (int i = 2000; i <= i2; i++) {
        [years addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    months = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 12; i++) {
        [months addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    selectedYear = @"2000";
    selectedMonth = @"1";
   
//    [self.delegate notifyChanged:selectedYear month:selectedMonth];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Private Methods

// 列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 2;
}

// 行数
- (NSInteger)pickerView:(UIPickerView*)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [years count];
    } else {
        return [months count];
    }
}

// 行の内容
-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    // 行インデックス番号を返す
    if (component == 0) {
        return [years objectAtIndex:row];
    } else {
        return [months objectAtIndex:row];
    }
}

// 選択された場合に呼ばれる
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        selectedYear = [years objectAtIndex:row];
        NSLog(@"year = %@", selectedYear);
    } else {
        selectedMonth = [months objectAtIndex:row];
        NSLog(@"month = %@", selectedMonth);
    }
    [self.delegate notifyChanged:selectedYear month:selectedMonth];
}

@end

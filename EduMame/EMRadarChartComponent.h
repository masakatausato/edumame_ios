//
//  EMRadarChartComponent.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/07.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMRadarChartComponent : NSObject

@property (nonatomic) NSMutableArray* itemArray;

@property (nonatomic) CGFloat scale;

- (id)initWithItemNumber:(NSInteger)itemNumber scale:(CGFloat)scale;

+ (id)radarChartWithItemNumber:(NSInteger)itemNumber scale:(CGFloat)scale;

- (void)calculate;

- (void)setItemLabel:(NSString*)label itemIndex:(NSInteger)itemIndex;

- (void)setItemLabelByArray:(NSArray*)labelArray;

- (void)setItemValue:(CGFloat)value itemIndex:(NSInteger)itemIndex;

- (void)setItemValueByArray:(NSArray*)valueArray;

- (NSString*)itemLabelAtIndex:(NSInteger)itemIndex;

- (CGFloat)itemValueAtIndex:(NSInteger)itemIndex;

- (CGFloat)itemRadianAtIndex:(NSInteger)itemIndex;

- (CGPoint)itemPointAtIndex:(NSInteger)itemIndex;

- (CGPoint)itemMaxPointAtIndex:(NSInteger)itemIndex;

- (CGPoint)itemPointAtIndex:(NSInteger)itemIndex forValue:(CGFloat)value;


@end

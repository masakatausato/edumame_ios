//
//  UILabelUnderlined.m
//  EduMame
//
//  Created by gclue_mita on 13/01/25.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UILabelUnderlined.h"

@implementation UILabelUnderlined

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    const CGFloat* colors = CGColorGetComponents(self.textColor.CGColor);
 
    CGContextSetRGBStrokeColor(ctx, colors[0], colors[1], colors[2], 1.0); // RGBA
 
    CGContextSetLineWidth(ctx, 1.0f);
 
    CGSize textSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(200, 9999)];
 
    // handle textAlignement
    int alignementXOffset = 0;

    CGContextMoveToPoint(ctx, alignementXOffset, self.bounds.size.height - 10);
    CGContextAddLineToPoint(ctx, alignementXOffset+textSize.width, self.bounds.size.height - 10);

    CGContextStrokePath(ctx);

    [super drawRect:rect];
}

@end

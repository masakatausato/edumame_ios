//
//  EMCalendarComponent.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "EMCalendarComponent.h"

#define EMCalendarComponentUnit (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit)

/**
 * カレンダーコンポーネント
 */
@implementation EMCalendarComponent

/**
 * 初期化を行う
 * @return id
 */
- (id)init
{
    self = [super init];
    if(self){
        self.currentCalendar = [NSCalendar currentCalendar];
        self.currentDate = [NSDate date];
    }
    return self;
}

/**
 * オブジェクトの生成と初期化を行う
 * @return id
 */
+ (id)calendarComponent
{
    return [[EMCalendarComponent alloc] init];
}

/**
 * 初期化を行う
 * @param date 日付
 * @return id
 */
- (id)initWithDate:(NSDate*)date
{
    self = [super init];
    if(self){
        self.currentCalendar = [NSCalendar currentCalendar];
        self.currentDate = date;
    }
    return self;
}

/**
 * オブジェクトの生成と初期化を行う
 * @param date 日付
 * @return id
 */
+ (id)calendarComoponentWithDate:(NSDate*)date
{
    return [[EMCalendarComponent alloc] initWithDate:date];
}

/**
 * 初期化を行う
 * @param date 日付
 * @param calendar カレンダー
 * @return id
 */
- (id)initWithDate:(NSDate*)date calendar:(NSCalendar*)calendar
{
    self = [super init];
    if(self){
        self.currentCalendar = calendar;
        self.currentDate = date;
    }
    return self;
}

/**
 * オブジェクトの生成と初期化を行う
 * @param date 日付
 * @param calendar カレンダー
 * @return id
 */
+ (id)calendarComponentWithDate:(NSDate*)date calendar:(NSCalendar*)calendar;
{
    return [[EMCalendarComponent alloc] initWithDate:date calendar:calendar];
}

/**
 * 日のオフセットを指定する
 * @param day 日
 */
- (void)offsetDay:(NSInteger)day
{
    NSDateComponents* components = [self components];
    components.day += day;
    _currentDate = [_currentCalendar dateFromComponents:components];
}

/**
 * 月のオフセットを指定する
 * @param month 月
 */
- (void)offsetMonth:(NSInteger)month
{
    NSDateComponents* components = [self components];
    components.month += month;
    if (components.month <= 0) {
        components.month += 12;
        components.year -= 1;
    } else if (components.month > 12) {
        components.month -= 12;
        components.year += 1;
    }
    self.currentDate = [self.currentCalendar dateFromComponents:components];
}

/**
 * 年のオフセットを指定する
 * @param year 年
 */
- (void)offsetYear:(NSInteger)year
{
    NSDateComponents* components = [self components];
    components.year += year;
    _currentDate = [_currentCalendar dateFromComponents:components];
}

/**
 * 日時アクセス用コンポーネントを取得する
 * @return コンポーネント
 */
- (NSDateComponents*)components
{
    return [_currentCalendar components:EMCalendarComponentUnit fromDate:_currentDate];
}

/**
 * 指定日の日時アクセス用コンポーネントを取得する
 * @param day 日
 * @return コンポーネント
 */
- (NSDateComponents*)componentsOfDay:(NSInteger)day
{
    NSDateComponents* components = [self components];
    components.day = day;
    NSDate* date = [_currentCalendar dateFromComponents:components];
    return [_currentCalendar components:EMCalendarComponentUnit fromDate:date];
}

/**
 * 指定セルの日時アクセス用コンポーネントを取得する
 * @param cell セル（カレンダーのx/y座標）
 * @return コンポーネント
 */
- (NSDateComponents*)componentsOfCell:(FixedPoint)cell
{
    NSDateComponents* firstComponents = [self componentsOfDay:1];
    NSInteger dayOffset = 1 - firstComponents.weekday;
    NSInteger index = [self indexOfCell:cell];

    NSDateComponents* components = [self components];
    components.day = 1 + (index + dayOffset);
    NSDate* date = [_currentCalendar dateFromComponents:components];
    return [_currentCalendar components:EMCalendarComponentUnit fromDate:date];
}

/**
 * 日の範囲を取得
 * @return 日の範囲
 */
- (NSRange)rangeOfDay
{
    return [_currentCalendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:_currentDate];
}

/**
 * 月の範囲を取得
 * @return 月の範囲
 */
- (NSRange)rangeOfMonth
{
    return [_currentCalendar rangeOfUnit:NSMonthCalendarUnit inUnit:NSYearCalendarUnit forDate:_currentDate];
}

/**
 * 指定インデックス（カレンダーの左上を基準とした順番）のセルを取得
 * @param index インデックス
 * @return セル
 */
- (FixedPoint)cellOfIndex:(NSInteger)index
{
    FixedPoint cell;
    cell.x = index % 7;
    cell.y = index / 7;
    return cell;
}

/**
 * 指定セルのインデックスを取得
 * @param cell セル
 * @return インデックス
 */
- (NSInteger)indexOfCell:(FixedPoint)cell
{
    return (cell.y * 7) + cell.x;
}

@end

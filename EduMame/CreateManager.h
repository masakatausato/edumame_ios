//
//  CreateManager.h
//  EduMame
//
//  Created by gclue_mita on 13/02/20.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"
#import "ManualSearchConditionData.h"

typedef enum CREATE_STATE {
    STATE_NONE = -1,
    STATE_PLAN,
    STATE_MANUAL,
    STATE_AUTO_DAY,
    STATE_AUTO_WEEK
} CreateState;

typedef enum MANUAL_STATE {
    MANUAL_STATE_NONE = -1,
    MANUAL_STATE_PRODUCTS,
    MANUAL_STATE_CONTENTS
} ManualState;

@interface CreateManager : NSObject

// プリントステート保持
@property CreateState createState;

// 手動ステート保持
@property ManualState manualState;

// カレンダータップ日付保持
@property (strong, nonatomic) NSString *tapDate;

// 検索条件保持
@property (strong, nonatomic) ManualSearchConditionData *searchConditionData;

+ (id)sharedCreateManager;

+ (void)setupWorkbookRecord:(NSDate *)date;
+ (void)setupWorkbookIndexRecord:(WorkbookData *)workbookData contentData:(ContentData *)contentData;
+ (void)setupContentDataRecord:(ContentData *)contentData;

+ (WorkbookData *)updateWorkbookRecord:(NSDate *)date;
+ (void)updateWorkbookIndexRecord:(WorkbookData *)workbookData contentData:(ContentData *)contentData;
+ (void)updateWorkbookIndexRecord:(WorkbookData *)workbookData contentArray:(NSMutableArray *)contentArray;
+ (void)updateContentDataRecord;

+ (NSDate*)getNextDate:(NSInteger)day;
+ (NSString*)getFormatDate:(NSDate*)date dateFormat:(NSString *)dateFormat;

+ (UIImage *)getPreviewFromContent:(ContentData *)contentData index:(NSUInteger)index;
+ (UIImage *)getPreviewFromProduct:(ProductData *)productData index:(NSUInteger)index;

+ (NSMutableArray *)getContentArrayFromJSON:(NSString*)contentName;

@end

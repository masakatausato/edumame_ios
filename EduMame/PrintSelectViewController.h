//
//  PrintSelectViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PrintGridViewController.h"

@interface PrintSelectViewController : UIViewController <
GridViewDelegate>

@property (strong, nonatomic) PrintGridViewController *gridViewCtrl;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;
@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) NSMutableArray *checkHiddenData;
@property (strong, nonatomic) NSMutableArray *printContentData;

- (IBAction)onPrint:(id)sender;

@end

//
//  DatabaseManager.m
//  EduMame
//
//  Created by gclue_mita on 12/12/21.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "DatabaseManager.h"

#ifdef DEBUG
#define DATABASE_DEBUG
#endif

#define DATABASE_FILENAME @"edumame.sqlite"

/**
 * データベースマネージャ
 */
@implementation DatabaseManager

/**
 * データベースを開く
 * @return 成功した場合はデータベースオブジェクトのインスタンスを返却する。失敗した場合はnilを返却する。
 */
+ (FMDatabase*)open
{
    // データベースファイルを準備する。
    // データベースファイルがドキュメントディレクトリに存在しない場合、リソースファイルからコピーする。
    NSArray* paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString* documentDirectory = [paths objectAtIndex:0];
    NSString* databasePath = [documentDirectory stringByAppendingPathComponent:DATABASE_FILENAME];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

#ifdef DEBUG
    if ([fileManager fileExistsAtPath:databasePath])
    {
        // オリジナルのデータベースに変更があった場合は削除
        NSDictionary* databaseAttributes = [fileManager attributesOfItemAtPath:databasePath error:nil];

        NSBundle* bundle = [NSBundle mainBundle];
        NSString* bundleDirectory = [bundle bundlePath];
        NSString* bundlePath = [bundleDirectory stringByAppendingPathComponent:DATABASE_FILENAME];
        NSDictionary* bundleAttributes = [fileManager attributesOfItemAtPath:bundlePath error:nil];
        
        NSDate* databaseDate = [databaseAttributes objectForKey:NSFileModificationDate];
        NSDate* bundleDate = [bundleAttributes objectForKey:NSFileModificationDate];
        NSComparisonResult result = [databaseDate compare:bundleDate];
        if(result == NSOrderedAscending)
        {
            [fileManager removeItemAtPath:databasePath error:nil];
            NSLog(@"remove database file");
        }
    }
#endif
    
    if (![fileManager fileExistsAtPath:databasePath])
    {
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* bundleDirectory = [bundle bundlePath];
        NSString* bundlePath = [bundleDirectory stringByAppendingPathComponent:DATABASE_FILENAME];
        if (![fileManager copyItemAtPath:bundlePath toPath:databasePath error:nil])
        {
            NSLog(@"failed copy database file");
            return nil;
        }
    }
    
    // 　FMDBを初期化してオープンする。
    FMDatabase *db = [FMDatabase databaseWithPath:databasePath];
    if(db == nil)
    {
        NSLog(@"failed init database");
        return nil;
    }
    if(![db open])
    {
        NSLog(@"failed open database");
        return nil;        
    }
    return db;
}

/**
 * データベースを閉じる
 * @param db オープン済みのデータベースオブジェクトのインスタンス
 */
+ (void)close:(FMDatabase*)db
{
    if(db == nil) return;
    [db close];
}

/**
 * 数値データを文字列データに変換する
 * @param value 変換する値
 * @return 変換後の文字列
 */
+ (NSNumber*)toString:(NSInteger)value
{
    return [NSString stringWithFormat:@"%d", value];
    //return [NSNumber numberWithInt:value];
}

/**
 * 日付型をデータベース用の文字列に変換
 * @parem date 日付
 * @return  日付文字列
 */
+ (NSString*)dateToString:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:date];
}

/**
 * データベース用の文字列を日付型に変換
 * @parem string 日付文字列
 * @return  日付型
 */
+ (NSDate*)dateFromString:(NSString*)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter dateFromString:string];
}

/**
 * データベース用の日付型を日付型に変換
 * @parem string 日付型
 * @return  日付型
 */
+ (NSDate*)dateFromDate:(NSDate*)date
{
    return [self dateFromString:[self dateToString:date]];
}

/**
 * 日付型をデータベース用のタイムスタンプに変換
 * @param date 日付
 * @return タイムスタンプ
 */
+ (NSInteger)dateToTimestamp:(NSDate*)date
{
    return [date timeIntervalSince1970];
}

/**
 * データベース用のタイムスタンプを日付型に変換
 * @param timestamp タイムスタンプ
 * @return 日付
 */
+ (NSDate*)dateFromTimestamp:(NSInteger)timestamp
{
    return [NSDate dateWithTimeIntervalSince1970:timestamp];
}

/**
 * 最初のレコードを取得
 * @param records レコード配列
 * @return 最初のレコード
 */
+ (id)first:(NSArray*)records
{
    if([records count] == 0) return nil;
    return [records objectAtIndex:0];
}

/**
 * SELECT文を発行する
 * @param sql SQL文
 * @param action 取得結果を処理する関数ポインタ
 * @return 取得レコード配列
 */
+ (NSArray*)selectData:(NSString*)sql action:(DatabaseManagerSelectAction)action
{
    NSMutableArray* records = [NSMutableArray array];
    
    FMDatabase* db = [self open];
    if(db == nil) return nil;
    
#ifdef DATABASE_DEBUG
    //NSLog(@"%@", sql);
#endif
    
    // SQLクエリを実行
    FMResultSet* resultSet = [db executeQuery:sql];
    if ([db hadError]) NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    
    // レコードを配列に格納する。
    while ([resultSet next])
    {
        [records addObject:action(db, resultSet)];
    }
    
    [self close:db];
    return records;    
}

/**
 * INSERT文を発行する
 * @param records 挿入するレコード配列
 * @param acrtion レコードを処理する関数ポインタ
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId action:(DatabaseManagerInsertAction)action
{
    FMDatabase* db = [self open];
    if(db == nil) return NO;
    
    [db beginTransaction];
    [db setShouldCacheStatements:YES];
    
    BOOL success = YES;
    for (NSInteger i = 0; i < [records count]; i++)
    {
        if(!action(db, i))
        {
            success = NO;
            break;
        }
    }
    
    if(lastInsertRowId)
    {
        *lastInsertRowId = db.lastInsertRowId;
    }
    
    if(success) [db commit];
    else [db rollback];
    [self close:db];
    return success;
    
}

/**
 * UPDATE文を発行する。
 * @param records 更新するレコード配列
 * @param acrtion レコードを処理する関数ポインタ
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateData:(NSArray*)records action:(DatabaseManagerUpdateAction)action
{
    FMDatabase* db = [self open];
    if(db == nil) return NO;
    
    [db beginTransaction];
    [db setShouldCacheStatements:YES];
    
    BOOL success = YES;
    for (NSInteger i = 0; i < [records count]; i++)
    {
        if(!action(db, i))
        {
            success = NO;
            break;
        }
    }
    
    if(success) [db commit];
    else [db rollback];
    [self close:db];
    return success;    
}

/**
 * DELETE文を発行する。
 * @param tableName テーブル名
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteData:(NSString*)tableName options:(NSString*)options
{
    FMDatabase* db = [self open];
    if(db == nil) return NO;
    
    [db beginTransaction];
    [db setShouldCacheStatements:YES];
    
    BOOL success = YES;
    
    NSString* sql = [NSString stringWithFormat:@"DELETE FROM %@", tableName];
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    [db executeUpdate:sql];
    if ([db hadError])
    {
        NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        success = NO;
    }
    
    if(success) [db commit];
    else [db rollback];
    [self close:db];
    return success;    
}

/**
 * ParentData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectParentData:(NSString*)options
{
    NSString* sql = @"SELECT parents_id, user_id, username, email, family_name, first_name, family_name_kana, first_name_kana, zip_code1, zip_code2, prefecture_code, city, address, building, tel, birthday, password, secret_question, secret_question_answer FROM parents";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        ParentData* record = [[ParentData alloc] init];
        record.parentsId = [resultSet intForColumnIndex:index++];
        record.userId = [resultSet stringForColumnIndex:index++];
        record.username = [resultSet stringForColumnIndex:index++];
        record.email = [resultSet stringForColumnIndex:index++];
        record.familyName = [resultSet stringForColumnIndex:index++];
        record.firstName = [resultSet stringForColumnIndex:index++];
        record.familyNameKana = [resultSet stringForColumnIndex:index++];
        record.firstNameKana = [resultSet stringForColumnIndex:index++];
        record.zipCode1 = [resultSet stringForColumnIndex:index++];
        record.zipCode2 = [resultSet stringForColumnIndex:index++];
        record.prefectureCode = [resultSet stringForColumnIndex:index++];
        record.city = [resultSet stringForColumnIndex:index++];
        record.address = [resultSet stringForColumnIndex:index++];
        record.building = [resultSet stringForColumnIndex:index++];
        record.tel = [resultSet stringForColumnIndex:index++];
        record.birthday = [resultSet stringForColumnIndex:index++];
        record.password = [resultSet stringForColumnIndex:index++];
        record.secretQuestion = [resultSet stringForColumnIndex:index++];
        record.secretQuestionAnswer = [resultSet stringForColumnIndex:index++];
        return record;
    }];
}

/**
 * ParentData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertParentData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ParentData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO parents (user_id, username, email, family_name, first_name, family_name_kana, first_name_kana, zip_code1, zip_code2, prefecture_code, city, address, building, tel, birthday, password, secret_question, secret_question_answer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", record.userId, record.username, record.email, record.familyName, record.firstName, record.familyNameKana, record.firstNameKana, record.zipCode1, record.zipCode2, record.prefectureCode, record.city, record.address, record.building, record.tel, record.birthday, record.password, record.secretQuestion, record.secretQuestionAnswer];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ParentData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateParentData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ParentData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE parents SET user_id = ?, username = ?, email = ?, family_name = ?, first_name = ?, family_name_kana = ?, first_name_kana = ?, zip_code1 = ?, zip_code2 = ?, prefecture_code = ?, city = ?, address = ?, building = ?, tel = ?, birthday = ?, password = ?, secret_question = ?, secret_question_answer = ? WHERE parents_id = ?", record.userId, record.username, record.email, record.familyName, record.firstName, record.familyNameKana, record.firstNameKana, record.zipCode1, record.zipCode2, record.prefectureCode, record.city, record.address, record.building, record.tel, record.birthday, record.password, record.secretQuestion, record.secretQuestionAnswer, [self toString: record.parentsId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ParentData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteParentData:(NSString*)options
{
    return [self deleteData:@"parents" options:options];
}

/**
 * ChildrenData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectChildrenData:(NSString*)options
{
    NSString* sql = @"SELECT child_id, username, password_type, password, family_name, first_name, family_name_kana, first_name_kana, birthday, gender, parents_id FROM children";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        ChildrenData* record = [[ChildrenData alloc] init];
        record.childId = [resultSet intForColumnIndex:index++];
        record.username = [resultSet stringForColumnIndex:index++];
        record.passwordType = [resultSet intForColumnIndex:index++];
        record.password = [resultSet stringForColumnIndex:index++];
        record.familyName = [resultSet stringForColumnIndex:index++];
        record.firstName = [resultSet stringForColumnIndex:index++];
        record.familyNameKana = [resultSet stringForColumnIndex:index++];
        record.firstNameKana = [resultSet stringForColumnIndex:index++];
        record.birthday = [resultSet stringForColumnIndex:index++];
        record.gender = [resultSet intForColumnIndex:index++];
        record.parentsId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * ChildrenData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectChildrenDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT children.child_id, children.username, children.password_type, children.password, children.family_name, children.first_name, children.family_name_kana, children.first_name_kana, children.birthday, children.gender, children.parents_id, parents.parents_id, parents.username, parents.email, parents.family_name, parents.first_name, parents.family_name_kana, parents.first_name_kana, parents.zip_code1, parents.zip_code2, parents.prefecture_code, parents.city, parents.address, parents.building, parents.tel, parents.birthday, parents.password, parents.secret_question, parents.secret_question_answer FROM children INNER JOIN parents ON children.parents_id = parents.parents_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        ChildrenData* childrenRecord = [[ChildrenData alloc] init];
        childrenRecord.childId = [resultSet intForColumnIndex:index++];
        childrenRecord.username = [resultSet stringForColumnIndex:index++];
        childrenRecord.passwordType = [resultSet intForColumnIndex:index++];
        childrenRecord.password = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyName = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstName = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.birthday = [resultSet stringForColumnIndex:index++];
        childrenRecord.gender = [resultSet intForColumnIndex:index++];
        childrenRecord.parentsId = [resultSet intForColumnIndex:index++];

        ParentData* parentRecord = [[ParentData alloc] init];
        parentRecord.parentsId = [resultSet intForColumnIndex:index++];
        parentRecord.username = [resultSet stringForColumnIndex:index++];
        parentRecord.email = [resultSet stringForColumnIndex:index++];
        parentRecord.familyName = [resultSet stringForColumnIndex:index++];
        parentRecord.firstName = [resultSet stringForColumnIndex:index++];
        parentRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        parentRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        parentRecord.zipCode1 = [resultSet stringForColumnIndex:index++];
        parentRecord.zipCode2 = [resultSet stringForColumnIndex:index++];
        parentRecord.prefectureCode = [resultSet stringForColumnIndex:index++];
        parentRecord.city = [resultSet stringForColumnIndex:index++];
        parentRecord.address = [resultSet stringForColumnIndex:index++];
        parentRecord.building = [resultSet stringForColumnIndex:index++];
        parentRecord.tel = [resultSet stringForColumnIndex:index++];
        parentRecord.birthday = [resultSet stringForColumnIndex:index++];
        parentRecord.password = [resultSet stringForColumnIndex:index++];
        parentRecord.secretQuestion = [resultSet stringForColumnIndex:index++];
        parentRecord.secretQuestionAnswer = [resultSet stringForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:childrenRecord, parentRecord, nil] forKeys:[NSArray arrayWithObjects:@"Children", @"Parent", nil]];
    }];
}

/**
 * ChildrenData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertChildrenData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ChildrenData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO children (username, password_type, password, family_name, first_name, family_name_kana, first_name_kana, birthday, gender, parents_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", record.username, [self toString:record.passwordType], record.password, record.familyName, record.firstName, record.familyNameKana, record.firstNameKana, record.birthday, [self toString:record.gender], [self toString:record.parentsId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ChildrenData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateChildrenData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ChildrenData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE children SET username = ?, password_type = ?, password = ?, family_name = ?, first_name = ?, family_name_kana = ?, first_name_kana = ?, birthday = ?, gender = ?, parents_id = ? WHERE child_id = ?", record.username, [self toString:record.passwordType], record.password, record.familyName, record.firstName, record.familyNameKana, record.firstNameKana, record.birthday, [self toString:record.gender], [self toString:record.parentsId], [self toString:record.childId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ChildrenData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteChildrenData:(NSString*)options
{
    return [self deleteData:@"children" options:options];
}

/**
 * AnswerdContentIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectAnswerdContentIndexData:(NSString*)options
{
    NSString* sql = @"SELECT content_id, progress, product_id FROM answerd_content_index";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        AnswerdContentIndexData* record = [[AnswerdContentIndexData alloc] init];
        record.contentId = [resultSet intForColumnIndex:index++];
        record.progress = [resultSet intForColumnIndex:index++];
        record.productId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * AnswerdContentIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectAnswerdContentIndexDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT answerd_content_index.content_id, answerd_content_index.progress, answerd_content_index.product_id, contents.contents_id, contents.title, contents.description, contents.keyword, contents.provider, contents.author, contents.revision, contents.level, contents.attribute, contents.target_flag, contents.content_type, contents.print_flag, contents.use_term, contents.valid_term_start, contents.valid_term_end, contents.valid_term_flag, contents.invalid_flag, contents.content, contents.preview, contents.created, contents.modified, contents.category_id, contents.subcategory_id, products.products_id, products.title, products.description, products.usage, products.keyword, products.provider, products.author, products.revision, products.level, products.target_flag, products.target_term, products.month_term, products.valid_term_start, products.valid_term_end, products.valid_term_flag, products.invalid_flag, products.product, products.preview, products.created, products.modified, products.category_id FROM answerd_content_index INNER JOIN contents ON answerd_content_index.content_id = contents.contents_id INNER JOIN products ON answerd_content_index.product_id = products.products_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        AnswerdContentIndexData* answerdContentIndexRecord = [[AnswerdContentIndexData alloc] init];
        answerdContentIndexRecord.contentId = [resultSet intForColumnIndex:index++];
        answerdContentIndexRecord.progress = [resultSet intForColumnIndex:index++];
        answerdContentIndexRecord.productId = [resultSet intForColumnIndex:index++];

        ContentData* contentRecord = [[ContentData alloc] init];
        contentRecord.contentsId = [resultSet intForColumnIndex:index++];
        contentRecord.title = [resultSet stringForColumnIndex:index++];
        contentRecord.description = [resultSet stringForColumnIndex:index++];
        contentRecord.keyword = [resultSet stringForColumnIndex:index++];
        contentRecord.provider = [resultSet stringForColumnIndex:index++];
        contentRecord.author = [resultSet stringForColumnIndex:index++];
        contentRecord.revision = [resultSet intForColumnIndex:index++];
        contentRecord.level = [resultSet intForColumnIndex:index++];
        contentRecord.attribute = [resultSet intForColumnIndex:index++];
        contentRecord.targetFlag = [resultSet intForColumnIndex:index++];
        contentRecord.contentType = [resultSet intForColumnIndex:index++];
        contentRecord.printFlag = [resultSet intForColumnIndex:index++];
        contentRecord.useTerm = [resultSet intForColumnIndex:index++];
        contentRecord.validTermStart = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermEnd = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermFlag = [resultSet intForColumnIndex:index++];
        contentRecord.invalidFlag = [resultSet intForColumnIndex:index++];
        contentRecord.content = [resultSet stringForColumnIndex:index++];
        contentRecord.preview = [resultSet stringForColumnIndex:index++];
        contentRecord.created = [resultSet stringForColumnIndex:index++];
        contentRecord.modified = [resultSet stringForColumnIndex:index++];
        contentRecord.categoryId = [resultSet intForColumnIndex:index++];
        contentRecord.subcategoryId = [resultSet intForColumnIndex:index++];

        ProductData* productRecord = [[ProductData alloc] init];
        productRecord.productsId = [resultSet intForColumnIndex:index++];
        productRecord.title = [resultSet stringForColumnIndex:index++];
        productRecord.description = [resultSet stringForColumnIndex:index++];
        productRecord.usage = [resultSet stringForColumnIndex:index++];
        productRecord.keyword = [resultSet stringForColumnIndex:index++];
        productRecord.provider = [resultSet stringForColumnIndex:index++];
        productRecord.author = [resultSet stringForColumnIndex:index++];
        productRecord.revision = [resultSet intForColumnIndex:index++];
        productRecord.level = [resultSet intForColumnIndex:index++];
        productRecord.targetFlag = [resultSet intForColumnIndex:index++];
        productRecord.targetTerm = [resultSet intForColumnIndex:index++];
        productRecord.monthTerm = [resultSet intForColumnIndex:index++];
        productRecord.validTermStart = [resultSet stringForColumnIndex:index++];
        productRecord.validTermEnd = [resultSet stringForColumnIndex:index++];
        productRecord.validTermFlag = [resultSet intForColumnIndex:index++];
        productRecord.invalidFlag = [resultSet intForColumnIndex:index++];
        productRecord.product = [resultSet stringForColumnIndex:index++];
        productRecord.preview = [resultSet stringForColumnIndex:index++];
        productRecord.created = [resultSet stringForColumnIndex:index++];
        productRecord.modified = [resultSet stringForColumnIndex:index++];
        productRecord.categoryId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:answerdContentIndexRecord, contentRecord, productRecord, nil] forKeys:[NSArray arrayWithObjects:@"AnswerdContentIndex", @"Content", @"Product", nil]];
    }];
}

/**
 * AnswerdContentIndexData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertAnswerdContentIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        AnswerdContentIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO answerd_content_index (content_id, progress, product_id) VALUES (?, ?, ?)", [self toString:record.contentId], [self toString:record.progress], [self toString:record.productId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * AnswerdContentIndexData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateAnswerdContentIndexData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        AnswerdContentIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE answerd_content_index SET progress = ?, product_id = ? WHERE content_id = ?", [self toString:record.progress], [self toString:record.productId], [self toString:record.contentId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * AnswerdContentIndexData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteAnswerdContentIndexData:(NSString*)options
{
    return [self deleteData:@"answerd_content_index" options:options];
}

/**
 * CalendarEventData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectCalendarEventData:(NSString*)options
{
    NSString* sql = @"SELECT calendar_event_id, event_type_code, planned_data, summary, description, child_id FROM calendar_events";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        CalendarEventData* record = [[CalendarEventData alloc] init];
        record.calendarEventId = [resultSet intForColumnIndex:index++];
        record.eventTypeCode = [resultSet intForColumnIndex:index++];
        record.plannedData = [resultSet stringForColumnIndex:index++];
        record.summary = [resultSet stringForColumnIndex:index++];
        record.description = [resultSet stringForColumnIndex:index++];
        record.childId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * CalendarEventData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectCalendarEventDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT calendar_events.calendar_event_id, calendar_events.event_type_code, calendar_events.planned_data, calendar_events.summary, calendar_events.description, calendar_events.child_id, children.child_id, children.username, children.password_type, children.password, children.family_name, children.first_name, children.family_name_kana, children.first_name_kana, children.birthday, children.gender, children.parents_id FROM calendar_events INNER JOIN children ON calendar_events.child_id = children.child_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        CalendarEventData* calendarEventRecord = [[CalendarEventData alloc] init];
        calendarEventRecord.calendarEventId = [resultSet intForColumnIndex:index++];
        calendarEventRecord.eventTypeCode = [resultSet intForColumnIndex:index++];
        calendarEventRecord.plannedData = [resultSet stringForColumnIndex:index++];
        calendarEventRecord.summary = [resultSet stringForColumnIndex:index++];
        calendarEventRecord.description = [resultSet stringForColumnIndex:index++];
        calendarEventRecord.childId = [resultSet intForColumnIndex:index++];

        ChildrenData* childrenRecord = [[ChildrenData alloc] init];
        childrenRecord.childId = [resultSet intForColumnIndex:index++];
        childrenRecord.username = [resultSet stringForColumnIndex:index++];
        childrenRecord.passwordType = [resultSet intForColumnIndex:index++];
        childrenRecord.password = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyName = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstName = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.birthday = [resultSet stringForColumnIndex:index++];
        childrenRecord.gender = [resultSet intForColumnIndex:index++];
        childrenRecord.parentsId = [resultSet intForColumnIndex:index++];
        
        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:calendarEventRecord, childrenRecord, nil] forKeys:[NSArray arrayWithObjects:@"CalendarEvent", @"Children", nil]];
    }];
}

/**
 * CalendarEventData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertCalendarEventData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        CalendarEventData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO calendar_events (event_type_code, planned_data, summary, description, child_id) VALUES (?, ?, ?, ?, ?)", [self toString:record.eventTypeCode], record.plannedData, record.summary, record.description, [self toString:record.childId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * CalendarEventData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateCalendarEventData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        CalendarEventData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE calendar_events SET event_type_code = ?, planned_data = ?, summary = ?, description = ?, child_id = ? WHERE calendar_event_id = ?", [self toString:record.eventTypeCode], record.plannedData, record.summary, record.description, [self toString:record.childId], [self toString:record.calendarEventId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * CalendarEventData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteCalendarEventData:(NSString*)options
{
    return [self deleteData:@"calendar_events" options:options];
}

/**
 * ContentData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectContentData:(NSString*)options
{
    NSString* sql = @"SELECT contents_id, title, description, keyword, provider, author, revision, level, attribute, target_flag, content_type, print_flag, use_term, valid_term_start, valid_term_end, valid_term_flag, invalid_flag, content, preview, created, modified, category_id, subcategory_id FROM contents";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        ContentData* record = [[ContentData alloc] init];
        record.contentsId = [resultSet intForColumnIndex:index++];
        record.title = [resultSet stringForColumnIndex:index++];
        record.description = [resultSet stringForColumnIndex:index++];
        record.keyword = [resultSet stringForColumnIndex:index++];
        record.provider = [resultSet stringForColumnIndex:index++];
        record.author = [resultSet stringForColumnIndex:index++];
        record.revision = [resultSet intForColumnIndex:index++];
        record.level = [resultSet intForColumnIndex:index++];
        record.attribute = [resultSet intForColumnIndex:index++];
        record.targetFlag = [resultSet intForColumnIndex:index++];
        record.contentType = [resultSet intForColumnIndex:index++];
        record.printFlag = [resultSet intForColumnIndex:index++];
        record.useTerm = [resultSet intForColumnIndex:index++];
        record.validTermStart = [resultSet stringForColumnIndex:index++];
        record.validTermEnd = [resultSet stringForColumnIndex:index++];
        record.validTermFlag = [resultSet intForColumnIndex:index++];
        record.invalidFlag = [resultSet intForColumnIndex:index++];
        record.content = [resultSet stringForColumnIndex:index++];
        record.preview = [resultSet stringForColumnIndex:index++];
        record.created = [resultSet stringForColumnIndex:index++];
        record.modified = [resultSet stringForColumnIndex:index++];
        record.categoryId = [resultSet intForColumnIndex:index++];
        record.subcategoryId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * ContentData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertContentData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    NSString* created = [DatabaseManager dateToString:[NSDate date]];
    NSString* modified = [DatabaseManager dateToString:[NSDate date]];
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ContentData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO contents (title, description, keyword, provider, author, revision, level, attribute, target_flag, content_type, print_flag, use_term, valid_term_start, valid_term_end, valid_term_flag, invalid_flag, content, preview, created, modified, category_id, subcategory_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", record.title, record.description, record.keyword, record.provider, record.author, [self toString:record.revision], [self toString:record.level], [self toString:record.attribute], [self toString:record.targetFlag], [self toString:record.contentType], [self toString:record.printFlag], [self toString:record.useTerm], record.validTermStart, record.validTermEnd, [self toString:record.validTermFlag], [self toString:record.invalidFlag], record.content, record.preview, created, modified, [self toString:record.categoryId], [self toString:record.subcategoryId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ContentData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateContentData:(NSArray*)records
{
    NSString* modified = [DatabaseManager dateToString:[NSDate date]];
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ContentData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE contents SET title = ?, description = ?, keyword = ?, provider = ?, author = ?, revision = ?, level = ?, attribute = ?, target_flag = ?, content_type = ?, print_flag = ?, use_term = ?, valid_term_start = ?, valid_term_end = ?, valid_term_flag = ?, invalid_flag = ?, content = ?, preview = ?, created = ?, modified = ?, category_id = ?, subcategory_id = ? WHERE contents_id = ?", record.title, record.description, record.keyword, record.provider, record.author, [self toString:record.revision], [self toString:record.level], [self toString:record.attribute], [self toString:record.targetFlag], [self toString:record.contentType], [self toString:record.printFlag], [self toString:record.useTerm], record.validTermStart, record.validTermEnd, [self toString:record.validTermFlag], record.content, record.preview, record.created, modified, [self toString:record.categoryId], [self toString:record.subcategoryId], [self toString:record.contentsId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ContentData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteContentData:(NSString*)options
{
    return [self deleteData:@"contents" options:options];
}

/**
 * ProductIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectProductIndexData:(NSString*)options
{
    NSString* sql = @"SELECT content_id, product_id FROM product_index";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        ProductIndexData* record = [[ProductIndexData alloc] init];
        record.contentId = [resultSet intForColumnIndex:0];
        record.productId = [resultSet intForColumnIndex:1];
        return record;
    }];
}

/**
 * ProductIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectProductIndexDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT product_index.content_id, product_index.product_id, contents.contents_id, contents.title, contents.description, contents.keyword, contents.provider, contents.author, contents.revision, contents.level, contents.attribute, contents.target_flag, contents.content_type, contents.print_flag, contents.use_term, contents.valid_term_start, contents.valid_term_end, contents.valid_term_flag, contents.invalid_flag, contents.content, contents.preview, contents.created, contents.modified, contents.category_id, contents.subcategory_id, products.products_id, products.title, products.description, products.usage, products.keyword, products.provider, products.author, products.revision, products.level, products.target_flag, products.target_term, products.month_term, products.valid_term_start, products.valid_term_end, products.valid_term_flag, products.invalid_flag, products.product, products.preview, products.created, products.modified, products.category_id FROM product_index INNER JOIN contents ON product_index.content_id = contents.contents_id INNER JOIN products ON product_index.product_id = products.products_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        ProductIndexData* productIndexRecord = [[ProductIndexData alloc] init];
        productIndexRecord.contentId = [resultSet intForColumnIndex:index++];
        productIndexRecord.productId = [resultSet intForColumnIndex:index++];

        ContentData* contentRecord = [[ContentData alloc] init];
        contentRecord.contentsId = [resultSet intForColumnIndex:index++];
        contentRecord.title = [resultSet stringForColumnIndex:index++];
        contentRecord.description = [resultSet stringForColumnIndex:index++];
        contentRecord.keyword = [resultSet stringForColumnIndex:index++];
        contentRecord.provider = [resultSet stringForColumnIndex:index++];
        contentRecord.author = [resultSet stringForColumnIndex:index++];
        contentRecord.revision = [resultSet intForColumnIndex:index++];
        contentRecord.level = [resultSet intForColumnIndex:index++];
        contentRecord.attribute = [resultSet intForColumnIndex:index++];
        contentRecord.targetFlag = [resultSet intForColumnIndex:index++];
        contentRecord.contentType = [resultSet intForColumnIndex:index++];
        contentRecord.printFlag = [resultSet intForColumnIndex:index++];
        contentRecord.useTerm = [resultSet intForColumnIndex:index++];
        contentRecord.validTermStart = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermEnd = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermFlag = [resultSet intForColumnIndex:index++];
        contentRecord.invalidFlag = [resultSet intForColumnIndex:index++];
        contentRecord.content = [resultSet stringForColumnIndex:index++];
        contentRecord.preview = [resultSet stringForColumnIndex:index++];
        contentRecord.created = [resultSet stringForColumnIndex:index++];
        contentRecord.modified = [resultSet stringForColumnIndex:index++];
        contentRecord.categoryId = [resultSet intForColumnIndex:index++];
        contentRecord.subcategoryId = [resultSet intForColumnIndex:index++];

        ProductData* productRecord = [[ProductData alloc] init];
        productRecord.productsId = [resultSet intForColumnIndex:index++];
        productRecord.title = [resultSet stringForColumnIndex:index++];
        productRecord.description = [resultSet stringForColumnIndex:index++];
        productRecord.usage = [resultSet stringForColumnIndex:index++];
        productRecord.keyword = [resultSet stringForColumnIndex:index++];
        productRecord.provider = [resultSet stringForColumnIndex:index++];
        productRecord.author = [resultSet stringForColumnIndex:index++];
        productRecord.revision = [resultSet intForColumnIndex:index++];
        productRecord.level = [resultSet intForColumnIndex:index++];
        productRecord.targetFlag = [resultSet intForColumnIndex:index++];
        productRecord.targetTerm = [resultSet intForColumnIndex:index++];
        productRecord.monthTerm = [resultSet intForColumnIndex:index++];
        productRecord.validTermStart = [resultSet stringForColumnIndex:index++];
        productRecord.validTermEnd = [resultSet stringForColumnIndex:index++];
        productRecord.validTermFlag = [resultSet intForColumnIndex:index++];
        productRecord.invalidFlag = [resultSet intForColumnIndex:index++];
        productRecord.product = [resultSet stringForColumnIndex:index++];
        productRecord.preview = [resultSet stringForColumnIndex:index++];
        productRecord.created = [resultSet stringForColumnIndex:index++];
        productRecord.modified = [resultSet stringForColumnIndex:index++];
        productRecord.categoryId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:productIndexRecord, contentRecord, productRecord, nil] forKeys:[NSArray arrayWithObjects:@"ProductIndex", @"Content", @"Product", nil]];
    }];
}

/**
 * ProductIndexData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertProductIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ProductIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO product_index (content_id, product_id) VALUES (?, ?)", [self toString:record.contentId], [self toString:record.productId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ProductIndexData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateProductIndexData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ProductIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE product_index SET product_id = ? WHERE content_id = ?", [self toString:record.productId], [self toString:record.contentId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ProductIndexData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteProductIndexData:(NSString*)options
{
    return [self deleteData:@"product_index" options:options];
}

/**
 * ProductData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectProductData:(NSString*)options
{
    NSString* sql = @"SELECT products_id, title, description, usage, keyword, provider, author, revision, level, target_flag, target_term, month_term, valid_term_start, valid_term_end, valid_term_flag, invalid_flag, product, preview, created, modified, category_id FROM products";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        ProductData* record = [[ProductData alloc] init];
        record.productsId = [resultSet intForColumnIndex:index++];
        record.title = [resultSet stringForColumnIndex:index++];
        record.description = [resultSet stringForColumnIndex:index++];
        record.usage = [resultSet stringForColumnIndex:index++];
        record.keyword = [resultSet stringForColumnIndex:index++];
        record.provider = [resultSet stringForColumnIndex:index++];
        record.author = [resultSet stringForColumnIndex:index++];
        record.revision = [resultSet intForColumnIndex:index++];
        record.level = [resultSet intForColumnIndex:index++];
        record.targetFlag = [resultSet intForColumnIndex:index++];
        record.targetTerm = [resultSet intForColumnIndex:index++];
        record.monthTerm = [resultSet intForColumnIndex:index++];
        record.validTermStart = [resultSet stringForColumnIndex:index++];
        record.validTermEnd = [resultSet stringForColumnIndex:index++];
        record.validTermFlag = [resultSet intForColumnIndex:index++];
        record.invalidFlag = [resultSet intForColumnIndex:index++];
        record.product = [resultSet stringForColumnIndex:index++];
        record.preview = [resultSet stringForColumnIndex:index++];
        record.created = [resultSet stringForColumnIndex:index++];
        record.modified = [resultSet stringForColumnIndex:index++];
        record.categoryId = [resultSet intForColumnIndex:index++];

        return record;
    }];
}

/**
 * ProductData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertProductData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    NSString* created = [DatabaseManager dateToString:[NSDate date]];
    NSString* modified = [DatabaseManager dateToString:[NSDate date]];
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ProductData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO products (title, description, usage, keyword, provider, author, revision, level, target_flag, target_term, month_term, valid_term_start, valid_term_end, valid_term_flag, invalid_flag, product, preview, created, modified, category_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", record.title, record.description, record.usage, record.keyword, record.provider, record.author, [self toString:record.revision], [self toString:record.level], [self toString:record.targetFlag], [self toString:record.targetTerm], [self toString:record.monthTerm], record.validTermStart, record.validTermEnd, [self toString:record.validTermFlag], [self toString:record.invalidFlag], record.product, record.preview, created, modified, [self toString:record.categoryId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ProductData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateProductData:(NSArray*)records
{
    NSString* modified = [DatabaseManager dateToString:[NSDate date]];
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ProductData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE products SET title = ?, description = ?, usage = ?, keyword = ?, provider = ?, author = ?, revision = ?, level = ?, target_flag = ?, target_term = ?, month_term = ?, valid_term_start = ?, valid_term_end = ?, valid_term_flag = ?, invalid_flag = ?, product = ?, preview = ?, created = ?, modified = ?, category_id = ? WHERE products_id = ?", record.title, record.description, record.usage, record.keyword, record.provider, record.author, [self toString:record.revision], [self toString:record.level], [self toString:record.targetFlag], [self toString:record.targetTerm], [self toString:record.monthTerm], record.validTermStart, record.validTermEnd, [self toString:record.validTermFlag], [self toString:record.invalidFlag], record.product, record.preview, record.created, modified, [self toString:record.categoryId], [self toString:record.productsId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ProductData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteProductData:(NSString*)options
{
    return [self deleteData:@"products" options:options];
}

/**
 * WorkbookIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectWorkbookIndexData:(NSString*)options
{
    NSString* sql = @"SELECT workbook_index_id, answer_raw_data, answer_status_code, answer_started_at, answer_finished_at, evaluation, evaluated_at, memo, again_flag, mark_raw_data, workbook_id, content_id FROM workbook_index";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        WorkbookIndexData* record = [[WorkbookIndexData alloc] init];
        record.workbookIndexId = [resultSet intForColumnIndex:index++];
        record.answerRawData = [resultSet stringForColumnIndex:index++];
        record.answerStatusCode = [resultSet intForColumnIndex:index++];
        record.answerStartedAt = [resultSet intForColumnIndex:index++];
        record.answerFinishedAt = [resultSet intForColumnIndex:index++];
        record.evaluation = [resultSet intForColumnIndex:index++];
        record.evaluationAt = [resultSet intForColumnIndex:index++];
        record.memo = [resultSet stringForColumnIndex:index++];
        record.againFlag = [resultSet intForColumnIndex:index++];
        record.markRawData = [resultSet stringForColumnIndex:index++];
        record.workbookId = [resultSet intForColumnIndex:index++];
        record.contentId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * WorkbookIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectWorkbookIndexDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT workbook_index.workbook_index_id, workbook_index.answer_raw_data, workbook_index.answer_status_code, workbook_index.answer_started_at, workbook_index.answer_finished_at, workbook_index.evaluation, workbook_index.evaluated_at, workbook_index.memo, workbook_index.again_flag, workbook_index.mark_raw_data, workbook_index.workbook_id, workbook_index.content_id, workbooks.workbook_id, workbooks.planned_at, workbooks.name, workbooks.child_id, contents.contents_id, contents.title, contents.description, contents.keyword, contents.provider, contents.author, contents.revision, contents.level, contents.attribute, contents.target_flag, contents.content_type, contents.print_flag, contents.use_term, contents.valid_term_start, contents.valid_term_end, contents.valid_term_flag, contents.invalid_flag, contents.content, contents.preview, contents.created, contents.modified, contents.category_id, contents.subcategory_id FROM workbook_index INNER JOIN workbooks ON workbook_index.workbook_id = workbooks.workbook_id INNER JOIN contents ON workbook_index.content_id = contents.contents_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        WorkbookIndexData* workbookIndexRecord = [[WorkbookIndexData alloc] init];
        workbookIndexRecord.workbookIndexId = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.answerRawData = [resultSet stringForColumnIndex:index++];
        workbookIndexRecord.answerStatusCode = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.answerStartedAt = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.answerFinishedAt = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.evaluation = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.evaluationAt = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.memo = [resultSet stringForColumnIndex:index++];
        workbookIndexRecord.againFlag = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.markRawData = [resultSet stringForColumnIndex:index++];
        workbookIndexRecord.workbookId = [resultSet intForColumnIndex:index++];
        workbookIndexRecord.contentId = [resultSet intForColumnIndex:index++];

        WorkbookData* workbookRecord = [[WorkbookData alloc] init];
        workbookRecord.workbookId = [resultSet intForColumnIndex:index++];
        workbookRecord.plannedAt = [resultSet intForColumnIndex:index++];
        workbookRecord.name = [resultSet stringForColumnIndex:index++];

        workbookRecord.childId = [resultSet intForColumnIndex:index++];

        ContentData* contentRecord = [[ContentData alloc] init];
        contentRecord.contentsId = [resultSet intForColumnIndex:index++];
        contentRecord.title = [resultSet stringForColumnIndex:index++];
        contentRecord.description = [resultSet stringForColumnIndex:index++];
        contentRecord.keyword = [resultSet stringForColumnIndex:index++];
        contentRecord.provider = [resultSet stringForColumnIndex:index++];
        contentRecord.author = [resultSet stringForColumnIndex:index++];
        contentRecord.revision = [resultSet intForColumnIndex:index++];
        contentRecord.level = [resultSet intForColumnIndex:index++];
        contentRecord.attribute = [resultSet intForColumnIndex:index++];
        contentRecord.targetFlag = [resultSet intForColumnIndex:index++];
        contentRecord.contentType = [resultSet intForColumnIndex:index++];
        contentRecord.printFlag = [resultSet intForColumnIndex:index++];
        contentRecord.useTerm = [resultSet intForColumnIndex:index++];
        contentRecord.validTermStart = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermEnd = [resultSet stringForColumnIndex:index++];
        contentRecord.validTermFlag = [resultSet intForColumnIndex:index++];
        contentRecord.invalidFlag = [resultSet intForColumnIndex:index++];
        contentRecord.content = [resultSet stringForColumnIndex:index++];
        contentRecord.preview = [resultSet stringForColumnIndex:index++];
        contentRecord.created = [resultSet stringForColumnIndex:index++];
        contentRecord.modified = [resultSet stringForColumnIndex:index++];
        contentRecord.categoryId = [resultSet intForColumnIndex:index++];
        contentRecord.subcategoryId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:workbookIndexRecord, workbookRecord, contentRecord, nil] forKeys:[NSArray arrayWithObjects:@"WorkbookIndex", @"Workbook", @"Content", nil]];
    }];
}

/**
 * WorkbookIndexData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertWorkbookIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        WorkbookIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO workbook_index (answer_raw_data, answer_status_code, answer_started_at, answer_finished_at, evaluation, evaluated_at, memo, again_flag, mark_raw_data, workbook_id, content_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", record.answerRawData, [self toString:record.answerStatusCode], [self toString:record.answerStartedAt], [self toString:record.answerFinishedAt], [self toString:record.evaluation], [self toString:record.evaluationAt], record.memo, [self toString:record.againFlag], record.markRawData, [self toString:record.workbookId], [self toString:record.contentId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * WorkbookIndexData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateWorkbookIndexData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        WorkbookIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE workbook_index SET answer_raw_data = ?, answer_status_code = ?, answer_started_at = ?, answer_finished_at = ?, evaluation = ?, evaluated_at = ?, memo = ?, again_flag = ?, mark_raw_data = ?, workbook_id = ?, content_id = ? WHERE workbook_index_id = ?", record.answerRawData, [self toString:record.answerStatusCode], [self toString:record.answerStartedAt], [self toString:record.answerFinishedAt], [self toString:record.evaluation], [self toString:record.evaluationAt], record.memo, [self toString:record.againFlag],  record.markRawData, [self toString:record.workbookId], [self toString:record.contentId], [self toString:record.workbookIndexId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * WorkbookIndexData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteWorkbookIndexData:(NSString*)options
{
    return [self deleteData:@"workbook_index" options:options];
}

/**
 * WorkbookData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectWorkbookData:(NSString*)options
{
    NSString* sql = @"SELECT workbook_id, planned_at, name, child_id FROM workbooks";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;

        WorkbookData* record = [[WorkbookData alloc] init];
        record.workbookId = [resultSet intForColumnIndex:index++];
        record.plannedAt = [resultSet intForColumnIndex:index++];
        record.name = [resultSet stringForColumnIndex:index++];
        record.childId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * WorkbookData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectWorkbookDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT workbooks.workbook_id, workbooks.planned_at, workbooks.name, workbooks.child_id, children.child_id, children.username, children.password_type, children.password, children.family_name, children.first_name, children.family_name_kana, children.first_name_kana, children.birthday, children.gender, children.parents_id FROM workbooks INNER JOIN children ON workbooks.child_id = children.child_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        WorkbookData* workbookRecord = [[WorkbookData alloc] init];
        workbookRecord.workbookId = [resultSet intForColumnIndex:index++];
        workbookRecord.plannedAt = [resultSet intForColumnIndex:index++];
        workbookRecord.name = [resultSet stringForColumnIndex:index++];
        workbookRecord.childId = [resultSet intForColumnIndex:index++];

        ChildrenData* childrenRecord = [[ChildrenData alloc] init];
        childrenRecord.childId = [resultSet intForColumnIndex:index++];
        childrenRecord.username = [resultSet stringForColumnIndex:index++];
        childrenRecord.passwordType = [resultSet intForColumnIndex:index++];
        childrenRecord.password = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyName = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstName = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.birthday = [resultSet stringForColumnIndex:index++];
        childrenRecord.gender = [resultSet intForColumnIndex:index++];
        childrenRecord.parentsId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:workbookRecord, childrenRecord, nil] forKeys:[NSArray arrayWithObjects:@"Workbook", @"Children", nil]];
    }];
}

/**
 * WorkbookData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertWorkbookData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        WorkbookData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO workbooks (planned_at, name, child_id) VALUES (?, ?, ?)", [self toString:record.plannedAt], record.name, [self toString:record.childId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * WorkbookData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateWorkbookData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        WorkbookData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE workbooks SET planned_at = ?, name = ?, child_id = ? WHERE workbook_id = ?", [self toString:record.plannedAt], record.name, [self toString:record.childId], [self toString:record.workbookId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * WorkbookData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteWorkbookData:(NSString*)options
{
    return [self deleteData:@"workbooks" options:options];
}



/**
 * CategoryData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectCategoryData:(NSString*)options
{
    NSString* sql = @"SELECT category_id, name FROM categories";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        CategoryData* record = [[CategoryData alloc] init];
        record.categoryId = [resultSet intForColumnIndex:index++];
        record.name = [resultSet stringForColumnIndex:index++];
        return record;
    }];
}

/**
 * CategoryData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertCategoryData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        CategoryData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO categories (name) VALUES (?)", record.name];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * CategoryData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateCategoryData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        CategoryData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE categories SET name = ? WHERE category_id = ?", record.name, [self toString:record.categoryId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * CategoryData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteCategoryData:(NSString*)options
{
    return [self deleteData:@"categories" options:options];
}



/**
 * SubCategoryData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectSubcategoryData:(NSString*)options
{
    NSString* sql = @"SELECT subcategory_id, name, category_id FROM subcategories";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        SubcategoryData* record = [[SubcategoryData alloc] init];
        record.subcategoryId = [resultSet intForColumnIndex:index++];
        record.name = [resultSet stringForColumnIndex:index++];
        record.categoryId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * SubCategoryData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectSubcategoryDataRecursive:(NSString*)options
{
    NSString* sql = @"SELECT subcategories.subcategory_id, subcategories.name, subcategories.category_id, categories.category_id, categories.name FROM subcategories INNER JOIN categories ON subcategories.category_id = categories.category_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        SubcategoryData* subcategoryRecord = [[SubcategoryData alloc] init];
        subcategoryRecord.subcategoryId = [resultSet intForColumnIndex:index++];
        subcategoryRecord.name = [resultSet stringForColumnIndex:index++];
        subcategoryRecord.categoryId = [resultSet intForColumnIndex:index++];
        
        CategoryData* categoryRecord = [[CategoryData alloc] init];
        categoryRecord.categoryId = [resultSet intForColumnIndex:index++];
        categoryRecord.name = [resultSet stringForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:subcategoryRecord, categoryRecord, nil] forKeys:[NSArray arrayWithObjects:@"Subcategory", @"Category", nil]];
    }];
}

/**
 * SubCategoryData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertSubcategoryData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        SubcategoryData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO subcategories (name, category_id) VALUES (?, ?)", record.name, [self toString:record.categoryId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * SubCategoryData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateSubcategoryData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        SubcategoryData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE subcategories SET name = ?, category_id = ? WHERE subcategory_id = ?", record.name, [self toString:record.categoryId], [self toString:record.subcategoryId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * SubCategoryData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteSubcategoryData:(NSString*)options
{
    return [self deleteData:@"subcategories" options:options];
}




/**
 * FunbookData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectFunbookData:(NSString*)options
{
    NSString* sql = @"SELECT funbook_id, product_id, child_id FROM funbooks";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        FunbookData* record = [[FunbookData alloc] init];
        record.funbookId = [resultSet intForColumnIndex:index++];
        record.productId = [resultSet intForColumnIndex:index++];
        record.childId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * FunbookData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectFunbookDataRecursive:(NSString *)options
{
    NSString* sql = @"SELECT funbooks.funbook_id, funbooks.product_id, funbooks.child_id, children.child_id, children.username, children.password_type, children.password, children.family_name, children.first_name, children.family_name_kana, children.first_name_kana, children.birthday, children.gender, children.parents_id FROM funbooks INNER JOIN children ON funbooks.child_id = children.child_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        FunbookData* funbookRecord = [[FunbookData alloc] init];
        funbookRecord.funbookId = [resultSet intForColumnIndex:index++];
        funbookRecord.productId = [resultSet intForColumnIndex:index++];
        funbookRecord.childId = [resultSet intForColumnIndex:index++];
        
        ChildrenData* childrenRecord = [[ChildrenData alloc] init];
        childrenRecord.childId = [resultSet intForColumnIndex:index++];
        childrenRecord.username = [resultSet stringForColumnIndex:index++];
        childrenRecord.passwordType = [resultSet intForColumnIndex:index++];
        childrenRecord.password = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyName = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstName = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.birthday = [resultSet stringForColumnIndex:index++];
        childrenRecord.gender = [resultSet intForColumnIndex:index++];
        childrenRecord.parentsId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:funbookRecord, childrenRecord, nil] forKeys:[NSArray arrayWithObjects:@"Funbook", @"Children", nil]];
    }];
}

/**
 * FunbookData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertFunbookData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        FunbookData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO funbooks (product_id, child_id) VALUES (?, ?)", [self toString:record.productId], [self toString:record.childId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * FunbookData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateFunbookData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        FunbookData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE funbooks SET product_id = ?, child_id = ? WHERE funbook_id = ?", [self toString:record.productId], [self toString:record.childId], [self toString:record.funbookId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * FunbookData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteFunbookData:(NSString*)options
{
    return [self deleteData:@"funbooks" options:options];
}




/**
 * FunbookIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectFunbookIndexData:(NSString*)options
{
    NSString* sql = @"SELECT funbook_index_id, read_at, canceled, funbook_id FROM funbook_index";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        FunbookIndexData* record = [[FunbookIndexData alloc] init];
        record.funbookIndexId = [resultSet intForColumnIndex:index++];
        record.readAt = [resultSet stringForColumnIndex:index++];
        record.canceled = [resultSet intForColumnIndex:index++];
        record.funbookId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * FunbookIndexData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectFunbookIndexDataRecursive:(NSString *)options
{
    NSString* sql = @"SELECT funbook_index.funbook_index_id, funbook_index.read_at, funbook_index.canceled, funbook_index.funbook_id, funbooks.funbook_id, funbooks.product_id, funbooks.child_id FROM funbook_index INNER JOIN funbooks ON funbook_index.funbook_id = funbooks.funbook_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        FunbookIndexData* funbookIndexRecord = [[FunbookIndexData alloc] init];
        funbookIndexRecord.funbookIndexId = [resultSet intForColumnIndex:index++];
        funbookIndexRecord.readAt = [resultSet stringForColumnIndex:index++];
        funbookIndexRecord.canceled = [resultSet intForColumnIndex:index++];
        funbookIndexRecord.funbookId = [resultSet intForColumnIndex:index++];
        
        FunbookData* funbookRecord = [[FunbookData alloc] init];
        funbookRecord.funbookId = [resultSet intForColumnIndex:index++];
        funbookRecord.productId = [resultSet intForColumnIndex:index++];
        funbookRecord.childId = [resultSet intForColumnIndex:index++];

        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:funbookIndexRecord, funbookRecord, nil] forKeys:[NSArray arrayWithObjects:@"FunbookIndex", @"Funbook", nil]];
    }];
}

/**
 * FunbookIndexData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertFunbookIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        FunbookIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO funbook_index (read_at, canceled, funbook_id) VALUES (?, ?, ?)", record.readAt, [self toString:record.canceled], [self toString:record.funbookId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * FunbookIndexData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateFunbookIndexData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        FunbookIndexData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE funbook_index SET read_at = ?, canceled = ?, funbook_id = ? WHERE funbook_index_id = ?", record.readAt, [self toString:record.canceled], [self toString:record.funbookId], [self toString:record.funbookIndexId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * FunbookIndexData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteFunbookIndexData:(NSString*)options
{
    return [self deleteData:@"funbook_index" options:options];
}




/**
 * ChildCalendarEventData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectChildCalendarEventData:(NSString*)options
{
    NSString* sql = @"SELECT child_calendar_event_id, date, evaluation, child_id FROM child_calendar_events";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        ChildCalendarEventData* record = [[ChildCalendarEventData alloc] init];
        record.childCalendarEventId = [resultSet intForColumnIndex:index++];
        record.date = [resultSet stringForColumnIndex:index++];
        record.evaluation = [resultSet intForColumnIndex:index++];
        record.childId = [resultSet intForColumnIndex:index++];
        return record;
    }];
}

/**
 * ChildCalendarEventData用SELECT文を発行する
 * @param options オプションSQL文
 * @return 取得レコード配列
 */
+ (NSArray*)selectChildCalendarEventDataRecursive:(NSString *)options
{
    NSString* sql = @"SELECT child_calendar_events.child_calendar_event_id, child_calendar_events.date, child_calendar_events.evaluation, child_calendar_events.child_id, children.child_id, children.username, children.password_type, children.password, children.family_name, children.first_name, children.family_name_kana, children.first_name_kana, children.birthday, children.gender, children.parents_id FROM child_calendar_events INNER JOIN children ON child_calendar_events.child_id = children.child_id";
    if(options)
    {
        sql = [sql stringByAppendingFormat:@" %@", options];
    }
    
    return [self selectData:sql action:^id(FMDatabase *db, FMResultSet *resultSet) {
        NSInteger index = 0;
        
        ChildCalendarEventData* childCalendarEventRecord = [[ChildCalendarEventData alloc] init];
        childCalendarEventRecord.childCalendarEventId = [resultSet intForColumnIndex:index++];
        childCalendarEventRecord.date = [resultSet stringForColumnIndex:index++];
        childCalendarEventRecord.evaluation = [resultSet intForColumnIndex:index++];
        childCalendarEventRecord.childId = [resultSet intForColumnIndex:index++];

        ChildrenData* childrenRecord = [[ChildrenData alloc] init];
        childrenRecord.childId = [resultSet intForColumnIndex:index++];
        childrenRecord.username = [resultSet stringForColumnIndex:index++];
        childrenRecord.passwordType = [resultSet intForColumnIndex:index++];
        childrenRecord.password = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyName = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstName = [resultSet stringForColumnIndex:index++];
        childrenRecord.familyNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.firstNameKana = [resultSet stringForColumnIndex:index++];
        childrenRecord.birthday = [resultSet stringForColumnIndex:index++];
        childrenRecord.gender = [resultSet intForColumnIndex:index++];
        childrenRecord.parentsId = [resultSet intForColumnIndex:index++];
        
        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:childCalendarEventRecord, childrenRecord, nil] forKeys:[NSArray arrayWithObjects:@"ChildCalendarEvent", @"Children", nil]];
    }];
}

/**
 * ChildCalendarEventData用INSERT文を発行する
 * @param records 挿入レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)insertChildCalendarEventData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId
{
    return [self insertData:records lastInsertRowId:lastInsertRowId action:^BOOL(FMDatabase *db, NSInteger index) {
        ChildCalendarEventData* record = [records objectAtIndex:index];
        [db executeUpdate:@"INSERT INTO child_calendar_events (date, evaluation, child_id) VALUES (?, ?, ?)", record.date, [self toString:record.evaluation], [self toString:record.childId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ChildCalendarEventData用UPDATE文を発行する
 * @param records 更新レコード配列
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)updateChildCalendarEventData:(NSArray*)records
{
    return [self updateData:records action:^BOOL(FMDatabase *db, NSInteger index) {
        ChildCalendarEventData* record = [records objectAtIndex:index];
        [db executeUpdate:@"UPDATE child_calendar_event SET date = ?, evaluation = ?, child_id = ? WHERE child_calendar_event_id = ?", record.date, [self toString:record.evaluation], [self toString:record.childId], [self toString:record.childCalendarEventId]];
        if ([db hadError])
        {
            NSLog(@"FMDB ERROR %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return NO;
        }
        return YES;
    }];
}

/**
 * ChildCalendarEventData用DELETE文を発行する
 * @param options オプションSQL文
 * @return 成功した場合はYESを返却する。失敗した場合はNOを返却する。
 */
+ (BOOL)deleteChildCalendarEventData:(NSString*)options
{
    return [self deleteData:@"child_calendar_events" options:options];
}

@end

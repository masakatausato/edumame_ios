//
//  ChildrenData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChildrenData : NSObject

@property (nonatomic) NSInteger childId;
@property (nonatomic) NSString* username;
@property (nonatomic) NSInteger passwordType;
@property (nonatomic) NSString* password;
@property (nonatomic) NSString* familyName;
@property (nonatomic) NSString* firstName;
@property (nonatomic) NSString* familyNameKana;
@property (nonatomic) NSString* firstNameKana;
@property (nonatomic) NSString* birthday;
@property (nonatomic) NSInteger gender;
@property (nonatomic) NSInteger parentsId;

- (id)init;

- (id)initWithChildId:(NSInteger)childId username:(NSString*)username passwordType:(NSInteger)passwordType password:(NSString*)password familyName:(NSString*)familyName firstName:(NSString*)firstName familyNameKana:(NSString*)familyNameKana firstNameKana:(NSString*)firstNameKana birthday:(NSString*)birthday gender:(NSInteger)gender parentsId:(NSInteger)parentsId;

@end

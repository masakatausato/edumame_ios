//
//  ScoringView.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ScoringView.h"

@implementation ScoringView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* hitView = [super hitTest:point withEvent:event];
    return (hitView == self ? nil : hitView);
}

@end

//
//  PlanConfirmTableItem.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "PlanConfirmTableItem.h"

@implementation PlanConfirmTableItem

- (id)init
{
    self = [super init];
    if(self){
        self.workbookIndexId = -1;
    }
    return self;
}

@end

//
//  EMCalendarView.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "EMCalendarView.h"
#import <QuartzCore/QuartzCore.h>

/**
 * カレンダービュー
 */
@implementation EMCalendarView

/**
 * 初期化
 */
- (id)init
{
    self = [super init];
    if (self) {
        _calendarComponent = [[EMCalendarComponent alloc] init];
    }
    return self;
}

/**
 * 初期化
 */
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _calendarComponent = [[EMCalendarComponent alloc] init];
    }
    return self;
}

/**
 * 初期化
 */
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _calendarComponent = [[EMCalendarComponent alloc] init];
    }
    return self;
}

/**
 * 前の月に移動する
 */
- (void)previousMonth
{
    [_calendarComponent offsetMonth:-1];
    [self reloadCalendar];
}

/**
 * 次の月に移動する
 */
- (void)nextMonth
{
    [_calendarComponent offsetMonth:1];
    [self reloadCalendar];
}

/**
 * タグから日ビューを取得する
 * @param tag タグ
 * @return 日ビュー
 */
- (UIView*)dayViewFromTag:(NSInteger)tag
{
    return [self viewWithTag:tag];
}

/**
 * 日ビューからボタンを取得する
 * @param dayView 日ビュー
 * @return 日ビュー内のボタン
 */
- (UIButton*)buttonFromDayView:(UIView*)dayView
{
    return [dayView.subviews objectAtIndex:0];
}

/**
 * 日ビューからラベルを取得する
 * @param dayView 日ビュー
 * @return 日ビュー内のラベル
 */
- (UILabel*)labelFromDayView:(UIView*)dayView
{
    return [dayView.subviews objectAtIndex:1];
}

/**
 * 日ビューからユーザービューを取得する
 * @param dayView 日ビュー
 * @param index ユーザービューのインデックス
 * @return 日ビュー内のユーザービュー
 */
- (UIView*)userViewFromDayView:(UIView*)dayView index:(NSInteger)index
{
    return [dayView.subviews objectAtIndex:2 + index];
}

/**
 * 指定セルのタグを取得する
 * @param cell セル
 * @return タグ
 */
- (NSInteger)tagOfCell:(FixedPoint)cell
{
    return 100 + ((cell.y * 7) + cell.x);
}

/**
 * 指定タグのセルを取得する
 * @param tag タグ
 * @return セル
 */
- (FixedPoint)cellOfTag:(NSInteger)tag
{
    FixedPoint cell;
    cell.x = (tag - 100) % 7;
    cell.y = (tag - 100) / 7;
    return cell;
}

/**
 * カレンダーを生成する
 * ViewController の viewDidLayoutSubviews で呼び出すこと
 */
- (void)constructCalendar
{
    // 各パーツのサイズを求める
    CGFloat headerWidth = floorf(self.frame.size.width / 7) * 7;
    CGFloat headerHeight = floorf(self.frame.size.height / 10);
    CGFloat headerButtonMargin = 4.0f;
    CGFloat headerButtonWidth = headerHeight - (headerButtonMargin * 2);
    CGFloat headerButtonHeight = headerButtonWidth;
    CGFloat dayWidth = floorf(self.frame.size.width / 7);
    CGFloat dayHeight = floorf((self.frame.size.height - headerHeight) / 7);    //TODO: なぜ 6では 7なのか原因不明
    CGFloat headerFontSize = headerHeight * 0.6f;
    CGFloat dayFontSize = dayHeight * 0.6f;
    
    // ヘッダービューを生成
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerWidth, headerHeight)];
    [self addSubview:headerView];
    
    // ヘッダービューのラベルを生成
    UILabel* headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerWidth, headerHeight)];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont systemFontOfSize:headerFontSize];
    [headerView addSubview:headerLabel];
    
    // ヘッダービューの前の月へボタンを生成
    UIButton* previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousButton setFrame:CGRectMake(headerButtonMargin, headerButtonMargin, headerButtonWidth, headerButtonHeight)];
    [previousButton setImage:[UIImage imageNamed:@"ChildCalendarLeftArrow"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(touchUpInsideLeftArrowButton:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:previousButton];

    // ヘッダービューの次の月へボタンを生成
    UIButton* nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setFrame:CGRectMake(headerWidth - headerButtonWidth - headerButtonMargin, headerButtonMargin, headerButtonWidth, headerButtonHeight)];
    [nextButton setImage:[UIImage imageNamed:@"ChildCalendarRightArrow"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(touchUpInsideRightArrowButton:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:nextButton];
    
    // ヘッダービューの生成を通知
    if(_delegate)
    {
        [_delegate doConstructHeaderView:headerView];
    }

    // 各セルの日ビューを生成する
    for(NSInteger y = 0; y < 6; y++)
    {
        for(NSInteger x = 0; x < 7; x++)
        {
            FixedPoint cell = { x, y };
            NSInteger tag = [self tagOfCell:cell];
            
            // 日ビューを生成
            UIView* dayView = [[UIView alloc] initWithFrame:CGRectMake(x * dayWidth, (y * dayHeight) + headerHeight, dayWidth, dayHeight)];
            dayView.tag = tag;
            [self addSubview:dayView];
            
            // ボタンを生成
            UIButton* dayButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, dayWidth, dayHeight)];
            dayButton.tag = tag;
            [dayButton addTarget:self action:@selector(touchUpInsideDayButton:) forControlEvents:UIControlEventTouchUpInside];
            [dayView addSubview:dayButton];
            
            // ラベルを生成
            UILabel* dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, dayWidth, dayHeight)];
            dayLabel.backgroundColor = [UIColor clearColor];
            dayLabel.textAlignment = NSTextAlignmentCenter;
            dayLabel.font = [UIFont systemFontOfSize:dayFontSize];
            [dayView addSubview:dayLabel];

            // 日ビューの生成を通知
            if(_delegate)
            {
                [_delegate doConstructDayView:dayView cell:cell];
            }
        }
    }
    
    // カレンダーをリロード
    [self reloadCalendar];
}

/**
 * カレンダーをリロードして再描画を行う
 */
- (void)reloadCalendar
{
    const NSString* monthTable[] =
    {
        @"January",
        @"February",
        @"March",
        @"April",
        @"May",
        @"June",
        @"July",
        @"August",
        @"September",
        @"October",
        @"November",
        @"December",
    };
    
    NSDateComponents* currentComponents = [_calendarComponent components];

    // ヘッダービューのラベルを更新
    UIView* headerView = [self.subviews objectAtIndex:0];
    UILabel* headerLabel = [headerView.subviews objectAtIndex:0];
    headerLabel.text = [NSString stringWithFormat:@"%d年 %d月 %@", currentComponents.year, currentComponents.month, monthTable[currentComponents.month - 1]];
    
    // ヘッダービューのリロードを通知
    if(_delegate)
    {
        [_delegate doReloadHeaderView:headerView];
    }

    // 各セルの日ビューを更新する
    for(NSInteger y = 0; y < 6; y++)
    {
        for(NSInteger x = 0; x < 7; x++)
        {
            FixedPoint cell = { x, y };
            NSDateComponents* components = [_calendarComponent componentsOfCell:cell];
            NSInteger tag = [self tagOfCell:cell];
            
            UIView* dayView = [self viewWithTag:tag];
            
            // ラベルを更新
            UILabel* dayLabel = [self labelFromDayView:dayView];
            dayLabel.text = [NSString stringWithFormat:@"%d", components.day];

            // 日ビューのリロードを通知
            if(_delegate)
            {
                [_delegate doReloadDayView:dayView cell:cell];
            }
        }
    }
}

/**
 * 日ビュータップアクション
 * @param sender
 */
- (IBAction)touchUpInsideDayButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideDayButton:sender];
    }
}

/**
 * 前の月へ移動タップアクション
 * @param sender
 */
- (IBAction)touchUpInsideLeftArrowButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideLeftArrowButton:sender];
    }
}

/**
 * 次の月へ移動タップアクション
 * @param sender
 */
- (IBAction)touchUpInsideRightArrowButton:(id)sender
{
    if(_delegate)
    {
        [_delegate touchUpInsideRightArrowButton:sender];
    }
}

@end

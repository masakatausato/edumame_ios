//
//  ContentManager.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentManager : NSObject

#define CONTENTS_ZIP_FILENAME @"contents.zip"
#define CONTENTS_DIRECTORY @"contents"
#define CONTENTS_JSON_FILENAME @"content.json"

#define PRODUCTS_ZIP_FILENAME @"product.zip"
#define PRODUCTS_DIRECTORY @"product"
#define PRODUCTS_JSON_FILENAME @"product.json"

#define ANSWER_DIRECTORY @"answer"
#define ANSWER_FILE_FORMAT @"answer%d.png"

#define CONTENT_ATTRIBUTE_LEARNING 1
#define CONTENT_ATTRIBUTE_FUN 2
#define CONTENT_ATTRIBUTE_BOTH 3

#define CONTENT_TARGET_FLAG_PARENT 1
#define CONTENT_TARGET_FLAG_CHILD 2
#define CONTENT_TARGET_FLAG_BOTH 3

#define CONTENT_CONTENT_TYPE_MOVIE 1
#define CONTENT_CONTENT_TYPE_BOOK 2
#define CONTENT_CONTENT_TYPE_INTRO 3

#define CONTENT_PRINT_FLAG_IMPOSSIBLE 1
#define CONTENT_PRINT_FLAG_NEED 2
#define CONTENT_PRINT_FLAG_POSSIBLE 3

#define CONTENT_TARGET_TERM_HIGH 1
#define CONTENT_TARGET_TERM_MIDDLE 2
#define CONTENT_TARGET_TERM_LOW 3
#define CONTENT_TARGET_TERM_UNDER_LOW 4
#define CONTENT_TARGET_TERM_NONE 5


+ (void)setup;

+ (NSString*)contentsPath;

+ (NSString*)contentsPath:(NSString*)contentId;

+ (NSString*)productsPath;

+ (NSString*)productsPath:(NSString*)productId;

+ (NSString*)answerPath;

+ (NSString*)answerPath:(NSInteger)workbookId;

@end

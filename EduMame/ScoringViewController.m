//
//  ScoringViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/19.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ScoringViewController.h"
#import "DatabaseManager.h"
#import "ContentManager.h"
#import "EMPlayer.h"

@interface ScoringViewController ()
{
    IBOutlet UIScrollView* _answerView;
    IBOutlet UIScrollView* _childAnswerView;
    IBOutlet UIImageView* _answerImageView;
    IBOutlet UIImageView* _childAnswerImageView;
    IBOutlet UILabel* _answerPageLabel;
    IBOutlet UILabel* _childAnswerPageLabel;
    IBOutlet UIView* _scoringView;
    
    IBOutlet UIView* _answerScoringView;
    IBOutlet UIButton* _answerScoringButton;
    IBOutlet UIButton* _answerLeftButton;
    IBOutlet UIButton* _answerRightButton;
    IBOutlet UISegmentedControl* _generalScoreSegmentedControl;
    IBOutlet UITextField* _commentTextField;
    IBOutlet UISwitch* _againSwitch;
    
    IBOutlet UIView* _childAnswerScoringView;
    IBOutlet UIButton* _childAnswerScoringButton;
    IBOutlet UIButton* _childAnswerLeftButton;
    IBOutlet UIButton* _childAnswerRightButton;
    IBOutlet UISegmentedControl* _scoreSegmentedControl;
    
    NSInteger _answerCurrentIndex;
    NSInteger _childAnswerCurrentIndex;
    NSMutableArray* _answerImages;
    NSMutableArray* _childAnswerImages;
    BOOL _showAnswerScoring;
    BOOL _showChildAnswerScoring;
    
    WorkbookIndexData* _workbookIndexRecord;
    NSMutableDictionary* _markRawData;
}
@end

@implementation ScoringViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary* record = [DatabaseManager first:[DatabaseManager selectWorkbookIndexDataRecursive:[NSString stringWithFormat:@"WHERE workbook_index_id = %d", _workbookIndexId]]];
    if(record == NULL) return;
    
    _workbookIndexRecord = [record objectForKey:@"WorkbookIndex"];
    ContentData* contentRecord = [record objectForKey:@"Content"];
    
    NSString* contentPath = [ContentManager contentsPath:contentRecord.content];
    EMPlayer* player = [[EMPlayer alloc] initWithContentPath:contentPath callbackHandler:nil];
    
    // 模範解答イメージ
    _answerCurrentIndex = 0;
    _answerImages = [NSMutableArray array];
    for(NSInteger i = 0; i < [player.answerPrograms count]; i++)
    {
        EMContent* content = [player.answerPrograms objectAtIndex:i];
        if(content.resourceType != EMResourceTypeImage) continue;
        
        [_answerImages addObject:[player.resourceList objectForKey:content.resourceFileName]];
    }
    
    // 子供の解答イメージ
    _childAnswerCurrentIndex = 0;
    _childAnswerImages = [NSMutableArray array];
    NSData* answerRawData = [_workbookIndexRecord.answerRawData dataUsingEncoding:NSStringEncodingConversionAllowLossy];
    NSDictionary* answerRawDataJSON = [NSJSONSerialization JSONObjectWithData:answerRawData options:NSJSONReadingAllowFragments error:nil];
    if(answerRawDataJSON)
    {
        NSArray* images = [answerRawDataJSON objectForKey:@"images"];
        for(NSInteger i = 0; i < [images count]; i++)
        {
            NSString* imageFileName = [images objectAtIndex:i];
            NSString* imagePath = [[ContentManager answerPath:_workbookIndexId] stringByAppendingPathComponent:imageFileName];
            NSData* imageData = [NSData dataWithContentsOfFile:imagePath];
            
            [_childAnswerImages addObject:[UIImage imageWithData:imageData]];
        }
    }
    
    // 個別採点のデータ
    _markRawData = [NSMutableDictionary dictionary];
    NSMutableArray* marks = [NSMutableArray array];
    for(NSInteger i = 0; i < [_childAnswerImages count]; i++)
    {
        [marks addObject:[NSNumber numberWithInteger:0]];
    }
    [_markRawData setObject:marks forKey:@"marks"];

    NSData* markRawData = [_workbookIndexRecord.markRawData dataUsingEncoding:NSStringEncodingConversionAllowLossy];
    NSDictionary* markRawDataJSON = [NSJSONSerialization JSONObjectWithData:markRawData options:NSJSONReadingAllowFragments error:nil];
    if(markRawDataJSON)
    {
        NSDictionary* saveJSON = [NSMutableDictionary dictionaryWithDictionary:markRawDataJSON];
        NSArray* saveMarks = [saveJSON objectForKey:@"marks"];
        for(NSInteger i = 0; i < [_answerImages count]; i++)
        {
            NSNumber* number = [saveMarks objectAtIndex:i];
            if(number)
            {
                [marks setObject:number atIndexedSubscript:i];
            }
        }
    }
    
    // 模範解答ビュー
    [_answerView.layer setBorderWidth:1.0f];
    [_answerView.layer setBorderColor:[UIColor blackColor].CGColor];

    // 子供の解答ビュー
    [_childAnswerView.layer setBorderWidth:1.0f];
    [_childAnswerView.layer setBorderColor:[UIColor blackColor].CGColor];

    // 採点ビュー
    [self closeAnswerScoring:YES];
    [self closeChildAnswerScoring:YES];
    _showAnswerScoring = NO;
    _showChildAnswerScoring = NO;
    
    // サブビューの更新
    [self updateSubviews];
    
    // キーボードイベント登録
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView
{
	return [scrollView.subviews objectAtIndex:0];
}

-(void)keyboardWillShow:(NSNotification*)note
{
#if 0
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat x = _scoringView.frame.origin.x;
    CGFloat y = keyboardFrameBegin.origin.x - _scoringView.frame.size.height - 150;
    CGFloat w = _scoringView.frame.size.width;
    CGFloat h = _scoringView.frame.size.height;
    [_scoringView setFrame:CGRectMake(x, y, w, h)];

    [UIView animateWithDuration:0.3 animations:^{
        CGFloat x = _scoringView.frame.origin.x;
        CGFloat y = keyboardFrameEnd.origin.x - _scoringView.frame.size.height - 100;
        CGFloat w = _scoringView.frame.size.width;
        CGFloat h = _scoringView.frame.size.height;
        [_scoringView setFrame:CGRectMake(x, y, w, h)];
    }];
#else
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat x = _scoringView.frame.origin.x;
        CGFloat y = 60;
        CGFloat w = _scoringView.frame.size.width;
        CGFloat h = _scoringView.frame.size.height;
        [_scoringView setFrame:CGRectMake(x, y, w, h)];
    }];
#endif

    //NSLog(@"%f - %f - %f - %f", keyboardFrameBegin.origin.x, keyboardFrameBegin.origin.y, keyboardFrameBegin.size.width, keyboardFrameBegin.size.height);
    //NSLog(@"%f - %f - %f - %f", keyboardFrameEnd.origin.x, keyboardFrameEnd.origin.y, keyboardFrameEnd.size.width, keyboardFrameEnd.size.height);
}

-(void)keyboardWillHide:(NSNotification*)note
{
#if 0
    CGRect keyboardFrameBegin = [[note.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGFloat x = _scoringView.frame.origin.x;
    CGFloat y = keyboardFrameBegin.origin.x - _scoringView.frame.size.height - 100;
    CGFloat w = _scoringView.frame.size.width;
    CGFloat h = _scoringView.frame.size.height;
    [_scoringView setFrame:CGRectMake(x, y, w, h)];
    
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat x = _scoringView.frame.origin.x;
        CGFloat y = keyboardFrameEnd.origin.x - _scoringView.frame.size.height - 150;
        CGFloat w = _scoringView.frame.size.width;
        CGFloat h = _scoringView.frame.size.height;
        [_scoringView setFrame:CGRectMake(x, y, w, h)];
    }];
#else
    [UIView animateWithDuration:0.3 animations:^{
        CGFloat x = _scoringView.frame.origin.x;
        CGFloat y = 400;
        CGFloat w = _scoringView.frame.size.width;
        CGFloat h = _scoringView.frame.size.height;
        [_scoringView setFrame:CGRectMake(x, y, w, h)];
    }];
#endif
    
    //NSLog(@"%f - %f - %f - %f", keyboardFrameBegin.origin.x, keyboardFrameBegin.origin.y, keyboardFrameBegin.size.width, keyboardFrameBegin.size.height);
    //NSLog(@"%f - %f - %f - %f", keyboardFrameEnd.origin.x, keyboardFrameEnd.origin.y, keyboardFrameEnd.size.width, keyboardFrameEnd.size.height);
}

- (IBAction)touchUpInsideAnswerScoringButton:(id)sender
{
    if(_showAnswerScoring)
    {
        [self closeAnswerScoring:NO];
        _showAnswerScoring = NO;
    }
    else
    {
        [self openAnswerScoring:NO];
        _showAnswerScoring = YES;
    }
    [self updateSubviews];
}

- (IBAction)touchUpInsideChildAnswerScoringButton:(id)sender
{
    if(_showChildAnswerScoring)
    {
        [self closeChildAnswerScoring:NO];
        _showChildAnswerScoring = NO;
    }
    else
    {
        [self openChildAnswerScoring:NO];
        _showChildAnswerScoring = YES;
    }
    [self updateSubviews];
}

- (IBAction)touchUpInsideAnswerPageLeftButton:(id)sender
{
    if(_answerCurrentIndex <= 0) return;
    _answerCurrentIndex--;
    [self updateSubviews];
}

- (IBAction)touchUpInsideAnswerPageRightButton:(id)sender
{
    if(_answerCurrentIndex >= (NSInteger)[_answerImages count] - 1) return;
    _answerCurrentIndex++;
    [self updateSubviews];
}

- (IBAction)touchUpInsideChildAnswerPageLeftButton:(id)sender
{
    if(_childAnswerCurrentIndex <= 0) return;
    _childAnswerCurrentIndex--;
    [self updateSubviews];
}

- (IBAction)touchUpInsideChildAnswerPageRightButton:(id)sender
{
    if(_childAnswerCurrentIndex >= (NSInteger)[_childAnswerImages count] - 1) return;
    _childAnswerCurrentIndex++;
    [self updateSubviews];
}

- (IBAction)valueChangedGeneralScoreSegmentedControl:(id)sender
{
    UISegmentedControl* segmentControl = sender;
    _workbookIndexRecord.evaluation = segmentControl.selectedSegmentIndex + 1;
    _workbookIndexRecord.evaluationAt = [DatabaseManager dateToTimestamp:[NSDate date]];
    [DatabaseManager updateWorkbookIndexData:[NSArray arrayWithObject:_workbookIndexRecord]];
}

- (IBAction)editingChangedCommentTextField:(id)sender
{
    UITextField* textField = sender;
    _workbookIndexRecord.memo = textField.text;
    [DatabaseManager updateWorkbookIndexData:[NSArray arrayWithObject:_workbookIndexRecord]];
}

- (IBAction)valueChangedAgainSwitch:(id)sender
{
    UISwitch* uiswitch = sender;
    _workbookIndexRecord.againFlag = uiswitch.isOn ? 1 : 0;
    [DatabaseManager updateWorkbookIndexData:[NSArray arrayWithObject:_workbookIndexRecord]];
}

- (IBAction)valueChangedScoreSegmentedControl:(id)sender
{
    UISegmentedControl* segmentControl = sender;

    NSMutableArray* marks = [_markRawData objectForKey:@"marks"];
    [marks setObject:[NSNumber numberWithInteger:segmentControl.selectedSegmentIndex + 1] atIndexedSubscript:_childAnswerCurrentIndex];

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:_markRawData options:kNilOptions error:nil];
    _workbookIndexRecord.markRawData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [DatabaseManager updateWorkbookIndexData:[NSArray arrayWithObject:_workbookIndexRecord]];
}

- (void)openAnswerScoring:(BOOL)instant
{
    CGRect scoringFrame = _answerScoringView.frame;
    CGFloat x = 0;
    CGFloat y = scoringFrame.origin.y;
    CGFloat w = scoringFrame.size.width;
    CGFloat h = scoringFrame.size.height;
    if(instant)
    {
        [_answerScoringView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_answerScoringView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)closeAnswerScoring:(BOOL)instant
{
    CGRect scoringFrame = _answerScoringView.frame;
    CGFloat x = 0 - scoringFrame.size.width;
    CGFloat y = scoringFrame.origin.y;
    CGFloat w = scoringFrame.size.width;
    CGFloat h = scoringFrame.size.height;
    if(instant)
    {
        [_answerScoringView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_answerScoringView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)openChildAnswerScoring:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect scoringFrame = _childAnswerScoringView.frame;
    CGFloat x = viewFrame.size.width - scoringFrame.size.width;
    CGFloat y = scoringFrame.origin.y;
    CGFloat w = scoringFrame.size.width;
    CGFloat h = scoringFrame.size.height;
    if(instant)
    {
        [_childAnswerScoringView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_childAnswerScoringView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)closeChildAnswerScoring:(BOOL)instant
{
    CGRect viewFrame = CGRectMake(0, 0, 1024, 768);
    CGRect scoringFrame = _childAnswerScoringView.frame;
    CGFloat x = viewFrame.size.width;
    CGFloat y = scoringFrame.origin.y;
    CGFloat w = scoringFrame.size.width;
    CGFloat h = scoringFrame.size.height;
    if(instant)
    {
        [_childAnswerScoringView setFrame:CGRectMake(x, y, w, h)];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [_childAnswerScoringView setFrame:CGRectMake(x, y, w, h)];
        }];
    }
}

- (void)updateSubviews
{
    if([_answerImages count] > 0)
    {
        _answerImageView.contentMode = UIViewContentModeScaleAspectFit;
        _answerImageView.image = [_answerImages objectAtIndex:_answerCurrentIndex];
        [_answerPageLabel setText:[NSString stringWithFormat:@"%d / %d", _answerCurrentIndex + 1, [_answerImages count]]];
    }
    else
    {
        [_answerPageLabel setText:[NSString stringWithFormat:@"%d / %d", 0, 0]];
        [_answerLeftButton setEnabled:NO];
        [_answerRightButton setEnabled:NO];
    }
    
    if([_childAnswerImages count] > 0)
    {
        _childAnswerImageView.contentMode = UIViewContentModeScaleAspectFit;
        _childAnswerImageView.image = [_childAnswerImages objectAtIndex:_childAnswerCurrentIndex];
        [_childAnswerPageLabel setText:[NSString stringWithFormat:@"%d / %d", _childAnswerCurrentIndex + 1, [_childAnswerImages count]]];
    }
    else
    {
        [_childAnswerPageLabel setText:[NSString stringWithFormat:@"%d / %d", 0, 0]];
        [_childAnswerScoringButton setEnabled:NO];
        [_childAnswerLeftButton setEnabled:NO];
        [_childAnswerRightButton setEnabled:NO];
    }

    [_answerScoringButton setTitle:_showAnswerScoring ? @"閉じる" : @"評価" forState:UIControlStateNormal];
    [_childAnswerScoringButton setTitle:_showChildAnswerScoring ? @"閉じる" : @"採点" forState:UIControlStateNormal];

    _generalScoreSegmentedControl.selectedSegmentIndex = _workbookIndexRecord.evaluation - 1;
    _commentTextField.text = _workbookIndexRecord.memo;
    [_againSwitch setOn:(_workbookIndexRecord.againFlag > 0) ? YES : NO animated:YES];

    NSMutableArray* marks = [_markRawData objectForKey:@"marks"];
    if([marks count] > 0)
    {
        _scoreSegmentedControl.selectedSegmentIndex = [(NSNumber*)[marks objectAtIndex:_childAnswerCurrentIndex] integerValue] - 1;
    }
}

@end

//
//  CalendarEventData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarEventData : NSObject

@property (nonatomic) NSInteger calendarEventId;
@property (nonatomic) NSInteger eventTypeCode;
@property (nonatomic) NSString* plannedData;
@property (nonatomic) NSString* summary;
@property (nonatomic) NSString* description;
@property (nonatomic) NSInteger childId;

- (id)init;

- (id)initWithCalendarEventId:(NSInteger)calendarEventId eventTypeCode:(NSInteger)eventTypeCode plannedData:(NSString*)plannedData summary:(NSString*)summary description:(NSString*)description childId:(NSInteger)childId;

@end

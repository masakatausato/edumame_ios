//
//  WorkbookViewController.h
//  EduMame
//
//  Created by gclue_mita on 12/10/17.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAModalPanel.h"
#import "PreviewDetailViewController.h"
#import "SelectTypeToCreateModalPanel.h"
#import "EMCalendarResponder.h"

@class PlanViewController;
@class ManualViewController;
@class ManualProductsViewController;
@class AutoDayViewController;
@class AutoWeekViewController;

@class WorkBookCalendarView;

@interface WorkbookViewController : UIViewController <
UAModalPanelDelegate,
SelectTypeDelegate,
EMCalendarResponder>

@property (strong, nonatomic) PlanViewController *planViewCtrl;
@property (strong, nonatomic) ManualViewController *manualViewCtrl;
@property (strong, nonatomic) ManualProductsViewController *manualProductsViewCtrl;
@property (strong, nonatomic) AutoDayViewController *autoDayViewCtrl;
@property (strong, nonatomic) AutoWeekViewController *autoWeekViewCtrl;

@property (strong, nonatomic) IBOutlet WorkBookCalendarView *calendarView;
@property (strong, nonatomic) SelectTypeToCreateModalPanel *modalPanel;

@end

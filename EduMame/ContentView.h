//
//  ContentView.h
//  EduMame
//
//  Created by gclue_mita on 13/02/11.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ContentData;

@interface ContentView : UIView

@property (strong, nonatomic) ContentData *contentData;
@property (strong, nonatomic) UIImageView *imageView;

@end

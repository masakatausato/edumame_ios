//
//  ParentPlayerViewController.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/03/04.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ParentPlayerViewController.h"
#import "EMPlayerHelper.h"
#import "EMPlayer.h"
#import "EMContent.h"

// ステータス
enum
{
    ParentPlayerStatusNone,
    ParentPlayerStatusPlay,
    ParentPlayerStatusNext,
    ParentPlayerStatusEnd,
};

@interface ParentPlayerViewController ()
{
    // 直前の更新時間
    NSDate* _previousDate;
    
    // EMプレイヤー
    EMPlayer* _player;
    
    // 現在の画像リソースを表示するビュー
    IBOutlet UIImageView* _frontImageView;
    
    // 直前の画像リソースを表示するビュー
    IBOutlet UIImageView* _backImageView;
    
    // AVプレイヤーレイヤー
    AVPlayerLayer* _avplayerLayer;
    
    // ステータス
    NSInteger _status;
    
    // 戻るフラグ
    BOOL _backFlag;
    
}
@end

@implementation ParentPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // コンテンツのパス
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentPath = [NSString stringWithFormat:@"%@/contents/%@", documentDirectory, _contentName];
    
    // EMプレイヤーをセットアップ
    _player = [[EMPlayer alloc] initWithContentPath:contentPath callbackHandler:self];
    
    // ステータスを初期化
    [self changeStatus:ParentPlayerStatusPlay];
    
    // 更新タイマーを起動
    _previousDate = [NSDate date];
    [NSTimer scheduledTimerWithTimeInterval:1.0f/60.0f target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ビューが閉じたイベント
- (void)viewDidDisappear:(BOOL)animated
{
    [_player cleanup];
}

// タイマーイベント
- (void)onTimer:(NSTimer*)timer
{
    // 直前のタイムアウトからの経過時間を計測
    NSDate* currentDate = [NSDate date];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_previousDate];
    _previousDate = currentDate;
    
    // ステータスごとの処理
    switch(_status)
    {
        case ParentPlayerStatusPlay:
        {
            // EMプレイヤーを更新
            [_player update:interval];
            break;
        }
        case ParentPlayerStatusNext:
        {
            // 自動的にシーケンスを進める
            [_player nextSequence];
            break;
        }
    }
}

// タップジェスチャー
- (IBAction)onTap:(id)sender
{
}

// 左スワイプジェスチャー
- (IBAction)onSwipeLeft:(id)sender
{
    [_player nextSequence];
}

// 右スワイプジェスチャー
- (IBAction)onSwipeRight:(id)sender
{
    NSInteger sequenceNo = _player.currentProgramIndex;
    
    NSArray* currentPrograms = [_player currentPrograms];
    if(currentPrograms == nil) return;
    
    // ２つ前の画像を探す
    NSInteger imageCount = 0;
    while(sequenceNo > 0)
    {
        EMContent* content = [currentPrograms objectAtIndex:sequenceNo];
        
        if(content.resourceType == EMResourceTypeImage)
        {
            imageCount++;
            if(imageCount >= 2) break;
        }
        
        sequenceNo--;
    }
    
    // 画像がない場合は音声or動画まで戻す
    if(sequenceNo == 0)
    {
        NSInteger soundCount = 0;
        
        sequenceNo = 1;
        while(sequenceNo <= _player.currentProgramIndex)
        {
            EMContent* content = [currentPrograms objectAtIndex:sequenceNo];
            
            if((content.resourceType == EMResourceTypeSound) || (content.resourceType == EMResourceTypeMovie))
            {
                soundCount++;
                break;
            }
            sequenceNo++;
        }
        
        // 音声もなければ戻ることはできない
        if(soundCount == 0) return;
    }
    
    // シーケンスを戻す
    _backFlag = YES;
    [_player moveSequence:sequenceNo];
    _backFlag = NO;
}

// コンテンツ開始イベント
- (void)playerStartContent
{
}

// コンテンツ終了イベント
- (void)playerEndContent
{
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

// 解答開始イベント
- (void)playerStartAnswer
{
    [self changeStatus:ParentPlayerStatusNext];
}

// 解答終了イベント
- (void)playerEndAnswer
{
    [self changeStatus:ParentPlayerStatusPlay];
}

// タップ待ち開始
- (void)playerStartTap
{
}

// タップ待ち終了
- (void)playerEndTap
{
}

// 画像設定イベント
- (void)playerSetImage:(UIImage*)image
{
    // 前面イメージを背面に、設定する画像を前面イメージに設定
    _backImageView.image = _frontImageView.image;
    _frontImageView.image = image;
    
    // 背面イメージ・前面イメージをスライド
    CGRect playerFrame = CGRectMake(0, 0, 1024, 768);
    CGFloat w = playerFrame.size.width;
    CGFloat h = playerFrame.size.height;
    if(_backFlag == NO)
    {
        _backImageView.frame = CGRectMake(0, 0, w, h);
        _frontImageView.frame = CGRectMake(w, 0, w, h);
        [UIView animateWithDuration:0.5 animations:^{
            _backImageView.frame = CGRectMake(-w, 0, w, h);
            _frontImageView.frame = CGRectMake(0, 0, w, h);
        }];
    }
    else
    {
        _backImageView.frame = CGRectMake(0, 0, w, h);
        _frontImageView.frame = CGRectMake(-w, 0, w, h);
        [UIView animateWithDuration:0.5 animations:^{
            _backImageView.frame = CGRectMake(w, 0, w, h);
            _frontImageView.frame = CGRectMake(0, 0, w, h);
        }];
    }
}

// 動画再生イベント
- (void)playerPlayMovie:(AVPlayer*)player playerItem:(AVPlayerItem*)playerItem
{
    _avplayerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    _avplayerLayer.frame = self.view.bounds;
    [self.view.layer addSublayer:_avplayerLayer];
}

// 動画停止イベント
- (void)playerStopMovie
{
    [_avplayerLayer removeFromSuperlayer];
}

// ステータスの変更
- (void)changeStatus:(NSInteger)status
{
    _status = status;
}

@end

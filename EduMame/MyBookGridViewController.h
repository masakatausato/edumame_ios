//
//  MyBookGridViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/21.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "UAModalPanel.h"
#import "MyBookGridViewCell.h"

@protocol MyBookGridViewDelegate <NSObject>

- (void)notifyDidSelectItemAtIndex:(NSInteger)index;

@end

@interface MyBookGridViewController : UIViewController  <AQGridViewDelegate, AQGridViewDataSource, UAModalPanelDelegate>

@property (strong, nonatomic) IBOutlet AQGridView *aqGridView;
@property (strong, nonatomic) IBOutlet MyBookGridViewCell *aqGridViewCell;
@property (strong, nonatomic) NSMutableArray *productData;

@property (strong, nonatomic) id<MyBookGridViewDelegate> delegate;

@property int state;

@end

//
//  EMCalendarView.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMCalendarComponent.h"
#import "EMCalendarResponder.h"

@interface EMCalendarView : UIView

@property id<EMCalendarResponder> delegate;

@property (readonly) EMCalendarComponent* calendarComponent;

- (void)constructCalendar;

- (void)reloadCalendar;

- (void)previousMonth;

- (void)nextMonth;

- (UIView*)dayViewFromTag:(NSInteger)tag;

- (UIButton*)buttonFromDayView:(UIView*)dayView;

- (UILabel*)labelFromDayView:(UIView*)dayView;

- (UIView*)userViewFromDayView:(UIView*)dayView index:(NSInteger)index;

- (NSInteger)tagOfCell:(FixedPoint)cell;

- (FixedPoint)cellOfTag:(NSInteger)tag;

@end

//
//  PlanViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoverFlowViewController.h"
#import "GridViewController.h"
#import "DateViewController.h"
#import "SelectTypeToCreateModalPanel.h"

@class DetailViewController;

@interface PlanViewController : UIViewController <
CoverFlowViewDelegate,
CoverFlowViewGestureDelegate,
GridViewDelegate,
GridViewGestureDelegate,
SelectTypeDelegate
>

@property (strong, nonatomic) CoverFlowViewController *coverflowViewCtrl;
@property (strong, nonatomic) GridViewController *gridViewCtrl;
@property (strong, nonatomic) DateViewController *dateViewCtrl;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;

@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) NSMutableArray *searchedContentData;

@property NSInteger workbookId;

@property (strong, nonatomic) DetailViewController *detailViewCtrl;
@property (strong, nonatomic) SelectTypeToCreateModalPanel *modalPanel;

@property (strong, nonatomic) NSMutableArray *resourceList;

@end

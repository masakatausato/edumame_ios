//
//  SearchModalPanel.m
//  EduMame
//
//  Created by gclue_mita on 12/10/31.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import "ReminderModalPanel.h"

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

@interface ReminderModalPanel ()
{
}
@end

@implementation ReminderModalPanel

@synthesize reminderView = _reminderView;
@synthesize scrollView = _scrollView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
       
        [[NSBundle mainBundle] loadNibNamed:@"ReminderView" owner:self options:nil];
                
        self.margin = UIEdgeInsetsMake(200.0, 260.0, 200.0, 260.0);
        self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        
        [self.contentView addSubview:self.reminderView];
                
        [self registerForKeyboardNotifications];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    
    CGRect frameRect = [self roundedRectFrame];
    CGRect closeRect = [self closeButtonFrame];
    [self.closeButton setFrame:CGRectMake(frameRect.size.width + closeRect.origin.x, closeRect.origin.y, 44, 44)];
	
	[self.reminderView setFrame:self.contentView.bounds];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGPoint scrollPoint = CGPointMake(0.0, 80.0);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
//    self.roundedRect.superview.frame = CGRectMake(0, -80, 0, -80);
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
//    self.roundedRect.superview.frame = CGRectMake(0, 0, 0, 0);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
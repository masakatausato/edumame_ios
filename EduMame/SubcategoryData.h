//
//  SubcategoryData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubcategoryData : NSObject

@property (nonatomic) NSInteger subcategoryId;
@property (nonatomic) NSString* name;
@property (nonatomic) NSInteger categoryId;

- (id)init;

- (id)initWithSubcategoryId:(NSInteger)subcategoryId name:(NSString*)name categoryId:(NSInteger)categoryId;

@end

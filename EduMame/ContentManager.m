//
//  ContentManager.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "ContentManager.h"
#import "DatabaseManager.h"
#import "Libs/ZipArchive/ZipArchive.h"

@implementation ContentManager

/**
 * コンテンツマネージャのセットアップ
 */
+ (void)setup
{
    [DatabaseManager deleteWorkbookData:nil];
    [DatabaseManager deleteWorkbookIndexData:nil];
    [DatabaseManager deleteChildCalendarEventData:nil];
    [DatabaseManager deleteFunbookData:nil];
    [DatabaseManager deleteFunbookIndexData:nil];
    
    [self setupCategories];
    [self setupContents];
    [self setupProducts];
    [self setupProductIndex];
}

/**
 * コンテンツのパスを返却する
 * @return コンテンツのパス
 */
+ (NSString*)contentsPath
{
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:CONTENTS_DIRECTORY];    
}

/**
 * コンテンツのパスを返却する
 * @param contentId コンテンツID
 * @return コンテンツのパス
 */
+ (NSString*)contentsPath:(NSString*)contentId
{
    return [[self contentsPath] stringByAppendingPathComponent:contentId];
}

/**
 * プロダクトのパスを返却する
 * @return プロダクトのパス
 */
+ (NSString*)productsPath
{
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:PRODUCTS_DIRECTORY];
}

/**
 * プロダクトのパスを返却する
 * @param productId プロダクトID
 * @return プロダクトのパス
 */
+ (NSString*)productsPath:(NSString*)productId
{
    return [[self productsPath] stringByAppendingPathComponent:productId];
}

/**
 * 答案のパスを返却する
 * @return 答案のパス
 */
+ (NSString*)answerPath
{
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:ANSWER_DIRECTORY];
}

/**
 * 答案のパスを返却する
 * @param workbookId ワークブックID
 * @return 答案のパス
 */
+ (NSString*)answerPath:(NSInteger)workbookId
{
    return [[self answerPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d", workbookId]];
}

/**
 * カテゴリのセットアップ
 */
+ (void)setupCategories
{
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* bundleDirectory = [bundle bundlePath];
    NSString* contentsZipPath = [bundleDirectory stringByAppendingPathComponent:CONTENTS_ZIP_FILENAME];
    
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentsPath = [documentDirectory stringByAppendingPathComponent:CONTENTS_DIRECTORY];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:contentsPath error:nil];
    
    ZipArchive* zip = [[ZipArchive alloc] init];
    [zip UnzipOpenFile:contentsZipPath];
    [zip UnzipFileTo:documentDirectory overWrite:YES];
    [zip UnzipCloseFile];
    
    NSArray* directoryList = [fileManager contentsOfDirectoryAtPath:contentsPath error:nil];
    if(directoryList == nil) return;
    
    NSMutableDictionary* categoryDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary* subcategoryDictionary = [NSMutableDictionary dictionary];
    NSMutableArray* categoryRecords = [NSMutableArray array];
    NSMutableArray* subcategoryRecords = [NSMutableArray array];
    for(NSInteger i = 0; i < [directoryList count]; i++)
    {
        NSString* directoryName = [directoryList objectAtIndex:i];
        
        // JSONデータの読み込み
        NSString* jsonPath = [NSString stringWithFormat:@"%@/%@/%@", contentsPath, directoryName, CONTENTS_JSON_FILENAME];
        NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
        if(jsonData == nil) continue;
        
        // JSONデータの解析
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        if(json == nil) continue;
        
        // カテゴリ
        NSString* category = [json objectForKey:@"category"];
        NSString* subcategory = [json objectForKey:@"subcategory"];
        
        // レコードの生成と格納
        if(![[categoryDictionary allKeys] containsObject:category])
        {
            CategoryData* categoryRecord = [[CategoryData alloc] init];
            categoryRecord.name = category;
            [categoryRecords addObject:categoryRecord];
        }

        if(![[subcategoryDictionary allKeys] containsObject:subcategory])
        {
            SubcategoryData* subcategoryRecord = [[SubcategoryData alloc] init];
            subcategoryRecord.name = subcategory;
            [subcategoryRecords addObject:subcategoryRecord];
        }
    }
    
    // データベースに格納
    [DatabaseManager deleteCategoryData:nil];
    [DatabaseManager deleteSubcategoryData:nil];
    [DatabaseManager insertCategoryData:categoryRecords lastInsertRowId:nil];
    [DatabaseManager insertSubcategoryData:subcategoryRecords lastInsertRowId:nil];
}

/**
 * コンテンツのセットアップ
 */
+ (void)setupContents
{
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* bundleDirectory = [bundle bundlePath];
    NSString* contentsZipPath = [bundleDirectory stringByAppendingPathComponent:CONTENTS_ZIP_FILENAME];
    
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* contentsPath = [documentDirectory stringByAppendingPathComponent:CONTENTS_DIRECTORY];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:contentsPath error:nil];
    
    ZipArchive* zip = [[ZipArchive alloc] init];
    [zip UnzipOpenFile:contentsZipPath];
    [zip UnzipFileTo:documentDirectory overWrite:YES];
    [zip UnzipCloseFile];
    
    NSArray* directoryList = [fileManager contentsOfDirectoryAtPath:contentsPath error:nil];
    if(directoryList == nil) return;
    
    NSMutableArray* contentRecords = [NSMutableArray array];
    for(NSInteger i = 0; i < [directoryList count]; i++)
    {
        NSString* directoryName = [directoryList objectAtIndex:i];
        
        // JSONデータの読み込み
        NSString* jsonPath = [NSString stringWithFormat:@"%@/%@/%@", contentsPath, directoryName, CONTENTS_JSON_FILENAME];
        NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
        if(jsonData == nil) continue;
        
        // JSONデータの解析
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        if(json == nil) continue;
        
        // カテゴリレコードの取得
        CategoryData* categoryRecord = [DatabaseManager first:[DatabaseManager selectCategoryData:[NSString stringWithFormat:@"WHERE name = \"%@\"", [json objectForKey:@"category"]]]];
        SubcategoryData* subcategoryRecord = [DatabaseManager first:[DatabaseManager selectSubcategoryData:[NSString stringWithFormat:@"WHERE name = \"%@\"", [json objectForKey:@"subcategory"]]]];
        
        NSString* attribute = [json objectForKey:@"attribute"];
        NSString* targetFlag = [json objectForKey:@"targetFlag"];
        NSString* contentType = [json objectForKey:@"contentType"];
        NSString* printFlag = [json objectForKey:@"printFlag"];

        // レコードの生成
        ContentData* contentRecord = [[ContentData alloc] init];
        contentRecord.title = [json objectForKey:@"title"];
        contentRecord.description = [json objectForKey:@"description"];
        contentRecord.keyword = [json objectForKey:@"keyword"];
        contentRecord.provider = [json objectForKey:@"provider"];
        contentRecord.author = [json objectForKey:@"author"];
        contentRecord.revision = [[json objectForKey:@"revision"] intValue];
        contentRecord.level = [[json objectForKey:@"level"] intValue];
        if([attribute isEqualToString:@"Learning"]) contentRecord.attribute = CONTENT_ATTRIBUTE_LEARNING;
        else if([attribute isEqualToString:@"Fun"]) contentRecord.attribute = CONTENT_ATTRIBUTE_FUN;
        else if([attribute isEqualToString:@"Both"]) contentRecord.attribute = CONTENT_ATTRIBUTE_BOTH;
        if([targetFlag isEqualToString:@"Parent"]) contentRecord.targetFlag = CONTENT_TARGET_FLAG_PARENT;
        else if([targetFlag isEqualToString:@"Child"]) contentRecord.targetFlag = CONTENT_TARGET_FLAG_CHILD;
        else if([targetFlag isEqualToString:@"Both"]) contentRecord.targetFlag = CONTENT_TARGET_FLAG_BOTH;
        if([contentType isEqualToString:@"Movie"]) contentRecord.contentType = CONTENT_CONTENT_TYPE_MOVIE;
        else if([contentType isEqualToString:@"Book"]) contentRecord.contentType = CONTENT_CONTENT_TYPE_BOOK;
        else if([contentType isEqualToString:@"Intro"]) contentRecord.contentType = CONTENT_CONTENT_TYPE_INTRO;
        if([printFlag isEqualToString:@"Impossible"]) contentRecord.printFlag= CONTENT_PRINT_FLAG_IMPOSSIBLE;
        else if([printFlag isEqualToString:@"Need"]) contentRecord.printFlag = CONTENT_PRINT_FLAG_NEED;
        else if([printFlag isEqualToString:@"Possible"]) contentRecord.printFlag = CONTENT_PRINT_FLAG_POSSIBLE;
        contentRecord.useTerm = [[json objectForKey:@"level"] intValue];
        contentRecord.validTermStart = [json objectForKey:@"validTermStart"];
        contentRecord.validTermEnd = [json objectForKey:@"validTermEnd"];
        contentRecord.validTermFlag = [[json objectForKey:@"validTermEnd"] boolValue];
        contentRecord.invalidFlag = [[json objectForKey:@"invalidFlag"] boolValue];
        contentRecord.content = [json objectForKey:@"contentId"];
        contentRecord.preview = [json objectForKey:@"preview"];
        contentRecord.categoryId = categoryRecord ? categoryRecord.categoryId : -1;
        contentRecord.subcategoryId = subcategoryRecord ? subcategoryRecord.subcategoryId : -1;
        
        // レコードを格納
        [contentRecords addObject:contentRecord];
    }
    
    // データベースに格納
    [DatabaseManager deleteContentData:nil];
    [DatabaseManager insertContentData:contentRecords lastInsertRowId:nil];
}

/**
 * プロダクトのセットアップ
 */
+ (void)setupProducts
{
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* bundleDirectory = [bundle bundlePath];
    NSString* productsZipPath = [bundleDirectory stringByAppendingPathComponent:PRODUCTS_ZIP_FILENAME];
    
    NSString* documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* productsPath = [documentDirectory stringByAppendingPathComponent:PRODUCTS_DIRECTORY];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:productsPath error:nil];

    ZipArchive* zip = [[ZipArchive alloc] init];
    [zip UnzipOpenFile:productsZipPath];
    [zip UnzipFileTo:documentDirectory overWrite:YES];
    [zip UnzipCloseFile];
    
    NSArray* directoryList = [fileManager contentsOfDirectoryAtPath:productsPath error:nil];
    if(directoryList == nil) return;
    
    NSMutableArray* productRecords = [NSMutableArray array];
    for(NSInteger i = 0; i < [directoryList count]; i++)
    {
        NSString* directoryName = [directoryList objectAtIndex:i];
        
        // JSONデータの読み込み
        NSString* jsonPath = [NSString stringWithFormat:@"%@/%@/%@", productsPath, directoryName, PRODUCTS_JSON_FILENAME];
        NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
        if(jsonData == nil) continue;
        
        // JSONデータの解析
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        if(json == nil) continue;

        // カテゴリレコードの取得
        CategoryData* categoryRecord = [DatabaseManager first:[DatabaseManager selectCategoryData:[NSString stringWithFormat:@"WHERE name = \"%@\"", [json objectForKey:@"category"]]]];

        NSString* usage = [json objectForKey:@"usage"];
        NSString* targetFlag = [json objectForKey:@"targetFlag"];
        NSString* targetTerm = [json objectForKey:@"targetTerm"];

        // レコードの生成
        ProductData* productRecord = [[ProductData alloc] init];
        productRecord.title = [json objectForKey:@"title"];
        productRecord.description = [json objectForKey:@"description"];
        productRecord.usage = [usage isEqual:[NSNull null]] ? @"" : usage;
        productRecord.keyword = [json objectForKey:@"keyword"];
        productRecord.provider = [json objectForKey:@"provider"];
        productRecord.author = [json objectForKey:@"author"];
        productRecord.revision = [[json objectForKey:@"revision"] intValue];
        productRecord.level = [[json objectForKey:@"level"] intValue];
        if([targetFlag isEqualToString:@"Parent"]) productRecord.targetFlag = CONTENT_TARGET_FLAG_PARENT;
        else if([targetFlag isEqualToString:@"Child"]) productRecord.targetFlag = CONTENT_TARGET_FLAG_CHILD;
        else if([targetFlag isEqualToString:@"Both"]) productRecord.targetFlag = CONTENT_TARGET_FLAG_BOTH;
        if([targetTerm isEqualToString:@"High"]) productRecord.targetTerm = CONTENT_TARGET_TERM_HIGH;
        else if([targetTerm isEqualToString:@"Middle"]) productRecord.targetTerm = CONTENT_TARGET_TERM_MIDDLE;
        else if([targetTerm isEqualToString:@"Low"]) productRecord.targetTerm = CONTENT_TARGET_TERM_LOW;
        else if([targetTerm isEqualToString:@"UnderLow"]) productRecord.targetTerm = CONTENT_TARGET_TERM_UNDER_LOW;
        else if([targetTerm isEqualToString:@"None"]) productRecord.targetTerm = CONTENT_TARGET_TERM_NONE;
        productRecord.monthTerm = [[json objectForKey:@"monthTerm"] intValue];
        productRecord.validTermStart = [json objectForKey:@"validTermStart"];
        productRecord.validTermEnd = [json objectForKey:@"validTermEnd"];
        productRecord.validTermFlag = [[json objectForKey:@"validTermFlag"] boolValue];
        productRecord.invalidFlag = [[json objectForKey:@"invalidFlag"] boolValue];
        productRecord.product = [json objectForKey:@"productId"];
        productRecord.preview = [json objectForKey:@"preview"];
        productRecord.categoryId = categoryRecord ? categoryRecord.categoryId : -1;

        // レコードを格納
        [productRecords addObject:productRecord];
    }
    
    // データベースに格納
    [DatabaseManager deleteProductData:nil];
    [DatabaseManager insertProductData:productRecords lastInsertRowId:nil];
}

/**
 * プロダクトインデックスのセットアップ
 */
+ (void)setupProductIndex
{
    NSMutableArray* productIndexRecords = [NSMutableArray array];
    
    NSArray* productRecords = [DatabaseManager selectProductData:nil];
    for(NSInteger i = 0; i < [productRecords count]; i++)
    {
        ProductData* productRecord = [productRecords objectAtIndex:i];
        
        // JSONデータの読み込み
        NSString* jsonPath = [[self productsPath:productRecord.product] stringByAppendingPathComponent:PRODUCTS_JSON_FILENAME];
        NSData* jsonData = [NSData dataWithContentsOfFile:jsonPath];
        if(jsonData == nil) continue;
        
        // JSONデータの解析
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        if(json == nil) continue;

        NSArray* contents = [json objectForKey:@"contents"];
        for(NSInteger j = 0; j < [contents count]; j++)
        {
            NSString* contentId = [contents objectAtIndex:j];
            ContentData* contentRecord = [DatabaseManager first:[DatabaseManager selectContentData:[NSString stringWithFormat:@"WHERE content = \"%@\"", contentId]]];
            if(contentRecord == nil) continue;
            
            // レコードの生成
            ProductIndexData* productIndexRecord = [[ProductIndexData alloc] init];
            productIndexRecord.productId = productRecord.productsId;
            productIndexRecord.contentId = contentRecord.contentsId;

            // レコードを格納
            [productIndexRecords addObject:productIndexRecord];
        }
    }
    
    // データベースに格納
    [DatabaseManager deleteProductIndexData:nil];
    [DatabaseManager insertProductIndexData:productIndexRecords lastInsertRowId:nil];
}

@end

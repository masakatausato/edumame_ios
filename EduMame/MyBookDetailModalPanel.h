//
//  MyBookDetailModalPanel.h
//  EduMame
//
//  Created by gclue_mita on 13/02/25.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "UATitledModalPanel.h"

@interface MyBookDetailModalPanel : UATitledModalPanel

@property (weak, nonatomic) IBOutlet UIView *detailView;

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *publisher;
@property (weak, nonatomic) IBOutlet UILabel *info;
@property (weak, nonatomic) IBOutlet UILabel *term;
@property (weak, nonatomic) IBOutlet UILabel *level;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

@end

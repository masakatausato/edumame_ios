//
//  GridViewCell.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "AQGridViewCell.h"

#define WIDTH 200
#define HEIGHT 150

@interface GridViewCell : AQGridViewCell

@property (strong, nonatomic) UIImageView *imageView;

@end

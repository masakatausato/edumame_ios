//
//  PlanConfirmTableDefine.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/27.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#ifndef EduMame_PlanConfirmTableDefine_h
#define EduMame_PlanConfirmTableDefine_h

#define PlanConfirmTableCellIdentifier @"PlanConfirmTableViewCellIdentifier"
#define PlanConfirmTableCellHeight 40.0f
#define PlanConfirmTableCellMargin 2.0f
#define PlanConfirmTableFontSize 14.0f

#endif

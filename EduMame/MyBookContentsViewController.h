//
//  MyBookContentsViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/26.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoverFlowViewController.h"
#import "GridViewController.h"
#import "DateViewController.h"

@class DetailViewController;
@class DragAndDropView;

@interface MyBookContentsViewController : UIViewController <
CoverFlowViewDelegate,
CoverFlowViewGestureDelegate,
GridViewDelegate,
GridViewGestureDelegate,
DateViewDelegate>
{
    DateView *dateTarget;
}

@property (strong, nonatomic) CoverFlowViewController *coverflowViewCtrl;
@property (strong, nonatomic) GridViewController *gridViewCtrl;
@property (strong, nonatomic) DateViewController *dateViewCtrl;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;

@property (strong, nonatomic) NSMutableArray *contentData;
@property (strong, nonatomic) NSMutableArray *dropContentData;

@property (strong, nonatomic) DetailViewController *detailViewCtrl;

@property (strong, nonatomic) DragAndDropView *dragAndDropView;

@property (strong, nonatomic) NSMutableArray *dropDateTargets;
@property (strong, nonatomic) UIView *lastDropDateTargets;

@property (nonatomic) int productsId;
@property (strong, nonatomic) NSString *productsTitle;
@property (weak, nonatomic) IBOutlet UILabel *productsTitleLabel;

@property (strong, nonatomic) NSMutableArray *resourceList;

@end

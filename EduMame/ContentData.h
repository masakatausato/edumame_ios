//
//  ContentData.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/29.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentData : NSObject

@property (nonatomic) NSInteger contentsId;
@property (nonatomic) NSString* title;
@property (nonatomic) NSString* description;
@property (nonatomic) NSString* keyword;
@property (nonatomic) NSString* provider;
@property (nonatomic) NSString* author;
@property (nonatomic) NSInteger revision;
@property (nonatomic) NSInteger level;
@property (nonatomic) NSInteger attribute;
@property (nonatomic) NSInteger targetFlag;
@property (nonatomic) NSInteger contentType;
@property (nonatomic) NSInteger printFlag;
@property (nonatomic) NSInteger useTerm;
@property (nonatomic) NSString* validTermStart;
@property (nonatomic) NSString* validTermEnd;
@property (nonatomic) BOOL validTermFlag;
@property (nonatomic) BOOL invalidFlag;
@property (nonatomic) NSString* content;
@property (nonatomic) NSString* preview;
@property (nonatomic) NSString* created;
@property (nonatomic) NSString* modified;
@property (nonatomic) NSInteger categoryId;
@property (nonatomic) NSInteger subcategoryId;

- (id)init;

- (id)initWithContentsId:(NSInteger)contentsId title:(NSString*)title description:(NSString*)description keyword:(NSString*)keyword provider:(NSString*)provider author:(NSString*)author revision:(NSInteger)revision level:(NSInteger)level attribute:(NSInteger)attribute targetFlag:(NSInteger)targetFlag contentType:(NSInteger)contentType printFlag:(NSInteger)printFlag useTerm:(NSInteger)useTerm validTermStart:(NSString*)validTermStart validTermEnd:(NSString*)validTermEnd validTermFlag:(BOOL)validTermFlag invalidFlag:(BOOL)invalidFlag content:(NSString*)content preview:(NSString*)preview created:(NSString*)created modified:(NSString*)modified categoryId:(NSInteger)categoryId subcategoryId:(NSInteger)subcategoryId;

@end

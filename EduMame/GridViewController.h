//
//  GridViewController.h
//  EduMame
//
//  Created by gclue_mita on 13/02/12.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"

@protocol GridViewDelegate <NSObject>

- (void)notifyDidSelectItemAtIndex:(NSInteger)index;

@end

@protocol GridViewGestureDelegate <NSObject>

- (void)notifyGestureStateBegan:(UIGestureRecognizer *)recognizer index:(NSInteger)index;
- (void)notifyGestureStateChanged:(UIGestureRecognizer *)recognizer;
- (void)notifyGestureStateEnded:(UIGestureRecognizer *)recognizer;

@end

@interface GridViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet AQGridView *aqGridView;
@property (strong, nonatomic) NSMutableArray *itemList;

@property (strong, nonatomic) id<GridViewDelegate> tapDelegate;
@property (strong, nonatomic) id<GridViewGestureDelegate> gestureDelegate;

// TODO:リファクタリング対象
@property BOOL isProduct;

- (void)initialize;
- (UIImage *)getPreview:(NSUInteger)index;

@end

//
//  EMCalendarComponent.h
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/24.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EMCalendarComponent : NSObject

@property NSCalendar* currentCalendar;

@property NSDate* currentDate;

- (id)init;

+ (id)calendarComponent;

- (id)initWithDate:(NSDate*)date;

+ (id)calendarComoponentWithDate:(NSDate*)date;

- (id)initWithDate:(NSDate*)date calendar:(NSCalendar*)calendar;

+ (id)calendarComponentWithDate:(NSDate*)date calendar:(NSCalendar*)calendar;

- (void)offsetDay:(NSInteger)day;

- (void)offsetMonth:(NSInteger)month;

- (void)offsetYear:(NSInteger)year;

- (NSDateComponents*)components;

- (NSDateComponents*)componentsOfDay:(NSInteger)day;

- (NSDateComponents*)componentsOfCell:(FixedPoint)cell;

- (NSRange)rangeOfDay;

- (NSRange)rangeOfMonth;

- (FixedPoint)cellOfIndex:(NSInteger)index;

- (NSInteger)indexOfCell:(FixedPoint)cell;

@end

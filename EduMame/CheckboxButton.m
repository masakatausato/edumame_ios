//
//  CheckboxButton.m
//  EduMame
//
//  Created by gclue_mita on 13/02/28.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "CheckboxButton.h"

@implementation CheckboxButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIImage* nocheck = [UIImage imageNamed:@"nocheck.png"];
    UIImage* checked = [UIImage imageNamed:@"check.png"];
    UIImage* disable = [UIImage imageNamed:@"disable_check.png"];
    [self setBackgroundImage:nocheck forState:UIControlStateNormal];
    [self setBackgroundImage:checked forState:UIControlStateSelected];
    [self setBackgroundImage:checked forState:UIControlStateHighlighted];
    [self setBackgroundImage:disable forState:UIControlStateDisabled];
    [self addTarget:self action:@selector(checkboxPush:) forControlEvents:UIControlEventTouchUpInside];
    [self setState:self];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImage* nocheck = [UIImage imageNamed:@"nocheck.png"];
        UIImage* checked = [UIImage imageNamed:@"check.png"];
        UIImage* disable = [UIImage imageNamed:@"disable_check.png"];
        [self setBackgroundImage:nocheck forState:UIControlStateNormal];
        [self setBackgroundImage:checked forState:UIControlStateSelected];
        [self setBackgroundImage:checked forState:UIControlStateHighlighted];
        [self setBackgroundImage:disable forState:UIControlStateDisabled];
        [self addTarget:self action:@selector(checkboxPush:) forControlEvents:UIControlEventTouchUpInside];
        [self setState:self];
    }
    return self;
}

- (void)checkboxPush:(CheckboxButton *)button
{
    button.checkboxSelected = !button.checkboxSelected;
    [button setState:button];
}

- (void)setState:(CheckboxButton *)button
{
    if (button.checkboxSelected) {
        [button setSelected:YES];
    } else {
        [button setSelected:NO];
    }
}

@end

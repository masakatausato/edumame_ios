//
//  DrawSettingsModalPanel.m
//  EduMame
//
//  Created by gclue_mita on 13/01/07.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "DrawSettingsModalPanel.h"

#define BLACK_BAR_COMPONENTS { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }

@implementation DrawSettingsModalPanel

@synthesize drawSettingsView = _drawSettingsView;
@synthesize scrollView = _scrollView;
@synthesize brush = _bush;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.headerLabel.textColor = [UIColor blackColor];
        self.contentColor = [UIColor whiteColor];
       
        [[NSBundle mainBundle] loadNibNamed:@"DrawSettings" owner:self options:nil];
                
        self.margin = UIEdgeInsetsMake(140, 300, 140.0, 300.0);
        self.padding = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
        
        [self.contentView addSubview:self.drawSettingsView];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	[self.drawSettingsView setFrame:self.contentView.bounds];
}

- (IBAction)sliderChanged:(id)sender
{    
    UISlider *changedSlider = (UISlider*)sender;
    
    if (changedSlider == self.brushControl) {
        
        self.brush = self.brushControl.value;
        
        UIGraphicsBeginImageContext(self.brushPreview.frame.size);
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), self.brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(),50, 50);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(),50, 50);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        self.brushPreview.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
}

- (void)initBrush:(CGFloat)aBrush
{
    self.brush = aBrush;
    self.brushControl.value = aBrush;
    [self sliderChanged:self.brushControl];
}

@end
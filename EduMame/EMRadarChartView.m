//
//  EMRadarChartView.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/02/07.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "EMRadarChartView.h"
#import <QuartzCore/QuartzCore.h>
#import "EMRadarChartComponent.h"

@interface EMRadarChartView ()
{
    EMRadarChartComponent* _radarChartComponent;
    CGFloat _currentValue;
    NSDate* _previousDate;
}
@end

@implementation EMRadarChartView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSMutableArray* valueArray = [NSMutableArray array];
        [valueArray addObject:[NSNumber numberWithFloat:1.0]];
        [valueArray addObject:[NSNumber numberWithFloat:0.9]];
        [valueArray addObject:[NSNumber numberWithFloat:0.8]];
        [valueArray addObject:[NSNumber numberWithFloat:0.9]];
        [valueArray addObject:[NSNumber numberWithFloat:1.0]];
        [valueArray addObject:[NSNumber numberWithFloat:0.7]];
        
        NSMutableArray* labelArray = [NSMutableArray array];
        [labelArray addObject:@"label 1"];
        [labelArray addObject:@"label 2"];
        [labelArray addObject:@"label 3"];
        [labelArray addObject:@"label 4"];
        [labelArray addObject:@"label 5"];
        [labelArray addObject:@"label 6"];
        
        _radarChartComponent = [EMRadarChartComponent radarChartWithItemNumber:[valueArray count] scale:300.0];
        [_radarChartComponent setItemValueByArray:valueArray];
        [_radarChartComponent setItemLabelByArray:labelArray];
        [_radarChartComponent calculate];
        _currentValue = 0.0;
        
        // 更新タイマーを起動
        _previousDate = [NSDate date];
        [NSTimer scheduledTimerWithTimeInterval:1.0f/60.0f target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGPoint offset = CGPointMake(768 / 2, 1024 / 2);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //  外枠の描画
    CGContextSetLineWidth(context, 4.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetRGBStrokeColor(context, 0.4, 0.4, 1.0, 1.0);
    CGContextBeginPath(context);
    for(NSInteger i = 0; i < [_radarChartComponent.itemArray count]; i++)
    {
        CGPoint point = [_radarChartComponent itemMaxPointAtIndex:i];
        if(i == 0)
        {
            CGContextMoveToPoint(context, offset.x + point.x, offset.y + point.y);
        }
        else
        {
            CGContextAddLineToPoint(context, offset.x + point.x, offset.y + point.y);
        }
    }
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathStroke);
    
    // 軸線の描画
    CGContextSetRGBStrokeColor(context, 0.4, 0.4, 1.0, 1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGFloat lengths[] = { 4, 2 };
    CGContextSetLineDash(context, 0, lengths, 2);
    CGContextBeginPath(context);
    for(NSInteger i = 0; i < [_radarChartComponent.itemArray count]; i++)
    {
        CGPoint point = [_radarChartComponent itemMaxPointAtIndex:i];
        CGContextMoveToPoint(context, offset.x, offset.y);
        CGContextAddLineToPoint(context, offset.x + point.x, offset.y + point.y);
    }
    CGContextMoveToPoint(context, offset.x, offset.y);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathStroke);
    
    // 値の描画
    CGContextSetLineWidth(context, 4.0);
    CGContextSetRGBStrokeColor(context, 0.25, 0.4, 0.3, 0.5);
    CGContextSetRGBFillColor(context, 0.5, 0.8, 0.6, 0.5);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetLineDash(context, 0, NULL, 0);
    CGContextBeginPath(context);
    for(NSInteger i = 0; i < [_radarChartComponent.itemArray count]; i++)
    {
        CGFloat value = [_radarChartComponent itemValueAtIndex:i];
        if(value > _currentValue)
        {
            value = _currentValue;
        }
        
        CGPoint point = [_radarChartComponent itemPointAtIndex:i forValue:value];
        if(i == 0)
        {
            CGContextMoveToPoint(context, offset.x + point.x, offset.y + point.y);
        }
        else
        {
            CGContextAddLineToPoint(context, offset.x + point.x, offset.y + point.y);
        }
    }
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    // ラベルの描画
    UIFont* font = [UIFont systemFontOfSize:24];
    [[UIColor colorWithRed:0 green:0 blue:0 alpha:1] set];
    for(NSInteger i = 0; i < [_radarChartComponent.itemArray count]; i++)
    {
        NSString* label = [_radarChartComponent itemLabelAtIndex:i];
        
        CGPoint point = [_radarChartComponent itemMaxPointAtIndex:i];
        point.x += offset.x;
        point.y += offset.y;
        
        CGSize size = [label sizeWithFont:font];
        point.x -= size.width / 2;
        point.y -= size.height / 2;
        
        CGFloat radian = [_radarChartComponent itemRadianAtIndex:i];
        point.x += sinf(radian) * 32.0;
        point.y += cosf(radian) * 32.0;
        
        [label drawAtPoint:point withFont:font];
    }
}

// タイマーイベント
- (void)onTimer:(NSTimer*)timer
{
    NSDate* currentDate = [NSDate date];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_previousDate];
    _previousDate = currentDate;
    
    _currentValue += interval * 1.0;
    if(_currentValue > 1.0f)
    {
        _currentValue = 1.0f;
    }
    
    [self setNeedsDisplay];
}

@end

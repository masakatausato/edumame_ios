//
//  WorkbookIndexData.m
//  EduMame
//
//  Created by Shinji Ochiai on 13/01/30.
//  Copyright (c) 2013年 gclue. All rights reserved.
//

#import "WorkbookIndexData.h"

@implementation WorkbookIndexData

- (id)init
{
    self = [super init];
    if(self){
        self.workbookIndexId = -1;
        self.answerRawData = @"";
        self.answerStatusCode = 0;
        self.answerStartedAt = 0;
        self.answerFinishedAt = 0;
        self.evaluation = 0;
        self.evaluationAt = 0;
        self.memo = @"";
        self.againFlag = 0;
        self.markRawData = @"";
        self.workbookId = -1;
        self.contentId = -1;
    }
    return self;
}

- (id)initWithWorkbookIndexId:(NSInteger)workbookIndexId answerRawData:(NSString*)answerRawData answerStatusCode:(NSInteger)answerStatusCode answerStartedAt:(NSInteger)answerStartedAt answerFinishedAt:(NSInteger)answerFinishedAt evaluation:(NSInteger)evaluation evaluationAt:(NSInteger)evaluationAt memo:(NSString*)memo againFlag:(NSInteger)againFlag markRawData:(NSString*)markRawData workbookId:(NSInteger)workbookId contentId:(NSInteger)contentId
{
    self = [super init];
    if(self){
        self.workbookIndexId = workbookIndexId;
        self.answerRawData = answerRawData;
        self.answerStatusCode = answerStatusCode;
        self.answerStartedAt = answerStartedAt;
        self.answerFinishedAt = answerFinishedAt;
        self.evaluation = evaluation;
        self.evaluationAt = evaluationAt;
        self.memo = memo;
        self.againFlag = againFlag;
        self.markRawData = markRawData;
        self.workbookId = workbookId;
        self.contentId = contentId;
    }
    return self;    
}

@end

//
//  EMPlayerDefine.h
//  EduMame
//
//  Created by Shinji Ochiai on 12/12/13.
//  Copyright (c) 2012年 Shinji Ochiai. All rights reserved.
//

#ifndef EduMame_EMPlayerDefine_h
#define EduMame_EMPlayerDefine_h

// プレイヤーモード
enum
{
    EMPlayerModeIntro,      // 問題モード
    EMPlayerModeAnswer,     // 解答モード
};

// リソース種別
enum
{
    EMResourceTypeNone,     // リソースなし
    EMResourceTypeImage,    // 画像
    EMResourceTypeSound,    // 音声
    EMResourceTypeMovie,    // 動画
};

// 遷移方法
enum
{
    EMChangeModeNone,       // 遷移せず
    EMChangeModeTap,        // タップ
    EMChangeModeAuto,       // 自動
    EMChangeModeAnswer,     // 解答
};

// エフェクト
enum
{
    EMPlayerEffectNone,     // エフェクトなし
    EMPlayerEffectScroll,   // スクロール
    EMPlayerEffectFade,     // フェード
};

// 属性
enum
{
    EMAttributeResourceFit = 0x00000001,    // リソースにあわせる
    EMAttributeInfinity = 0x00000002,       // 時間無制限
    EMAttributeContinueSound = 0x00000004,  // 音声を継続
};

#endif

//
//  DatabaseManager.h
//  EduMame
//
//  Created by gclue_mita on 12/12/21.
//  Copyright (c) 2012年 gclue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "ParentData.h"
#import "ChildrenData.h"
#import "AnswerdContentIndexData.h"
#import "CalendarEventData.h"
#import "ContentData.h"
#import "ProductIndexData.h"
#import "ProductData.h"
#import "WorkbookIndexData.h"
#import "WorkbookData.h"
#import "CategoryData.h"
#import "SubcategoryData.h"
#import "FunbookData.h"
#import "FunbookIndexData.h"
#import "ChildCalendarEventData.h"

typedef id (^DatabaseManagerSelectAction)(FMDatabase* db, FMResultSet* resultSet);
typedef BOOL (^DatabaseManagerInsertAction)(FMDatabase* db, NSInteger index);
typedef BOOL (^DatabaseManagerUpdateAction)(FMDatabase* db, NSInteger index);

@interface DatabaseManager : NSObject

+ (FMDatabase*)open;
+ (void)close:(FMDatabase*)db;

+ (NSString*)toString:(NSInteger)value;
+ (NSString*)dateToString:(NSDate*)date;
+ (NSDate*)dateFromString:(NSString*)string;
+ (NSDate*)dateFromDate:(NSDate*)date;
+ (NSInteger)dateToTimestamp:(NSDate*)date;
+ (NSDate*)dateFromTimestamp:(NSInteger)timestamp;

+ (id)first:(NSArray*)records;

+ (NSArray*)selectData:(NSString*)sql action:(DatabaseManagerSelectAction)action;
+ (BOOL)insertData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId action:(DatabaseManagerInsertAction)action;
+ (BOOL)updateData:(NSArray*)records action:(DatabaseManagerUpdateAction)action;
+ (BOOL)deleteData:(NSString*)tableName options:(NSString*)options;

+ (NSArray*)selectParentData:(NSString*)options;
+ (BOOL)insertParentData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateParentData:(NSArray*)records;
+ (BOOL)deleteParentData:(NSString*)options;

+ (NSArray*)selectChildrenData:(NSString*)options;
+ (NSArray*)selectChildrenDataRecursive:(NSString*)options;
+ (BOOL)insertChildrenData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateChildrenData:(NSArray*)records;
+ (BOOL)deleteChildrenData:(NSString*)options;

+ (NSArray*)selectAnswerdContentIndexData:(NSString*)options;
+ (NSArray*)selectAnswerdContentIndexDataRecursive:(NSString*)options;
+ (BOOL)insertAnswerdContentIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateAnswerdContentIndexData:(NSArray*)records;
+ (BOOL)deleteAnswerdContentIndexData:(NSString*)options;

+ (NSArray*)selectCalendarEventData:(NSString*)options;
+ (NSArray*)selectCalendarEventDataRecursive:(NSString*)options;
+ (BOOL)insertCalendarEventData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateCalendarEventData:(NSArray*)records;
+ (BOOL)deleteCalendarEventData:(NSString*)options;

+ (NSArray*)selectContentData:(NSString*)options;
+ (BOOL)insertContentData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateContentData:(NSArray*)records;
+ (BOOL)deleteContentData:(NSString*)options;

+ (NSArray*)selectProductIndexData:(NSString*)options;
+ (NSArray*)selectProductIndexDataRecursive:(NSString*)options;
+ (BOOL)insertProductIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateProductIndexData:(NSArray*)records;
+ (BOOL)deleteProductIndexData:(NSString*)options;

+ (NSArray*)selectProductData:(NSString*)options;
+ (BOOL)insertProductData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateProductData:(NSArray*)records;
+ (BOOL)deleteProductData:(NSString*)options;

+ (NSArray*)selectWorkbookIndexData:(NSString*)options;
+ (NSArray*)selectWorkbookIndexDataRecursive:(NSString*)options;
+ (BOOL)insertWorkbookIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateWorkbookIndexData:(NSArray*)records;
+ (BOOL)deleteWorkbookIndexData:(NSString*)options;

+ (NSArray*)selectWorkbookData:(NSString*)options;
+ (NSArray*)selectWorkbookDataRecursive:(NSString*)options;
+ (BOOL)insertWorkbookData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateWorkbookData:(NSArray*)records;
+ (BOOL)deleteWorkbookData:(NSString*)options;

+ (NSArray*)selectCategoryData:(NSString*)options;
+ (BOOL)insertCategoryData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateCategoryData:(NSArray*)records;
+ (BOOL)deleteCategoryData:(NSString*)options;

+ (NSArray*)selectSubcategoryData:(NSString*)options;
+ (NSArray*)selectSubcategoryDataRecursive:(NSString*)options;
+ (BOOL)insertSubcategoryData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateSubcategoryData:(NSArray*)records;
+ (BOOL)deleteSubcategoryData:(NSString*)options;

+ (NSArray*)selectFunbookData:(NSString*)options;
+ (NSArray*)selectFunbookDataRecursive:(NSString*)options;
+ (BOOL)insertFunbookData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateFunbookData:(NSArray*)records;
+ (BOOL)deleteFunbookData:(NSString*)options;

+ (NSArray*)selectFunbookIndexData:(NSString*)options;
+ (NSArray*)selectFunbookIndexDataRecursive:(NSString*)options;
+ (BOOL)insertFunbookIndexData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateFunbookIndexData:(NSArray*)records;
+ (BOOL)deleteFunbookIndexData:(NSString*)options;

+ (NSArray*)selectChildCalendarEventData:(NSString*)options;
+ (NSArray*)selectChildCalendarEventDataRecursive:(NSString*)options;
+ (BOOL)insertChildCalendarEventData:(NSArray*)records lastInsertRowId:(NSInteger*)lastInsertRowId;
+ (BOOL)updateChildCalendarEventData:(NSArray*)records;
+ (BOOL)deleteChildCalendarEventData:(NSString*)options;

@end
